trigger moduleStatusRestrict on Nrich__Module__c (after update) {
    
     Map<id,Nrich__Module__c > Moduleid = new Map<id,Nrich__Module__c >();
    if(trigger.isafter && trigger.isupdate)
    {
        
        for(Nrich__Module__c modu: trigger.new)
        {
            if(modu.Nrich__Status__c!= trigger.oldMap.get(modu.Id).Nrich__Status__c && modu.Nrich__Status__c.equalsIgnoreCase(Label.Completed))
            {              
                Moduleid.put(modu.id,modu);
            }
        }
        if(Moduleid.size()>0)
           ModuleStatusRestrictHandler.CheckStatusOfrelatedUserStory(Moduleid);   
           
    }
}