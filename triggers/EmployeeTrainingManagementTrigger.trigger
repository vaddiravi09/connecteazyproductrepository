trigger EmployeeTrainingManagementTrigger on Employee_Training_Management__c (After Update) {
    EmployeeTrainingManagementHandler.createAnnouncementRcrd(Trigger.newMap, Trigger.oldMap);
}