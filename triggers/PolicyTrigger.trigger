trigger PolicyTrigger on Nrich__Policy__c (after update) {
    if(trigger.isUpdate && trigger.isAfter){
        list<String> PolicyIdList = new list<String>();
        for(Nrich__Policy__c policy: trigger.New){
            if(trigger.oldMap.get(policy.id).Nrich__status__c!=policy.Nrich__status__c && policy.Nrich__status__c.equalsIgnoreCase(Label.Published)){               
                PolicyIdList.add(policy.id);
            }              
        }
        
        if(PolicyIdList.size()>0){
           EmployeePolicyCreation_Batch epc = new EmployeePolicyCreation_Batch();
           epc.policyIdList = PolicyIdList;
           database.executeBatch(epc, 1);  
        }   
    }
}