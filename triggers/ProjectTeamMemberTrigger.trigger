trigger ProjectTeamMemberTrigger on Nrich__Project_Team_Member__c (before insert, before update) {
    set<String> projectEmpIdSet = new set<String>();
    set<String> teammemberSet = new set<String>();
    for(Nrich__Project_Team_Member__c teammember: trigger.new){
        projectEmpIdSet.add(teammember.Nrich__EmployeeProject__c);
        teammemberSet.add(teammember.Id);
    }
    list<String> oldteammemberSet = new list<String>();
    for(Nrich__Project_Team_Member__c oldteammember: [Select Id, Nrich__EmployeeProject__c from Nrich__Project_Team_Member__c where Nrich__EmployeeProject__c IN: projectEmpIdSet AND ID NOT IN: teammemberSet AND Nrich__Active__c=TRUE]){
        oldteammemberSet.add(oldteammember.Nrich__EmployeeProject__c);
    }
    for(Nrich__Project_Team_Member__c teammember: trigger.new){
        if(oldteammemberSet.size()>0 && oldteammemberSet.contains(teammember.Nrich__EmployeeProject__c))
            teammember.addError(Label.Project_Team_Member_Error);
    }
    
}