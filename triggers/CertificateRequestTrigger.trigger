trigger CertificateRequestTrigger on Nrich__Certification_Request__c (after update) {
	if(trigger.isAfter && trigger.isUpdate){
        list<Sobject> certificationRequestList = new list<Sobject>();
        Set<Id> certificationIdset = new Set<Id>();
        for(Nrich__Certification_Request__c certreq: trigger.new){
            if(certreq.Nrich__Approval_Status__c!=trigger.oldMap.get(certreq.id).Nrich__Approval_Status__c && certreq.Nrich__Approval_Status__c.equalsIgnoreCase(Label.Pending))
                certificationRequestList.add(certreq);
            else if(certreq.Nrich__Approval_Status__c!=trigger.oldMap.get(certreq.id).Nrich__Approval_Status__c && !certreq.Nrich__Approval_Status__c.equalsIgnoreCase(Label.Pending) && !certreq.Nrich__Approval_Status__c.equalsIgnoreCase(Label.Applied))
                    certificationIdset.add(certreq.Id);
        }
        
        if(certificationRequestList.size()>0)
            CreationofToDoList.CreationofToDoListMethod(certificationRequestList, 'Nrich__Certification_Request__c');
    	
        if(certificationIdset.size()>0)
            CreationofToDoList.updateToDoListMethod(certificationIdset);
    }
}