trigger TravelRequestTrigger on Nrich__Travel_Request__c (after update) {
   if(trigger.isAfter && trigger.isUpdate){
        list<Sobject> travelRequestList = new list<Sobject>();
        Set<Id> travelIdset = new Set<Id>();
        for(Nrich__Travel_Request__c travelreq: trigger.new){
            if(travelreq.Nrich__Status__c!=trigger.oldMap.get(travelreq.id).Nrich__Status__c && travelreq.Nrich__Status__c.equalsIgnoreCase(Label.Submitted_For_Approval))
                travelRequestList.add(travelreq);
            else if(travelreq.Nrich__Status__c!=trigger.oldMap.get(travelreq.id).Nrich__Status__c && !travelreq.Nrich__Status__c.equalsIgnoreCase(Label.Submitted_For_Approval) && !travelreq.Nrich__Status__c.equalsIgnoreCase(Label.In_Draft))
                travelIdset.add(travelreq.Id);
        }
        
        if(travelRequestList.size()>0)
            CreationofToDoList.CreationofToDoListMethod(travelRequestList, 'Nrich__Travel_Request__c');
    
        if(travelIdset.size()>0)
            CreationofToDoList.updateToDoListMethod(travelIdset);
    }
}