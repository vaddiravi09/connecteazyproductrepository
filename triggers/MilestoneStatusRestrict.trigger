trigger MilestoneStatusRestrict on Nrich__Milestone__c (before update) {
    if(Trigger.IsBefore && Trigger.IsUpdate)
    {
        set<id> setMilesId = new set<id>();
        for(Nrich__Milestone__c miles: trigger.new)
        {
            if(miles.Nrich__Status__c!=null)
            {
                if(miles.Nrich__Status__c!= trigger.oldMap.get(miles.Id).Nrich__Status__c && (miles.Nrich__Status__c.equalsIgnoreCase(Label.Complete) || miles.Nrich__Status__c.equalsIgnoreCase(Label.CompletedDelayed)))
                {                    
                    setMilesId.add(miles.id);
                }
                
            }        
        }
        if(setMilesId.size()>0)
                Milestonestatusrestricthandler.checkStatusOfRelatedSprint(setMilesId, trigger.new);
    }
}