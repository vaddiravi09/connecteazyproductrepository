/**********************************
 // Company: Absyz
 // Developer: Pushmitha
 // Trigger Name : InterviewRoundTrigger 
 // Object : Interview_Round__c
 // Functionality : When the status field in Interview Round object is updated to 'Selected/Rejected/In Progress' 
                    Create a new Interview Round by fetching the details from Job Round
 // Testclass Name : nextInterviewRoundHandlerTest
 *********************************/
trigger InterviewRoundTrigger on Nrich__Interview_Round__c (after update) {
    //Initialization
    /*set<id> jobIdSet = new set<id>(); 
    set<Decimal> jobOrderSet = new set<Decimal>();
    set<Decimal> currentjobOrderSet = new set<Decimal>();
    set<id> jobApplicationIdSet = new set<id>();
    set<id> interviewScheduleIdSet = new set<id>();
    set<id> interviewRejectedIdSet = new set<id>();
    set<id> interviewSchedulePanelIdSet = new set<id>();
    List<Interview_Round__c> interviewRoundList = new List<Interview_Round__c>();
    //When the status field is updated get the set of ids
    if(trigger.isUpdate){
        if(CheckTriggerRecursion.checkRun()){
        for(Interview_Round__c tests : trigger.new){
            String statusValueSelected = System.Label.InterviewStatusSelected;
            if(tests.status__c!=trigger.oldMap.get(tests.Id).status__c && tests.status__c==statusValueSelected){
                interviewRoundList.add(tests);
                jobIdSet.add(tests.Job_Id__c);
                if(tests.Job_Order__c!=NULL)
                  jobOrderSet.add(tests.Job_Order__c+1);
                jobApplicationIdSet.add(tests.Job_Application__c);
                currentjobOrderSet.add(tests.Job_Order__c);
            }
            //if the status of the Interview Round is Scheduled update the Job Application Status to In Progress
            String statusValueSchedule = System.Label.InterviewStatusScheduled;
            if(tests.status__c!=trigger.oldMap.get(tests.Id).status__c && tests.status__c==statusValueSchedule){
                interviewScheduleIdSet.add(tests.Job_Application__c);
            }
            //if the status of the Interview Round is Rejected update the Job Application Status to Rejected
            String statusValueRejected = System.Label.InterviewStatusRejected;
            if(tests.status__c!=trigger.oldMap.get(tests.Id).status__c && tests.status__c==statusValueRejected){
                interviewRejectedIdSet.add(tests.Job_Application__c);
            }
        }
        system.debug('interviewRoundList '+interviewRoundList);
        //Passing the List of Interview Rounds, Job Id and job round Order to the handler
        system.debug('checkJobRound'+interviewRoundList +'@@'+ jobIdSet+ '@@'+ jobOrderSet );
            if(jobIdSet.size()>0 && interviewRoundList.size()>0 && jobOrderSet.size()>0)
            
            InterviewRoundTriggerHandler.checkJobRoundCount(interviewRoundList, jobIdSet, jobOrderSet);
        //Passing the set of Ids to the handler
        if(interviewScheduleIdSet.size()>0)
            InterviewRoundTriggerHandler.updateStatusProgress(interviewScheduleIdSet);
        //Passing the set of Ids to the handler
        if(interviewRejectedIdSet.size()>0)
            InterviewRoundTriggerHandler.updateStatusRejected(interviewRejectedIdSet);
        //Passing the set of Ids and Job Round Order to the handler
        if(jobApplicationIdSet.size()>0 && currentjobOrderSet.size()>0)
            InterviewRoundTriggerHandler.updateStatusShortlisted(jobApplicationIdSet,currentjobOrderSet);
      }
    }*/
}