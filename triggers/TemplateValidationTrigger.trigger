trigger TemplateValidationTrigger on Nrich__Template__c (before insert, before update) {
    set<String> jobIdSet = new set<String>();
    if(trigger.isInsert){        
        for(Nrich__Template__c template: trigger.new){
            if(template.Nrich__Is_Active__c)
               jobIdSet.add(template.Nrich__Job__c);
        }
    }
    if(trigger.isUpdate){
       for(Nrich__Template__c template: trigger.new){
            if(trigger.oldMap.get(template.id).Nrich__Is_Active__c!=template.Nrich__Is_Active__c && template.Nrich__Is_Active__c)
               jobIdSet.add(template.Nrich__Job__c);
        }
    }
        
        if(jobIdSet.size()>0){
            map<String, Integer> jobActiveTemplateMap = new map<String, Integer>();
            map<String, Integer> jobRoundActiveTemplateMap = new map<String, Integer>();
            for(Nrich__Template__c templates : [Select Id, Nrich__Job_Round__c, Nrich__Job__c, Nrich__Type_of_Template__c from Nrich__Template__c where Nrich__Is_Active__c=TRUE AND Nrich__Job__c IN: jobIdSet]){
                if(string.isNotBlank(templates.Nrich__Type_of_Template__c) && templates.Nrich__Type_of_Template__c!=Label.Feedback){
                    if(jobActiveTemplateMap!=NULL && jobActiveTemplateMap.size()>0 && jobActiveTemplateMap.containsKey(templates.Nrich__Job__c+';'+templates.Nrich__Type_of_Template__c)){
                         Integer count = jobActiveTemplateMap.get(templates.Nrich__Job__c+';'+templates.Nrich__Type_of_Template__c);
                         count = count+1;
                        jobActiveTemplateMap.put(templates.Nrich__Job__c+';'+templates.Nrich__Type_of_Template__c, count);
                    }else
                        jobActiveTemplateMap.put(templates.Nrich__Job__c+';'+templates.Nrich__Type_of_Template__c, 1);
                }
                if(string.isNotBlank(templates.Nrich__Type_of_Template__c) && templates.Nrich__Type_of_Template__c==Label.Feedback){
                    if(jobRoundActiveTemplateMap!=NULL && jobRoundActiveTemplateMap.size()>0 && jobRoundActiveTemplateMap.containsKey(templates.Nrich__Job_Round__c+';'+templates.Nrich__Type_of_Template__c)){
                         Integer count = jobRoundActiveTemplateMap.get(templates.Nrich__Job_Round__c+';'+templates.Nrich__Type_of_Template__c);
                         count = count+1;
                        jobRoundActiveTemplateMap.put(templates.Nrich__Job_Round__c+';'+templates.Nrich__Type_of_Template__c, count);
                    }else
                        jobRoundActiveTemplateMap.put(templates.Nrich__Job_Round__c+';'+templates.Nrich__Type_of_Template__c, 1);
                }
            }
            
            for(Nrich__Template__c template: trigger.new){
                if(template.Nrich__Type_of_Template__c!=Label.Feedback && jobActiveTemplateMap!=NULL && jobActiveTemplateMap.size()>0 && jobActiveTemplateMap.containsKey(template.Nrich__Job__c+';'+template.Nrich__Type_of_Template__c) && jobActiveTemplateMap.get(template.Nrich__Job__c+';'+template.Nrich__Type_of_Template__c)>0){
                    template.addError(Label.Already_Template_Active_In_Job);
                }
                if(template.Type_of_Template__c==Label.Feedback && jobRoundActiveTemplateMap!=NULL && jobRoundActiveTemplateMap.size()>0 && jobRoundActiveTemplateMap.containsKey(template.Nrich__Job_Round__c+';'+template.Nrich__Type_of_Template__c) && jobRoundActiveTemplateMap.get(template.Nrich__Job_Round__c+';'+template.Nrich__Type_of_Template__c)>0){
                    template.addError(Label.Already_A_Template_Active_In_Job_Round);
                }
            }
        }
}