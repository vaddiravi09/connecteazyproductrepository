/****************************************************************************************************
    * Company: Absyz
    * Developer: Thadeus Ronn Thomas && Sai Kesa
    * Created Date: 22/10/2018
    * Description: Batch class to calculate the no. of Leaves available at any instant of time.
    *****************************************************************************************************/ 
trigger CompanyLeaveStructureTrigger on Nrich__Company_Leave_Structure__c(before insert, before update, after update, after insert) {
    if(Trigger.isbefore){
       List<Nrich__Company_Leave_Structure__c> clist= [select name,Nrich__Is_Active__c from Nrich__Company_Leave_Structure__c where Nrich__Is_Active__c =True AND id!=:trigger.new[0].id];
       Nrich__Company_Leave_Structure__c clstruct=new Nrich__Company_Leave_Structure__c();
       for(Nrich__Company_Leave_Structure__c cls:trigger.new){ 
           if(cls.Nrich__Is_Active__c==true && clist.size()>0)
              cls.addError(Label.CompanyLeaveStructureException);
       }
    }
    if(trigger.isAfter && trigger.isUpdate){
       list<String> companyLeaveStructureIdList = new list<String>(); 
       list<Nrich__Company_Leave_Structure__c> companyLeaveStructureList = new list<Nrich__Company_Leave_Structure__c>();
       for(Nrich__Company_Leave_Structure__c cls: trigger.new){ 
          if(trigger.oldMap.get(cls.Id).Nrich__Is_Active__c!=cls.Nrich__Is_Active__c & !cls.Nrich__Is_Active__c)
             companyLeaveStructureIdList.add(cls.Id);
          else if(trigger.oldMap.get(cls.Id).Nrich__Is_Active__c!=cls.Nrich__Is_Active__c & cls.Nrich__Is_Active__c)
             companyLeaveStructureList.add(cls);
       }
       
       if(companyLeaveStructureIdList.size()>0){
          Renewleavecard_Batch leavecardRenew = new Renewleavecard_Batch();
          leavecardRenew.CompanyLeaveStructureIdList = companyLeaveStructureIdList;
          Database.executeBatch(leavecardRenew, 10);
       }
        
       if(companyLeaveStructureList.size()>0)
          Database.executeBatch(new IssuenewLeavecard_Batch(companyLeaveStructureList[0]), 10);          
    }
}