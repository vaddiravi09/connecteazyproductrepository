/********************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 10/10/2018
* Description: Trigger for Leave Request Object.
********************************************************************/
trigger LeaveRequestTrigger on Nrich__Leave_Request__c (before insert, before update, after insert, after update) {
    if(trigger.isBefore && (trigger.isUpdate || trigger.isInsert)){
        set<String> employeeIdSet = new set<String>();
        set<String> EmployeeLeaveCardSet = new set<String>();
        for(Nrich__Leave_Request__c leavereq: trigger.new){
            if(leavereq.Nrich__Start_Date__c!=NULL && leavereq.Nrich__End_Date__c!=NULL && (trigger.isInsert || (trigger.isUpdate && (leavereq.Nrich__End_Date__c!=trigger.oldmap.get(leavereq.id).Nrich__End_Date__c || leavereq.Nrich__Start_Date__c!=trigger.oldmap.get(leavereq.id).Nrich__Start_Date__c)))){
                employeeIdSet.add(leavereq.Nrich__Requested_By__c);
                EmployeeLeaveCardSet.add(leavereq.Nrich__EmployeeLeaveCard__c);
            }
        }           
    }
    if(trigger.isAfter && trigger.isUpdate){
        list<Sobject> leaveRequestList = new list<Sobject>();
        list<Sobject> LeaveRequestApprovedList = new list<Sobject>();
        Set<Id> leaveRequestIdset = new Set<Id>();
        for(Nrich__Leave_Request__c leavereq: trigger.new){
            if(leavereq.Nrich__Status__c!=trigger.oldMap.get(leavereq.id).Nrich__Status__c && leavereq.Nrich__Status__c.equalsIgnoreCase(Label.Pending))
                leaveRequestList.add(leavereq);
            else if(leavereq.Nrich__Status__c!=trigger.oldMap.get(leavereq.id).Nrich__Status__c && !leavereq.Nrich__Status__c.equalsIgnoreCase(Label.Pending) && !leavereq.Nrich__Status__c.equalsIgnoreCase(Label.Applied))
                leaveRequestIdset.add(leavereq.Id);
            else if(Leavereq.Nrich__Status__c ==Label.Approved  && Leavereq.Nrich__Status__c!=trigger.oldMap.get(Leavereq.id).Nrich__Status__c)              
                LeaveRequestApprovedList.add(Leavereq);
        }
        
        if(leaveRequestList.size()>0)
            CreationofToDoList.CreationofToDoListMethod(leaveRequestList, 'Nrich__Leave_Request__c');
        
        if(leaveRequestIdset.size()>0)
            CreationofToDoList.updateToDoListMethod(leaveRequestIdset);
    }
}