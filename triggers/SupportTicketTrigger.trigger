trigger SupportTicketTrigger on Nrich__Support_Ticket__c (before insert, before update) {
   Nrich__InitialSetup__c setup = Nrich__InitialSetup__c.getOrgDefaults();
   if(setup.Nrich__Automatic_Assignment_of_Support_Tickets__c){
       list<Nrich__Support_Ticket__c> supportTicketList = new list<Nrich__Support_Ticket__c>();
       set<string> categorySet = new set<String>();
       for(Nrich__Support_Ticket__c st: trigger.new){
           if(trigger.isInsert){
               if(String.isNotBlank(st.Nrich__IssueCategory__c)){
                   supportTicketList.add(st);
                   categorySet.add(st.Nrich__IssueCategory__c);
               }
           }
           if(trigger.isUpdate){
               if(String.isNotBlank(st.Nrich__IssueCategory__c) && trigger.oldmap.get(st.Id).Nrich__IssueCategory__c!=st.Nrich__IssueCategory__c){
                   supportTicketList.add(st);
                   categorySet.add(st.Nrich__IssueCategory__c);
               }
           }
       }
       
       if(supportTicketList.size()>0){
           SupportDetailsController.assignSupportTickets(supportTicketList, categorySet);
       }
    }
}