trigger ProjectForecastTrigger on Nrich__Project_Team_Forecast__c (before insert, before update) {
    set<String> monthyearSet = new set<String>();
    set<String> forecasteIdset = new set<String>();
    for(Nrich__Project_Team_Forecast__c forecast: trigger.new){
        monthyearSet.add(forecast.Nrich__MonthYear__c);
        forecasteIdset.add(forecast.Id);
    }
    list<String> forecastOldSet = new list<String>();
    for(Nrich__Project_Team_Forecast__c forecastOld: [Select Id, Nrich__MonthYear__c from Nrich__Project_Team_Forecast__c where Nrich__MonthYear__c IN: monthyearSet AND  Id NOT IN: forecasteIdset]){
        forecastOldSet.add(forecastOld.Nrich__MonthYear__c);
    }
    for(Nrich__Project_Team_Forecast__c forecast: trigger.new){
        if(forecastOldSet.size()>0 && forecastOldSet.contains(forecast.Nrich__MonthYear__c))
            forecast.addError(Label.Project_Forecast_Error);
    }
}