/**********************************
// Company: Absyz
// Developer: Pushmitha
// Trigger Name : JobRoundTrigger 
// Object : Job_Round__c
// Functionality : 
// Testclass Name : jobRoundNumberTest
*********************************/
trigger JobRoundTrigger on Nrich__Job_Round__c (before insert, before update, after delete) {
    set<id> jobIdSet = new set<id>(); 
    set<id> jobIdsSet = new set<id>();
    set<id> jobIdUpdateSet = new set<id>();
    if(trigger.isInsert){
        for(Nrich__Job_Round__c tests : trigger.new){
            jobIdSet.add(tests.Nrich__Job__c);
        }
    }
    
    if(trigger.isUpdate){
        for(Nrich__Job_Round__c tests : trigger.new){
            jobIdUpdateSet.add(tests.Nrich__Job__c);
        }
    }
    
    if(trigger.isDelete){
        for(Nrich__Job_Round__c tests : Trigger.old){
            jobIdsSet.add(tests.Nrich__Job__c);
        }
    }
    system.debug('jobIdSet ->'+jobIdSet);
    if(jobIdSet.size()>0){
        JobRoundTriggerHandler.insertJobRound(trigger.new,jobIdSet);
    }
    if(jobIdUpdateSet.size()>0)
        JobRoundTriggerHandler.updateJobRound(trigger.new,trigger.oldmap, jobIdUpdateSet);
    
    if(jobIdsSet.size()>0){
        JobRoundTriggerHandler.deleteJobRound(trigger.old,jobIdsSet);
    }
}