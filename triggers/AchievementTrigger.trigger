trigger AchievementTrigger on Nrich__Achievements__c (before insert, before update) {
    if(trigger.isInsert){
        for(Nrich__Achievements__c ach: trigger.new){
            if(ach.Nrich__Employeee__c!=NULL && ach.Nrich__User_Id_Technical__c!=NULL)
                ach.Nrich__User__c = ach.Nrich__User_Id_Technical__c;
        }
    }
    
    if(trigger.isUpdate){
        for(Nrich__Achievements__c ach: trigger.new){
            system.debug('achnew' +ach);
            if(ach.Nrich__Employeee__c!=trigger.oldMap.get(ach.id).Nrich__Employeee__c && ach.Nrich__User_Id_Technical__c!=NULL)
                system.debug('achif' +ach);
            ach.Nrich__User__c = ach.Nrich__User_Id_Technical__c;
        }
    }
}