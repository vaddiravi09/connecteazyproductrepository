trigger projectStatusUpdate on Nrich__Project__c (before update) {
    Map<Id, Nrich__Project__c > projectMap = new map<Id,Nrich__Project__c >();
    if(trigger.isupdate && trigger.isbefore){
        for(Nrich__Project__c project: trigger.new){
            if(project.Nrich__Status__c!=trigger.oldMap.get(project.Id).Nrich__Status__c && project.Nrich__Status__c.equalsIgnoreCase(Label.Completed))
            {
                if(project.Nrich__Count_Of_Sprint__c> 0)
                   project.addError(Label.One_or_More_Dependent_Sprint_Yet_To_Be_Completed );
                  projectMap.put(project.Id, project);                
            }
            
        }
        if(projectMap.size()>0)
        projectstatusupdatehandler.checkstatus(projectMap);      
    }
}