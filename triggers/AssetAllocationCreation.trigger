trigger AssetAllocationCreation on Nrich__Asset_Request__c (after insert, after update ) {
    
    if(trigger.isAfter)
    {
        List<Nrich__Asset_Request__c>AllocationList=new List<Nrich__Asset_Request__c>();
        for(Nrich__Asset_Request__c Asset:trigger.new)
        {
            if(trigger.isInsert && Asset.Nrich__Status__c==Label.Approved){
                AllocationList.add(Asset);
            }
            
            if(trigger.isupdate && Asset.Nrich__Status__c!=trigger.oldmap.get(Asset.id).Nrich__Status__c && Asset.Nrich__Status__c==Label.Approved)
            {
                AllocationList.add(Asset);
            }
        }
        
        if(AllocationList.size()>0 && AllocationList!=NULL )
        {
            AllocationTriggerHandler.handle(AllocationList);
            system.debug('AllocationList'+AllocationList);
        }
    }
    
    if(trigger.isAfter && trigger.isUpdate){
        list<Sobject> assetRequestList = new list<Sobject>();
        Set<Id> assetIdset = new Set<Id>();
        for(Nrich__Asset_Request__c assetreq: trigger.new){
            if(assetreq.Nrich__Status__c!=trigger.oldMap.get(assetreq.id).Nrich__Status__c && assetreq.Nrich__Status__c.equalsIgnoreCase(Label.Pending))
                assetRequestList.add(assetreq);
            else if(assetreq.Nrich__Status__c!=trigger.oldMap.get(assetreq.id).Nrich__Status__c && !assetreq.Nrich__Status__c.equalsIgnoreCase(Label.Pending) && !assetreq.Nrich__Status__c.equalsIgnoreCase(Label.Applied))
                assetIdset.add(assetreq.Id);
        }
        
        if(assetRequestList.size()>0)
            CreationofToDoList.CreationofToDoListMethod(assetRequestList, 'Nrich__Asset_Request__c');
        
        if(assetIdset.size()>0)
            CreationofToDoList.updateToDoListMethod(assetIdset);
    }
    
    
}