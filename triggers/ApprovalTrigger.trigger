trigger ApprovalTrigger on Nrich__Approval__c (after update) {
    
    if(trigger.isUpdate   && trigger.isAfter){
        list<Nrich__Approval__c > ApprovalList = new list<Nrich__Approval__c >();
        
        for(Nrich__Approval__c td: trigger.new){  
            if(trigger.oldmap.get(td.id).Nrich__Status__c!=td.Nrich__Status__c ){
                ApprovalList.add(td);
            }
        }
        
        if(ApprovalList.size()>0)
            ApprovalTriggerHelper.updateTimesheetdetails(ApprovalList);
    }
    
    
}