/****************************************************************************************************
* Company: Absyz
* Developer: Thadeus Ronn Thomas
* Created Date: 15/10/2018
* Description: Creating Employee Leave cards, Creation of Employee Policies, Related User Validation 
*****************************************************************************************************/   

trigger EmployeeLeaveCardCreation on Nrich__Employee__c (before insert,after insert,before update, after update) {
    map<id,string> Emplist= new map<id,string>();
    Map<Nrich__Employee__c, String> resmap = new Map<Nrich__Employee__c, String>(); 
    set<id> updtlst= new set<id>();
    for(Nrich__Employee__c Emp: trigger.new){
        if(trigger.isBefore){
            if(trigger.isUpdate){
                if(Emp.Nrich__Related_User__c!=trigger.oldmap.get(Emp.id).Nrich__Related_User__c){
                    resmap.put(Emp, Emp.Nrich__Related_User__c);
                }
            }
            if(trigger.isInsert && String.isNotBlank(Emp.Nrich__Related_User__c)){                    
                resmap.put(Emp,Emp.Nrich__Related_User__c);
            }
            
            if(trigger.isInsert && Emp.Nrich__Date_of_Joining__c==null)
                Emp.Nrich__Date_of_Joining__c=System.today();
        }            
    }
    list<String> empeventList = new list<String>();
    if(trigger.isAfter && trigger.isInsert){
        for(Nrich__Employee__c emp: trigger.new){
            if(emp.Nrich__Active__c){
               empeventList.add(emp.Id);   
               Emplist.put(Emp.id, Emp.Name+' - '+System.today().year());  
            }             
        }               
    }
    if(trigger.isAfter && trigger.isUpdate){
        for(Nrich__Employee__c emp: trigger.new){
            if(trigger.oldMap.get(emp.id).Nrich__Active__c!=emp.Nrich__Active__c)
                empeventList.add(emp.Id);              
        }
    }
    
    if(empeventList.size()>0){
       CreationofEmployeePolicy_Batch epc = new CreationofEmployeePolicy_Batch();
       epc.EmployeeIdList = empeventList;
       database.executeBatch(epc, 1);  
    }   
    
    if(resmap.size()>0)
        employeeRelatedUserCheck.relateduser(resmap);
        
    if(Emplist.size()>0)
        LeaveCardCreation.LeaveCardonCreationEmployee(Emplist); 
}