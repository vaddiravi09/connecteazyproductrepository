/****************************************************************************
* Company: Absyz
* Developer: Pushmitha
* Trigger Name: JobTrigger 
* Created Date: 26/06/2018
* Description: Send an Approval Email Alert   
* Test Class: jobApprovalEmailAlertTest
****************************************************************************/
trigger JobTrigger on Nrich__Job__c (after insert) {
    
    if(trigger.isInsert){
        for(Nrich__Job__c tests : trigger.new){
            Nrich__InitialSetup__c emailAlert = Nrich__InitialSetup__c.getOrgDefaults();
            if(tests.Nrich__Approver__c != null){
                if(emailAlert.Nrich__Automated_ApprovalProcess_on_JobCreation__c == TRUE){
                    Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                    req.setComments(System.Label.ApprovalMessage); 
                    req.setObjectId(tests.Id); 
                    req.setProcessDefinitionNameOrId(System.Label.ApprovalProcessName);
                    req.setNextApproverIds(new Id[] {tests.Nrich__Approver__c});
                    Approval.process(req);
                }
            }
        }
    }
}