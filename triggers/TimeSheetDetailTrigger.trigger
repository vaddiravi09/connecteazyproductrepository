trigger TimeSheetDetailTrigger on Nrich__Timesheet_Detail__c (after update) {
    if(trigger.isUpdate && trigger.isAfter){
        list<Nrich__Timesheet_Detail__c> timesheetDetailList = new list<Nrich__Timesheet_Detail__c>();
         for(Nrich__Timesheet_Detail__c td: trigger.new){  
             if(trigger.oldmap.get(td.id).Nrich__Status__c!=td.Nrich__Status__c && td.Nrich__Status__c.equalsIgnoreCase(Label.Approved)){
                timesheetDetailList.add(td);
             }
         }
        
        if(timesheetDetailList.size()>0)
           TimeSheetDetailTriggerHelper.createProjectForecasts(timesheetDetailList);
    }
}