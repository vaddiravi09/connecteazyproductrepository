trigger userStoryUpdateTrigger on Nrich__User_Story__c  (after insert, after update, after delete, before delete, before insert, before update) {
    
    list < id > LstSprintId = new list < id > (); // custom roll up at sprint level
    set < Id > TeamIds = new set < Id > ();
    list < Nrich__User_Story__c  > LstUsRecord = new list < Nrich__User_Story__c  > (); // contain user story record when sprint is present
    set< Id > SprntIds = new set< Id >();
    list < String > sprintid = new list < String > (); // for custom roll up of hour field in sprint
    list < String > projectid = new list < String > (); // custom roll up at sprint on delete of user story
    set < id > Userid = new set < id > (); //contains set of user story id to restrict user story being completed without its all related task/bug is not completed 
    set < id > projecthourupdate = new set < id > ();
    
    if (trigger.isupdate) { 
        for (Nrich__User_Story__c  UserStryRecord: trigger.new) {             
            if (trigger.isbefore) {               
                if (String.isNotBlank(UserStryRecord.Nrich__Assigned_Too__c)) {
                    TeamIds.add(UserStryRecord.Nrich__Assigned_Too__c);
                    LstUsRecord.add(UserStryRecord);
                }
                if (String.isNotBlank(UserStryRecord.Nrich__Sprint__c)) {
                    Userid.add(UserStryRecord.id);
                }
                if (String.isNotBlank(UserStryRecord.Nrich__Status__c) && (UserStryRecord.Nrich__Status__c != trigger.oldMap.get(UserStryRecord.Id).Nrich__Status__c)) {
                    if (UserStryRecord.Nrich__Status__c.equalsIgnoreCase(Label.Completed)) {
                        Userid.add(UserStryRecord.id);                        
                    }
                    if (trigger.oldMap.get(UserStryRecord.id).Nrich__Status__c.equalsIgnoreCase(Label.Completed)) {
                        UserStryRecord.adderror(label.triggeruponuserstory_er2);
                    }
                }                                
            }
                        
            if (trigger.isafter) {
                if(String.isNotBlank(UserStryRecord.Nrich__sprint__c) && String.isNotBlank(UserStryRecord.Nrich__Status__c) && UserStryRecord.Nrich__Status__c.equalsIgnoreCase(Label.Completed)) // for status update on sprint
                { 
                    SprntIds.add(UserStryRecord.Nrich__sprint__c);                   
                }
                projecthourupdate.add(UserStryRecord.Nrich__Project__c);
                
                if (String.isNotBlank(UserStryRecord.Nrich__Sprint__c)) { // for custom roll up at sprint
                    sprintid.add(UserStryRecord.Nrich__Sprint__c);                    
                }
                if (UserStryRecord.Nrich__Sprint__c != trigger.oldMap.get(UserStryRecord.Id).Nrich__Sprint__c) {
                    projectid.add(trigger.oldMap.get(UserStryRecord.Id).Nrich__Sprint__c);
                }
                
            }
        }
        
        if (projectid.size() > 0) {
            UserStoryUpdateTriggerHandler.usstryUpdateOnRemove(projectid);
        }
        if (sprintid.size() > 0) {
            system.debug('IDE IS= ' + sprintid);
            UserStoryUpdateTriggerHandler.usrstryHourUpdate(sprintid);
        }
        if (SprntIds.size() > 0) {
            UserStoryUpdateTriggerHandler.changeSprintStatus(SprntIds);
        }
        
        if (Userid.size() > 0) {
            UserStoryUpdateTriggerHandler.checkStatusOfTicket(Userid, trigger.new);
        }
        
    }
    
    if (trigger.isinsert) {       
        for (Nrich__User_Story__c  InsrtUserStoryRecrd: trigger.new) {
            
            if (trigger.isbefore) {
                if (InsrtUserStoryRecrd.Nrich__Sprint__c != null) {
                    Userid.add(InsrtUserStoryRecrd.Id);
                }
                if (InsrtUserStoryRecrd.Nrich__Assigned_Too__c != Null) {
                    TeamIds.add(InsrtUserStoryRecrd.Nrich__Assigned_Too__c);
                    LstUsRecord.add(InsrtUserStoryRecrd);
                }
                
            }
            
            if (trigger.isafter) {
                projecthourupdate.add(InsrtUserStoryRecrd.Nrich__Project__c);
                
                if (InsrtUserStoryRecrd.Nrich__sprint__c != null)
                    LstSprintId.add(InsrtUserStoryRecrd.Nrich__Sprint__c);
                
                if (InsrtUserStoryRecrd.Nrich__sprint__c != null && InsrtUserStoryRecrd.Nrich__Status__c == Label.Completed) // for status update on sprint
                {
                    SprntIds.add(InsrtUserStoryRecrd.Nrich__sprint__c);
                    
                }
                
            }
        }
        
        
        
        if (LstSprintId.size() > 0) {
            UserStoryUpdateTriggerHandler.usrstryHourUpdate(LstSprintId);
        }
        if (SprntIds.size() > 0) {
            UserStoryUpdateTriggerHandler.changeSprintStatus(SprntIds);
        }
        
    }
    
    if (trigger.isdelete) {
        
        if (trigger.isbefore && (trigger.isdelete)) {
            UserStoryUpdateTriggerHandler.ValidateUsrstryLoe(trigger.old);
        }
        if(trigger.isafter){
            
            System.debug('delete here  ' + trigger.old);
            set< Id > UserIds = new set< Id >();
            //for sprint status update 
            set< Id > ProjIds = new set< Id >();
            //set< Id > SprntIds = new set< Id >();
            list < string > sprintrollup = new list < string > (); // for sprint  custom rollup
            for (Nrich__User_Story__c  DelUsrStryRecrd: trigger.old) {
                UserIds.add(DelUsrStryRecrd.id);
                
                if (DelUsrStryRecrd.Nrich__Status__c == Label.Completed) {
                    DelUsrStryRecrd.adderror(Label.Cannot_Delete_UserStory_Once_Completed);
                }    
                else{
                    ProjIds.add(DelUsrStryRecrd.Nrich__Project__c); 
                    
                    if (DelUsrStryRecrd.Nrich__Sprint__c != null) {
                        
                        sprintrollup.add(DelUsrStryRecrd.Nrich__Sprint__c);
                        SprntIds.add(DelUsrStryRecrd.Nrich__sprint__c);
                        
                    }     
                }                
            }
            
            if (sprintrollup.size() > 0) {
                UserStoryUpdateTriggerHandler.usrstryHourUpdate(sprintrollup);
            }
            
            if (SprntIds.size() > 0) {
                UserStoryUpdateTriggerHandler.changeSprintStatus(SprntIds);
            }
            if(ProjIds.size() > 0){
                UserStoryUpdateTriggerHandler.projectRollout(ProjIds,UserIds);
            }
            
            
        }
        
    }
    
    if (Userid.size() > 0)
        UserStoryUpdateTriggerHandler.checkSprintStatus(Userid);
        
    if (TeamIds.Size() > 0)
        UserStoryUpdateTriggerHandler.updateTeamMemberValues(TeamIds, LstUsRecord);
    
    if (projecthourupdate.size() > 0)
        UserStoryUpdateTriggerHandler.projectRollUp(projecthourupdate);
    
}