/**********************************
 // Company: Absyz
 // Developer: Pushmitha
 // Trigger Name : JobApplicationTrigger 
 // Object : Job_Application__c
 // Functionality : When a record is inserted, updated or deleted from the Job Application 
 // Testclass Name : rollUpSummaryForStatusHandlerTest
 *********************************/
trigger JobApplicationTrigger on Nrich__Job_Application__c (before insert, after insert, before update, after update, after delete) {
    //Initialization
    set<id> jobApplicationIdSet = new set<id>();  
    //Record inserted then call a method passing the parameters as a set of ids
    if(trigger.isInsert){
        for(Nrich__Job_Application__c tests : trigger.new){
            jobApplicationIdSet.add(tests.Nrich__Job__c);
            system.debug('tests.Nrich__Job__c'+tests.Nrich__Job__c);
            system.debug('trigger.new'+trigger.new);
        }
        JobApplicationTriggerHandler.totalOfStatus(jobApplicationIdSet);
    }
    //Record deleted then call a method passing the parameters as a set of ids
    if(trigger.isDelete){
        for(Nrich__Job_Application__c tests : Trigger.old){
            jobApplicationIdSet.add(tests.Nrich__Job__c);
        }
        JobApplicationTriggerHandler.totalOfStatus(jobApplicationIdSet);
    }
    //Record undeleted then call a method passing the parameters as a set of ids
    if(trigger.isUnDelete){
        for(Nrich__Job_Application__c tests : Trigger.old){
            jobApplicationIdSet.add(tests.Nrich__Job__c);
        }
        JobApplicationTriggerHandler.totalOfStatus(jobApplicationIdSet);
    }
    //Record updated then call a method passing the parameters as a set of ids
    if(trigger.isUpdate){
        for(Nrich__Job_Application__c tests : trigger.new){
            jobApplicationIdSet.add(tests.Nrich__Job__c);
            jobApplicationIdSet.add(trigger.oldmap.get(tests.id).Nrich__Job__c);
        }
        JobApplicationTriggerHandler.totalOfStatus(jobApplicationIdSet);
    }
}