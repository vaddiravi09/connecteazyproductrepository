/**************************************************************************
  Created by      : Pushmitha
  Trigger Name : CandidateTrigger 
  Object : Candidate__c 
  Created Date: 26/06/2018
  Functionality : When the status field in Candidate object is updated to 'On-Boarding' 
                    Create a new Employee by fetching the details from Candidate
  Testclass Name : cantidateToEmployeeCreationTest
 *************************************************************************/
trigger CandidateTrigger on Nrich__Candidate__c (after update) {
    set<Id> candidateIdSet = new set<Id>();
    if(trigger.isUpdate){
        for(Nrich__Candidate__c tests : trigger.new){
            String statusValueOnBoarded = System.Label.CandidateOnBoarded;
            if(tests.Nrich__Candidate_Status__c!=trigger.oldMap.get(tests.Id).Nrich__Candidate_Status__c && tests.Nrich__Candidate_Status__c==statusValueOnBoarded){
                candidateIdSet.add(tests.Id);
            }          
        }
        if(candidateIdSet.size()>0)
            CandidateTriggerHandler.insertNewEmployee(candidateIdSet);
    }
}