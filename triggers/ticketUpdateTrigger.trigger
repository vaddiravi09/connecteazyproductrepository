trigger ticketUpdateTrigger on Nrich__Task__c (before insert, before update, after delete, after insert, after update) {
    
    //Variable Declarations
    list <Nrich__Task__c > lstTaskwithTeam = new list < Nrich__Task__c > ();
    list < Nrich__Task__c > lstTaskUsrStryCheck = new list <Nrich__Task__c > (); // contains task when user story is not null
    set < id > userstorycheck = new set < id > (); // contains sprint id to check status of sprint on insertion and deletion
    list < Nrich__Task__c > lstTaskSprintcheck = new list < Nrich__Task__c > (); //contains task when sprint is not null
    set < id > setSprintCheck = new set < id > (); //contains sprint id to check status of sprint on insertion and deletion
    set < id > sprintstatus = new set < id > (); // to update the status of user story
    set < id > userstorystatusupdate = new set < id > (); // update status of user story if all ticket is completed
    set < id > projecthourupdate = new set < id > (); //for custom rollup of est.hour and actual hour at project level
    set < id > withoutUS = new set < id > (); // ticket without user story
    set < id > onchangeofsprint = new set < id > (); // ticket on change of sprint from one to other
    Set < id > ticketid = new set < id > (); // ticket with user story
    set < id > onlinktoUS = new set < id > (); // ticket on link to user story, if previously their is no user story
    set < id > ondelinktoUS = new set < id > (); // ticket on delink to user story, if previously their was user story
    set < id > onchangeUserstory = new set < id > (); // ticket on change from one user story to other
    set < Id > ProjTask = new set < Id > ();
    set < Id > TicketTeam = new set < Id > ();
    map < Id, Nrich__Task__c > mapTasks = new map < Id, Nrich__Task__c > ();
    //map<string, Project_Pick_Values__c> Settingvalues =  Project_Pick_Values__c.getall();
    
    
    if (trigger.isInsert || trigger.isUpdate) {
        for (Nrich__Task__c cc: trigger.new) {
            if (trigger.isBefore) {
                
                if (cc.Nrich__Assigned_To__c != Null){
                    TicketTeam.add(cc.Nrich__Assigned_To__c);
                    lstTaskwithTeam.add(cc);
                }
                
                
                
                // Restrict to update task if task status is completed
                //if (trigger.isupdate && String.IsNotBlank(trigger.oldMap.get(cc.Id).Status__c) && String.IsNotBlank(cc.Status__c) && cc.Status__c.equalsIgnoreCase(Label.Working_Fine)) {                  
                 if (trigger.isupdate && (trigger.oldMap.get(cc.id).Nrich__Status__c== Label.Working_Fine)) {
                    system.debug(trigger.oldMap.get(cc.id).Nrich__Status__c+ '---fourth-----' + cc.Nrich__Status__c);
                    
                    //if the Task is Completed Than Restrict the user to update 
                    if (cc.Nrich__Status__c!= Null && trigger.oldMap.get(cc.id).Nrich__Status__c== cc.Nrich__Status__c) {
                        system.debug('=====' + label.tickettrigger_er3);
                        cc.adderror(label.tickettrigger_er3);
                    }
                    
                }
                // check user story is completed
                if (cc.Nrich__User_Story__c != Null) {
                    system.debug('first-------- User Story ');
                    userstorycheck.add(cc.Nrich__User_Story__c);
                    lstTaskUsrstryCheck.add(cc);                   
                }
                
                // to check if sprint is completed
                if (cc.Nrich__Sprint__c != null) {
                    system.debug('second------- Sprint');
                    setSprintCheck.add(cc.Nrich__Sprint__c);
                    lstTaskSprintcheck.add(cc);                   
                }                       
            }
            
            if (trigger.isAfter) {
                if (trigger.isinsert || trigger.isupdate) {                   
                    If(trigger.Isinsert ){                        
                        //Rollup the Ticket values to user story    
                        if (cc.Nrich__User_Story__c != Null)
                            ticketId.add(cc.Nrich__User_Story__c );
                        
                        //Rollup the Ticket Hours to Sprint
                        if (cc.Nrich__Sprint__c != Null)
                            onchangeofsprint.add(cc.Nrich__Sprint__c);
                        
                        // for  custom rollup of hour field on project level//
                        if (cc.Nrich__User_Story__c == null) {
                            projecthourupdate.add(cc.Nrich__Project__c);
                        }
                        
                        //for insert if the ticket is complete and user story is not null update the status to complete   
                        if (cc.Nrich__User_Story__c != null && 
                            ( cc.Nrich__Status__c!= null && cc.Nrich__Status__c== Label.Working_Fine)) {
                                 userstorystatusupdate.add(cc.Nrich__User_Story__c);
                                 system.debug('for update/insert if the ticket is complete and user story is not null');
                             }
                        
                        // insert Ticket with completed and only sprint is present than update the status to complete 
                        if (cc.Nrich__User_Story__c == null && cc.Nrich__Sprint__c != null && 
                            (cc.Nrich__Status__c!= null && cc.Nrich__Status__c== Label.Working_Fine)) {
                                 sprintstatus.add(cc.Nrich__Sprint__c);
                                 system.debug('update/insert Ticket with completed and only sprint is present than update the status ');
                             }                                              
                    }
                    
                    if ((trigger.isupdate)) {
                        //Rollup the Ticket values to user story    
                        if (cc.Nrich__User_Story__c != Null)
                            ticketId.add(cc.Nrich__User_Story__c );
                        
                        //Rollup the Ticket Hours to Sprint
                        if (cc.Nrich__Sprint__c != Null)
                            onchangeofsprint.add(cc.Nrich__Sprint__c);
                        
                        //Rollup the Hours to project  
                        //  if (trigger.oldmap.get(cc.id).ProjectEnrich__User_Story__c == null && cc.ProjectEnrich__User_Story__c != Null) {
                        projecthourupdate.add(cc.Nrich__Project__c);
                        // }
                        
                        //for update if the ticket is complete and user story is not null update the status   
                        if ( cc.Nrich__Status__c!= null && cc.Nrich__Status__c== Label.Working_Fine) {
                                 
                                 if(cc.Nrich__User_Story__c != Null){   
                                     userstorystatusupdate.add(cc.Nrich__User_Story__c);
                                     system.debug('for update/insert if the ticket is complete and user story is not null');}
                                 
                                 //update Ticket with completed and only sprint is present than update the status 
                                 if(cc.Nrich__Sprint__c != Null && cc.Nrich__User_Story__c == Null){
                                     sprintstatus.add(cc.Nrich__Sprint__c);
                                     system.debug('update/insert Ticket with completed and only sprint is present than update the status ');}
                                 
                             }
                        
                        
                        //for update of Ticket only Sprint irrespective of status 
                        if (cc.Nrich__User_Story__c == null && cc.Nrich__Sprint__c != null) // if their is no user story  and hour field rollup to sprint level
                        {
                            //for update if the sprint gets changed make te rollup values to popup at sprint
                            if (trigger.oldmap.get(cc.id).Nrich__Sprint__c != cc.Nrich__Sprint__c && trigger.oldmap.get(cc.id).Nrich__Sprint__c !=Null) {
                                system.debug('if the sprint is changed than status has to be update at sprint ');
                                onchangeofsprint.add(trigger.oldmap.get(cc.id).Nrich__Sprint__c ); // on change of sprint
                                system.debug('for update/insert of Ticket only Sprint irrespective of status ');
                                //withoutUS
                                onchangeofsprint.add(cc.Nrich__Sprint__c );
                            }
                            
                        }   
                        
                        //for update of completed ticket get the Id of User Story to update the status  
                        if (cc.Nrich__User_Story__c  != null && trigger.oldmap.get(cc.id).Nrich__Status__c!= Label.Working_Fine && cc.Nrich__Status__c== Label.Working_Fine) {
                                userstorystatusupdate.add(cc.Nrich__User_Story__c );
                                
                                // it update the old user story status values when the user story is null
                                if (trigger.oldmap.get(cc.id).Nrich__User_Story__c  != null && cc.Nrich__User_Story__c  == null)
                                    userstorystatusupdate.add(trigger.oldmap.get(cc.id).Nrich__User_Story__c );
                                
                            }
                        
                        if(cc.Nrich__Sprint__c  != null && trigger.oldmap.get(cc.id).Nrich__Sprint__c  == Null)
                            onchangeofsprint.add(cc.Nrich__Sprint__c   );
                        if(cc.Nrich__Sprint__c  == null && trigger.oldmap.get(cc.id).Nrich__Sprint__c  != Null)
                            onchangeofsprint.add(trigger.oldmap.get(cc.id).Nrich__Sprint__c    );
                        
                        //}
                        
                        //need to discuss multiple times this update taes place
                        if (trigger.oldmap.get(cc.id).Nrich__User_Story__c  == null && cc.Nrich__User_Story__c  != null && cc.Nrich__Sprint__c != null) {
                            //onlinktoUS
                            system.debug('old value of Sprint== ' + trigger.oldmap.get(cc.id).Nrich__Sprint__c);
                            onchangeofsprint.add(cc.Nrich__Sprint__c);
                        }
                        
                        //update the rollup values to the user story 
                        if (trigger.oldmap.get(cc.id).Nrich__User_Story__c != null && cc.Nrich__User_Story__c == null){
                            ticketid.add(trigger.oldmap.get(cc.id).Nrich__User_Story__c);
                            
                            //user story is null than populate hours in sprint 
                            if(cc.Nrich__Sprint__c != null){
                                
                                onchangeofsprint.add(cc.Nrich__Sprint__c);
                            }
                            
                        }
                        
                        if (cc.Nrich__User_Story__c != null) {
                            
                            if (trigger.oldmap.get(cc.id).Nrich__User_Story__c != cc.Nrich__User_Story__c)                                
                                ticketid.add(cc.Nrich__User_Story__c);
                            
                        }
                        
                        if (trigger.oldmap.get(cc.id).Nrich__User_Story__c != null && cc.Nrich__User_Story__c == null && cc.Nrich__Sprint__c == null)
                            userstorystatusupdate.add(trigger.oldmap.get(cc.id).Nrich__User_Story__c);
                        
                        //reduce the Ticket hours values in Project 
                        If(trigger.isupdate && trigger.oldmap.get(cc.id).Nrich__User_Story__c == null && cc.Nrich__User_Story__c != Null) {
                            mapTasks.put(cc.Id, cc);
                            ProjTask.add(cc.Nrich__Project__c);
                        }
                    }
                    
                    
                }
            } 
            
        }
    }
    
    if(TicketTeam.size()>0)
        TicketUpdateTriggerHandler.updateTeamMemberValues(TicketTeam, lstTaskwithTeam);       
    
    //for user story check point to restrict 
    if (lstTaskUsrstryCheck.size() > 0) {
        TicketUpdateTriggerHandler.checkusstatusofuserstry(userstorycheck, lstTaskUsrstryCheck);
    }
    //for Sprint check point to restrict 
    if (lstTaskSprintcheck.size() > 0) {
        TicketUpdateTriggerHandler.checkusstatusofSprint(setSprintCheck, lstTaskSprintcheck);
    }
    
    if (ticketid.size() > 0) {
        system.debug(ticketid);
        TicketUpdateTriggerHandler.updateonUS(ticketid);
        
    }
    if (ProjTask.size() > 0)
        TicketUpdateTriggerHandler.projectrollout(ProjTask, mapTasks);
    
    if (projecthourupdate.size() > 0) {
        TicketUpdateTriggerHandler.projectrollup(projecthourupdate);
    }
    
    if (onchangeofsprint.size() > 0) {
        System.debug('onchangeofsprint====' + onchangeofsprint);
        TicketUpdateTriggerHandler.updatesprint(onchangeofsprint);
    }
    
    if (userstorystatusupdate.size() > 0) {
        system.debug('userstorystatusupdate====' + userstorystatusupdate);
        TicketUpdateTriggerHandler.userstorystatusupdate(userstorystatusupdate, trigger.new);
    }
    if (sprintstatus.size() > 0) {
        system.debug('sprintstatus=====' + sprintstatus);
        TicketUpdateTriggerHandler.updatesprintstatus(sprintstatus);
    }
    
    
    
    if (trigger.isdelete) {
        set < id > sprintstatusondelete = new set < id > (); // check status of sprint if completed ticket is deleted
        set < id > userid = new set < id > (); // check status of user story if completed ticket is deleted
        set < id > sprintid = new set < id > (); // for custom roll up at sprint level
        Map<Id, Nrich__Task__c> MapTask = new Map<Id, Nrich__Task__c>();
        
        for (Nrich__Task__c idis: trigger.old) {
            
            system.debug('Hello' + idis.Nrich__User_Story__c);
            if(idis.Nrich__Status__c== Label.Working_Fine){
                idis.adderror(Label.Cannot_Delete_Task);
            }else{
                
                if (idis.Nrich__User_Story__c != null){
                    sprintid.add(idis.Nrich__User_Story__c);
                    if(idis.Nrich__Status__c!= Null){
                        userid.add(idis.Nrich__User_Story__c);
                    }
                }
                if (idis.Nrich__Sprint__c != null){
                    withoutUS.add(idis.Nrich__Sprint__c);
                    if(idis.Nrich__Status__c!= Null){
                        sprintstatusondelete.add(idis.Nrich__Sprint__c);
                    }
                }
                if (idis.Nrich__User_Story__c == null) // for custom rollup on project level
                    projecthourupdate.add(idis.Nrich__Project__c);
                
            }    
            
        }
        if (projecthourupdate.size() > 0) {
            TicketUpdateTriggerHandler.projectrollup(projecthourupdate);
        }
        
        if (sprintid.size() > 0) {
            TicketUpdateTriggerHandler.updateonUS(sprintid);
        }
        if (withoutUS.size() > 0) {
            TicketUpdateTriggerHandler.updatesprint(withoutUS);
        }
        if (userid.size() > 0) {
            TicketUpdateTriggerHandler.onticketdelete(userid);
        }
        
        if (sprintstatusondelete.size() > 0) {
            TicketUpdateTriggerHandler.statusupdateonSprintticketdelete(sprintstatusondelete);
        }
        
    }
}