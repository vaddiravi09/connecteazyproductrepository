/************************************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 26/06/2018
* Class Name : emailToPanelNotificationTemplate
* Purpose : Visualforce Template - Email to Panel Members when the interview round is scheduled or rejected.
* Testclass Name : emailToPanelNotificationTemplateTest
***********************************************************************************/

public with sharing class EmailToPanelNotificationTemplate {
    public String ObjType {get;set;}
    public ID relatedId {get;set;}
    public set<String> interviewRoundIdSet {get;set;}
    public list<Nrich__Feedback__c> feedbackList{get; set;}
    public String startDateTime {get;set;}
    public String endDateTime {get;set;} 
    public String jobRoundName {get;set;}
    public String candidateName {get;set;}
    public String candidatePhone {get;set;}
    public String interviewMode {get;set;}
    public list<Nrich__Interview_Round__c> interviewRoundList {get;set;}
    
    /*******************************************************************************************
Created by      : Pushmitha
Overall Purpose : Fetch Interview Panel Member Details
InputParams     : NA
OutputParams    : List of Interview Panel Members
*******************************************************************************************/
    public List<Nrich__Interview_Panel_Member__c> getpanelList (){
        try{
        feedbackList = new list<Nrich__Feedback__c>();
        interviewRoundList = new List<Nrich__Interview_Round__c>();
        List<Nrich__Interview_Panel_Member__c> panelMemberList = new List<Nrich__Interview_Panel_Member__c>();
        if(relatedId != null){
            list<Nrich__Interview_Panel_Member__c> interviewPanelList = [select id,Nrich__Interview_Round__r.Nrich__Interview_Mode__c,Nrich__Interview_Round__r.Nrich__Job_Application__r.Nrich__Candidate__r.Nrich__Primary_Phone__c, Nrich__Interview_Round__r.Name, Nrich__Interview_Round__r.Nrich__Interview_Start_Time__c, Nrich__Interview_Round__r.Nrich__Job_Application__r.Nrich__Candidate__r.Name, Nrich__Interview_Round__r.Nrich__Interview_End_Time__c, Nrich__Interview_Round__c, Nrich__Interview_Round__r.Nrich__Job_Application__c from Nrich__Interview_Panel_Member__c where Id =: relatedId];
            interviewRoundIdSet = new set<String>();
            if(interviewPanelList!=NULL && interviewPanelList.size()>0){
                startDateTime = interviewPanelList[0].Nrich__Interview_Round__r.Nrich__Interview_Start_Time__c.format();
                endDateTime = interviewPanelList[0].Nrich__Interview_Round__r.Nrich__Interview_End_Time__c.format();
                interviewMode = interviewPanelList[0].Nrich__Interview_Round__r.Nrich__Interview_Mode__c;
                jobRoundName = interviewPanelList[0].Nrich__Interview_Round__r.Name;
                candidateName = interviewPanelList[0].Nrich__Interview_Round__r.Nrich__Job_Application__r.Nrich__Candidate__r.Name;
                candidatePhone = interviewPanelList[0].Nrich__Interview_Round__r.Nrich__Job_Application__r.Nrich__Candidate__r.Nrich__Primary_Phone__c;
                for(Nrich__Interview_Round__c ir : [select id, Name, Nrich__Job_Application__c from Nrich__Interview_Round__c where Nrich__Job_Application__c =: interviewPanelList[0].Nrich__Interview_Round__r.Nrich__Job_Application__c]){
                    String integerRound = ir.Id;
                    string intId= interviewPanelList[0].Nrich__Interview_Round__c;
                    if(integerRound!=NULL && (integerRound.left(15)!=intId.left(15)))
                        interviewRoundIdSet.add(ir.Id);
                }
                for(Nrich__Interview_Panel_Member__c panelMember:  [select id, Nrich__Interview_Round__r.Nrich__Interview_Start_Time__c, Nrich__Interview_Round__r.Nrich__Interview_End_Time__c, Nrich__Interview_Round__r.Name, Nrich__Feedback_Page_URL__c,Nrich__Interview_Round__r.Nrich__Job_Application__r.Name,Nrich__Interview_Round__r.Nrich__Job_Application__r.Nrich__Resume__c,Nrich__Interview_Round__r.Nrich__Job_Application__r.Nrich__Candidate__r.Name, Nrich__Interview_Round__r.Nrich__Job_Application__c,Nrich__Feedback__c,Nrich__Employee__r.Name, Nrich__Employee__r.Nrich__Primary_Email__c, Nrich__Employee__r.Nrich__Mobile__c from Nrich__Interview_Panel_Member__c where Nrich__Interview_Round__c=:interviewPanelList[0].Nrich__Interview_Round__c]){
                    panelMemberList.add(panelMember);
                }
                if(ObjectFieldAccessCheck.checkAccessible('Nrich__Feedback__c', 'Nrich__Feedback_Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Feedback__c', 'Nrich__Feedback_Description__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Feedback__c', 'Nrich__Interview_Round__c'))
                    for(Nrich__Feedback__c feedback: [Select Id, Nrich__Feedback_Status__c, Nrich__Feedback_Description__c, Nrich__Interview_Round__r.Name, Nrich__Interview_Round__r.Nrich__Interview_Level__c, Nrich__Interview_Round__r.Nrich__Interview_Mode__c, Nrich__Interview_Panel_Member__r.Nrich__Employee__r.Name from Nrich__Feedback__c where Nrich__Interview_Round__c IN: interviewRoundIdSet]){
                        feedbackList.add(feedback);
                    }
                
                //New
                if(ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Job_Application__c'))
                    interviewRoundList = [select id, Name, Nrich__Job_Application__c,(Select id,Name,Nrich__Feedback_Status__c, Nrich__Feedback_Description__c,Nrich__Interview_Panel_Member__r.Nrich__Employee__r.Name,Nrich__Interview_Round__r.Name,Nrich__Interview_Round__r.Nrich__Interview_Level__c, Nrich__Interview_Round__r.Nrich__Interview_Mode__c from Nrich__Feedbacks__r) from Nrich__Interview_Round__c where Nrich__Job_Application__c =: interviewPanelList[0].Nrich__Interview_Round__r.Nrich__Job_Application__c];
            }
        }
        return panelMemberList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmailToPanelNotificationTemplate.getpanelList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}