@isTest
public class CallOutToGetApplicationDetailsTest {
    
    
    public static TestMethod Void Method1(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        user intuser = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Integration User', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='intuser@practo.com');
        insert intuser;
        test.startTest();
        Nrich__Job_Application__c Job_ApplicationInstance= new Nrich__Job_Application__c();
        Nrich__Job_Application__c Job_ApplicationInstance2= new Nrich__Job_Application__c();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__Employee__c emp=UtilityForTest.Employee_Utility();
        Nrich__Job_Application__c jobapp=UtilityForTest.job_Application_Utility(cand.id,job.id);
        Nrich__Interview_Round__c intRound=UtilityForTest.Interview_Round_Utility(jobapp.id);
        
        Job_ApplicationInstance.Name='Test Application';
        Job_ApplicationInstance.Nrich__Primary_Phone__c='9652321966';
        Job_ApplicationInstance.Nrich__Email__c='testa@yopmail.com';
        Job_ApplicationInstance.Nrich__Source__c='Twitter';
        Job_ApplicationInstance.Nrich__Job__c=job.id;
        Job_ApplicationInstance.Nrich__Candidate__c=cand.id;
        
        
        map<string,string> mapstr=new map<String,string>();
        mapstr.put('Name','Test Application');
        mapstr.put('Nrich__Primary_Phone__c','9652321966');
        mapstr.put('Nrich__Email__c','testa@yopmail.com');
        mapstr.put('Nrich__Source__c','Twitter');
        mapstr.put('Nrich__Job__c',job.id);
        mapstr.put('Nrich__Candidate__c',cand.id);
        
        
        String JsonMsg=JSON.serialize(mapstr);
        system.runAs(intuser){
            RestRequest req= new RestRequest();
            RestResponse res=new RestResponse();
            req.requestURI='/services/apexrest/appdetails';
            req.httpMethod='POST';
            req.requestBody=Blob.valueOf(JsonMsg);
            RestContext.request=req;
            RestContext.response=res;
            Test.setMock(HttpCalloutMock.class,  new MockHttpResponseGenerator());
            
            CallOutToGetApplicationDetails.doPost(); 
        }
        System.assertEquals('Test Application', Job_ApplicationInstance.Name);
        
        test.stopTest();
        
    }
     public static TestMethod Void Method2(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        user intuser = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Integration User', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='intuser@practo.com');
        insert intuser;
        test.startTest();
        Nrich__Job_Application__c Job_ApplicationInstance= new Nrich__Job_Application__c();
        Nrich__Job_Application__c Job_ApplicationInstance2= new Nrich__Job_Application__c();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__Employee__c emp=UtilityForTest.Employee_Utility();
        Nrich__Job_Application__c jobapp=UtilityForTest.job_Application_Utility(cand.id,job.id);
        Nrich__Interview_Round__c intRound=UtilityForTest.Interview_Round_Utility(jobapp.id);
        
        Job_ApplicationInstance.Name='Test Application';
        Job_ApplicationInstance.Nrich__Primary_Phone__c='9652321966';
        Job_ApplicationInstance.Nrich__Email__c='testa@yopmail.com';
        Job_ApplicationInstance.Nrich__Source__c='Twitter';
        Job_ApplicationInstance.Nrich__Job__c=job.id;
        Job_ApplicationInstance.Nrich__Candidate__c=cand.id;
        
        
        map<string,string> mapstr=new map<String,string>();
        mapstr.put('Name','Test Application');
        mapstr.put('Nrich__Primary_Phone__c','9652321966');
        mapstr.put('Nrich__Email__c','testa@yopmail.com');
        mapstr.put('Nrich__Source__c','Twitte');
        mapstr.put('Nrich__Job__c',job.id);
        mapstr.put('Nrich__Candidate__c',cand.id);
        
        
        String JsonMsg=JSON.serialize(mapstr);
        system.runAs(intuser){
            RestRequest req= new RestRequest();
            RestResponse res=new RestResponse();
            req.requestURI='/services/apexrest/appdetails';
            req.httpMethod='POST';
            req.requestBody=Blob.valueOf(JsonMsg);
            RestContext.request=req;
            RestContext.response=res;
            Test.setMock(HttpCalloutMock.class,  new MockHttpResponseGeneratorError());
            
            CallOutToGetApplicationDetails.doPost(); 
        }
         System.assertEquals('9652321966', Job_ApplicationInstance.Nrich__Primary_Phone__c);
        
        test.stopTest();
        
    }
}