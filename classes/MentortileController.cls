public with sharing class MentortileController {
    public static string strEmployeeID;
    
    // Employee wrapper class
    public class EmployeeWrapper{
        @auraEnabled
        public Nrich__Employee__c objEmployeeDetails{get;set;}
        @auraEnabled
        public list<Nrich__Employee__c> lstMentees{get;set;}
    }
    
    //For fetching details
    @AuraEnabled
    public static EmployeeWrapper fetchEmployeeDetails(){
        try{
        EmployeeWrapper objWrap = new EmployeeWrapper();
        Nrich__Employee__c objEmp = new Nrich__Employee__c();
        string strEmpID = userinfo.getuserid();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__ProfilePic__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__EmployeeDesignation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Employee_Id__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Mentor__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Email__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Experience_in_Organization__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Total_Experience__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Phone__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Date_of_Joining__c')
          )
            objEmp = [select id,
                      Name,
                      Nrich__EmployeeDesignation__c,
                      Nrich__Employee_Id__c,
                      Nrich__Mentor__c,
                      Nrich__Mentor__r.Name,
                      Nrich__Mentor__r.Nrich__Employee_Id__c,
                      Nrich__Mentor__r.Nrich__EmployeeDesignation__c,
                      Nrich__Experience_in_Organization__c,
                      Nrich__Total_Experience__c
                      
                      from Nrich__Employee__c 
                      where Nrich__Related_User__c =: strEmpID
                      limit 1]; 
        system.debug('objEmp****'+objEmp);
        objWrap.objEmployeeDetails = objEmp;
        strEmployeeID = objEmp.id;
        list<Nrich__Employee__c> lstEmp = new list<Nrich__Employee__c>();
        lstEmp = [select id,
                  Name,
                  Nrich__EmployeeDesignation__c,
                  Nrich__Employee_Id__c,
                  Nrich__Mentor__c,
                  Nrich__Mentor__r.Name,
                  Nrich__Mentor__r.Nrich__EmployeeDesignation__c,
                  Nrich__Experience_in_Organization__c,
                  Nrich__Total_Experience__c,
                  Nrich__Date_of_Joining__c,
                  Nrich__Primary_Email__c, 
                  Nrich__Primary_Phone__c   
                  from Nrich__Employee__c 
                  where Nrich__Mentor__c=: objEmp.id];
        system.debug('lstEmp****'+objEmp);
        objWrap.lstMentees = lstEmp;
        system.debug('objWrap****'+objWrap);
        return objWrap;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='MentortileController.fetchEmployeeDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}