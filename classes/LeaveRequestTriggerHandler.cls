/********************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 22/05/2019
* Description: Creating timesheets and or leave request validation.
********************************************************************/
public with sharing class LeaveRequestTriggerHandler {
    
    public static void CreateTimeSheet(map<Nrich__Leave_Request__c, Integer> LeaveRequestMap){
      try {
        list<Nrich__Timesheet__c>Timesheetdetailslist=new list<Nrich__Timesheet__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Start_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Requested_By__c') &&
           ObjectFieldAccessCheck.checkAccessible('Nrich__Timesheet__c', 'Nrich__Date__c') &&   ObjectFieldAccessCheck.checkAccessible('Nrich__Timesheet__c', 'Nrich__Employeee__c') ){
               for(Nrich__Leave_Request__c Leave:LeaveRequestMap.keyset()){
                   for(Integer i=0;i<=LeaveRequestMap.get(Leave);i++) {
                       Nrich__Timesheet__c Timesheet=new Nrich__Timesheet__c ();
                       Timesheet.Nrich__Date__c=Leave.Nrich__Start_Date__c.adddays(i);
                       Timesheet.Name=string.valueOf(Leave.Nrich__Start_Date__c.adddays(i));
                       Timesheet.Nrich__Employeee__c=Leave.Nrich__Requested_By__c;
                       Timesheetdetailslist.add(Timesheet);
                   }
               }
           }
        
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Timesheet__c.getSObjectType().getDescribe();        
        if(objectDescriptioncontent.isCreateable() && Timesheetdetailslist.size()>0)
            Database.Upsert(Timesheetdetailslist);
      }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='LeaveRequestTriggerHandler.CreateTimeSheet', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          } 
      }
    }
    
    public static void leaveRequestValidation(list<Nrich__Leave_Request__c>LeaveRequestList, Set<String> employeeIdSet, Set<String> EmployeeLeaveCardSet){
      try{
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Start_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__End_Date__c')  &&
          ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Requested_By__c') ){
            for(Nrich__Leave_Request__c leaveRequest: [Select Id, Nrich__Start_Date__c, Nrich__End_Date__c, Nrich__Requested_By__c from Nrich__Leave_Request__c where Nrich__Requested_By__c IN: employeeIdSet AND Nrich__EmployeeLeaveCard__c IN: EmployeeLeaveCardSet AND Nrich__Status__c=:Label.Approved]){
                for(Nrich__Leave_Request__c currentleaveRequest: LeaveRequestList){
                    if((currentleaveRequest.Nrich__Requested_By__c==leaveRequest.Nrich__Requested_By__c) && (currentleaveRequest.Nrich__Start_Date__c>=leaveRequest.Nrich__Start_Date__c) && (currentleaveRequest.Nrich__End_Date__c<=leaveRequest.Nrich__End_Date__c)){
                        currentleaveRequest.addError(System.label.LeaveDateOverlapMessage);
                    }
                }
            }
        }
      }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='LeaveRequestTriggerHandler.leaveRequestValidation', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          } 
      }
    }
}