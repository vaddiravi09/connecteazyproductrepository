/****************************************************************************************************
    * Company: Absyz
    * Developer: Ravi Teja
    * Created Date: 23/10/2018
    * Description: Json Parser for Application Details
*****************************************************************************************************/
public class JsonParserforApplicationDetails {
    
    public ApplicationDetails ApplicationDetails{get;set;}
    public class ApplicationDetails{
       
        public string ApplicationName{get;set;}
        public string email{get;set;}
        public string applicationstatus{get;set;}
        //public string candidateid{get;set;}
        
    } 

}