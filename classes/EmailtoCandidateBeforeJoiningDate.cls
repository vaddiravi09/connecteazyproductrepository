/********************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 30/06/2018
* Description: Email Alert to Job Applicant.
* Test Class: EmailTemplatesTest
********************************************************************/
public with sharing class EmailtoCandidateBeforeJoiningDate {
    public String objType {get;set;}
    public ID relatedId {get;set;}
    
    /*******************************************************************************************
Created by      : Pushmitha
Overall Purpose : Fetch Job Application Details
InputParams     : NA
OutputParams    : List of Job Application
*******************************************************************************************/
    public List<Nrich__Job_Application__c> getJobApplicationList (){
        try{
        List<Nrich__Job_Application__c> jobAppList = new List<Nrich__Job_Application__c>();
        if(relatedId != null){
            jobAppList = [select Id, Candidate__r.Name, Pre_Joining_URL__c, Name from Job_Application__c where id = : relatedId];
        }
        return jobAppList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmailtoCandidateBeforeJoiningDate.getJobApplicationList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}