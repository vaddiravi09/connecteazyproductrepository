public class BirthdayEmailController {
    
    public String objType {get;set;}
    public String imageURL{get{
       string URLval =URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id=';
        List< document> documentList=[select Id from document where Name=:Label.BirthdayBackgroundImage];
        
        if(documentList.size()>0)
        {
            URLval=URLval+documentList[0].id+'&oid='+UserInfo.getOrganizationId();
        }
        return URLval;
     } set;
    }
    public ID relatedId {get;set;}
  
}