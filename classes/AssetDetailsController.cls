/****************************************************************************************************
	* Company: Absyz
	* Developer: Raviteja
	* Created Date: 18/10/2018
	* Description: Fetching the records of Asserts
	*****************************************************************************************************/
public with sharing class AssetDetailsController {
    
    public static string strEmployeeID;
    
    //Fetching Asset Request Details 
    @AuraEnabled
    public static list<Nrich__Asset_Request__c > ViewAssetRequestDetails(){
        try{
        list<Nrich__Asset_Request__c> AssetReqDetails=new  list<Nrich__Asset_Request__c>();
        string strEmpID = userinfo.getuserid();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__Date_Approved__c')  && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__AllocationExpectedDate__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__Reason__c') &&
           ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__Reason__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__Status__c') 
          
          ){
              AssetReqDetails=[select id,Name,Nrich__Date_Approved__c ,Nrich__Employeee__r.Name,Nrich__AllocationExpectedDate__c ,
                               Nrich__Reason__c, Nrich__Status__c ,Nrich__Asset_Category__r.Name from Nrich__Asset_Request__c where Nrich__Employeee__r.Nrich__Related_User__c=:strEmpID 
                              
                              ];
          }
        
        return AssetReqDetails;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='AssetDetailsController.ViewAssetRequestDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    //Fetching Details of Asset Allocation Details
    @AuraEnabled 
    public static list<Nrich__Asset_Allocation__c>viewAssetRecords()
    {
        try{
        list<Nrich__Asset_Allocation__c> AssetDetails=new list<Nrich__Asset_Allocation__c>();
        string strEmpID = userinfo.getuserid();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Allocation__c', 'Nrich__Allocation_StartDate__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Allocation__c', 'Nrich__Allocation_EndDate__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Allocation__c', 'Nrich__Status__c') 
           &&ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Allocation__c', 'Nrich__Approved_date__c') 
          ){
              AssetDetails= [select  id  ,Nrich__Allocation_StartDate__c , Nrich__Allocation_EndDate__c ,Nrich__Approved_date__c ,Nrich__Expected_Date__c, 
                             Name,Nrich__Status__c,Nrich__Asset_Items__r.name,Nrich__Asset_Items__r.Nrich__Brand__c, Nrich__Asset_Items__r.Nrich__Status__c,
                             Nrich__Asset_Items__r.Nrich__Asset__r.Name, Nrich__Asset_Items__r.Nrich__Asset__r.Nrich__IconType__c,Nrich__Asset_Items__r.Nrich__Asset__r.Nrich__Designation__c
                             from  Nrich__Asset_Allocation__c where Nrich__Assigned_To__r.Nrich__Related_user__c=:strEmpID];
          }        
        System.debug('Assetdetails' +AssetDetails);
        return AssetDetails;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='AssetDetailsController.viewAssetRecords', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    
    
    //For Inserting Asset Request 
    @AuraEnabled
    public static Id insertnewrecords(Nrich__Asset_Request__c  assetdetails, Id AsId)
        
    {
        Nrich__Employee__c objEmp = new Nrich__Employee__c();
        string strEmpID = userinfo.getuserid();
        objEmp = [select id,
                  Name
                  from Nrich__Employee__c 
                  where Nrich__Related_User__c =: strEmpID
                  limit 1]; 
        system.debug('objEmp****'+objEmp);
        strEmployeeID = objEmp.id;
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Asset_Request__c.getSObjectType().getDescribe();
        if( objectDescriptioncontent.isCreateable()  && ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__Employeee__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__Asset_Category__c')){
            assetdetails.Nrich__Employeee__c =strEmployeeID;
            assetdetails.Nrich__Asset_Category__c  = AsId;
            
            
            database.insert(assetdetails);
        }
        system.debug(assetdetails);
        return assetdetails.Id;
        
    }
    
    //For Submitting Approval Through Button
    @AuraEnabled
    public static void submitforapproval(string asr)
    {
        
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments(System.Label.ApprovalMessage); 
        req.setObjectId(asr); 
        req.setProcessDefinitionNameOrId('Asset_Request');
        // req.setNextApproverIds(new Id[] {cer.Employee_Name__c});
        Approval.process(req);
        system.debug('req'+req);
        
    }
    
    //Fetching Records In a Picklist of Asset
    @AuraEnabled
    public static list<Nrich__Asset__c > fetchoptions(){
        string strEmpID = userinfo.getuserid();
        list<Nrich__Employee__c > EmplList=new list<Nrich__Employee__c>();
        string Designationset='';
        if( ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__EmployeeDesignation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Related_User__c')){
            EmplList=[Select id , Nrich__EmployeeDesignation__c   from Nrich__Employee__c where Nrich__Related_User__c =:strEmpID];
        }
        System.debug('EmplList' +EmplList);
        for(Nrich__Employee__c Employyee:EmplList)
        {
            Designationset=Designationset+Employyee.Nrich__EmployeeDesignation__c;
        }
        System.debug('Designation' +Designationset);
        list<Nrich__Asset__c > Assetlist=new list<Nrich__Asset__c >();
        list<Nrich__Asset__c> AssetRetrunlist=new list<Nrich__Asset__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Asset__c', 'Nrich__Designation__c'))
            Assetlist=[Select Name ,Id,Nrich__Designation__c from Nrich__Asset__c where Nrich__Designation__c INCLUDES (:Designationset)];
        return Assetlist;
    }
    
    
}