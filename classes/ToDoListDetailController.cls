public class ToDoListDetailController {
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 22/11/2018
* Description: Updating Approval Process Dynamically
*****************************************************************************************************/
    @AuraEnabled
    public static List<Nrich__ToDoList__c> DynamicApprovalProcess(String todoString, Boolean approve){
        try{
            if(String.isNotBlank(todoString)){
                List<Nrich__ToDoList__c> toDoList = (List<Nrich__ToDoList__c>)System.JSON.deserialize(todoString, List<Nrich__ToDoList__c>.class);
                if(toDoList!=NULL && toDoList.size()>0){
                    Set<Id> recordIdSet = new Set<Id>();
                    Map<String, Nrich__ToDoList__c> todoMap = new Map<String, Nrich__ToDoList__c>();
                    if(ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Record_Id__c'))
                        for(Nrich__ToDoList__c todo: toDoList){
                            recordIdSet.add(todo.Nrich__Record_Id__c);
                            todoMap.put(todo.Nrich__Record_Id__c, todo);
                        }
                    
                    if(recordIdSet.size()>0){
                        list<Approval.ProcessWorkitemRequest> prWkItemList = new list<Approval.ProcessWorkitemRequest>();
                        map<Id, ProcessInstance> processInsMap = new map<Id, ProcessInstance>([Select ID, Status From ProcessInstance Where TargetObjectID IN:recordIdSet AND Status = :Label.Pending]);
                        if(processInsMap!=NULL && processInsMap.size()>0){
                            for(ProcessInstanceWorkitem workitem : [select Id,OriginalActorId from ProcessInstanceWorkitem where ProcessInstanceId IN:processInsMap.keySet()]){
                                Approval.ProcessWorkitemRequest prWkItem = new Approval.ProcessWorkitemRequest();
                                prWkItem.setWorkItemID(workitem.id);
                                if(approve)
                                    prWkItem.setAction(Label.Approve);
                                else
                                    prWkItem.setAction(Label.Reject);
                                
                                prWkItemList.add(prWkItem);                            
                            }
                            
                            if(prWkItemList.size()>0){
                                list<Approval.ProcessResult> appResultList = Approval.process(prWkItemList);
                                List<Nrich__ToDoList__c> todoupdateList= new List<Nrich__ToDoList__c>();
                                if(appResultList!=NULL && appResultList.size()>0){
                                    for(Approval.ProcessResult result: appResultList){
                                        if(result.isSuccess() && todoMap.size()>0 && todoMap.containsKey(result.entityid) && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c','Nrich__Status__c')
                                           && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Body__c')
                                          ){
                                              Nrich__ToDoList__c todoupdate = new Nrich__ToDoList__c();
                                              todoupdate.Id = todoMap.get(result.entityid).Id;
                                              todoupdate.Nrich__Status__c = Label.Closed;
                                              todoupdateList.add(todoupdate);
                                          }else if(!result.isSuccess() && todoMap.size()>0 && todoMap.containsKey(result.entityid)){
                                              Nrich__ToDoList__c todoupdate = new Nrich__ToDoList__c();
                                              todoupdate.Id = todoMap.get(result.entityid).Id;
                                              if(String.isNotBlank(todoMap.get(result.entityid).Nrich__Body__c) && result.errors.size()>0)
                                                  todoupdate.Nrich__Body__c = todoMap.get(result.entityid).Nrich__Body__c+ Label.Error_In_Approval +result.errors[0];
                                              else if(result.errors.size()>0 && !String.isNotBlank(todoMap.get(result.entityid).Nrich__Body__c))
                                                  todoupdate.Nrich__Body__c = Label.Error_In_Approval +result.errors[0];
                                              todoupdateList.add(todoupdate);
                                          }
                                    }
                                    
                                    if(todoupdateList.size()>0)
                                        database.update(todoupdateList, false);
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
        catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ToDoListDetailController.DynamicApprovalProcess', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
        }
        return EmployeeDashboardController.fetchToDoListMethod();
    }
}