@isTest
public class TestSupportNotification {
    
    @isTest static void  Supportlist()
    {
        Test.startTest();
        /*Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 
        User u = new User(Alias = 'sta12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id,ManagerId =u1.id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com1234');
        //   System.runAs(u){
        Company_Leave_Structure__c cleave=new Company_Leave_Structure__c(Applicable_From__c =system.today(),Applicable_Till__c=system.today(),Is_Active__c= true);
        insert cleave;
        Leave_Category__c lleave =new Leave_Category__c(Name='Test',Increment_Frequency__c=1,Increment_Step__c =2,
                                                        Will_Carry_Forward__c =False,
                                                        Will_Lapse__c =False,Applicable_Leave_Structure__c =cleave.id
                                                        
                                                       );
        
        insert lleave;
        Employee__c emp=new Employee__c(Name='TestUser1' , EmployeeDesignation__c='Developer',
                                        
                                        Total_Experience__c=11,
                                        Related_User__c=u.id );
        
        insert emp;*/
        UtilityForTest.Employee_Utility();
        Support_Ticket__c SupportTicket=new Support_Ticket__c();
        SupportTicket.Subject__c='Test';
        SupportTicket.Status__c='New';
        insert SupportTicket;
        
        list<Support_Ticket__c>SupportList=new list<Support_Ticket__c>();
        SupportList.add(SupportTicket);
        
        SupportNotificationController em= new SupportNotificationController();
        em.objType='Support_Ticket__c';
        em.SupportApplist=SupportList;
        em.getSupportList();
        
        System.assertEquals('Test', SupportTicket.Subject__c);
        
        Test.stopTest();
        
        
        //   }
        
        
    }
    
    
}