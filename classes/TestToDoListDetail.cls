@isTest
public with sharing class TestToDoListDetail {
    
    /****************************************************************************************************
* Company: Absyz
* Developer:Ashish Nag K 
* Created Date: 28/11/2018
* Description: Test class covering The ToDoListDetail
*****************************************************************************************************/ 
    
    
    @isTest static void Approval(){
        Test.startTest();
     /*   Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        
        
        Company_Leave_Structure__c cleave=new Company_Leave_Structure__c(Applicable_From__c =system.today(),Applicable_Till__c=system.today()+365,Is_Active__c=False
                                                                         
                                                                        );
        insert cleave;
        Leave_Category__c lleave =new Leave_Category__c(Name='Test',Increment_Frequency__c=1,Increment_Step__c =2,
                                                        Will_Carry_Forward__c =False,Leave_category_code__c ='1',
                                                        Will_Lapse__c =False,Applicable_Leave_Structure__c =cleave.id, Default__c=1,
                                                        Carry_Forward_Frequency__c=0
                                                       );
        
        insert lleave;
        cleave.Is_Active__c=True;
        update cleave;
        
        Employee_Leave_Card__c EmpLeaveCard=new Employee_Leave_Card__c();
        EmpLeaveCard.Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        Employee__c emp=new Employee__c(Name='TestUser' , EmployeeDesignation__c='Developer',Active__c=TRUE,
                                        
                                        Total_Experience__c=10,Related_User__c=u1.id);
        
        
        insert emp;*/
        
        Boolean approve;
        Nrich__ToDoList__c todo=new Nrich__ToDoList__c();
        todo.Nrich__Status__c='Open ';
        todo.Nrich__Due_Date__c=System.today();
        list<Nrich__ToDoList__c> todolist=new list<Nrich__ToDoList__c>();
        todolist.add(todo);
        String tod=JSON.serialize(todolist);
        
        
        Nrich__Certification__c certification=new Nrich__Certification__c(Name='Test12');
        certification.Nrich__Cost__c=50;
        insert certification;
        Nrich__Certification_Request__c  cer =new Nrich__Certification_Request__c(Nrich__Certification_Name__c=certification.id, Nrich__Employeee__c =UtilityForTest.Employee_Utility().Id,Nrich__Approval_Status__c='Applied');
        insert cer;
        
       /* Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Hii Every one' ); 
        req.setObjectId(cer.Id); 
        req.setNextApproverIds(new Id[] {cer.Employeee__c});
        Approval.ProcessResult result = Approval.process(req);
        
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        
        
        Approval.ProcessWorkitemRequest req2 =
            new Approval.ProcessWorkitemRequest();
        
        req2.setComments('Approving request.');
        
        req2.setAction('Approve');
        
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        
        req2.setWorkitemId(newWorkItemIds.get(0));
        
        // Submit the request for approval
       Approval.ProcessResult result2 =  Approval.process(req2);
        
        ProcessInstance per=new ProcessInstance();
        
        ProcessInstanceWorkitem piw=new ProcessInstanceWorkitem();
        // piw.ProcessInstanceId=per.id;
        */
        ToDoListDetailController.DynamicApprovalProcess(tod,approve);
        System.assertEquals('Open','Open');
        Test.stopTest();
        //   }
        
    }
    
    
}