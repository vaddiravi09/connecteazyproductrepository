@isTest
public with sharing class InterviewRoundTriggerHandlerTest {
    public static testmethod void nextInterviewRoundHandlerTest(){
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__Job_Round__c jr = UtilityForTest.Job_Round_Utility(job.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        irRecord.Nrich__Status__c = System.Label.InterviewStatusScheduled;
        update irRecord;
        system.assertEquals('Scheduled','Scheduled');
        irRecord.Nrich__Status__c = System.Label.InterviewStatusSelected;
        update irRecord;
        system.assertEquals('Selected','Selected');
        test.stopTest();
    }
    
  /*  public static testmethod void nextInterviewRoundHandlerTest1(){
        test.startTest();
        InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Candidate__c cand = UtilityForTest.Candidate_Utility();
        Department__c dept = UtilityForTest.Department_Utility();
        Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Job__c testJob =new Job__c();
        testJob.Department__c=dept.Id;
        testJob.Approver__c = UserInfo.getUserId();
        testJob.End_Date__c=system.today().adddays(4);
        testJob.Start_Date__c=system.today();
        testJob.Open_Positions__c=1;
        insert testJob;
        Job_Round__c jr = UtilityForTest.Job_Round_Utility(testJob.Id);
        Job_Round__c jr1 = UtilityForTest.Job_Round_Utility(job.Id);
        List<Job_Round__c> jrList = new List<Job_Round__c>();
        jrList.add(jr);
        jrList.add(jr1);
        job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        irRecord.Status__c = System.Label.InterviewStatusScheduled;
        update irRecord;
        irRecord.Status__c = System.Label.InterviewStatusSelected;
        update irRecord;
        system.assertEquals('Scheduled','Scheduled');
        
        test.stopTest();
    }
    
    public static testmethod void nextInterviewRoundHandlerTest2(){
        test.startTest();
        InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Candidate__c cand = UtilityForTest.Candidate_Utility();
        Department__c dept = UtilityForTest.Department_Utility();
        Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Job_Round__c jr = UtilityForTest.Job_Round_Utility(job.Id);
        Job_Round__c jr1 = UtilityForTest.Job_Round_Utility1(job.Id);
        job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        
        irRecord.Status__c = System.Label.InterviewStatusScheduled;
        update irRecord;
        irRecord.Status__c = System.Label.InterviewStatusRejected;
        update irRecord;
        system.assertEquals('Rejected','Rejected');
        test.stopTest();
    } */
}