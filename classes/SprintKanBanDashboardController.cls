/****************************************************************************************************
* Company: Absyz
* Developer: Seshu Varma
* Created Date: 22/10/2018
* Description: Fetching details for SprintKanBanDashboard Controller
*****************************************************************************************************/
public with sharing class SprintKanBanDashboardController {
    public static final String STORY_MOVED = 'Moved successfully.';
    @AuraEnabled
    public static Nrich__InitialSetup__c  GetDetails(){
        try{
        Nrich__InitialSetup__c InitialSetup= new  Nrich__InitialSetup__c();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Cancelled_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Completed_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Inprogress_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Onhold_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__New_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Fixed_Color__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__QA_Failed_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__QA_In_Pogress_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Ready_for_Build_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Ready_for_Design_Color__c')
           
           
          )
            InitialSetup = [SELECT Id, Nrich__Cancelled_Color__c , Nrich__Completed_Color__c, Nrich__Inprogress_Color__c, Nrich__Onhold_Color__c, Nrich__New_Color__c,Nrich__Fixed_Color__c,Nrich__QA_Failed_Color__c,Nrich__QA_In_Pogress_Color__c,
                            Nrich__Ready_for_Build_Color__c,Nrich__Ready_for_Design_Color__c from Nrich__InitialSetup__c];        
        return InitialSetup;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='SprintKanBanDashboardController.GetDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    @AuraEnabled
    Public static List < SprintkanbanWrap > getSprintUserstorydetails(String ProjectID, String SprintId, String UserStoryId, Date Startdate, Date Enddate) {
        try{
        List < SprintkanbanWrap > LstOfWrapStrint = new List < SprintkanbanWrap > ();
        string Sprint_User_Stories;
        if(String.isNotBlank(ProjectID)){
            If(String.isNotBlank(UserStoryId) && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Project__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Start_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__End_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Completion_Date__c' ) )
                Sprint_User_Stories= 'SELECT Id, Name, Nrich__Project__c, Nrich__Status__c, Nrich__Start_Date__c, Nrich__End_Date__c, Nrich__Completion_Date__c, (Select Id, Nrich__Assigned_To_Name__c , Name, Nrich__Project__c, Nrich__Project__r.Name, Nrich__Status__c, Nrich__Sprint__c, Nrich__Assigned_Too__r.Name, Nrich__Assigned_Too__c, Nrich__Estimated_Effort__c  , Nrich__Actual_Effort__c, Nrich__Priority__c, Nrich__Subject__c, Nrich__Completion_Percentage__c FROM Nrich__User_Stories__r where Id=:UserStoryId), (Select Id, Nrich__Subject__c, Name, Nrich__Project__c, Nrich__Project__r.Name, Nrich__Sprint__c, Nrich__Assigned_To__r.Name, Nrich__Assigned_To__c, Nrich__Estimated_Effort__c   , Nrich__Actual_Effort__c, Nrich__Priority__c, Nrich__Completion_Percentage__c, Nrich__Status__c, Nrich__Assigned_To_Name__c from Nrich__Tasks__r  where Nrich__User_Story__c=NULL) FROM Nrich__Sprint__c  WHERE Nrich__Project__c=:ProjectID';
            else
                Sprint_User_Stories= 'SELECT Id, Name, Nrich__Project__c, Nrich__Status__c, Nrich__Start_Date__c, Nrich__End_Date__c, Nrich__Completion_Date__c, (Select Id, Nrich__Assigned_To_Name__c , Name, Nrich__Project__c, Nrich__Project__r.Name, Nrich__Status__c, Nrich__Sprint__c, Nrich__Assigned_Too__r.Name, Nrich__Assigned_Too__c, Nrich__Estimated_Effort__c  , Nrich__Actual_Effort__c, Nrich__Priority__c, Nrich__Subject__c, Nrich__Completion_Percentage__c FROM Nrich__User_Stories__r), (Select Id, Nrich__Subject__c, Name, Nrich__Project__c, Nrich__Project__r.Name, Nrich__Sprint__c, Nrich__Assigned_To__r.Name, Nrich__Assigned_To__c, Nrich__Estimated_Effort__c   , Nrich__Actual_Effort__c, Nrich__Priority__c, Nrich__Completion_Percentage__c, Nrich__Status__c, Nrich__Assigned_To_Name__c from Nrich__Tasks__r  where Nrich__User_Story__c=NULL) FROM Nrich__Sprint__c  WHERE Nrich__Project__c =:ProjectID'; 
            If(String.isNotBlank(SprintId)) {      
                Sprint_User_Stories = Sprint_User_Stories + ' AND ID=:SprintId';                         
            }
            If(Startdate!= Null) {    
                Sprint_User_Stories = Sprint_User_Stories + ' AND Nrich__Start_Date__c >: Startdate ';                
            }
            If(Enddate!=Null) {    
                Sprint_User_Stories = Sprint_User_Stories + ' AND Nrich__End_Date__c <: Enddate';                
            }
            Sprint_User_Stories = Sprint_User_Stories + ' Order by Nrich__Start_Date__c LIMIT 6';
            List < Sobject > backlogList = new List < Sobject >();
            String BackLog_user_Story = 'SELECT Id, Nrich__Assigned_To_Name__c, Name, Nrich__Project__c, Nrich__Sprint__c,Nrich__Status__c, Nrich__Assigned_Too__r.Name, Nrich__Assigned_Too__c, Nrich__Estimated_Effort__c  ,Nrich__Priority__c, Nrich__Subject__c, Nrich__Completion_Percentage__c  FROM Nrich__User_Story__c WHERE  Nrich__Project__c =:ProjectID AND Nrich__Sprint__c =NULL';
            List < Sobject > List_of_User_Story = Database.query(BackLog_user_Story);            
            backlogList.addAll(List_of_User_Story);
            String taskQuery = 'Select Id, Nrich__Assigned_To_Name__c, Nrich__Status__c, Nrich__Subject__c, Name, Nrich__Project__c, Nrich__Project__r.Name, Nrich__Sprint__c, Nrich__Assigned_To__r.Name, Nrich__Assigned_To__c, Nrich__Estimated_Effort__c , Nrich__Actual_Effort__c, Nrich__Priority__c, Nrich__Completion_Percentage__c from Nrich__Task__c where Nrich__Project__c =:ProjectID AND Nrich__Sprint__c =NULL AND Nrich__User_Story__c=NULL';
            List < Sobject > List_of_Task = Database.query(taskQuery);    
            backlogList.addAll(List_of_Task);
            LstOfWrapStrint.add(new SprintkanbanWrap(backlogList, Null, TRUE, ''));
            System.Debug('Sprint_User_Stories---' +Sprint_User_Stories  );
            for (Nrich__Sprint__c   sprintRec: Database.query(Sprint_User_Stories)) { 
                list<Sobject> userStoryList = sprintRec.Nrich__User_Stories__r;
                list<Sobject> taskList = sprintRec.Nrich__Tasks__r ;
                list<Sobject> objList = new list<Sobject>();
                objList.addAll(userStoryList);
                objList.addAll(taskList);
                String statusval='';
                if(string.isNotBlank(sprintRec.Nrich__Status__c) && sprintRec.Nrich__Status__c.equalsIgnoreCase(Label.In_Progress) && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__End_Date__c')){
                    if(sprintRec.Nrich__End_Date__c!=null && sprintRec.Nrich__End_Date__c<System.today()){
                        Integer days = sprintRec.Nrich__End_Date__c.daysBetween(System.today());
                        statusval =Label.In_Progress_and_Delayed_by +String.valueOf(days)+ Label.days;
                    }else
                        statusval = sprintRec.Nrich__Status__c;
                }
                else if(string.isNotBlank(sprintRec.Nrich__Status__c) && sprintRec.Nrich__Status__c.equalsIgnoreCase(Label.Completed) && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Status__c')){
                    if(sprintRec.Nrich__Completion_Date__c!=null && sprintRec.Nrich__End_Date__c!=null && sprintRec.Nrich__Completion_Date__c>sprintRec.Nrich__End_Date__c){
                        Integer days = sprintRec.Nrich__End_Date__c.daysBetween(sprintRec.Nrich__Completion_Date__c);
                        statusval = Label.Completed_with_a_Delay_of +String.valueOf(days)+Label.days;
                    }else
                        statusval = sprintRec.Nrich__Status__c;
                }else if(string.isNotBlank(sprintRec.Nrich__Status__c))
                    statusval = sprintRec.Nrich__Status__c;
                LstOfWrapStrint.add(new SprintkanbanWrap(objList, sprintRec, FALSE, statusval));
                //LstOfWrapStrint.add(new SprintkanbanWrap(sprintRec.Tickets__r, sprintRec, FALSE));
            }
            system.debug('outputlist '+LstOfWrapStrint);
            return LstOfWrapStrint;            
        }
        else
            return null;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='SprintKanBanDashboardController.getSprintUserstorydetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    @AuraEnabled
    public static StatusUpdateResult UpdateUserStory(String userStoryId, String sprintId) {
        try{
            Id objId = userStoryId;
            Schema.SObjectType objType = objId.getSobjectType();
            SObject obj = objType.newSObject();
            obj.put('Id',objId);
            if(String.isNotBlank(sprintId))
                obj.put('Nrich__Sprint__c',sprintId);
            else
                obj.put('Nrich__Sprint__c',NULL);
            Database.SaveResult result = Database.update(obj, false);
            if(!result.isSuccess()){
                StatusUpdateResult statusresult = new StatusUpdateResult(FALSE, Label.Error, result.getErrors()[0].getMessage());
                return statusresult;
            }else{
                StatusUpdateResult statusresult = new StatusUpdateResult(TRUE, Label.Success, STORY_MOVED);
                return statusresult;
            }
        }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='SprintKanBanDashboardController.UpdateUserStory', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    public class SprintkanbanWrap {
        @AuraEnabled
        List < Sobject > Userstories {
            get;
            set;
        }
        @AuraEnabled
        Nrich__Sprint__c
            Sprints {
                get;
                set;
            }
        
        @AuraEnabled
        Boolean showBacklog {
            get;
            set;
        }
        
        @AuraEnabled
        String status {
            get;
            set;
        }
        
        public SprintkanbanWrap(List < Sobject > recs, Nrich__Sprint__c pVals, Boolean bg, String status) {
            this.Userstories = recs;
            this.Sprints = pVals;
            this.showBacklog = bg;
            this.status = status;
        }
    }
    
    public class StatusUpdateResult {
        @AuraEnabled
        public Boolean isSuccess                { get; set; }
        
        @AuraEnabled
        public String title                     { get; set; }
        
        @AuraEnabled
        public String message                   { get; set; }
        
        public StatusUpdateResult( Boolean isSuccess, String title, String message ) {
            this.isSuccess  = isSuccess;
            this.title      = title;
            this.message    = message;
        }
    }
}