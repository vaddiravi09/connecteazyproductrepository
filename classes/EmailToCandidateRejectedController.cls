/********************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 30/06/2018
* Description: Email Template..
* Test Class: EmailTemplatesTest
********************************************************************/
public with sharing  class EmailToCandidateRejectedController {
	public String objType {get;set;}
    public ID relatedId {get;set;}
    
    /*******************************************************************************************
    Created by      : Pushmitha
    Overall Purpose : Fetch Interview Panel Member Details
    InputParams     : NA
    OutputParams    : List of Interview Panel Members
    *******************************************************************************************/
    public List<Nrich__Interview_Round__c> getInterviewRoundList (){
        try{
        List<Nrich__Interview_Round__c> InterviewList = new List<Nrich__Interview_Round__c>();
        if(InterviewList != null && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Job_Application__c')){    
            InterviewList = [select Id,Nrich__Job_Application__r.Nrich__Candidate__r.Name, Name from Nrich__Interview_Round__c where id = : relatedId];
        }
        return InterviewList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmailToCandidateRejectedController.getInterviewRoundList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}