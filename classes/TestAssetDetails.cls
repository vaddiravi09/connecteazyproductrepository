@isTest()
public class TestAssetDetails {
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Ashish Nag K
* Created Date: 27/12/2018
* Description: Test class covering Asset Modal.
*****************************************************************************************************/  
    
    public static string strEmployeeID;
    
    @isTest static void insertrecords()
    {
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
            //insert lleave;
            cleave.Nrich__Is_Active__c=True;
            //update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            
            //  UtilityForTest.Employee_Utility();
            
            Nrich__Asset__c assett=new Nrich__Asset__c ();
            assett.Name='Test11';
            insert assett;
            
            Nrich__Asset_Request__c   Asq =new Nrich__Asset_Request__c(Nrich__Asset_Category__c =assett.id, Nrich__Reason__c='Testingg');
            AssetDetailsController.insertnewrecords(Asq,assett.id);
            AssetDetailsController.ViewAssetRequestDetails();
            
            Nrich__Asset_Items__c assetitems=new Nrich__Asset_Items__c();
            assetitems.Name='Abc';
            assetitems.Nrich__Asset__c= assett.id;
            assetitems.Nrich__Status__c='Available';
            insert assetitems;
            // list<Asset_Items__c> Assetlist=new list<Asset_Items__c>();
            // Assetlist.add(assetitems);
            
            
            Nrich__Asset_Allocation__c AssetAllocation=new Nrich__Asset_Allocation__c();
            AssetAllocation.Nrich__Asset_Items__c=assetitems.id;
            AssetAllocation.Nrich__Allocation_EndDate__c=System.today();
            AssetAllocation.Nrich__Status__c='Pending';
            list<Nrich__Asset_Allocation__c> AllocationList=new list<Nrich__Asset_Allocation__c>();
            AllocationList.add(AssetAllocation);
            
            
            list<Nrich__Asset_Allocation__c> Assertlist=AssetDetailsController.viewAssetRecords();
            System.assertEquals('Pending',  AssetAllocation.Nrich__Status__c);
            Test.StopTest();
        }
        
        
    }
    
    @isTest static void submitApproval(){
        Test.startTest();
        /*   Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User']; 

Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 

User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
LocaleSidKey='en_US', ProfileId = p1.Id, 
TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
insert u1;

User u = new User(Alias = 'sta12356', Email='standarduser@testorg.com', 
EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
LocaleSidKey='en_US', ProfileId = p.Id, ManagerId =u1.id,
TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com1234');

//   System.runAs(u){

Company_Leave_Structure__c cleave=new Company_Leave_Structure__c(Applicable_From__c =system.today(),Applicable_Till__c=system.today(),Is_Active__c= true);
insert cleave;
Leave_Category__c lleave =new Leave_Category__c(Name='Test',Increment_Frequency__c=1,Increment_Step__c =2,
Will_Carry_Forward__c =False,
Will_Lapse__c =False,Applicable_Leave_Structure__c =cleave.id

);

insert lleave;
Employee__c emp=new Employee__c(Name='TestUser1' , EmployeeDesignation__c='Developerr',

Total_Experience__c=11,
Related_User__c=u.id );

insert emp;*/
        
        
        
        Nrich__Asset__c  Asset1=new Nrich__Asset__c ();
        Asset1.Name='Laptop';
        insert Asset1;
        
        Nrich__Asset_Request__c  Asq =new Nrich__Asset_Request__c(Nrich__Asset_Category__c =Asset1.id, Nrich__Employeee__c =UtilityForTest.Employee_Utility().Id,Nrich__Reason__c ='Testing');
        
        insert Asq;
        
        
        /*  Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
req.setComments('Hii Every one' ); 
req.setObjectId(Asq.Id); 
req.setNextApproverIds(new Id[] {Asq.Approver_Id__c});
AssetDetailsController.submitforapproval(Asq.id); 

System.assertEquals('Testing', Asq.Reason__c);
Test.stopTest();*/
        System.assertEquals('Testing', Asq.Nrich__Reason__c);
        
        
    } 
    
    
    @isTest static void  viewassetpicklist()
    {
        Test.startTest();
        
        /*  Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 
User u = new User(Alias = 'sta12356', Email='standarduser@testorg.com', 
EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
LocaleSidKey='en_US', ProfileId = p.Id, 
TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com1234');

System.runAs(u){
List <Asset__c> AssetList=new list<Asset__c>();

Company_Leave_Structure__c cleave=new Company_Leave_Structure__c(Applicable_From__c =system.today(),Applicable_Till__c=system.today(),Is_Active__c= true);
insert cleave;
Leave_Category__c lleave =new Leave_Category__c(Name='Test',Increment_Frequency__c=1,Increment_Step__c =2,
Will_Carry_Forward__c =False,
Will_Lapse__c =False,Applicable_Leave_Structure__c =cleave.id

);

insert lleave;
Employee__c emp=new Employee__c(Name='TestUser' , EmployeeDesignation__c='Developer',

Total_Experience__c=10,
Related_User__c=u.id );

insert emp;*/
        UtilityForTest.Employee_Utility();
        List <Nrich__Asset__c> AssetList=new list<Nrich__Asset__c>();
        
        Nrich__Asset__c  Asset1=new Nrich__Asset__c ();
        Asset1.Name='Laptop';
        //Asset1.EmployeeDesignation__c=emp.EmployeeDesignation__c;
        
        AssetList.add(Asset1);
        insert AssetList;
        
        List<Nrich__Asset__c>AssetList1=AssetDetailsController.fetchoptions();
        
        System.assertEquals('Laptop', Asset1.Name);
        
        Test.stopTest();
        
        
        // }
        
    }
    
    
}