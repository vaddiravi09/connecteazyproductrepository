@isTest
public class Test_EmailtoTravelReqController {
    
    
    @isTest public static void savetravel(){
       /* Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
        //System.runAs(u) {
            
            // The following code runs as user 'u' 
            
            
            Company_Leave_Structure__c cleave=new Company_Leave_Structure__c(Applicable_From__c =system.today(),Applicable_Till__c=system.today(),Is_Active__c= true);
            insert cleave;
            Leave_Category__c lleave =new Leave_Category__c(Name='Test',Increment_Frequency__c=1,Increment_Step__c =2,
                                                            Will_Carry_Forward__c =False,
                                                            Will_Lapse__c =False,Applicable_Leave_Structure__c =cleave.id
                                                            
                                                           );
            
            insert lleave;
            Employee__c emp=new Employee__c(Name='TestUser' , EmployeeDesignation__c='Developer',
                                            
                                            Total_Experience__c=10,
                                            Related_User__c=u.id );
            
            insert emp;*/
        
            
            Nrich__Travel_Request__c  TRequest=new Nrich__Travel_Request__c ();
            TRequest.Nrich__Employeee__c=UtilityForTest.Employee_Utility().Id;
            TRequest.Nrich__Status__c='In Draft';
            TRequest.Nrich__Start_Date__c =System.today();
            TRequest.Nrich__End_Date__c=System.today()+1;
            TRequest.Nrich__Associated_Clients__c='testing';
            TRequest.Nrich__Visiting_Addresses__c='Otp';
            TRequest.Nrich__Source_Location__c='Test';
            TRequest.Nrich__Destination_Location__c='Test1';
            insert TRequest;
            
            
            list<Nrich__Travel_Request__c> TravelList=new list<Nrich__Travel_Request__c>();
            TravelList.add(TRequest);
            
            EmailtoTravelReqController EmailTRV=new EmailtoTravelReqController();
            EmailTRV.objType='Nrich__Travel_Request__c';
            EmailTRV.TravelRequestAppList=TravelList;
            EmailTRV.getTravelList();
            System.assertEquals('Test', TRequest.Nrich__Source_Location__c);
            
        //}
    }
    
    
}