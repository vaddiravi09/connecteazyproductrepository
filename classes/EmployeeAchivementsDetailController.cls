public class EmployeeAchivementsDetailController {
  
	/****************************************************************************************************
	* Company: Absyz
	* Developer: Raviteja
	* Created Date: 18/10/2018
	* Description: Fetching Achievements Categories
	*****************************************************************************************************/   
    @AuraEnabled
    public static list<categorywrapper> fetchAchievementCategory(){
        try{
        List<Schema.PicklistEntry> picklistval = Nrich__Achievements__c.Nrich__Category__c.getDescribe().getPicklistValues();
        list<categorywrapper> picklistvalList = new list<categorywrapper>();
        Integer count = 0;
        for(Schema.PicklistEntry f : picklistval)
	    {
	      categorywrapper ct = new categorywrapper();
	      ct.categoryname = f.getValue();
	      ct.categoryId = 'cat'+count;
	      picklistvalList.add(ct);
	      count++;
	    } 
	    if(picklistvalList.size()>0)
	       return picklistvalList;
	    else
	       return null;  
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeAchivementsDetailController.fetchAchievementCategory', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return Null;
      }
        
    }

    public class categorywrapper{
    	@AuraEnabled
    	public string categoryname {get; set;}

    	 @AuraEnabled
    	 public string categoryId {get; set;}
    }
}