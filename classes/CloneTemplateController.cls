/****************************************************************************
* Company: Absyz
* Developer: Hrishikesh
* Created Date: 19/07/2018
* Description: Apex Controller for Template Clone page  
* Test Class: cloneTemplateControllerTest
****************************************************************************/
public with sharing class CloneTemplateController {
    //*******************************************************************************************
    //Created by      : Hrishikesh
    //Method Name     : fetchtemplatedetails
    //Overall Purpose : This method is used for fetching the Template record
    //InputParams     : templateId
    //OutputParams    : Template Record
    //*******************************************************************************************  
    @AuraEnabled
    public static Nrich__Template__c fetchTemplateDetails(String templateId) {
        try{
        Nrich__Template__c template= new Nrich__Template__c();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Background_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job_Round_Name__c') && 
          ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Is_Active__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job_Round__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Type_of_Template__c')
          )
       template = [Select Id, Nrich__Background_Color__c, Nrich__Job_Name__c, Nrich__Job_Round_Name__c, Nrich__Is_Active__c, Nrich__Job__c, Nrich__Job_Round__c, Name, Nrich__Type_of_Template__c from Nrich__Template__c where Id=:templateId ];
        return template;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CloneTemplateController.fetchTemplateDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }

    //***********************************************************************************************************
    //Created by      : Hrishikesh
    //Method Name     : cloneTemplate
    //Overall Purpose : This method is used for Clone the Template record with its Secction and Section Fields
    //InputParams     : templateId,templateName,jobId,jobroundId,Active
    //OutputParams    : newly created TemplateId 
    //***********************************************************************************************************  
    
    @AuraEnabled
    public static Id cloneTemplate(String templateId,String templateName,Id jobId,Id jobRoundId,Boolean isActive) {
        try{
        List<Nrich__Template__c> templist = new List<Nrich__Template__c>{};
        List<Nrich__Section__c> secList = new List<Nrich__Section__c>{};
        List<Nrich__Section_Fields__c> secFieldList = new List<Nrich__Section_Fields__c>{};
            Nrich__Template__c template= new Nrich__Template__c();
        map<String,List<Nrich__Section_Fields__c>> mapSectionWithsectionFields = new map<String,List<Nrich__Section_Fields__c>>();
          if(ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Background_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job_Round_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Mapping_Object_API_Name__c')
         && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Is_Active__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job_Round__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Type_of_Template__c'))
          
         template = [Select Id, Nrich__Background_Color__c,Nrich__Background_Image__c, Nrich__Job_Name__c, Nrich__Job_Round_Name__c, Nrich__Is_Active__c, Nrich__Job__c, Nrich__Job_Round__c, Name,Nrich__Mapping_Object_API_Name__c, Nrich__Type_of_Template__c from Nrich__Template__c where Id=:templateId ];
        
        Nrich__Template__c tempNew = template.clone(false, true); //See sObject Clone Method
        tempNew.Nrich__Job__c = jobId;
        tempNew.Nrich__Is_Active__c = isActive;
        tempNew.Name = templateName;
        if(jobRoundId!=NULL){
            tempNew.Nrich__Job_Round__c = jobRoundId;    
        }
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Template__c.getSObjectType().getDescribe();
        if(objectDescriptioncontent.iscreateable())
         database.insert(tempNew);
        system.debug(tempNew);
        
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Section__c', 'Nrich__Order__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section__c', 'Nrich__Template__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Coloumn__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Object_API_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Mapping_Field_API_Name__c')
          && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Field_Length__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Field_Type__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Mandatory__c')
  && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Order__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Regex__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Section__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Validation_Type__c')
           )
        for(Nrich__Section__c sec : [Select Id, Name, Nrich__Order__c, (Select Id, Nrich__Coloumn__c, Nrich__Object_API_Name__c, Nrich__Mapping_Field_API_Name__c, Nrich__Field_Length__c, Nrich__Field_Type__c, Nrich__Mandatory__c, Nrich__Order__c, Nrich__Regex__c, Nrich__Section__c, Name, Nrich__Validation_Type__c from Nrich__Section_Fields__r) from Nrich__Section__c where Nrich__Template__c = :templateId]){
            Nrich__Section__c newSec = sec.clone(false, true);
            newSec.Nrich__Template__c = tempNew.Id; //set parent ref
            secList.add(newSec);
            
            mapSectionWithsectionFields.put(sec.Name+String.valueOf(sec.Nrich__Order__c),sec.Nrich__Section_Fields__r);
        }
        Schema.DescribeSObjectResult SectionDescriptioncontent = Nrich__Section__c.getSObjectType().getDescribe();
        
        if(SectionDescriptioncontent.isCreateable())
        database.insert(secList);
           
        for(Nrich__Section__c section : secList){
            if(mapSectionWithsectionFields.containsKey(section.Name+String.valueOf(section.Nrich__Order__c))){
                List<Nrich__Section_Fields__c> sfList = mapSectionWithsectionFields.get(section.Name+String.valueOf(section.Nrich__Order__c));
                for(Nrich__Section_Fields__c sf : sfList){
                    if(sf!=NULL){
                        Nrich__Section_Fields__c newsf = sf.clone(false, true);
                        newsf.Nrich__Section__c = section.Id; //set parent ref
                        secFieldList.add(newsf);
                    }
                }
            }
        }
        Schema.DescribeSObjectResult SectionFieldsDescriptioncontent = Nrich__Section_Fields__c.getSObjectType().getDescribe();
        if(SectionFieldsDescriptioncontent.isCreateable())
        database.insert(secFieldList) ; 
        return tempNew.Id;
    
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CloneTemplateController.cloneTemplate', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}