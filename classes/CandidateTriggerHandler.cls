/*************************************
// Class Name : CandidateTriggerHandler 
// Purpose : When the status field in Candidate object is updated to 'On-Boarding Completed' 
Create a new Employee by fetching the details from Candidate
// Testclass Name : cantidateToEmployeeCreationHandlerTest
************************************/
public with sharing class CandidateTriggerHandler {
    
    /*************************************
// Method Name : insertNewEmployee
// Purpose : When the status field in Job Application object is updated to 'On-Boarding Completed' 
Create a new Employee by fetching the details from Job Application and Candidate
************************************/
    public static void insertNewEmployee(set<Id> candidateIdSet){
        try{
        map<String, String> candidateEmployeeMap = new map<String, String>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Candidate_to_Employee__mdt', 'Nrich__Candidate_Field__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Candidate_to_Employee__mdt', 'Nrich__Employee_Field__c'))
            for(Nrich__Candidate_to_Employee__mdt candidateEmployee: [select Nrich__Candidate_Field__c,Nrich__Employee_Field__c from Nrich__Candidate_to_Employee__mdt where id!=NULL ]){
                candidateEmployeeMap.put(candidateEmployee.Nrich__Candidate_Field__c, candidateEmployee.Nrich__Employee_Field__c);
            }
        if(candidateEmployeeMap!=NULL && candidateEmployeeMap.size()>0){ 
            string candQuery = 'Select Id';
            for(String candFlds: candidateEmployeeMap.keySet()){
                candQuery = candQuery+', '+candFlds;
            }
            candQuery = candQuery+' FROM Nrich__Candidate__c where Id IN: candidateIdSet';    
            list<Nrich__Employee__c> employeeList = new list<Nrich__Employee__c>();
            for(Nrich__Candidate__c candidate: database.query(candQuery)){
                Nrich__Employee__c emp = new Nrich__Employee__c();
                for(String candFlds: candidateEmployeeMap.keySet()){
                    if(candidate.get((candFlds))!=NULL && candidateEmployeeMap.containsKey(candFlds))
                        emp.put(candidateEmployeeMap.get(candFlds), candidate.get(candFlds));
                }
                employeeList.add(emp);
            }           
            if(employeeList.size()>0)
                database.insert(employeeList, false);
        }
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CandidateTriggerHandler.insertNewEmployee', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}