/****************************************************************************************************
* Company:Absyz
* Developer:Ashish
* Created Date:29/10/18
* Description:Fetching the Profile Details 
*****************************************************************************************************/
public class ProjectTableDisplayModalController {
    
    @AuraEnabled
    public static wrapperclass ViewProjectDetails(){
        try{
        wrapperclass wrap=new wrapperclass();
        string empUserId = userinfo.getUserId();
        list<Nrich__Project_Team_Member__c > ProjectDetails=new list<Nrich__Project_Team_Member__c >();
        list<Nrich__Project_Team_Member__c>PastProjectDetails=new list<Nrich__Project_Team_Member__c>();
        List<Nrich__Project_Team_Member__c>OwnProjectDetails=new list<Nrich__Project_Team_Member__c>();
        
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Employee_Start_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Employee_End_Date__c') &&
           ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__ProjectManagerName__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Project_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Active__c')
          )
            ProjectDetails=[select Name, Nrich__Employee_Start_Date__c, Nrich__Employee_End_Date__c, Nrich__ProjectManagerName__c, Nrich__Project_Name__c  from Nrich__Project_Team_Member__c where Nrich__Active__c=TRUE AND Nrich__Employeee__r.Nrich__Related_User__c=:empUserId];
        
        PastProjectDetails=[select Name, Nrich__Employee_Start_Date__c, Nrich__Employee_End_Date__c, Nrich__ProjectManagerName__c, Nrich__Project_Name__c   from Nrich__Project_Team_Member__c where Nrich__Active__c=FALSE AND Nrich__Employeee__r.Nrich__Related_User__c=:empUserId];    
        
        OwnProjectDetails=[select name, Nrich__Employee_Start_Date__c, Nrich__Employee_End_Date__c, Nrich__ProjectManagerName__c, Nrich__Project_Name__c    from Nrich__Project_Team_Member__c where Nrich__Active__c=TRUE AND Nrich__Project__r.Nrich__ProjectManagerr__r.Nrich__Related_User__c=:empUserId];
        
        
        System.debug('Current Prjdetails' +ProjectDetails);
        
        
        wrap.CurrentProjectList=ProjectDetails;
        wrap.PastProjectList=PastProjectDetails;
        wrap.OwnProjectList=OwnProjectDetails;
        
        
        if( wrap!=NULL )
            return wrap;
        else
            return null;
        
        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ProjectTableDisplayModalController.ViewProjectDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    public class wrapperclass{
        @AuraEnabled
        public list<Project_Team_Member__c > CurrentProjectList;
        @AuraEnabled
        public list<Project_Team_Member__c > PastProjectList;
        @AuraEnabled
        public list<Project_Team_Member__c > OwnProjectList;
        
        
    }
    
    
    
}