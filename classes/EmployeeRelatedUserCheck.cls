/****************************************************************************************************
    * Company: Absyz
    * Developer: Sai Kesa
    * Created Date: 26/10/
    * Description: employee and it's RelatedUser duplicate check
    *****************************************************************************************************/   
public class EmployeeRelatedUserCheck{
    public static void relateduser(Map<Nrich__Employee__c, String> newmap) {
        try{
        if(newmap!=NULL && newmap.size()>0){
            list<String> Relatedusers= newmap.values();
            map<String, Integer> countMap = new map<String, Integer>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Related_User__c'))
            for(Nrich__Employee__c objCS : [select Id, Nrich__Related_User__c from Nrich__Employee__c where Nrich__Related_User__c IN: Relatedusers ]){      
                if(objCS.Nrich__Related_User__c!=NULL && !newmap.containsKey(objCS)){
                    if(countMap.containsKey(objCS.Nrich__Related_User__c)){
                        integer countval = countMap.get(objCS.Nrich__Related_User__c);
                        countMap.put(objCS.Nrich__Related_User__c, countval+1);
                    }
                    else
                       countMap.put(objCS.Nrich__Related_User__c, 1); 
                }
            }
            system.debug('emp '+countMap);
            for(Nrich__Employee__c emp: newmap.keySet()){
                if(countMap.size()>0 && countMap.containskey(emp.Nrich__Related_User__c) && countMap.get(emp.Nrich__Related_User__c)>0)
                    emp.addError(Label.RelatedUserError);
            }
        }      
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeRelatedUserCheck.relateduser', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}