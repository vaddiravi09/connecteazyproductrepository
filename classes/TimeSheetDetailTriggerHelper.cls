/********************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 22/05/2019
* Description: Updating Project forecast actual hours on approval of timesheet
********************************************************************/
public class TimeSheetDetailTriggerHelper {
    public static void createProjectForecasts(list<Nrich__Timesheet_Detail__c> timesheetDetailList){
      try{
        map<String, Decimal> hoursmap = new map<String, Decimal>();
        
        if (ObjectFieldAccessCheck.checkAccessible('Nrich__Timesheet_Detail__c', 'Nrich__EmployeeId__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Timesheet_Detail__c', 'Nrich__Project__c') &&
            ObjectFieldAccessCheck.checkAccessible('Nrich__Timesheet_Detail__c', 'Nrich__Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Timesheet_Detail__c', 'Nrich__No_of_hours__c')
           ){
               for(Nrich__Timesheet_Detail__c td: timesheetDetailList){      
                   string empid = td.Nrich__EmployeeId__c;
                   empid = empid.left(15);
                   string projid = td.Nrich__Project__c;
                   projid = projid.left(15);
                   if(hoursmap.size()>0 && hoursmap.containsKey(empid+';'+projid+';'+td.Nrich__Date__c.month()+';'+td.Nrich__Date__c.year())){
                       decimal hr = hoursmap.get(empid+';'+projid+';'+td.Nrich__Date__c.month()+';'+td.Nrich__Date__c.year());
                       hoursmap.put(empid+';'+projid+';'+td.Nrich__Date__c.month()+';'+td.Nrich__Date__c.year(), hr+td.Nrich__No_of_hours__c);
                   }else
                       hoursmap.put(empid+';'+projid+';'+td.Nrich__Date__c.month()+';'+td.Nrich__Date__c.year(), td.Nrich__No_of_hours__c);
               }
           }
        system.debug('map '+hoursmap);
        if(hoursmap.size()>0 && ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Forecast__c', 'Nrich__MonthYear__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Forecast__c', 'Nrich__Actual__c'))
        {
            list<Nrich__Project_Team_Forecast__c> forecastUpdateList = new list<Nrich__Project_Team_Forecast__c>();
            for(Nrich__Project_Team_Forecast__c projforecast: [Select Id, Nrich__MonthYear__c, Nrich__Actual__c from Nrich__Project_Team_Forecast__c  where Nrich__MonthYear__c IN: hoursmap.keySet()  ]){
                if(hoursmap.containskey(projforecast.Nrich__MonthYear__c)){
                    projforecast.Nrich__Actual__c = projforecast.Nrich__Actual__c+hoursmap.get(projforecast.Nrich__MonthYear__c);    
                    forecastUpdateList.add(projforecast);
                }
            }
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Project_Team_Forecast__c.getSObjectType().getDescribe();
            if(forecastUpdateList.size()>0 && objectDescriptioncontent.isUpdateable())
                database.update(forecastUpdateList, false);
        }
      }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TimeSheetDetailTriggerHelper.createProjectForecasts', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
  }
}