@istest
public class TestEmployeeLeaveCardDetailController {
    
    @istest static void fetchingemployeeleavecard(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 
        User u = new User(Alias = 'sta12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com1234');
        
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
          //  insert lleave;
            cleave.Nrich__Is_Active__c=True;
          //  update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            
            
            
            
            Nrich__Employee_Leave_Category__c emleavecategory = new Nrich__Employee_Leave_Category__c();
            emleavecategory.Nrich__Leave_Card__c=empleavecard.id;
            
            insert emleavecategory;
            
            
            
            list<Nrich__Leave_Request__c>lvlist=new list<Nrich__Leave_Request__c>();
            Nrich__Leave_Request__c lv1= new Nrich__Leave_Request__c();
            lv1.Nrich__Status__c='Applied';
            lv1.Nrich__Status__c='Pending';
            lv1.Nrich__Status__c='Approved';
            lv1.Nrich__Status__c='Rejected';
            
            lvlist.add(lv1);
            
            Nrich__Leave_Request__c lv=new Nrich__Leave_Request__c();
            lv.Nrich__Start_Date__c=System.today();
            lv.Nrich__End_Date__c=System.today()+1;
            lv.Nrich__Comments__c='Test';
            lv.Nrich__Employee_Leave_Category__c=emleavecategory.id;
            lv.Nrich__Requested_By__c=emp.id;
            lv.Nrich__Status__c='Applied';
            insert lv;
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Hii Every one' ); 
            req.setObjectId(lv.Id); 
            
            EmployeeLeaveCardDetailController.fetchEmployeeLeaveCard();
            EmployeeLeaveCardDetailController.fetchEmployeeLeaveRequests('Availed');
            EmployeeLeaveCardDetailController.submitLeaveRequest(lv.Id);
            EmployeeLeaveCardDetailController.saveEmployeeLeaveRequest(lv, emp.id);
            System.assertEquals('Applied', lv.Nrich__Status__c);
            
        }
    }
    
    
    @isTest static void frTrigger(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 
        User u = new User(Alias = 'sta12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com1234');
        
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
            //insert lleave;
            cleave.Nrich__Is_Active__c=True;
           // update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            Nrich__Employee_Leave_Category__c emleavecategory = new Nrich__Employee_Leave_Category__c();
            emleavecategory.Nrich__Leave_Card__c=empleavecard.id;
            
            insert emleavecategory;
            
            
            list<Nrich__Leave_Request__c>lvlist=new list<Nrich__Leave_Request__c>();
            Nrich__Leave_Request__c lv1= new Nrich__Leave_Request__c();
            
           
            lv1.Nrich__Start_Date__c=System.today();
            lv1.Nrich__End_Date__c=System.today()+1;
            lv1.Nrich__Comments__c='Test';
            lv1.Nrich__Employee_Leave_Category__c=emleavecategory.id;
            lv1.Nrich__Requested_By__c=emp.id;
            insert lv1;
            lv1.Nrich__Status__c='Rejected';
            /*update lv1;
            
             lv1.Nrich__Status__c='Approved';
            update lv1;*/
            System.assertEquals('Rejected', 'Rejected');
            
        }
        
        
    }
    
    
}