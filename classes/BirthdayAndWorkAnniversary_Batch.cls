global class BirthdayAndWorkAnniversary_Batch implements Database.Batchable < sObject >{
    
    
    date TodaysDate=date.today();
    // string TodaysMonth=System.today().month();
    string query='select id,Name,Nrich__Date_of_Birth__c , Nrich__Date_of_Joining__c,Nrich__ProfilePic__c ,Nrich__Active__c, Nrich__Primary_Email__c,Nrich__Related_User__c,Nrich__BirthdayTechnical__c from Nrich__Employee__c where Nrich__Active__c=TRUE AND Nrich__Date_of_Birth__c=:TodaysDate AND Nrich__Primary_Email__c!=null ';
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC,list<Nrich__Employee__c> EmployeeList){
        try{
            list<Nrich__Employee__c>EmployeeBirthdayList= new list<Nrich__Employee__c>();
            for(Nrich__Employee__c Emp:EmployeeList){
                Emp.Nrich__BirthdayTechnical__c=True;
                EmployeeBirthdayList.add(Emp);
            }
            System.debug('EmployeeBirthdayList' +EmployeeBirthdayList);
            
            list<Nrich__Employee__c>EmpList=[select id,Name,Nrich__AnniversaryTechinical__c,Nrich__Date_of_Birth__c , Nrich__Date_of_Joining__c,Nrich__ProfilePic__c,Nrich__Primary_Email__c,Nrich__Related_User__c from Nrich__Employee__c where Nrich__Active__c=TRUE AND Nrich__Primary_Email__c!=null];
            list<Nrich__Employee__c>EmpAnniversaryList=new list<Nrich__Employee__c>();
            for(Nrich__Employee__c em:EmpList){
                Integer Experience= TodaysDate.daysBetween(em.Nrich__Date_of_Joining__c);
                Integer Remainder=math.mod(Experience,365);
                if(Remainder==0){
                    em.Nrich__AnniversaryTechinical__c=True;
                }
                EmpAnniversaryList.add(em);
               
                
                
            }
             System.debug('EmpAnniversaryList'+EmpAnniversaryList);
            System.debug('EmployeeBirthdayList' +EmployeeBirthdayList);
            Database.update(EmployeeBirthdayList);
            Database.update(EmpAnniversaryList);
            
            
        }
        catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='BirthdayAndWorkAnniversary_Batch', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
        }
    }
    
    public void finish(Database.BatchableContext BC) {}
    
    
    
    
}