@isTest
public class TestMentortile {
    
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Ashish Nag K
* Created Date: 30/10/2018
* Description: Test class covering Mentor Modal.
*****************************************************************************************************/  
    
    
    
    @istest  static void fetchdetails(){
        MentortileController.EmployeeWrapper objWrap = new MentortileController.EmployeeWrapper();
        list<Nrich__Employee__c> emplist=new list<Nrich__Employee__c>();
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
        //    insert lleave;
            cleave.Nrich__Is_Active__c=True;
        //    update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            
            objWrap.objEmployeeDetails=emp;
            
            /* BasEnrich__Employee__c emplist1=[select id,BasEnrich__Employee_Id__c , name, EmployeeDesignation__c, Mentor__c ,
Experience_in_Organization__c ,Total_Experience__c ,BasEnrich__Date_of_Joining__c from BasEnrich__Employee__c];*/
            
            Nrich__Employee__c mentee =new Nrich__Employee__c();
            mentee.Name='User';
            mentee.Nrich__EmployeeDesignation__c ='Developer';
            mentee.Nrich__Experience_in_Organization__c=10;
            mentee.Nrich__Total_Experience__c=12;
            mentee.Nrich__Mentor__c=emp.Id;
            //insert mentee;
            list<Nrich__Employee__c> menteelist=new list<Nrich__Employee__c>();
            menteelist.add(mentee);
            insert menteelist;
            objWrap.lstMentees=menteelist;
            
            MentortileController.EmployeeWrapper emp2=new MentortileController.EmployeeWrapper();
            
            emp2 =MentortileController.fetchEmployeeDetails();
            //system.debug('');
            System.assertEquals( emp2,emp2);
            
            Test.stopTest();
            
        }
        
        
    }
    
}