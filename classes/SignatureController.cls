/****************************************************************************************************
* Company: Absyz
* Developer: Seshu Varma
* Created Date: 22/10/2018
* Description: Class for uploading signature
*****************************************************************************************************/
public with sharing class SignatureController {
    @AuraEnabled
    public static String uploadSignature(String name,String demoReportId, String b64SignData){
        try {
            ContentVersion cvToInsert = new ContentVersion();
            cvToInsert.versionData = EncodingUtil.base64Decode(b64SignData);
            //String fileName = 'Sig_'+UserInfo.getFirstName()+'_'+UserInfo.getLastName()+'_'+String.valueOf(Date.today()).substring(0,10);
            String fileName = 'Sig_'+name+'_'+String.valueOf(Date.today()).substring(0,10);
            cvToInsert.title = fileName;
            cvToInsert.pathOnClient = '/'+fileName+'.png';
            if( ((Schema.sObjectType.ContentVersion.isCreateable())) ){
                insert cvToInsert;
            }            
            system.debug('@@@contentversion record before select: ' + cvToInsert);
            List<ContentDocumentLink> cd = new list<ContentDocumentLink>();
            List<ContentDocument> cdList = new List<ContentDocument>();     
            ContentDocument conDoc = [SELECT Id,parentId from ContentDocument WHERE LatestPublishedVersionId =: cvToInsert.id limit 1];
            system.debug('ContentDocument :'+conDoc);
            
            // Insert Content Document Link
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = conDoc.Id;
            cdl.LinkedEntityId = demoReportId;
            cdl.ShareType = 'V';
            cdl.Visibility = 'AllUsers';
            system.debug('@@@contentdocumentlink record: ' + cdl);
            system.debug('@@@contentversion record after select: ' + cvToInsert);
            insert cdl;
            //upsert cdList;
            
            return '';
        } catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='SignatureController.uploadSignature', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
}