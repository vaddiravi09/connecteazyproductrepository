/********************************************************************
* Company: Absyz
* Developer: Vivek
* Created Date: 22/06/2018
* Description: Accepting timeslots from the Interviewer.
* Test Class: PanelMemberTimeSlotsControllerTest
********************************************************************/
public with sharing class PanelMemberTimeSlotsController {
    public string currentRecordId;  // This variable is used to get employee id from url.
    public date weekstart {get;set;}
    public list<weekdays> weekdayslist {get;set;}
    public map<date, list<dateTimeslotWrapper>> datetimeslotMap {get; set;}
    public map<Date, Nrich__Preffered__c> prefferedMap;
    public string seleted_Date{get;set;}		// Added by Vivek
    public date selectedDate{get;set;}			// Added by Vivek
    public boolean showAm{get;set;}
    public PanelMemberTimeSlotsController(){
        currentRecordId = Apexpages.currentPage().getParameters().get('id');        
        weekdayslist = new list<weekdays>();
        weekstart=date.today().toStartofWeek();
        weekstart=weekstart.addDays(1);
        dayscalculation(weekstart);
        selectedDate=weekstart;
        showAm=true;        
    }
    
    public void dayscalculation(date startdate){
        try{
        datetimeslotMap = new map<date, list<dateTimeslotWrapper>>();
        date endDate = startdate.addDays(7);
        prefferedMap = new map<Date, Nrich__Preffered__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Preffered__c', 'Nrich__Start_Date_Time__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Preffered__c', 'Nrich__Available_Slots__c')){
            for(Nrich__Preffered__c preffered: [select id,Nrich__Start_Date_Time__c,Nrich__Available_Slots__c from Nrich__Preffered__c where Nrich__Employee__c=:currentRecordId and Nrich__Start_Date_Time__c>=:startdate and Nrich__Start_Date_Time__c<=:endDate]){
                if(preffered.Nrich__Start_Date_Time__c!=NULL && String.isNotBlank(preffered.Nrich__Available_Slots__c))
                    prefferedMap.put(preffered.Nrich__Start_Date_Time__c, preffered);
            }
        }
        weekdayslist.clear(); 		// To show only 7 days of the week.
        for(Integer j=0; j<7; j++){
            date datevalue = startdate.addDays(j);
            weekdays week = new weekdays();
            week.daterec=datevalue;
            datetime dtt= (DateTime)week.daterec;
            week.day=dtt.format('E');
            weekdayslist.add(week);
            list<dateTimeslotWrapper> dateTimeslotWrapperList = new list<dateTimeslotWrapper>();
            map<String, String> existingMap = new map<String, String>();
            map<String,dateTimeslotWrapper> existing_instance_map=new map<String,dateTimeslotWrapper>();
            if(prefferedMap!=NULL && prefferedMap.size()>0 && prefferedMap.containsKey(datevalue)){
                String timeslotval = prefferedMap.get(datevalue).Nrich__Available_Slots__c;
                if(string.isNotBlank(timeslotval)){
                    list<String> timelist = timeslotval.split(';');
                    for(String times: timelist){
                        dateTimeslotWrapper datetimeval = new dateTimeslotWrapper();
                        datetimeval.dateval = datevalue;
                        datetimeval.timeslot = times;
                        if(times.contains(' AM'))
                            datetimeval.timeslottype=' AM';
                        else if(times.contains(' PM')){
                            datetimeval.timeslottype=' PM';
                        }
                        datetimeval.isAvailable = true;
                        datetimeval.timeslotId = prefferedMap.get(datevalue).id;
                        //dateTimeslotWrapperList.add(datetimeval); 		// Removed Now
                        existingMap.put(times, times);
                        existing_instance_map.put(times,datetimeval);		// Added newly.
                    }
                }
            }
            for (integer i = 12; i < 36; i++) {
                integer num;
                String timeValue = '';
                String ampm='';
                if(i>23){
                    num = i -  24;
                }else{
                    num = i;
                }
                if(num == 0){
                    timeValue = String.valueOf(num+12);
                }else if(num <= 11){
                    timeValue = String.valueOf(num+12);
                }else{
                    timeValue = String.valueOf(num);
                }
                
                if(num > 11 && num < 24){
                    if(num==12 && i==12){
                        num=0;
                        timeValue = String.valueOf(num); 
                    }
                    if(num == 12 && i==24){
                        timeValue = String.valueOf(num);   
                    }else if(num > 12){
                        timeValue = String.valueOf(num-12);   
                    }
                    ampm = ' AM';
                }else{
                    ampm = ' PM';             
                }
                if(timeValue.length()==1){
                    timeValue='0'+timeValue;
                }
                
                
                dateTimeslotWrapper datetimeval = new dateTimeslotWrapper();
                datetimeval.dateval = datevalue;
                datetimeval.timeslot = timeValue+':00'+ampm;
                datetimeval.isAvailable = false;
                datetimeval.timeslottype = ampm;
                if(existingMap!=NULL && existingMap.size()>0){
                    if(!existingMap.containsKey(timeValue+':00'+ampm))
                        dateTimeslotWrapperList.add(datetimeval);
                    else{	// Newly added
                        dateTimeslotWrapperList.add(existing_instance_map.get(timeValue+':00'+ampm)); 
                    }
                    
                }else{
                    dateTimeslotWrapperList.add(datetimeval);
                }
                
                dateTimeslotWrapper datetimeval2 = new dateTimeslotWrapper();
                datetimeval2.dateval = datevalue;
                datetimeval2.timeslot = timeValue+':30'+ampm;
                datetimeval2.isAvailable = false;
                datetimeval2.timeslottype = ampm;
                if(existingMap!=NULL && existingMap.size()>0){
                    if(!existingMap.containsKey(timeValue+':30'+ampm))
                        dateTimeslotWrapperList.add(datetimeval2);
                    else{	//added newly
                        dateTimeslotWrapperList.add(existing_instance_map.get(timeValue+':30'+ampm));                            
                    }
                }else{
                    dateTimeslotWrapperList.add(datetimeval2);
                }
            }
            set<dateTimeslotWrapper> dateTimeslotWrapperset = new set<dateTimeslotWrapper>();
            dateTimeslotWrapperset.addall(dateTimeslotWrapperList);
            datetimeslotMap.put(datevalue, dateTimeslotWrapperList);
            
        } 
        system.debug('mapp'+datetimeslotMap);
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='PanelMemberTimeSlotsController.dayscalculation', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    public void prev(){
        weekstart=weekstart.addDays(-7);
        dayscalculation(weekstart);
        selectedDate=date.newInstance(weekstart.year(), weekstart.month(), weekstart.day());
    } 
    public void next(){
        weekstart=weekstart.addDays(7);
        dayscalculation(weekstart);
        selectedDate=date.newInstance(weekstart.year(), weekstart.month(), weekstart.day()); 
    }
    
    public void save(){
        try{
        list<Nrich__Preffered__c> PrefferedUpsertList = new list<Nrich__Preffered__c>();
        list<dateTimeslotWrapper> dateTimeslotWrappernewlist = new list<dateTimeslotWrapper>();
        
        for(weekdays week: weekdayslist){
            if(datetimeslotMap!=NULL && datetimeslotMap.size()>0 && datetimeslotMap.containsKey(week.daterec))
                dateTimeslotWrappernewlist.addAll(datetimeslotMap.get(week.daterec));
        }
        system.debug('dateTimeslotWrappernewlist----- '+dateTimeslotWrappernewlist);
        map<Date, Nrich__Preffered__c> PrefferednewMap = new map<Date, Nrich__Preffered__c>();
        set<Date> availabledates = new set<Date>();
        
        for(dateTimeslotWrapper datetimeval: dateTimeslotWrappernewlist){
            if(datetimeval.isAvailable){
                if(PrefferednewMap!=NULL && PrefferednewMap.size()>0 && PrefferednewMap.containsKey(datetimeval.dateval)){
                    Nrich__Preffered__c Preffered = PrefferednewMap.get(datetimeval.dateval);
                    Preffered.Nrich__Available_Slots__c = Preffered.Nrich__Available_Slots__c+';'+datetimeval.timeslot;
                    PrefferednewMap.put(datetimeval.dateval, Preffered);
                }
                else{
                    Nrich__Preffered__c Preffered = new Nrich__Preffered__c();
                    if(String.isNotBlank(datetimeval.timeslotId))
                        Preffered.Id = datetimeval.timeslotId;
                    else if(prefferedMap.size()>0 && prefferedMap.containsKey(datetimeval.dateval))
                        Preffered.Id = prefferedMap.get(datetimeval.dateval).Id;
                    if(ObjectFieldAccessCheck.checkUpdateAccess('Nrich__Preffered__c', 'Nrich__Available_Slots__c') && ObjectFieldAccessCheck.checkCreateAccess('Nrich__Preffered__c', 'Nrich__Available_Slots__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Preffered__c', 'Nrich__Available_Slots__c'))
                        Preffered.Nrich__Available_Slots__c = datetimeval.timeslot;
                    if(ObjectFieldAccessCheck.checkUpdateAccess('Nrich__Preffered__c', 'Nrich__Employee__c') && ObjectFieldAccessCheck.checkCreateAccess('Nrich__Preffered__c', 'Nrich__Employee__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Preffered__c', 'Nrich__Employee__c'))
                        Preffered.Nrich__Employee__c = currentRecordId;
                    if(ObjectFieldAccessCheck.checkUpdateAccess('Nrich__Preffered__c', 'Nrich__Start_Date_Time__c') && ObjectFieldAccessCheck.checkCreateAccess('Nrich__Preffered__c', 'Nrich__Start_Date_Time__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Preffered__c', 'Nrich__Start_Date_Time__c'))
                        Preffered.Nrich__Start_Date_Time__c = datetimeval.dateval;
                    if(ObjectFieldAccessCheck.checkUpdateAccess('Nrich__Preffered__c', 'Nrich__End_Date_Time__c') && ObjectFieldAccessCheck.checkCreateAccess('Nrich__Preffered__c', 'Nrich__End_Date_Time__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Preffered__c', 'Nrich__End_Date_Time__c'))
                        Preffered.Nrich__End_Date_Time__c = datetimeval.dateval;
                    PrefferednewMap.put(datetimeval.dateval, Preffered);
                }
                availabledates.add(datetimeval.dateval); 
            }        
        }
        
        if(prefferedMap!=NULL && prefferedMap.size()>0){
            list<Nrich__Preffered__c> PrefferedDeleteList = new list<Nrich__Preffered__c>();
            for(Date dt: prefferedMap.keySet()){
                if(!availabledates.contains(dt)){
                    Nrich__Preffered__c Preffered = new Nrich__Preffered__c();
                    Preffered.Id = prefferedMap.get(dt).Id;
                    PrefferedDeleteList.add(Preffered);
                }
            }
            
            Schema.DescribeSObjectResult Prefferedobjectcontent = Nrich__Preffered__c.getSObjectType().getDescribe();            
            if(Prefferedobjectcontent.isDeletable() && PrefferedDeleteList.size()>0)
                Database.delete(PrefferedDeleteList, false);
        }
        
        for(Nrich__Preffered__c preffer: PrefferednewMap.values()){
            PrefferedUpsertList.add(preffer);
        }
        Schema.DescribeSObjectResult PrefferedobjectDescriptioncontent = Nrich__Preffered__c.getSObjectType().getDescribe();
        if(PrefferedobjectDescriptioncontent.isCreateable() && PrefferedUpsertList.size()>0){
            Database.upsert(PrefferedUpsertList, false);
        }        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='PanelMemberTimeSlotsController.save', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    public void seleteddate(){
        
        String myDate = seleted_date;
        
        String strMnth    = myDate.substring(4,7);
        String day        = myDate.substring(8,10);
        String year       = myDate.substring(24,28);
        string strMonth ;
        if(strMnth=='Jan')
            strMonth ='1';
        else if (strMnth == 'Feb' )
            strMonth ='2';
        else if (strMnth == 'Mar' )
            strMonth ='3';
        else if (strMnth == 'Apr' )
            strMonth ='4';
        else if (strMnth == 'May' )
            strMonth ='5';
        else if (strMnth == 'Jun' )
            strMonth ='6';
        else if (strMnth == 'Jul' )
            strMonth ='7';
        else if (strMnth == 'Aug' )
            strMonth ='8';
        else if (strMnth == 'Sep' )
            strMonth ='9';
        else if (strMnth == 'Oct' )
            strMonth ='10';
        else if (strMnth == 'Nov' )
            strMonth ='11';
        else if (strMnth == 'Dec' )
            strMonth ='12';
        
        String strDate = strMonth +'/'+day+'/'+year;
        date mydate1 = date.parse(strDate);
        selectedDate = mydate1;
    }
    
    public void showAm(){
        showAm=true;
    }
    public void showPm(){
        showAm=false;
    }
    public class weekdays{
        public Date daterec {get; set;}
        public String day {get; set;}
        public weekdays(){
        }
    }
    public class dateTimeslotWrapper{
        public String timeslot {get; set;}
        public Date dateval {get; set;}
        public String timeslotId {get; set;}
        public Boolean isAvailable {get; set;}
        public String timeslottype {get; set;}
        public dateTimeslotWrapper(){        
        }
    }
}