public with sharing class ExpenseModalController {
    
    
    
    public static string strEmployeeID=userinfo.getUserId();
    public static string  EmployyeID;
    public static string ExpenseId;
    
    
    
    @AuraEnabled
    public static list<string> fetchexpensetypeoptions(){
        Schema.DescribeFieldResult fieldResult = Nrich__Expense__c.Nrich__Expense_Type__c.getDescribe();
        list<String> options = Optionshelper(fieldResult, true);
        return options;
        
        
    }    
   
     @AuraEnabled
    public static list<string> fetchStatus(){
        Schema.DescribeFieldResult fieldResult = Nrich__Expense__c.Nrich__Expense_Status__c.getDescribe();
        list<String> options = Optionshelper(fieldResult, false);
        system.debug('options===>'+options);
        return options;
    }
   
    @AuraEnabled
    public static list<Nrich__Expense__c > fetchoptions(){
        try{
            list<Nrich__Expense__c> ExpenseList=[select Name,Nrich__Project__c,Nrich__Project__r.Name  from Nrich__Expense__c ];
            return ExpenseList;
        }
        
        catch(exception e ){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ExpenseModalController.fetchoptions', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
            
        }
        
    }
    
    public static list<String> Optionshelper(Schema.DescribeFieldResult fieldval, boolean empty){
        list<String> options = new list<String>();
        List<Schema.PicklistEntry> ple = fieldval.getPicklistValues();
        if(empty)
            options.add('--None--');
        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getValue());
        }       
        return options;
    }
    
    @AuraEnabled
    public static list<Nrich__Expense__c> ViewExpenserecords(){
        try{
            Nrich__Employee__c objEmp = new Nrich__Employee__c();
            
            objEmp = [select id
                      from Nrich__Employee__c 
                      where Nrich__Related_User__c =:strEmployeeID
                      limit 1]; 
            list<Nrich__Expense__c>ExpenseList=[select id,Name, Nrich__Approver_Name__c, Nrich__End_Date__c,Nrich__Start_Date__c, 
                                                Nrich__Expense_Amount__c, Nrich__Expense_Status__c, Nrich__Expense_Type__c, Nrich__ProjectName__c,Nrich__SubmitterName__c,
                                                Nrich__Reimbursed_Amount__c from Nrich__Expense__c where Nrich__Submitter__c=:objEmp.Id
                                               ];
            return ExpenseList;
            
        }
        Catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ExpenseModalController.SaveFile', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
        
        
        
        
    }
    
    
    
    
    @AuraEnabled
    public static Id insertnewexpense(Nrich__Expense__c ExpenseDetails){ 
        try{
            Nrich__Employee__c objEmp = new Nrich__Employee__c();
            
            objEmp = [select id
                      from Nrich__Employee__c 
                      where Nrich__Related_User__c =:strEmployeeID
                      limit 1]; 
            
            ExpenseDetails.Nrich__Submitter__c=objEmp.Id;
            System.debug('ExpenseDetails' +ExpenseDetails);
            
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Expense__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable())
                //database.SaveResult Exp= database.insert(ExpenseDetails);
                insert ExpenseDetails;
            return ExpenseDetails.Id;
           
        }
        catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ExpenseModalController.SaveFile', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    @AuraEnabled
    public static void SaveFile(String fileName, String base64Data, String contentType,string ExpID){
        try{
            system.debug('fileName' +fileName);
            Nrich__Employee__c objEmp=new Nrich__Employee__c();
            objEmp = [select id,
                      Name,Nrich__ProfilePic__c,
                      Nrich__EmployeeDesignation__c,
                      Nrich__Employee_Id__c
                      from Nrich__Employee__c 
                      where Nrich__Related_User__c =:strEmployeeID
                      limit 1]; 
            system.debug('objEmp****'+objEmp);
            
            EmployyeID=objEmp.id;
            String contentDocumentIdval;
            
            if(string.isnotBlank(fileName) && string.isNotBlank(base64Data)){
                base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
                if(objEmp!=NULL){
                    
                    ContentVersion cv = new ContentVersion();
                    cv.ContentLocation = 'S';
                    cv.VersionData =EncodingUtil.base64Decode(base64Data);
                    cv.Title = fileName;
                    cv.PathOnClient = fileName;
                    database.SaveResult contentResult = database.insert(cv);
                    
                    if(contentResult!=NULL && contentResult.isSuccess()){
                        list<ContentDocumentLink> ContentDocumentLinklist = new list<ContentDocumentLink>();
                        list<ContentDistribution> ContentDistributionList = new list<ContentDistribution>();
                        for(ContentVersion ContentVersionlist : [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id =: contentResult.getId()])
                        {
                            ContentDocumentLink cdl = new ContentDocumentLink();
                            cdl.ContentDocumentId = ContentVersionlist.ContentDocumentId;
                            cdl.LinkedEntityId = ExpenseId;
                            cdl.ShareType = 'V';
                            ContentDocumentLinklist.add(cdl); 
                            System.debug('Content documentlist' +ContentDocumentLinklist);
                            contentDocumentIdval = contentResult.getId();
                        }
                        
                        
                        Schema.DescribeSObjectResult objectDescriptioncontentdoc = ContentDocumentLink.getSObjectType().getDescribe();
                        if(objectDescriptioncontentdoc.isCreateable() && ContentDocumentLinklist.size()>0){
                            insert ContentDocumentLinklist;
                            
                        }
                    }
                }
                
            }
        }
        
        
        
        catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ExpenseModalController.SaveFile', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
        }
        
        
    }
}