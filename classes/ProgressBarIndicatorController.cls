/****************************************************************************************************
* Company:Absyz
* Developer:Seshu Varma
* Created Date:29/10/18
* Description:Fetching data for progress bar
*****************************************************************************************************/
public class ProgressBarIndicatorController {
    @AuraEnabled
    public static List<Nrich__Milestone__c  > initMethod( String  projectID){
        try{
        System.debug('Value of projectID ::'+projectID);
        List<Nrich__Milestone__c> temp= new list<Nrich__Milestone__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Milestone__c', 'Nrich__Start_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Milestone__c', 'Nrich__End_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Milestone__c', 'Nrich__Status__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Milestone__c', 'Nrich__Milestone_Percentage_Completed__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Milestone__c', 'Nrich__Completion_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Milestone__c', 'Nrich__Project__c')
           
          )
            temp = [SELECT Id, Name, Nrich__Start_Date__c,Nrich__End_Date__c,Nrich__Status__c,Nrich__Milestone_Percentage_Completed__c,Nrich__Completion_Date__c from Nrich__Milestone__c Where Nrich__Project__c=:projectID  order by Nrich__Start_Date__c];
        System.debug('Value of ttemp ::'+temp);
        return temp;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ProgressBarIndicatorController.initMethod', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}