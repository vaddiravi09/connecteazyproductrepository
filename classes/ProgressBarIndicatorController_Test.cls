@isTest
public class ProgressBarIndicatorController_Test {
    
    @isTest
    public static void pgbar(){
        Nrich__Project__c manage_project_Obj = new Nrich__Project__c();
        manage_project_Obj.Nrich__Sub_Type__c = 'Billable';
        manage_project_Obj.Nrich__Project_Start_Date__c = Date.today();
        manage_project_Obj.Nrich__Project_End_Date__c = Date.today();
        manage_project_Obj.Nrich__Status__c = 'Proposed';
        manage_project_Obj.Nrich__Project_Type__c = 'Paid Project';
        Insert manage_project_Obj; 
        
        Nrich__Milestone__c  milestonestest= new Nrich__Milestone__c(Name = 'Name854', Nrich__Status__c = 'New', Nrich__Project__c = manage_project_Obj.Id);
        Insert milestonestest; 
        ProgressBarIndicatorController.initMethod(manage_project_Obj.Id);
        
        System.assertEquals('New', milestonestest.Nrich__Status__c);
        
    }

}