@RestResource(urlMapping='/appdetails')
Global class CallOutToGetApplicationDetails {
    
    //*******************************************************************************************
    //Created by      : Prasad Vievk
    //Method Name 	  : doPost
    //Overall Purpose : This method is used Insert Job Application Details.
    //InputParams     : N/A
    //OutputParams    : N/A
    //*******************************************************************************************    
    @HttpPost
    global static void doPost(){
        RestRequest req = RestContext.request;
        RestResponse res=RestContext.response;
        try{
            
            system.debug('-----------data--------' + req);
            System.debug('jjk'+ req.requestBody.toString());
            object data1 = (object) JSON.deserializeUntyped(req.requestBody.toString());
            Map < String, Object > result = (Map < String, Object > ) data1;
            map<string,string> apptocandMap =new map<string,string>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Applicant_to_Candidate__mdt', 'Nrich__Mapping_Applicant_Field__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Applicant_to_Candidate__mdt', 'Nrich__Mapping_Candidate_Field__c'))
                for(Nrich__Applicant_to_Candidate__mdt atoc: [Select Id, Nrich__Mapping_Applicant_Field__c, Nrich__Mapping_Candidate_Field__c from Nrich__Applicant_to_Candidate__mdt where Id!=NULL]){
                    apptocandMap.put(atoc.Nrich__Mapping_Applicant_Field__c, atoc.Nrich__Mapping_Candidate_Field__c);
                }
            Nrich__Candidate__c candidate = new Nrich__Candidate__c();            
            // candidate INsertion End
            Nrich__Job_Application__c jobApp=new Nrich__Job_Application__c();
            for( string s:result.Keyset()){
                if(apptocandMap.containskey(s)){
                    system.debug('s---'+s);
                    candidate.put(s,result.get(s));
                    jobApp.put(s,result.get(s));
                }
                else{
                    jobApp.put(s,result.get(s));
                }
            }
            system.debug('candidate-->'+candidate);
            Database.SaveResult candidateInsert=Database.insert(candidate,false);
            if(candidateInsert.isSuccess()){
                system.debug('candidate'+candidate);
                jobApp.Nrich__Candidate__c=candidate.id;
                Database.SaveResult jobAppInsert=Database.insert(jobApp,false);
                if(jobAppInsert.isSuccess()){
                    res.statusCode=200;
                    res.responseBody=blob.valueOf('Inserted Job Application ID ---'+jobAppInsert.id);
                }
            }
            else{
                res.statusCode=406;
                for(Database.Error e:candidateInsert.getErrors()){
                    res.responseBody=Blob.valueOf(e.getStatusCode()+'--'+e.getFields()+'--'+e.message);
                }
            }
        }
        catch(Exception e){
            system.debug('exception e'+e);
            res.statusCode=400;
            res.responseBody=blob.valueOf('Exception ---'+e.getMessage());
        }
    }
}