/************************************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 
* Class Name : InterviewRoundTriggerHandler 
* Purpose : When the status field in Interview Round object is updated to 'Selected' 
Create a new Interview Round by fetching the details from Job Round
* Testclass Name : nextInterviewRoundHandlerTest
***********************************************************************************/
public with sharing class InterviewRoundTriggerHandler {
    
    /*************************************
Method Name : checkJobRoundCount
Purpose : When the status field in Interview Round object is updated to 'Selected' 
Create a new Interview Round by fetching the details from Job Round
************************************/
    public static void checkJobRoundCount(List<Nrich__Interview_Round__c> interviewList, set<Id> jobIdSet, set<Decimal> jobOrderSet){
        //Initialization
        try{
        map<String, Nrich__Job_Round__c> jobRoundMap = new map<String, Nrich__Job_Round__c>();
        List<Nrich__Interview_Round__c> jobInterviewInsertList = new List<Nrich__Interview_Round__c>();
        List<Nrich__Job_Application__c> updateJobApplicationList = new List<Nrich__Job_Application__c>();
        set<Id> jobOrderIdSet = new set<Id>();
        //Query the Job Round with Job Id and Job round order number
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__job__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job_Round_Order_Number__c'))
            for(Nrich__Job_Round__c jobRounds : [select id,Name,Nrich__job__c,Nrich__Job_Round_Order_Number__c from Nrich__Job_Round__c where Nrich__job__c IN :jobIdSet AND Nrich__Job_Round_Order_Number__c IN: jobOrderSet ]){
                jobRoundMap.put((jobRounds.job__c+';'+jobRounds.Job_Round_Order_Number__c), jobRounds);
                jobOrderIdSet.add(jobRounds.Id);
            }
        system.debug('jobRoundMap '+jobRoundMap);
        map<String, String> templateMap = new map<String, String>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job_Round__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Is_Active__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Type_of_Template__c'))
            for(Nrich__Template__c templist : [select id, Nrich__Job_Round__c, Nrich__Job__c from Nrich__Template__c where Nrich__Is_Active__c=TRUE AND Nrich__Type_of_Template__c=:Label.Feedback AND Nrich__Job_Round__c IN: jobOrderIdSet ]){
                templateMap.put(templist.Job__c, templist.id);
            }
        
        system.debug('templateMap '+templateMap);
        
        for(Nrich__Interview_Round__c intRound : interviewList){
            decimal dc = intRound.Job_Order__c+1;
            //Assign values to Interview Round Fields from Job Rounds
            if(jobRoundMap.size()>0 && jobRoundMap.containskey(intRound.Nrich__Job_Id__c+';'+dc)){
                Nrich__Interview_Round__c interview = new Nrich__Interview_Round__c();
                If(Schema.sObjectType.Nrich__Interview_Round__c.fields.Name.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Name') ){
                    interview.Name = jobRoundMap.get(intRound.Job_Id__c+';'+dc).Name;
                }
                If(Schema.sObjectType.Nrich__Interview_Round__c.fields.Nrich__Job_Application__c.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Job_Application__c')){
                    interview.Nrich__Job_Application__c = intRound.Nrich__Job_Application__c;
                }
                If(Schema.sObjectType.Nrich__Interview_Round__c.fields.Nrich__Job_Order__c.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Job_Order__c')){
                    interview.Nrich__Job_Order__c = dc;
                }
                If(Schema.sObjectType.Nrich__Interview_Round__c.fields.Nrich__Status__c.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Status__c')){
                    interview.Nrich__Status__c = System.Label.InterviewToBeSchedule;
                }
                if(templateMap!=NULL && templateMap.size()>0 && templateMap.containsKey(intRound.Nrich__Job_Id__c)){
                    If(Schema.sObjectType.Nrich__Interview_Round__c.fields.Nrich__Feedback_URL__c.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Feedback_URL__c')){
                        interview.Nrich__Feedback_URL__c = System.Label.SitePageURL+'/apex/'+Label.FeedbackURL+'?templateId='+templateMap.get(intRound.Nrich__Job_Id__c);
                    }
                }
                jobInterviewInsertList.add(interview);
            }   
        }
        system.debug('jobInterviewInsertList '+jobInterviewInsertList);
        //Insert the List of Interview Rounds
        Schema.DescribeSObjectResult objectDescription = Nrich__Interview_Round__c.getSObjectType().getDescribe();
        if(objectDescription.isCreateable() && jobInterviewInsertList.size()>0)
            database.insert(jobInterviewInsertList, false);
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='InterviewRoundTriggerHandler.checkJobRoundCount', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    /*************************************
Method Name : updateStatusProgress
Purpose : When the status field in Interview Round object is updated to 'Scheduled' 
Update the Job Application status to 'In Progress'
************************************/
    public static void updateStatusProgress(set<Id> interviewScheduleIdSet){
        try{
        List<Nrich__Job_Application__c> updateJobApplicationStatusList = new List<Nrich__Job_Application__c>();
        //Iterating on the Job Application List
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Application_Status__c'))
            for(Nrich__Job_Application__c jobApp : [select id, Name, Nrich__Application_Status__c from Nrich__Job_Application__c where ID IN : interviewScheduleIdSet ]){
                If(Schema.sObjectType.Nrich__Job_Application__c.fields.Nrich__Application_Status__c.isUpdateable()){
                    jobApp.Nrich__Application_Status__c = System.Label.JobApplicationInProgress;
                }
                //Add them to a List
                updateJobApplicationStatusList.add(jobApp);
            }
        //Update the List of Job Applications
        Schema.DescribeSObjectResult objectDescription = Nrich__Job_Application__c.getSObjectType().getDescribe();
        if(objectDescription.isUpdateable() && updateJobApplicationStatusList.size()>0)
            database.update(updateJobApplicationStatusList, false);
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='InterviewRoundTriggerHandler.updateStatusProgress', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    /*************************************
Method Name : updateStatusRejected
Purpose : When the status field in Interview Round object is updated to 'Rejected' 
Update the Job Application status to 'Rejected'
************************************/
    public static void updateStatusRejected(set<Id> interviewRejectedIdSet){
        try{
        List<Nrich__Job_Application__c> updateJobApplicationStatusList = new List<Nrich__Job_Application__c>();
        //Iterating on the Job Application List
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Application_Status__c'))
            for(Nrich__Job_Application__c jobApp : [select id, Name, Nrich__Application_Status__c from Nrich__Job_Application__c where ID IN : interviewRejectedIdSet ]){
                If(Schema.sObjectType.Nrich__Job_Application__c.fields.Nrich__Application_Status__c.isUpdateable()){
                    jobApp.Nrich__Application_Status__c = System.Label.JobApplicationRejected;
                }
                //Add them to a List
                updateJobApplicationStatusList.add(jobApp);
            }
        //Update the List of Job Applications
        Schema.DescribeSObjectResult objectDescription = Nrich__Job_Application__c.getSObjectType().getDescribe();
        if(objectDescription.isUpdateable() && updateJobApplicationStatusList.size()>0)
            database.update(updateJobApplicationStatusList, false);
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='InterviewRoundTriggerHandler.updateStatusRejected', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    /*************************************
Method Name : updateStatusRejected
Purpose : When the status field in Interview Round object is updated to 'Selected' and check for the final round
Update the Job Application status to 'Shortlisted'
************************************/
    public static void updateStatusShortlisted(set<Id> jobApplicationIdSet,set<Decimal> currentjobOrderSet){
        try{
        List<Nrich__Job_Application__c> jobApplicationList= new list<Nrich__Job_Application__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Job__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Rounds__c', 'Nrich__Job_Order__c'))
            jobApplicationList = [select id, Name, Nrich__Job__r.Nrich__No_of_Rounds__c,(select Id, Name, Nrich__Job_Order__c from Nrich__Interview_Rounds__r where Nrich__Job_Order__c IN : currentjobOrderSet) from Nrich__Job_Application__c where Id In : jobApplicationIdSet];
        List<Nrich__Job_Application__c> updateJobApplicationStatusList = new List<Nrich__Job_Application__c>();        
        //Iterating on the Job Application List
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Job_Order__c'))
            for(Nrich__Job_Application__c jobApp : jobApplicationList){
                //Iterating on the Interview Round from Job Application List
                for(Nrich__Interview_Round__c ir : jobApp.Nrich__Interview_Rounds__r){
                    //Check if the current interview Round is the Last Round 
                    if(ir.Nrich__Job_Order__c == jobApp.Nrich__Job__r.Nrich__No_of_Rounds__c){
                        If(Schema.sObjectType.Nrich__Job_Application__c.fields.Nrich__Application_Status__c.isUpdateable() &&ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Application_Status__c')){
                            jobApp.Nrich__Application_Status__c = System.Label.JobApplicationShortlisted;
                        }
                        updateJobApplicationStatusList.add(jobApp);
                    }
                }
            }
        //Update the List of Job Applications
        Schema.DescribeSObjectResult objectDescription = Nrich__Job_Application__c.getSObjectType().getDescribe();
        if(objectDescription.isUpdateable() && updateJobApplicationStatusList.size()>0)
            database.update(updateJobApplicationStatusList, false);
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='InterviewRoundTriggerHandler.updateStatusShortlisted', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}