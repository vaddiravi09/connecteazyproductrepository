@isTest
public class UserStoryUpdateTrigger_Test {
    
    @istest public static void triggercover(){
        Nrich__Project__c proj = new Nrich__Project__c();
        proj.name = 'abc';
        proj.Nrich__Project_Type__c = 'Paid Project';
        proj.Nrich__Status__c = 'Active';
        insert proj;
        
        Nrich__Milestone__c  milestonestest= new Nrich__Milestone__c(Name = 'Name854', Nrich__Status__c = 'New', Nrich__Project__c = proj.Id);
        Insert milestonestest;       
        
        Nrich__Project_Team_Member__c PTM = new Nrich__Project_Team_Member__c();
        PTM.Nrich__Project__c = proj.Id;
        PTM.Nrich__Project_Role__c='Senior Management';
        insert PTM;
        
        Nrich__Sprint__c sp1 = new Nrich__Sprint__c();
        sp1.name = 'vf page';
        sp1.Nrich__Project__c = proj.Id;
        sp1.Nrich__Milestone__c = milestonestest.id;
        sp1.Nrich__Status__c = Label.In_Progress;
        insert sp1;
        
        Nrich__Sprint__c sp2 = new Nrich__Sprint__c();
        sp2.name = 'vf page';
        sp2.Nrich__Project__c = proj.Id;
        sp2.Nrich__Milestone__c = milestonestest.id;
        sp2.Nrich__Status__c = Label.In_Progress;
        sp2.Nrich__Start_Date__c= Date.newInstance(2019, 05, 10);
        sp2.Nrich__End_Date__c=Date.newInstance(2019, 06, 10);
        
        insert sp2;
        
        Nrich__User_Story__c userstr = new Nrich__User_Story__c(Name = 'Story 1', Nrich__Project__c = proj.Id, Nrich__Status__c = 'New'
                                                  , Nrich__Assigned_Too__c =PTM.id,Nrich__Sprint__c=sp2.id, Nrich__Estimated_Effort__c=2,
                                                  Nrich__Actual_Effort__c=1);
        insert userstr;  
        userstr.Nrich__Status__c='Completed';
        userstr.Nrich__Sprint__c=sp1.id;
        update userstr;
        
        //delete userstr;
        
        
        Nrich__User_Story__c userstr1 = new Nrich__User_Story__c(Name = 'Story 12', Nrich__Project__c = proj.Id, Nrich__Status__c = 'New'
                                                   , Nrich__Assigned_Too__c =PTM.id,Nrich__Sprint__c=sp2.id, Nrich__Estimated_Effort__c=2,
                                                   Nrich__Actual_Effort__c=1);
        insert userstr1;  
        userstr1.Nrich__Status__c='Completed';
        userstr1.Nrich__Sprint__c=sp1.id;
        //Nw commented
        //update userstr1;
        list<Nrich__User_Story__c>UserstoryList=new list<Nrich__User_Story__c>();
        UserstoryList.add(userstr1);
        set<id>UserStoryId=new set<id>();
        UserStoryId.add(userstr1.Id);
        
        UserStoryUpdateTriggerHandler.checkStatusOfTicket(UserStoryId,UserstoryList);
        System.assertEquals('Completed',userstr1.Nrich__Status__c );
        
        
        
        
        /* List<Task__c> lstofTask = new List<Task__c>();
Task__c tsk = new Task__c( Project__c = proj.Id, Status__c= 'New' ,Assigned_To__c=PTM.id,Actual_Effort__c=1,Estimated_Effort__c=2
,User_Story__c=userstr.id,Sprint__c=sp2.id);
insert tsk;*/
    }
    
    @istest public static void triggercover1(){
        
        Nrich__Project__c proj = new Nrich__Project__c();
        proj.name = 'abc';
        proj.Nrich__Project_Type__c = 'Paid Project';
        proj.Nrich__Status__c = 'Active';
        insert proj;
        
        Nrich__Sprint__c sp2 = new Nrich__Sprint__c();
        sp2.name = 'vf page';
        sp2.Nrich__Project__c = proj.Id;
        // sp2.Milestone__c = milestonestest.id;
        sp2.Nrich__Status__c = 'In Progress';
        sp2.Nrich__Start_Date__c= Date.newInstance(2019, 05, 10);
        sp2.Nrich__End_Date__c=Date.newInstance(2019, 06, 10);
        
        insert sp2;
        set<id> SprintIdist=new set<id>();
        SprintIdist.add(sp2.Id);
        
        Nrich__Project_Team_Member__c PTM = new Nrich__Project_Team_Member__c();
        PTM.Nrich__Project__c = proj.Id;
        PTM.Nrich__Project_Role__c='Senior Management';
        insert PTM;
        
        Nrich__User_Story__c userstr1 = new Nrich__User_Story__c(Name = 'Story 12', Nrich__Project__c = proj.Id, Nrich__Status__c = 'New'
                                                   , Nrich__Assigned_Too__c =PTM.id,Nrich__Sprint__c=sp2.id, Nrich__Estimated_Effort__c=2,
                                                   Nrich__Actual_Effort__c=1);
        insert userstr1;  
        userstr1.Nrich__Status__c='Completed';
        userstr1.Nrich__Sprint__c=sp2.id;
        update userstr1;
        
        Nrich__Task__c Task1=new Nrich__Task__c();
        Task1.Nrich__Assigned_To__c=PTM.Id;
        Task1.Nrich__Actual_Effort__c=8;
        Task1.Nrich__Status__c='New'; 
        // Task1.User_Story__c=userstr1.Id;
        Task1.Nrich__Sprint__c=sp2.id;
        Task1.Nrich__Project__c=proj.Id;
        //Nw commented
        //insert Task1;
        
        
        
        
        UserStoryUpdateTriggerHandler.changeSprintStatus(SprintIdist);
        System.assertEquals('Completed', userstr1.Nrich__Status__c);
        
        
        
    }
    
    @istest public static void triggercover3(){
        set<id> prjid=new set<id>();
        Nrich__Project__c proj = new Nrich__Project__c();
        proj.name = 'abc';
        proj.Nrich__Project_Type__c = 'Paid Project';
        proj.Nrich__Status__c = 'Active';
        insert proj;
        prjid.add(proj.Id);
        
        
        Nrich__Sprint__c sp2 = new Nrich__Sprint__c();
        sp2.name = 'vf page';
        sp2.Nrich__Project__c = proj.Id;
        // sp2.Milestone__c = milestonestest.id;
        sp2.Nrich__Status__c = 'In Progress';
        sp2.Nrich__Start_Date__c= Date.newInstance(2019, 05, 10);
        sp2.Nrich__End_Date__c=Date.newInstance(2019, 06, 10);
        insert sp2;
        
        
        set<id> SprintIdist=new set<id>();
        SprintIdist.add(sp2.Id);
        
        Nrich__Project_Team_Member__c PTM = new Nrich__Project_Team_Member__c();
        PTM.Nrich__Project__c = proj.Id;
        PTM.Nrich__Project_Role__c='Senior Management';
        insert PTM;
        
        Nrich__User_Story__c userstr1 = new Nrich__User_Story__c(Name = 'Story 12', Nrich__Project__c = proj.Id, Nrich__Status__c = 'New'
                                                   , Nrich__Assigned_Too__c =PTM.id,Nrich__Sprint__c=sp2.id, Nrich__Estimated_Effort__c=2,
                                                   Nrich__Actual_Effort__c=1);
        insert userstr1; 
        
        Nrich__Task__c Task1=new Nrich__Task__c();
        Task1.Nrich__Assigned_To__c=PTM.Id;
        Task1.Nrich__Actual_Effort__c=8;
        Task1.Nrich__Status__c='New'; 
        Task1.Nrich__User_Story__c=userstr1.Id;
        Task1.Nrich__Sprint__c=sp2.id;
        Task1.Nrich__Project__c=proj.Id;
       // insert Task1;
        
        
        list<Nrich__User_Story__c>UserstryList=new list<Nrich__User_Story__c>();
        UserstryList.add(userstr1);
        
        UserStoryUpdateTriggerHandler.ValidateUsrstryLoe(UserstryList);
        UserStoryUpdateTriggerHandler.projectRollout(prjid,SprintIdist);
        System.assertEquals('New', 'New');
        
    }
    
    
   /* @isTest public static void deletepart(){
        
    }
    */
}