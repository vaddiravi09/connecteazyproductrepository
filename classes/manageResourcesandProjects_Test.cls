@isTest
public with sharing class manageResourcesandProjects_Test {
    
    @testSetup static void setup() {
        // Create common test accounts
        
        Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                         
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        insert lleave;
        cleave.Nrich__Is_Active__c=True;
        update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        Nrich__Employee__c  emp=new Nrich__Employee__c ();
        emp.Nrich__EmployeeDesignation__c ='Developer';
        //  emp.EmployeeDesignation__c ='CEO';
        emp.Name='test';
        insert emp;
        
        Nrich__Project__c proj = new Nrich__Project__c();
        proj.name = 'abc';
        proj.Nrich__Project_Type__c = 'Paid Project';
        proj.Nrich__Status__c = 'Active';
        insert proj;
        
        
        
        Nrich__Project_Team_Member__c PTM = new Nrich__Project_Team_Member__c();
        PTM.Nrich__Project__c = proj.Id;
        PTM.Nrich__Project_Role__c='Senior Management';
        PTM.Nrich__Employeee__c=emp.id;
        insert PTM;
        
        Nrich__Project_Team_Member__c PTM1= new Nrich__Project_Team_Member__c();
        PTM1.Nrich__Project__c=proj.Id;
        PTM1.Nrich__Project_Role__c='Project Manager';
        PTM1.Nrich__Employeee__c=emp.id;
        insert PTM1;
        
        
        map<string,string>prjmap=new map<string,string>();
        prjmap.put(PTM.Nrich__Project__c,PTM.Nrich__Employeee__c);
        System.assertEquals('Senior Management', PTM.Nrich__Project_Role__c);
        
    }
    
    @istest public static void Prjteammember(){
        List<Nrich__Project_Team_Member__c > resultList = [SELECT id, Name, Nrich__Project__c FROM Nrich__Project_Team_Member__c where Nrich__Project_Role__c='Senior Management'];
        System.assertEquals(1, resultList.size());
        //System.debug('testresult' +resultList);
    }
    
}