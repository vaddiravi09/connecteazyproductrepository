@isTest
public class SignatureController_Test {
    
    @isTest 
    public static void signatureupload(){
        
        
        string filename='abc';
        string filetype='jpeg';
        string version='testing';
        list<Nrich__Employee__c> emplist=new list<Nrich__Employee__c>();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
        Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                         
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        insert lleave;
        cleave.Nrich__Is_Active__c=True;
        update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',
                                        Nrich__Total_Experience__c=10,
                                        Nrich__Related_User__c=u.id );
        
        
        insert emp; 
        try{
            Test.startTest();    
            ContentVersion cv = new ContentVersion();
            cv.title = filename;      
            cv.PathOnClient =filename;           
            cv.VersionData =EncodingUtil.base64Decode(version); 
            cv.IsMajorVersion = true;
            insert cv;       
            list<contentDocument> doclist = [select id,FileType from contentdocument];
            system.debug('doclist '+doclist);
            ContentDocumentLink newFileShare = new ContentDocumentLink();
            newFileShare.contentdocumentid = doclist[0].Id;
            newFileShare.LinkedEntityId = emp.id;
            newFileShare.ShareType= 'V';
            insert newFileShare;
            
            System.assertEquals('abc',cv.title);
            
            SignatureController.uploadSignature('Test',emp.id,'sales');
            
            if(Test.isRunningTest())
            {
                Exception e;
                throw e;
            }
            Test.stopTest();
        }
        catch (exception e) {
            System.debug('e' +e);
            
        }
        
        
        
    }
    
    
    
    
    
    
    
    
}