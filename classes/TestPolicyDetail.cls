@isTest()
public class TestPolicyDetail {
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Ashish Nag K
* Created Date: 27/12/2018
* Description: Test class covering Policy Detail Modal.
*****************************************************************************************************/  
    
    public static string strEmployeeID;
    
    @isTest static void insertrecords(){
        
        Boolean accept;
       /* Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0);  
        User u = new User(Alias = 's12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
        System.runAs(u){
            Company_Leave_Structure__c cleave=new Company_Leave_Structure__c(Applicable_From__c =system.today(),Applicable_Till__c=system.today(),Is_Active__c= true);
            insert cleave;
            Leave_Category__c lleave =new Leave_Category__c(Name='Test',Increment_Frequency__c=1,Increment_Step__c =2,
                                                            Will_Carry_Forward__c =False,
                                                            Will_Lapse__c =False,Applicable_Leave_Structure__c =cleave.id
                                                           );
            
            insert lleave;
            
            
            Employee__c emp=new Employee__c(Name='TestUser' ,EmployeeDesignation__c ='Developer',
                                            
                                            Total_Experience__c=10,
                                            Related_User__c=u.id );
            
            insert emp;*/
        UtilityForTest.Employee_Utility();
            
            Nrich__Policy__c  policy=new Nrich__Policy__c ();
            policy.name='Test';
            policy.Nrich__Policy_Applicable_From__c=System.today();
            policy.Nrich__Pulished_Date__c=System.today();
            insert policy;
            
            
            
            Nrich__EmployeePolicy__c emppolicy=new Nrich__EmployeePolicy__c();
            emppolicy.Nrich__Status__c='Accepted';
            emppolicy.Nrich__Date_Responded__c=System.today();
            emppolicy.Nrich__Policy__c=policy.id;
            insert emppolicy;
            
            
            list<Nrich__EmployeePolicy__c> emlist=new list<Nrich__EmployeePolicy__c>();
            emlist.add(emppolicy);
            //insert emlist;
            
            
            string Emm=JSON.serialize(emlist);
            List<Nrich__EmployeePolicy__c> emplist=PolicyDetailComponentController.AcceptorRejectPolicy(Emm);
            
            System.assertEquals('Accepted',emppolicy.Nrich__Status__c);
            
            
            
            
       // }  
        
        
    }
    
    @isTest static void insertrecords1()
    {
     /*   Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0);  
        User u = new User(Alias = 's12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
        System.runAs(u){
            Company_Leave_Structure__c cleave=new Company_Leave_Structure__c(Applicable_From__c =system.today(),Applicable_Till__c=system.today(),Is_Active__c= true);
            insert cleave;
            Leave_Category__c lleave =new Leave_Category__c(Name='Test',Increment_Frequency__c=1,Increment_Step__c =2,
                                                            Will_Carry_Forward__c =False,
                                                            Will_Lapse__c =False,Applicable_Leave_Structure__c =cleave.id
                                                            
                                                           );
            
            insert lleave;
            
            
            Employee__c emp=new Employee__c(Name='TestUser' , EmployeeDesignation__c ='Developer',
                                            Total_Experience__c=10,
                                            Related_User__c=u.id );
            
            insert emp;*/
        UtilityForTest.Employee_Utility();
            
            Nrich__Policy__c  policy=new Nrich__Policy__c ();
            policy.name='Test';
            policy.Nrich__Policy_Applicable_From__c=System.today();
            policy.Nrich__Pulished_Date__c=System.today();
            insert policy;
            
            
            
            Nrich__EmployeePolicy__c emppolicy=new Nrich__EmployeePolicy__c();
            emppolicy.Nrich__Status__c='Accepted';
            emppolicy.Nrich__Date_Responded__c=System.today();
            emppolicy.Nrich__Policy__c=policy.id;
            insert emppolicy;
            
            
            list<Nrich__EmployeePolicy__c> emlist=new list<Nrich__EmployeePolicy__c>();
            emlist.add(emppolicy);
            
            
            
            string Emm=JSON.serialize(emlist);
            List<Nrich__EmployeePolicy__c> emplist1=PolicyDetailComponentController.AcceptorRejectPolicy(Emm);
            
            
            
            //Related to Policy Document
            string filename='abc';
            string filetype='jpeg';
            string version='testing';
            ContentVersion cv = new ContentVersion();
            cv.title = filename;      
            cv.PathOnClient =filename;           
            cv.VersionData =EncodingUtil.base64Decode(version); 
            cv.IsMajorVersion = true;
            insert cv;       
            list<contentDocument> doclist = [select id,FileType from contentdocument];
            system.debug('doclist '+doclist);
            ContentDocumentLink newFileShare = new ContentDocumentLink();
            newFileShare.contentdocumentid = doclist[0].Id;
            newFileShare.LinkedEntityId =policy.id;
            newFileShare.ShareType= 'V';
            insert newFileShare;
            
            
            
            
            PolicyDetailComponentController.getPolicyDocument(policy.Id);
            System.assertEquals('abc', cv.title);
            
            
            
            
            
       // }
        
    }
    
    @isTest static void nullpart(){
        Nrich__Policy__c  policy=new Nrich__Policy__c ();
        policy.name='Test';
        policy.Nrich__Policy_Applicable_From__c=System.today();
        policy.Nrich__Pulished_Date__c=System.today();
        insert policy;
        
        PolicyDetailComponentController.getPolicyDocument(policy.Id);
        System.assertEquals('Test', policy.name);
        
        
        
    }
    
}