/************************************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 
* Class Name : jobRoundTriggerHandler
* Purpose : 
* Testclass Name : jobRoundTriggerHandlerTest
***********************************************************************************/
public class JobRoundTriggerHandler {
    
    /*************************************
Method Name : updateJobRound
Purpose : 
************************************/
    public static void insertJobRound(list<Nrich__Job_Round__c> triggernew, set<Id> jobIdSet){
        try{
        system.debug('jobIdSet ->'+jobIdSet);
        list<Nrich__Job_Round__c> countList = new list<Nrich__Job_Round__c>();
        list<Nrich__Job_Round__c> jobRoundList= new list<Nrich__Job_Round__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job_Round_Order_Number__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__job__c'))
            jobRoundList = [SELECT id, Name, Nrich__Job_Round_Order_Number__c, Nrich__job__c  FROM Nrich__Job_Round__c  where Nrich__Job__c IN :jobIdSet  order by Nrich__Job_Round_Order_Number__c ASC];
        map<String,list<Nrich__Job_Round__c>> jobMap = new map<string,list<Nrich__Job_Round__c>>();
        for(Nrich__Job_Round__c jbr : jobRoundList){
            if(jobMap!=NULL && jobMap.size()>0 && jobMap.containsKey(jbr.Nrich__job__c)){
                list<Nrich__Job_Round__c>  roundlist = jobMap.get(jbr.Nrich__job__c);
                roundlist.add(jbr);
                jobMap.put(jbr.Nrich__job__c, roundlist);
            }
            else{
                jobMap.put(jbr.Nrich__job__c, new list<Nrich__Job_Round__c>{jbr});
            }
        }
        map<String, list<Nrich__Job_Round__c>> newjobMap = new map<String, list<Nrich__Job_Round__c>>();
        map<String, Boolean> jobMapCheck = new map<String, Boolean>();
        for(Nrich__Job_Round__c jr: triggernew) {       
            If(Schema.sObjectType.Nrich__Job_Round__c.fields.Nrich__Job_Round_Order_Number__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job_Round_Order_Number__c')){
                if(jobMap!=NULL && jobMap.size()>0 && jobMap.containsKey(jr.Nrich__Job__c) && !jobMapCheck.containsKey(jr.Nrich__Job__c)){
                    integer count = jobMap.get(jr.Nrich__Job__c).size();
                    count= count+1;
                    if(jr.Nrich__Job_Round_Order_Number__c!=NULL && count!=jr.Nrich__Job_Round_Order_Number__c){
                        jr.addError(Label.Round_Specified_Not_In_Order);
                        jobMapCheck.put(jr.Nrich__Job__c, TRUE);
                    }else if(jr.Nrich__Job_Round_Order_Number__c==NULL){
                        If(Schema.sObjectType.Nrich__Job_Round__c.fields.Nrich__Job_Round_Order_Number__c.isUpdateable()){
                            jr.Nrich__Job_Round_Order_Number__c = count;
                        }
                    }
                }
                else{
                    if(newjobMap!=NULL && newjobMap.size()>0 && newjobMap.containsKey(jr.Nrich__Job__c) && !jobMapCheck.containsKey(jr.Nrich__Job__c)){
                        list<Nrich__Job_Round__c>  roundlist = jobMap.get(jr.Nrich__job__c);
                        integer newcount = roundlist.size();
                        newcount=newcount+1;
                        if(jr.Nrich__Job_Round_Order_Number__c!=NULL && jr.Nrich__Job_Round_Order_Number__c!=newcount)
                            jr.addError(Label.Round_Specified_Not_In_Order);
                        else if(jr.Nrich__Job_Round_Order_Number__c==NULL)
                            If(Schema.sObjectType.Nrich__Job_Round__c.fields.Nrich__Job_Round_Order_Number__c.isUpdateable()){
                                jr.Nrich__Job_Round_Order_Number__c = newcount;
                            }
                        roundlist.add(jr);
                        newjobMap.put(jr.Nrich__job__c, roundlist);
                    }else if(!jobMapCheck.containsKey(jr.Nrich__Job__c)){
                        if(jr.Nrich__Job_Round_Order_Number__c!=NULL && jr.Nrich__Job_Round_Order_Number__c!=1){
                            jr.addError(Label.Round_Specified_Not_In_Order);
                            jobMapCheck.put(jr.Nrich__job__c, TRUE);
                        }
                        else if(jr.Nrich__Job_Round_Order_Number__c==NULL)
                            If(Schema.sObjectType.Nrich__Job_Round__c.fields.Nrich__Job_Round_Order_Number__c.isUpdateable()){
                                jr.Nrich__Job_Round_Order_Number__c = 1;
                            }
                        newjobMap.put(jr.Nrich__job__c, new list<Nrich__Job_Round__c>{jr});
                    }
                }
            }
        }    
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='JobRoundTriggerHandler.insertJobRound', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    public static void updateJobRound(list<Nrich__Job_Round__c> triggernew, map<ID,Nrich__Job_Round__c> oldmap, set<Id> jobIdSet){
        try{
        list<Nrich__Job_Round__c> countList = new list<Nrich__Job_Round__c>();
        list<Nrich__Job_Round__c> jobRoundList= new list<Nrich__Job_Round__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job_Round_Order_Number__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__job__c'))
            jobRoundList = [SELECT id, Name, Nrich__Job_Round_Order_Number__c, Nrich__job__c  FROM Nrich__Job_Round__c where Nrich__Job__c IN :jobIdSet  order by Nrich__Job_Round_Order_Number__c ASC];
        map<String,list<Nrich__Job_Round__c>> jobMap = new map<string,list<Nrich__Job_Round__c>>();
        for(Nrich__Job_Round__c jbr : jobRoundList){
            if(jobMap!=NULL && jobMap.size()>0 && jobMap.containsKey(jbr.Nrich__job__c)){
                list<Nrich__Job_Round__c>  roundlist = jobMap.get(jbr.Nrich__job__c);
                roundlist.add(jbr);
                jobMap.put(jbr.Nrich__job__c, roundlist);
            }
            else{
                jobMap.put(jbr.Nrich__job__c, new list<Nrich__Job_Round__c>{jbr});
            }
        }
        map<String, list<Nrich__Job_Round__c>> newjobMap = new map<String, list<Nrich__Job_Round__c>>();
        map<String, Boolean> jobMapCheck = new map<String, Boolean>();
        for(Nrich__Job_Round__c jr: triggernew) {       
            If(Schema.sObjectType.Nrich__Job_Round__c.fields.Nrich__Job_Round_Order_Number__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job_Round_Order_Number__c')){
                if(jobMap!=NULL && jobMap.size()>0 && jobMap.containsKey(jr.Nrich__Job__c) && !jobMapCheck.containsKey(jr.Nrich__Job__c)){
                    integer count = jobMap.get(jr.Nrich__Job__c).size();
                    if(jr.Nrich__Job_Round_Order_Number__c!=NULL && count!=jr.Nrich__Job_Round_Order_Number__c && jr.Nrich__Job_Round_Order_Number__c != integer.valueOf(oldmap.get(jr.ID).Nrich__Job_Round_Order_Number__c)){
                        jr.addError(Label.Round_Specified_Not_In_Order );
                        jobMapCheck.put(jr.Nrich__Job__c, TRUE);
                    }else if(jr.Nrich__Job_Round_Order_Number__c==NULL){                            
                        count= count+1;
                        If(Schema.sObjectType.Nrich__Job_Round__c.fields.Nrich__Job_Round_Order_Number__c.isUpdateable() &&ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job_Round_Order_Number__c')){
                            jr.Nrich__Job_Round_Order_Number__c = count;
                        }
                    }
                }
                else{
                    if(newjobMap!=NULL && newjobMap.size()>0 && newjobMap.containsKey(jr.Nrich__Job__c) && !jobMapCheck.containsKey(jr.Nrich__Job__c)){
                        list<Nrich__Job_Round__c>  roundlist = jobMap.get(jr.Nrich__job__c);
                        integer newcount = roundlist.size();
                        newcount=newcount+1;
                        if(jr.Nrich__Job_Round_Order_Number__c!=NULL && jr.Nrich__Job_Round_Order_Number__c!=newcount)
                            jr.addError(Label.Round_Specified_Not_In_Order);
                        else if(jr.Nrich__Job_Round_Order_Number__c==NULL)
                            If(Schema.sObjectType.Nrich__Job_Round__c.fields.Nrich__Job_Round_Order_Number__c.isUpdateable()){
                                jr.Nrich__Job_Round_Order_Number__c = newcount;
                            }
                        roundlist.add(jr);
                        newjobMap.put(jr.Nrich__job__c, roundlist);
                    }else if(!jobMapCheck.containsKey(jr.Nrich__Job__c)){
                        if(jr.Nrich__Job_Round_Order_Number__c!=NULL && jr.Nrich__Job_Round_Order_Number__c!=1){
                            jr.addError(Label.Round_Specified_Not_In_Order);
                            jobMapCheck.put(jr.Nrich__job__c, TRUE);
                        }
                        else if(jr.Nrich__Job_Round_Order_Number__c==NULL)
                            If(Schema.sObjectType.Nrich__Job_Round__c.fields.Nrich__Job_Round_Order_Number__c.isUpdateable()){
                                jr.Nrich__Job_Round_Order_Number__c = 1;
                            }
                        newjobMap.put(jr.Nrich__job__c, new list<Nrich__Job_Round__c>{jr});
                    }
                }
            }
        }  
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='JobRoundTriggerHandler.updateJobRound', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    public static void deleteJobRound(list<Nrich__Job_Round__c> triggerold, set<Id> jobIdsSet){
        try{
        list<Nrich__Job_Round__c> countList = new list<Nrich__Job_Round__c>();
        list<Nrich__Job_Round__c> jobRoundList= new list<Nrich__Job_Round__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job_Round_Order_Number__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__job__c'))
            jobRoundList = [SELECT id, Name, Nrich__Job_Round_Order_Number__c, Nrich__job__c  FROM Nrich__Job_Round__c where Nrich__Job__c IN :jobIdsSet order by Nrich__Job_Round_Order_Number__c ASC];
        map<String, Integer> jobMap = new map<String, Integer>();
        for(Nrich__Job_Round__c jbr : jobRoundList){
            if(jobMap!=NULL && jobMap.size()>0 && jobMap.ContainsKey(jbr.Nrich__job__c)){
                integer roundval = jobMap.get(jbr.Nrich__job__c);
                roundval = roundval+1;
                if(jbr.Nrich__Job_Round_Order_Number__c!=roundval)
                    countList.add(jbr);
                If(Schema.sObjectType.Nrich__Job_Round__c.fields.Nrich__Job_Round_Order_Number__c.isUpdateable()){
                    jbr.Nrich__Job_Round_Order_Number__c = roundval;
                }
                jobMap.put(jbr.Nrich__job__c, roundval);
                
            }
            else{
                integer roundval =1;
                if(jbr.Nrich__Job_Round_Order_Number__c!=roundval)
                    countList.add(jbr);
                If(Schema.sObjectType.Nrich__Job_Round__c.fields.Nrich__Job_Round_Order_Number__c.isUpdateable()){
                    jbr.Nrich__Job_Round_Order_Number__c = roundval;
                }
                jobMap.put(jbr.Nrich__job__c, roundval);                
            }           
        }
        system.debug('coyunbrtt'+countList);
        Schema.DescribeSObjectResult objectDescription = Nrich__Job__c.getSObjectType().getDescribe();
        if(objectDescription.isUpdateable()){
            if(countList.size()>0)
                database.update(countList);
        }
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='JobRoundTriggerHandler.deleteJobRound', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}