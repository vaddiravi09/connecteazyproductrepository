/********************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 10/05/2019
* Description: Updating timesheet details
********************************************************************/
public class ApprovalTriggerHelper {
    public static void updateTimesheetdetails(list<Nrich__Approval__c>ApprovalList){
      try{
        wrapperclass wrap=new wrapperclass();
        list<Nrich__TimeSheet_Detail__c>Timesheetdetailslist=new list<Nrich__TimeSheet_Detail__c>();
        list<Nrich__TimeSheet_Detail__c>UpdatedTimeSheetDetails=new list<Nrich__TimeSheet_Detail__c>();
        map<Id,wrapperclass>ApprovalMap=new map<id,wrapperclass>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Approval__c', 'Nrich__Start_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Approval__c', 'Nrich__End_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Approval__c', 'Nrich__Status__c'))
        {
            for(Nrich__Approval__c t:ApprovalList){
                wrap.StDate=t.Nrich__Start_Date__c;
                wrap.EdDate=t.Nrich__End_Date__c;
                wrap.Status=t.Nrich__Status__c;
                ApprovalMap.put(t.Nrich__ProjectId__c,wrap);
            }
        }
        
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Project__c')){
            Timesheetdetailslist=[select id,Name,Nrich__Status__c,Nrich__Project__c ,Nrich__Date__c   from Nrich__TimeSheet_Detail__c where Nrich__Project__c =:ApprovalMap.keyset()];
            for(Nrich__TimeSheet_Detail__c tdetail:Timesheetdetailslist){
                if(tdetail.Nrich__Date__c>=ApprovalMap.get(tdetail.Nrich__Project__c).StDate ){
                    if(tdetail.Nrich__Date__c<=ApprovalMap.get(tdetail.Nrich__Project__c).EdDate){
                        tdetail.Nrich__Status__c=ApprovalMap.get(tdetail.Nrich__Project__c).Status;
                        UpdatedTimeSheetDetails.add(tdetail);
                        System.debug('UpdatedTimeSheetDetails' +UpdatedTimeSheetDetails);
                    }
                }
            }
        }
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__TimeSheet_Detail__c.getSObjectType().getDescribe();
        if(objectDescriptioncontent.IsUpdateable()){
            Database.SaveResult [] result = database.update(UpdatedTimeSheetDetails);
        }
      }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ApprovalTriggerHelper.updateTimesheetdetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          } 
      }
    }
        
    public class wrapperclass{
        @AuraEnabled
        public Date StDate;
        @AuraEnabled
        public Date EdDate;
        @AuraEnabled
        public string Status;
    }
    
}