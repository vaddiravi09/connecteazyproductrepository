public with sharing class EmployeeDashboardController {
    
    public static string strEmployeeID;
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 18/10/2018
* Description: Fetching Leave Card Details
*****************************************************************************************************/
    
    @AuraEnabled
    public static Nrich__Employee_Leave_Card__c fetchEmployeeLeaveCardDetails(){
        try{
            list<Nrich__Employee_Leave_Card__c> EmployeeLeaveCardList=new list<Nrich__Employee_Leave_Card__c>();
            string empUserId = userinfo.getUserId();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Card__c', 'Nrich__Leaves_Availed__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Card__c', 'Nrich__Leaves_Obtained__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Card__c', 'Nrich__Leaves_Remaining__c'))
                EmployeeLeaveCardList = [Select id, Nrich__Leaves_Availed__c, Nrich__Leaves_Obtained__c, Nrich__Leaves_Remaining__c from Nrich__Employee_Leave_Card__c where Nrich__Employee__r.Nrich__Related_User__c=:empUserId AND Nrich__Is_Active__c=TRUE ];
            if(EmployeeLeaveCardList!=NULL && EmployeeLeaveCardList.size()>0)
                return EmployeeLeaveCardList[0];
            else
                return null;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.fetchEmployeeLeaveCardDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 18/10/2018
* Description: Fetching Achievements
*****************************************************************************************************/   
    @AuraEnabled
    public static list<Nrich__Achievements__c> fetchAchievements(){
        try{
            string empUserId = userinfo.getUserId();
            list<Nrich__Achievements__c> AchievementsList=new list<Nrich__Achievements__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Achievements__c', 'Nrich__Applicable_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Achievements__c', 'Nrich__Category__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Achievements__c', 'Nrich__Awarded_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Achievements__c', 'Nrich__Approval_Status__c'))
                AchievementsList = [Select id, Name, Nrich__Applicable_Date__c, Nrich__Category__c, Nrich__Awarded_Date__c from Nrich__Achievements__c where Nrich__Employeee__r.Nrich__Related_User__c=:empUserId AND Nrich__Approval_Status__c=:Label.Approved order by Nrich__Awarded_Date__c desc ];
            if(AchievementsList!=NULL && AchievementsList.size()>0)
                return AchievementsList;
            else
                return null;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.fetchAchievements', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    @AuraEnabled
    public static list<Nrich__Announcements__c> fetchDetails(){
        try{
            string empUserId = userinfo.getUserId();
            Set<String> groupIdSet = new Set<String>();
            for(GroupMember gm: [SELECT Id, Group.id FROM GroupMember where UserOrGroupId=:empUserId]){
                groupIdSet.add(gm.Group.Id);
            }
            DateTime currentTime = System.now();
            list<Nrich__Announcements__c> accList = new List<Nrich__Announcements__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Announcements__c', 'Nrich__Title__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Announcements__c', 'Nrich__Body__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Announcements__c', 'Nrich__Start_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Announcements__c', 'Nrich__Type_of_Announcement__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Announcements__c', 'Nrich__End_Date__c')) 
                accList = [select id, Nrich__Title__c, Nrich__Body__c, Nrich__Start_Date__c, Nrich__Type_of_Announcement__c from Nrich__Announcements__c where Nrich__End_Date__c>=:currentTime AND Nrich__Publication_Status__c=:Label.Published AND (Nrich__Type_of_Announcement__c=:Label.Public OR (Nrich__Type_of_Announcement__c=:Label.Specific_Employee  AND Nrich__Employeee__r.Nrich__Related_User__c=:empUserId) OR (Nrich__Type_of_Announcement__c=:Label.Group AND OwnerId IN: groupIdSet) )] ; 
            return accList;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.fetchDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    @AuraEnabled
    public static list<Nrich__ToDoList__c> fetchToDoListMethod(){
        try{
            string empUserId = userinfo.getUserId();
            DateTime currentTime = System.now();
            list<Nrich__ToDoList__c> todoList = new List<Nrich__ToDoList__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Related_User__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Title__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Body__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Due_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Status__c')
               && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Related_User__c')
              )
                todoList = [select id, Nrich__Related_User__c, Nrich__Title__c, Nrich__Body__c, Nrich__Due_Date__c, Nrich__Record_Id__c from Nrich__ToDoList__c where Nrich__Status__c=:Label.Open AND Nrich__Related_User__c=:empUserId   order by createddate ]; 
            return todoList;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.fetchToDoListMethod', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    @AuraEnabled
    public static list<Nrich__EmployeePolicy__c> fetchEmployeePolicyMethod(){
        try{
            string empUserId = userinfo.getUserId();
            list<Nrich__EmployeePolicy__c> employeepolicyList= new list<Nrich__EmployeePolicy__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__EmployeePolicy__c', 'Nrich__status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__EmployeePolicy__c', 'Nrich__Policy__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__EmployeePolicy__c', 'Nrich__Policy_Title__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__EmployeePolicy__c', 'Nrich__Date_Issued__c')
               && ObjectFieldAccessCheck.checkAccessible('Nrich__EmployeePolicy__c', 'Nrich__Date_Responded__c'))
                
                employeepolicyList = [select id, Nrich__status__c, Nrich__Policy__c, Nrich__Policy_Title__c, Nrich__Policy__r.Nrich__Policy_Descriptionn__c, Nrich__Date_Issued__c, Nrich__Date_Responded__c from Nrich__EmployeePolicy__c where Nrich__Employeee__r.Nrich__Related_User__c=:empUserId AND Nrich__Policy__r.Nrich__status__c=:Label.Published ]; 
            return employeepolicyList;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.fetchEmployeePolicyMethod', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    
    @AuraEnabled
    public static list<Nrich__Travel_Request__c> fetchTravelRequestMethod(){
        try{
            string empUserId = userinfo.getUserId();
            list<Nrich__Travel_Request__c> travelrequestList= new list<Nrich__Travel_Request__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Source_Location__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Destination_Location__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Start_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__End_Date__c')
               && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Number_of_Travellers__c'))
                
                travelrequestList = [select id, Nrich__Status__c, Name, Nrich__Source_Location__c, Nrich__Destination_Location__c, Nrich__Start_Date__c, Nrich__End_Date__c, Nrich__Number_of_Travellers__c from Nrich__Travel_Request__c where Nrich__Employeee__r.Nrich__Related_user__c=:empUserId ]; 
            return travelrequestList;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.fetchTravelRequestMethod', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Hrishikesh
* Created Date: 23/06/2018
* Description: Fetching Details of Custom Setting object InitialSetup__c used for Initial Setup Page
*****************************************************************************************************/
    @AuraEnabled
    public static Nrich__InitialSetup__c fetchcustomSetting(){
        Nrich__InitialSetup__c setup = Nrich__InitialSetup__c.getInstance();
        return setup;
    }
    
    @AuraEnabled
    public static list<Nrich__Certification__c > fetchoptions(){
        list<Nrich__Certification__c> Certlist=new list<Nrich__Certification__c>();
        if( ObjectFieldAccessCheck.checkAccessible('Nrich__Certification__c', 'Name'))
            Certlist=[Select name,Id from Nrich__Certification__c ];
        return Certlist;
    }
    
    @AuraEnabled
    public static EmployeeWrapper fetchEmployeeDetails1(){
        try{
            EmployeeWrapper objWrap = new EmployeeWrapper();
            Nrich__Employee__c objEmp = new Nrich__Employee__c();
            string strEmpID = userinfo.getuserid();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__ProfilePic__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__EmployeeDesignation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Employee_Id__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Mentor__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Email__c')
               && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Experience_in_Organization__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Total_Experience__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Phone__c')
               && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Date_of_Joining__c')
              )
                objEmp = [select id,
                          Name,
                          Nrich__ProfilePic__c ,
                          Nrich__EmployeeDesignation__c,
                          Nrich__Employee_Id__c,
                          Nrich__Mentor__c,
                          Nrich__Primary_Email__c ,
                          Nrich__Mentor__r.Name,
                          Nrich__Mentor__r.Nrich__Employee_Id__c,
                          Nrich__Mentor__r.Nrich__EmployeeDesignation__c,
                          Nrich__Experience_in_Organization__c,
                          Nrich__Total_Experience__c
                          
                          from Nrich__Employee__c 
                          where Nrich__Related_User__c =: strEmpID 
                          limit 1]; 
            objWrap.objEmployeeDetails = objEmp;
            list<Nrich__Employee__c> lstEmp = new list<Nrich__Employee__c>();
            lstEmp = [select id,
                      Name,Nrich__ProfilePic__c ,
                      Nrich__EmployeeDesignation__c,
                      Nrich__Employee_Id__c,
                      Nrich__Mentor__c,
                      Nrich__Mentor__r.Name,
                      //BasEnrich__Employee_Id__c,
                      Nrich__Mentor__r.Nrich__EmployeeDesignation__c,
                      Nrich__Experience_in_Organization__c,
                      Nrich__Total_Experience__c,
                      Nrich__Date_of_Joining__c,
                      Nrich__Primary_Email__c, 
                      Nrich__Primary_Phone__c   
                      from Nrich__Employee__c 
                      where Nrich__Mentor__c=:objEmp.id];
            system.debug('lstEmp****'+lstEmp);
            objWrap.lstMentees =lstEmp;
            system.debug('objWrap****'+objWrap);
            return objWrap;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.fetchEmployeeDetails1', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    
    //code for profile picture end
    
    public class EmployeeWrapper{
        @auraEnabled
        public Nrich__Employee__c objEmployeeDetails{get;set;}
        @auraEnabled
        public list<Nrich__Employee__c> lstMentees{get;set;}
    }
    
    //code for OrgChart
    @AuraEnabled
    public static Nrich__Employee__c getOrgChartData(String empId){
        try{
            string strEmpID = userinfo.getuserid();
            list<Nrich__Employee__c> empList=new list<Nrich__Employee__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__ProfilePic__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__EmployeeDesignation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Mentor__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Email__c')
               && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Experience_in_Organization__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Phone__c')
               && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Date_of_Joining__c')
              )
                empList = [select Id, Name, Nrich__Mentor__c, Nrich__Mentor__r.Nrich__Mentor__c, Nrich__ProfilePic__c, Nrich__Mentor__r.Nrich__Mentor__r.Nrich__ProfilePic__c, Nrich__Mentor__r.Nrich__ProfilePic__c, Nrich__EmployeeDesignation__c,Nrich__Mentor__r.Name,Nrich__Mentor__r.Nrich__EmployeeDesignation__c,Nrich__Mentor__r.Nrich__Mentor__r.Name,Nrich__Mentor__r.Nrich__Mentor__r.Nrich__EmployeeDesignation__c from Nrich__Employee__c   where Nrich__Related_User__c=:strEmpID  ];
            if(empList!=NULL && empList.size()>0)
                return empList[0];
            else
                return null;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.getOrgChartData', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    @AuraEnabled
    public static list<Nrich__Certification_Request__c > viewrecords1()
    {
        try{
            string empUserId = userinfo.getUserId();
            list<Nrich__Certification_Request__c > cerdetails= new list<Nrich__Certification_Request__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Result__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Approval_Status__c') 
               && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Employeee__c')
               
              )
                cerdetails=[select Nrich__Result__c,Name,Nrich__Approval_Status__c ,Nrich__Certification_Name__r.Nrich__Certification_Name__c,Nrich__Certification_Name__r.Nrich__Cost__c,Nrich__Certification_Name__r.Nrich__Number_of_Successful_Certification_Exams__c,Nrich__Employeee__r.id from Nrich__Certification_Request__c where Nrich__Employeee__r.Nrich__Related_User__c =:empUserId ] ;
            system.debug('details' +cerdetails);
            return cerdetails;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.viewrecords1', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    
    //displaying mentors tile
    @AuraEnabled
    public static list<Nrich__Employee__c  > viewmentorrecords( )
    {
        try{
            string empUserId = userinfo.getUserId();
            list<Nrich__Employee__c> Empdetails= new list<Nrich__Employee__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Mentor__c'))
                Empdetails=[select id,Nrich__Mentor__r.name,Name from Nrich__Employee__c where Nrich__Mentor__r.Nrich__Related_user__c=:empUserId  ];
            system.debug('Empdetails'+Empdetails);
            return Empdetails;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.viewmentorrecords', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    //For displaying Assets Tile 
    @AuraEnabled
    public static list<Nrich__Asset_Allocation__c> viewAssetRecords()
    {
        try{
            string empUserId = userinfo.getUserId();
            list<Nrich__Asset_Allocation__c> AssetDetails= new list<Nrich__Asset_Allocation__c>();
            // if(ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Allocation__c', 'Nrich__Status__c')) 
            
            AssetDetails=[select id, Nrich__Asset_Items__r.name, Nrich__Status__c,Nrich__Asset_Items__r.Nrich__Asset__r.Name,Nrich__Asset_Items__r.Nrich__Asset__r.Nrich__IconType__c,
                          Nrich__Asset_Items__r.Nrich__Asset__r.Nrich__Designation__c from Nrich__Asset_Allocation__c where Nrich__Assigned_To__r.Nrich__Related_user__c=:empUserId ] ;
            System.debug('AssetDetails' +AssetDetails);
            return AssetDetails;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.viewAssetRecords', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    @AuraEnabled
    public static list<Nrich__Support_Ticket__c> viewSupportRecords(){
        try{
            string empUserId = userinfo.getUserId();
            list<Nrich__Support_Ticket__c> SupportDetails= New list<Nrich__Support_Ticket__c>();
            /*  if(ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Department_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Department__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Issue_Description__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Location__c')
&& ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Priority__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Proposed_solution__c')
&& ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c ', 'Nrich__Status__c') 
)*/
            SupportDetails=[select id,name,Nrich__Department_Name__c, Nrich__Department__c, Nrich__Issue_Description__c ,Nrich__Location__c ,Nrich__Priority__c ,Nrich__Proposed_solution__c,Nrich__Status__c ,Nrich__Subject__c from Nrich__Support_Ticket__c where Nrich__Employeee__r.Nrich__Related_user__c=:empUserId];
            system.debug('SupportDetails' +SupportDetails);
            return SupportDetails;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.viewSupportRecords', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    @AuraEnabled
    public static list<Nrich__Timesheet_Detail__c>ViewTimeSheetDetails(){
        try{
            string empUserId = userinfo.getUserId();
            list<Nrich__Timesheet_Detail__c> TimeSheetDetails=new list<Nrich__Timesheet_Detail__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Timesheet_Detail__c', 'Nrich__Activity__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Timesheet_Detail__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Timesheet_Detail__c', 'Nrich__Employee_Name__c') 
              )
                TimeSheetDetails=[select Nrich__Activity__c,Nrich__Status__c ,Nrich__Employee_Name__c,Nrich__Timesheet__r.Name,Nrich__Timesheet__r.Nrich__Employeee__c,Nrich__Project_Name__c ,
                                  Nrich__Timesheet__r.Nrich__Date__c,Nrich__Timesheet__r.Nrich__TotalHours__c from Nrich__Timesheet_Detail__c where Nrich__Timesheet__r.Nrich__Employeee__r.Nrich__Related_user__c=:empUserId];
            System.debug('TimeSheetDetails' +TimeSheetDetails);
            
            if(TimeSheetDetails.size()>0 && TimeSheetDetails!=NULL )
                return TimeSheetDetails;
            else
                return null;
            
            
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.ViewTimeSheetDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    
    @AuraEnabled
    public static wrapperclass1 ViewProjectteammemberDetails(){
        try{
            wrapperclass1 wrap=new wrapperclass1();
            string empUserId = userinfo.getUserId();
            list<Nrich__Project_Team_Member__c > ProjectDetails=new list<Nrich__Project_Team_Member__c >();
            list<Nrich__Project_Team_Member__c> PastProjectDetails=new list<Nrich__Project_Team_Member__c>();
            List<Nrich__Project_Team_Member__c> OwnProjectDetails=new list<Nrich__Project_Team_Member__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Employee_Start_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Employee_End_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__ProjectManagerName__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Project_Name__c')
              )
                ProjectDetails=[select Name, Nrich__Employee_Start_Date__c, Nrich__Employee_End_Date__c, Nrich__ProjectManagerName__c, Nrich__Project_Name__c from Nrich__Project_Team_Member__c where Nrich__Active__c=TRUE AND Nrich__Employeee__r.Nrich__Related_user__c=:empUserId];
            
            PastProjectDetails=[select Name, Nrich__Employee_Start_Date__c, Nrich__Employee_End_Date__c, Nrich__ProjectManagerName__c, Nrich__Project_Name__c from Nrich__Project_Team_Member__c where Nrich__Active__c=FALSE AND Nrich__Employeee__r.Nrich__Related_user__c=:empUserId];    
            
            OwnProjectDetails=[select name, Nrich__Employee_Start_Date__c, Nrich__Employee_End_Date__c, Nrich__ProjectManagerName__c, Nrich__Project_Name__c from Project_Team_Member__c where Nrich__Active__c=TRUE AND  Nrich__Project__r.Nrich__ProjectManagerr__r.Nrich__Related_User__c=:empUserId];        
            
            wrap.CurrentProjectteamList=ProjectDetails;
            wrap.PastProjectteamList=PastProjectDetails;
            wrap.OwnProjectteamList=OwnProjectDetails;
            
            if(wrap!=NULL )
                return wrap;
            else
                return null;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeDashboardController.ViewProjectteammemberDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    @AuraEnabled
    public static list<Nrich__Expense__c> ViewExpenserecords(){
        try{
            Nrich__Employee__c objEmp = new Nrich__Employee__c();
            strEmployeeID=userinfo.getUserId();
            objEmp = [select id
                      from Nrich__Employee__c 
                      where Nrich__Related_User__c =:strEmployeeID
                      limit 1]; 
            list<Nrich__Expense__c>ExpenseList=[select id,Name, Nrich__Approver_Name__c, Nrich__End_Date__c,Nrich__Start_Date__c, 
                                                Nrich__Expense_Amount__c, Nrich__Expense_Status__c, Nrich__Expense_Type__c, Nrich__ProjectName__c,Nrich__SubmitterName__c,
                                                Nrich__Reimbursed_Amount__c from Nrich__Expense__c  where Nrich__Submitter__c=:objEmp.Id
                                               ];
            return ExpenseList;
            
        }
        Catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ExpenseModalController.SaveFile', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
        
        
        
        
    }
    
    
    
    public class wrapperclass1{
        @AuraEnabled
        public list<Nrich__Project_Team_Member__c> CurrentProjectteamList;
        @AuraEnabled
        public list<Nrich__Project_Team_Member__c> PastProjectteamList;
        @AuraEnabled
        public list<Nrich__Project_Team_Member__c> OwnProjectteamList;
        
        
    }
    
}