/****************************************************************************************************
* Company: Absyz
* Developer: Ashish Nag
* Created Date: 15/10/2018
* Description: Fetching records for Milestone Email template.
*****************************************************************************************************/ 
public with sharing class MileStoneEmailTemplateController {
    public String objType {get;set;}
    public ID relatedId {get;set;}   
    public list<Nrich__Milestone__c> MileStoneAppList{get;set;}
    
    public list<Nrich__Milestone__c> getMileStonedetailslist(){
        try{
        System.debug('relatedId' +relatedId);
        List<Nrich__Milestone__c> MileStoneAppList = new List<Nrich__Milestone__c>();
        if(relatedId != null && ObjectFieldAccessCheck.checkAccessible('Nrich__Milestone__c', 'Nrich__Account_Manager_Email__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Milestone__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Milestone__c', 'Nrich__Completion_Date__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__Milestone__c', 'Nrich__Milestone_Percentage_Completed__c')
          ){
              MileStoneAppList = [select Id,Name ,Nrich__Project__r.Nrich__Account_Manager__r.Name, Nrich__Account_Manager_Email__c  ,Nrich__Status__c ,Nrich__Completion_Date__c ,Nrich__Project__r.Name,Nrich__Milestone_Percentage_Completed__c from Nrich__Milestone__c where id = : relatedId ];
              system.debug('MileStoneAppList' +MileStoneAppList);
          }
        return MileStoneAppList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='MileStoneEmailTemplateController.getMileStonedetailslist', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}