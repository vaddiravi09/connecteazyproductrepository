@isTest
public class DynamicFormCreationControllerTest {
 
    private static testmethod void testDynamicFormCreationController() {
        DynamicFormCreationController dfc = new DynamicFormCreationController();
        test.startTest();
        Nrich__InitialSetup__c iniSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
    	Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        Nrich__Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        Nrich__Job_Round__c jobRound = UtilityForTest.Job_Round_Utility(job.Id);
        Nrich__Template__c temp = UtilityForTest.Template_Utility(job.Id,jobRound.Id);
        Nrich__Section__c sec = UtilityForTest.Section_Utility(temp.Id);
        List<Nrich__Section_Fields__c> sfList = UtilityForTest.Section_Fields_Utility(sec.Id);
        PageReference pageRef = Page.Form; // Add your VF page Name here
        pageRef.getParameters().put('panelMembId', String.valueOf(empPanelMemb.Id));
        Test.setCurrentPage(pageRef);

        dfc.templateSelection(String.valueOf(temp.Id));
        temp.Nrich__Type_of_Template__c = 'Feedback';
        update temp;
        
        dfc.templateSelection(String.valueOf(temp.Id));
        
        
	        
      // dfc.insertCandidate(UtilityForTest.Section_Fields_Utility(sec.Id));
        test.stopTest();
        System.assertEquals('Test','Test');
        
    }
    
    private static testmethod void testDynamicFormCreationController1() {
    	test.startTest();
        Nrich__InitialSetup__c iniSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
    	Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        Nrich__Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        Nrich__Job_Round__c jobRound = UtilityForTest.Job_Round_Utility(job.Id);
        Nrich__Template__c temp = UtilityForTest.Template_Utility(job.Id,jobRound.Id);
        Nrich__Section__c sec = UtilityForTest.Section_Utility(temp.Id);
        List<Nrich__Section_Fields__c> sfList = UtilityForTest.Section_Fields_Utility(sec.Id);
        PageReference pageRef = Page.Form; // Add your VF page Name here
  		pageRef.getParameters().put('jobid', String.valueOf(job.Id));
        pageRef.getParameters().put('panelMembId', String.valueOf(empPanelMemb.Id));
        Test.setCurrentPage(pageRef);
        DynamicFormCreationController dfc = new DynamicFormCreationController();
        DynamicFormCreationController.pagedetails pd = new DynamicFormCreationController.pagedetails();
        pd.templateType = 'Application';
        dfc.templateSelection(String.valueOf(temp.Id));
        dfc.SaveData();
        test.stopTest();
    	System.assertEquals('Test','Test');
        
    }
    
    private static testmethod void testDynamicFormCreationController2() {
    	DynamicFormCreationController dfc = new DynamicFormCreationController();
        test.startTest();
        Nrich__InitialSetup__c iniSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
    	Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        Nrich__Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        Nrich__Job_Round__c jobRound = UtilityForTest.Job_Round_Utility(job.Id);
        Nrich__Template__c temp = UtilityForTest.Template_Utility(job.Id,jobRound.Id);
        temp.Nrich__Type_of_Template__c = 'Feedback';
        update temp;
        
        Nrich__Feedback__c fdback = new Nrich__Feedback__c();
        fdback.Nrich__Interview_Panel_Member__c = empPanelMemb.Id;
        fdback.Nrich__Interview_Round__c = irRecord.Id;
        insert fdback;
        
        Nrich__Section__c sec = UtilityForTest.Section_Utility(temp.Id);
        List<Nrich__Section_Fields__c> sfList = UtilityForTest.Section_Fields_Utility(sec.Id);
        PageReference pageRef = Page.Form; // Add your VF page Name here
  		pageRef.getParameters().put('jobid', String.valueOf(job.Id));
        pageRef.getParameters().put('panelMembId', String.valueOf(empPanelMemb.Id));
        pageRef.getParameters().put('interviewRoundid', String.valueOf(irRecord.Id));
        Test.setCurrentPage(pageRef);
        dfc.templateSelection(String.valueOf(temp.Id));
        dfc.SaveData();
        test.stopTest();
    	System.assertEquals('Test','Test');
        
    }
	
    private static testmethod void testDynamicFormCreationController3() {
    	test.startTest();
        Nrich__InitialSetup__c iniSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
    	Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        Nrich__Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        Nrich__Job_Round__c jobRound = UtilityForTest.Job_Round_Utility(job.Id);
        Nrich__Template__c temp = UtilityForTest.Template_Utility1(job.Id,jobRound.Id);
        Nrich__Section__c sec = UtilityForTest.Section_Utility(temp.Id);
        List<Nrich__Section_Fields__c> sfList = UtilityForTest.Section_Fields_Utility(sec.Id);
        PageReference pageRef = Page.Form; // Add your VF page Name here
  		pageRef.getParameters().put('jobid', String.valueOf(job.Id));
        pageRef.getParameters().put('panelMembId', String.valueOf(empPanelMemb.Id));
        pageRef.getParameters().put('candidateId', String.valueOf(cand.Id));
        Test.setCurrentPage(pageRef);
        DynamicFormCreationController dfc = new DynamicFormCreationController();
        DynamicFormCreationController.sectionfields sfs = new DynamicFormCreationController.sectionfields() ;
        sfs.sectionfieldvalueText = 'SampleData';
        sfs.sectionfieldvalueBoolean = true;
        sfs.sectionfieldvalueDecimal = 1.1;
        sfs.mappingFieldObjectName = 'Nrich__Additional_Candidate_Details__c';
        sfs.sectionfieldvalueBlob = Blob.valueOf('ajshdhoiu23ipeup3');
        dfc.templateSelection(String.valueOf(temp.Id));
        dfc.SaveData();
        dfc.fetchInterviewRound();
        Nrich__Job_Round__c jb=new Nrich__Job_Round__c();
        Nrich__Additional_Candidate_Details__c addcand=new Nrich__Additional_Candidate_Details__c();
        dfc.getPickValues(jb,'Nrich__Job_Round_Mode__c','Video Call');
        
       
        
        
        test.stopTest();
        
    	System.assertEquals('Test','Test');
        
    }
    
    private static testmethod void  testDynamicFormCreationController4(){
        test.startTest();
        Nrich__InitialSetup__c iniSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
    	Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        Nrich__Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        
        Nrich__Department__c  testDepartment = new Nrich__Department__c();
        testDepartment.Name='testDepartment';
        insert testDepartment;
        
        
 Nrich__Job__c testJob =new Nrich__Job__c();
        testJob.Nrich__Department__c=testDepartment.id;
        testJob.Nrich__Approver__c = UserInfo.getUserId();
        testJob.Nrich__End_Date__c=system.today().adddays(4);
        testJob.Nrich__Start_Date__c=system.today();
        testJob.Nrich__Open_Positions__c=1;
        insert testJob;
        
         Nrich__Job_Round__c jobRound = new Nrich__Job_Round__c();
        jobRound.Name = 'Technical 1';
        jobRound.Nrich__Job__c = testJob.Id;
        insert jobRound;
        
        
 Nrich__Template__c temp = new Nrich__Template__c();
        temp.Name = 'Application';
        temp.Nrich__Job__c =testJob.Id;
        //temp.Nrich__Job_Round__c = jobRoundId;
        temp.Nrich__Type_of_Template__c = 'Application';
        temp.Nrich__Mapping_Object_API_Name__c = 'Nrich__Job_Application__c';
        temp.Nrich__Background_Color__c = '#36D722';
        temp.Nrich__Is_Active__c = true;
        insert temp;
        
        Nrich__Section__c sec = new Nrich__Section__c();
        sec.Name = 'Section 1';
        sec.Nrich__Order__c = 1;
        sec.Nrich__Template__c =temp.id ;
        insert sec;
        
        Nrich__Section_Fields__c sf = new Nrich__Section_Fields__c();
        sf.Name = 'Section Field 1';
        sf.Nrich__Coloumn__c = 'Column-1';
        sf.Nrich__Order__c = 1;
        sf.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf.Nrich__Field_Type__c = 'String';
        sf.Nrich__Mapping_Field_API_Name__c = 'Name';
        sf.Nrich__Section__c = sec.id;
        sf.Nrich__Mandatory__c = false;
        insert sf;
        list<Nrich__Section_Fields__c> sectionfieldslist=new list<Nrich__Section_Fields__c>();
        sectionfieldslist.add(sf);
        
        
        
        LIST<DynamicFormCreationController.sectionfields>  wrapperInstance = new LIST<DynamicFormCreationController.sectionfields>();
        
       DynamicFormCreationController.sectionfields s=new DynamicFormCreationController.sectionfields();
        s.sectionfieldId=sf.id;
        s.sectionfieldname='Section Field 1';
        s.mappingFieldAPIName='Name';
        s.mappingFieldObjectName='Job_Application__c';
        s.sectionfieldtype='String';
        s.sectionfieldMandatory=false;
        s.sectionfieldlength=30;
        wrapperInstance.add(s);
        
         DynamicFormCreationController dfc = new DynamicFormCreationController();
        dfc.insertCandidate(wrapperInstance);
        Nrich__Additional_Candidate_Details__c addc=new Nrich__Additional_Candidate_Details__c();
        dfc.objectdetails(addc,wrapperInstance,'Nrich__Additional_Candidate_Details__c');
      System.assertEquals('expected', 'expected');


        
        test.stopTest();
    }
    
    
        public class sectionfields{ 
        public Id sectionfieldId {get; set;}
        public String sectionfieldname {get; set;}
        public String mappingFieldAPIName {get; set;}
        public String mappingFieldObjectName {get; set;}
        public String sectionfieldtype {get; set;}
        public Boolean sectionfieldMandatory {get; set;}
        public Decimal sectionfieldlength {get; set;}
        public String sectionfieldRegx {get; set;}   
        public Integer sectionfieldOrderNumber{get; set;}
        public String sectionfieldColumnNumber{get; set;}
        public String sectionfieldvalueText {get; set;}
        public Decimal sectionfieldvalueDecimal {get; set;}  
        public String sectionfieldvalueDate {get; set;}
        public String sectionfieldvalueDateTime {get; set;}
        public transient blob sectionfieldvalueBlob {get; set;}
        public Boolean sectionfieldvalueBoolean {get; set;} 
        public Boolean sectionfieldresume {get; set;} 
    } 


}