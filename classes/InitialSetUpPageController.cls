public class InitialSetUpPageController {
    @AuraEnabled
    public static void savedetails(Boolean selectResult1, Boolean selectResult2, String Approved,String Submitted,String Holiday,String Leave, Integer TotalHrs){
        try{
        Nrich__InitialSetup__c settings = Nrich__InitialSetup__c.getOrgDefaults();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Approved__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Submitted__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Holiday__c') &&
           ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Leave__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__TotalHoursCap__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Timesheet_Type__c')
          )
            settings.Nrich__Approved__c=Approved;
        settings.Nrich__Submitted__c=Submitted;
        settings.Nrich__Holiday__c=Holiday;
        settings.Nrich__Leave__c=Leave;
        settings.Nrich__TotalHoursCap__c=TotalHrs;
        
        if(selectResult1==TRUE && selectResult2==FALSE){
            settings.Nrich__Timesheet_Type__c =Label.Monthly;
        }
        else if(selectResult1==FALSE && selectResult2==TRUE){
            settings.Nrich__Timesheet_Type__c = Label.Weekly;
        }
        upsert settings Nrich__InitialSetup__c.Id;
        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='InitialSetUpPageController.savedetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    //Employee Module Initial Setup Method
    @AuraEnabled
    public static void EmpSetupsaveDetails(Boolean IsProfileTileRequired, Boolean IsAchievementsTileRequired, Boolean IsCertificationTileRequired, Boolean IsLeaveChartTileRequired, Boolean IsMentorMenteeTileRequired, Boolean IsOrgChartTileRequired, Boolean IsAssetTileRequired , Boolean IsTravelTileRequired  ,Boolean IsSupportTileRequired  , Boolean IsPolicyTileRequired , Boolean esignature, String CertificationUserId,String LeaveRequestUserId, String AssetUserId, String PolicyUserId ,Boolean IsTimeSheetTilerequired ,Boolean IsProjectTilerequired, Boolean IsExpenseTilerequired   ,integer Due_Days_For_Certification_Request,integer Due_Days_For_Leave_Request,Boolean AutomaticAssigment){
        try{ 
        List<Nrich__InitialSetup__c> cstList= new list<Nrich__InitialSetup__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__ESignature_for_Policy_Approval__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_Achievements_Tile_Required__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_Certification_Tile_Required__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_LeaveChart_Tile_Require__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_TimeSheet_Tile_Required_c__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_Project_Tile_Required__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_MentorMentee_Tile_Required__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_OrgChart_Tile_Required__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_Profile_Tile_Required__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_Asset_Tile_Required__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_Travel_Request_Tile_Required__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_Support_Request_Tile_Required__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_Policy_Tile_Required__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Number_of_DueDays_For_Certification_Req__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__approver_for_certificationid__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__approver_for_leaveid__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Approver_For_AssetId__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Approver_For_PolicyId__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Number_Of_DueDays_For_Leave_Request__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Automatic_Assignment_of_Support_Tickets__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Is_Expense_Tile_Required__c')
          )
            
            
            cstList= [SELECT id, Nrich__ESignature_for_Policy_Approval__c, Nrich__Is_Achievements_Tile_Required__c,Nrich__Is_Certification_Tile_Required__c,Nrich__Is_LeaveChart_Tile_Require__c,Nrich__Is_TimeSheet_Tile_Required_c__c ,Nrich__Is_Project_Tile_Required__c,Nrich__Is_Expense_Tile_Required__c ,
                      Nrich__Is_MentorMentee_Tile_Required__c,Nrich__Is_OrgChart_Tile_Required__c,Nrich__Is_Profile_Tile_Required__c, Nrich__Is_Asset_Tile_Required__c, Nrich__Is_Travel_Request_Tile_Required__c  ,Nrich__Is_Support_Request_Tile_Required__c  ,Nrich__Is_Policy_Tile_Required__c  ,Nrich__Number_of_DueDays_For_Certification_Req__c,Nrich__approver_for_certificationid__c ,Nrich__approver_for_leaveid__c ,Nrich__Approver_For_AssetId__c ,Nrich__Approver_For_PolicyId__c ,
                      Nrich__Number_Of_DueDays_For_Leave_Request__c, Nrich__Automatic_Assignment_of_Support_Tickets__c  FROM Nrich__InitialSetup__c ];
        
        
        
        system.debug('cstlist'+cstList);
        if(cstList.size()>0){
            List<Nrich__InitialSetup__c> updateList =new List<Nrich__InitialSetup__c>();
            for(Nrich__InitialSetup__c cstobj : cstList ){
                Nrich__InitialSetup__c obj = new Nrich__InitialSetup__c();
                obj.Id = cstobj.Id;
                obj.Nrich__Is_Profile_Tile_Required__c =IsProfileTileRequired!=Null?IsProfileTileRequired:false;
                obj.Nrich__Is_Certification_Tile_Required__c =IsCertificationTileRequired!=NUll?IsCertificationTileRequired:false;
                obj.Nrich__Is_LeaveChart_Tile_Require__c =IsLeaveChartTileRequired!=Null?IsLeaveChartTileRequired:false;
                obj.Nrich__Is_MentorMentee_Tile_Required__c =IsMentorMenteeTileRequired!=NUll?IsMentorMenteeTileRequired:false;
                obj.Nrich__Is_OrgChart_Tile_Required__c =IsOrgChartTileRequired!=NULL?IsOrgChartTileRequired:false;
                obj.Nrich__Is_Achievements_Tile_Required__c =IsAchievementsTileRequired!=NUll?IsAchievementsTileRequired:false;
                obj.Nrich__Is_Asset_Tile_Required__c =IsAssetTileRequired!=NULL?IsAssetTileRequired:false;
                obj.Nrich__Is_Travel_Request_Tile_Required__c =IsTravelTileRequired!=NULL?IsTravelTileRequired:false;
                obj.Nrich__Is_Support_Request_Tile_Required__c =IsSupportTileRequired!=NULL?IsSupportTileRequired:false;
                obj.Nrich__Is_Policy_Tile_Required__c =IsPolicyTileRequired!=NULL?IsPolicyTileRequired:false;
                obj.Nrich__Is_TimeSheet_Tile_Required_c__c =IsTimeSheetTilerequired!=NULL?IsTimeSheetTilerequired:false;
                obj.Nrich__Is_Project_Tile_Required__c =IsProjectTilerequired!=NULL?IsProjectTilerequired:false;
                obj.Nrich__Is_Expense_Tile_Required__c=IsExpenseTilerequired!=NULL?IsExpenseTilerequired:false;
                obj.Nrich__ESignature_for_Policy_Approval__c = esignature!=NULL?esignature:false;
                obj.Nrich__Automatic_Assignment_of_Support_Tickets__c =AutomaticAssigment!=NULL?AutomaticAssigment:false;
                obj.Nrich__Number_Of_DueDays_For_Leave_Request__c =Integer.valueOf(Due_Days_For_Leave_Request);
                obj.Nrich__Number_of_DueDays_For_Certification_Req__c =Integer.valueOf(Due_Days_For_Certification_Request);
                obj.Nrich__approver_for_certificationid__c =CertificationUserId;
                obj.Nrich__approver_for_leaveid__c =LeaveRequestUserId;
                obj.Nrich__Approver_For_AssetId__c =AssetUserId;
                obj.Nrich__Approver_For_PolicyId__c =PolicyUserId;
                
                updateList.add(obj);
            }
            update updateList;
            System.debug('updateList' +updateList);
        }
        else{
            InitialSetup__c obj = new InitialSetup__c();
            obj.Nrich__Is_Profile_Tile_Required__c=IsProfileTileRequired!=Null?IsProfileTileRequired:false;
            obj.Nrich__Is_Certification_Tile_Required__c=IsCertificationTileRequired!=NUll?IsCertificationTileRequired:false;
            obj.Nrich__Is_LeaveChart_Tile_Require__c=IsLeaveChartTileRequired!=Null?IsLeaveChartTileRequired:false;
            obj.Nrich__Is_MentorMentee_Tile_Required__c=IsMentorMenteeTileRequired!=NUll?IsMentorMenteeTileRequired:false;
            obj.Nrich__Is_OrgChart_Tile_Required__c=IsOrgChartTileRequired!=NULL?IsOrgChartTileRequired:false;
            obj.Nrich__Is_Achievements_Tile_Required__c=IsAchievementsTileRequired!=NUll?IsAchievementsTileRequired:false;
            obj.Nrich__Is_Asset_Tile_Required__c=IsAssetTileRequired!=NULL?IsAssetTileRequired:false;
            obj.Nrich__Is_Travel_Request_Tile_Required__c=IsTravelTileRequired!=NULL?IsTravelTileRequired:false;
            obj.Nrich__Is_Support_Request_Tile_Required__c=IsSupportTileRequired!=NULL?IsSupportTileRequired:false;
            obj.Nrich__Is_Policy_Tile_Required__c=IsPolicyTileRequired!=NULL?IsPolicyTileRequired:false;
            obj.Nrich__Is_TimeSheet_Tile_Required_c__c =IsTimeSheetTilerequired!=NULL?IsTimeSheetTilerequired:false;
            obj.Nrich__Is_Project_Tile_Required__c =IsProjectTilerequired!=NULL?IsProjectTilerequired:false;
            obj.Nrich__Is_Expense_Tile_Required__c=IsExpenseTilerequired!=NULL?IsExpenseTilerequired:false;
            obj.Nrich__ESignature_for_Policy_Approval__c = esignature!=NULL?esignature:false;
            obj.Nrich__Automatic_Assignment_of_Support_Tickets__c =AutomaticAssigment!=NULL?AutomaticAssigment:false;
            obj.Nrich__Number_Of_DueDays_For_Leave_Request__c=Due_Days_For_Leave_Request;
            obj.Nrich__Number_of_DueDays_For_Certification_Req__c=Due_Days_For_Certification_Request;
            obj.Nrich__approver_for_certificationid__c=CertificationUserId;
            obj.Nrich__approver_for_leaveid__c=LeaveRequestUserId;
            obj.Nrich__Approver_For_AssetId__c=AssetUserId;
            obj.Nrich__Approver_For_PolicyId__c=PolicyUserId;
            insert  obj;
            System.debug('insertion' +obj);
        }
        if(CertificationUserId!=NUll && LeaveRequestUserId!=NUll && AssetUserId!=NULL && PolicyUserId!=NULL){
            
            InsertUsersToQueueClass.insertusersQueue(CertificationUserId, LeaveRequestUserId,AssetUserId,PolicyUserId);
        }
        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='InitialSetUpPageController.EmpSetupsaveDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    //Project Module Initial Setup Method
    @AuraEnabled
    public static void  PrjInitialSetupSavedetails(String Completed ,String Cancelled, String Onhold, String Progress, String Open, String Fixed, String QAFailed, String QAInProgress, String ReadyforBuild, String ReadyforDesign){
        try{
        Nrich__InitialSetup__c settings = Nrich__InitialSetup__c.getOrgDefaults();
        
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Cancelled_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Completed_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Inprogress_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Onhold_Color__c') &&
           ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__New_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Fixed_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__QA_Failed_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__QA_In_Pogress_Color__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Ready_for_Build_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Ready_for_Design_Color__c')
          )
            settings.Nrich__Cancelled_Color__c =Cancelled;
        settings.Nrich__Completed_Color__c =Completed;
        settings.Nrich__Inprogress_Color__c =Progress;
        settings.Nrich__Onhold_Color__c =Onhold;
        settings.Nrich__New_Color__c    =Open;
        settings.Nrich__Fixed_Color__c =Fixed;
        settings.Nrich__QA_Failed_Color__c =QAFailed;
        settings.Nrich__QA_In_Pogress_Color__c =QAInProgress;
        settings.Nrich__Ready_for_Build_Color__c =ReadyforBuild;
        settings.Nrich__Ready_for_Design_Color__c =ReadyforDesign;
        Schema.DescribeSObjectResult objectDescriptioncontentdoc = Nrich__InitialSetup__c.getSObjectType().getDescribe();
        Database.UpsertResult result=database.upsert(settings);
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='InitialSetUpPageController.PrjInitialSetupSavedetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    
    @AuraEnabled
    public static Nrich__InitialSetup__c GetDetails(){
        try{
        Nrich__InitialSetup__c  InitialSetup = Nrich__InitialSetup__c.getOrgDefaults(); 
        /*Nrich__InitialSetup__c InitialSetup= [SELECT id, Nrich__ESignature_for_Policy_Approval__c, Nrich__Is_Achievements_Tile_Required__c,Nrich__Is_Certification_Tile_Required__c,Nrich__Is_LeaveChart_Tile_Require__c,Nrich__Is_TimeSheet_Tile_Required_c__c ,Nrich__Is_Project_Tile_Required__c,
Nrich__Is_MentorMentee_Tile_Required__c,Nrich__Is_OrgChart_Tile_Required__c,Nrich__Is_Profile_Tile_Required__c, Nrich__Is_Asset_Tile_Required__c, Nrich__Is_Travel_Request_Tile_Required__c  ,Nrich__Is_Support_Request_Tile_Required__c  ,Nrich__Is_Policy_Tile_Required__c  ,Nrich__Number_of_DueDays_For_Certification_Req__c,Nrich__approver_for_certificationid__c ,Nrich__approver_for_leaveid__c ,Nrich__Approver_For_AssetId__c ,Nrich__Approver_For_PolicyId__c,
Nrich__Number_Of_DueDays_For_Leave_Request__c  FROM Nrich__InitialSetup__c];*/
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__approver_for_certificationid__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__approver_for_leaveid__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Approver_For_AssetId__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Approver_For_PolicyId__c'))
            
            system.debug('OrgDefaults' +InitialSetup.Nrich__approver_for_certificationid__c);
        list<user> u1 = [select id,Name from User where id =:InitialSetup.Nrich__approver_for_certificationid__c];
        if(u1!=NULL && u1.size()>0)
            InitialSetup.Nrich__approver_for_certificationid__c = u1[0].Name;
        else
            InitialSetup.Nrich__approver_for_certificationid__c = '';
        list<user> u2 = [select id,Name from User where id =:InitialSetup.Nrich__approver_for_leaveid__c ];
        if(u2!=NULL && u2.size()>0)
            InitialSetup.Nrich__approver_for_leaveid__c  = u2[0].Name;
        else
            InitialSetup.Nrich__approver_for_leaveid__c  = '';
        list<user> u3=[select id,Name from User where id=:InitialSetup.Nrich__Approver_For_AssetId__c ];
        if(u3!=NULL && u3.size()>0)
            InitialSetup.Nrich__Approver_For_AssetId__c =u3[0].Name;
        else
            InitialSetup.Nrich__Approver_For_AssetId__c ='';
        list<user> u4=[select id,name from User where id=:InitialSetup.Nrich__Approver_For_PolicyId__c ];
        if(u4!=NULL && u4.size()>0)
            InitialSetup.Nrich__Approver_For_PolicyId__c =u4[0].Name;
        else
            InitialSetup.Nrich__Approver_For_PolicyId__c ='';
        return InitialSetup;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='InitialSetUpPageController.GetDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    //RecruitMent Initial Setup Method
    @AuraEnabled
    public static void saveRecruitmentDetails(Double number1, Boolean boolean1, Boolean boolean2, Boolean boolean3, Boolean boolean4, Boolean boolean5, Boolean boolean6){
        try{
        List<Nrich__InitialSetup__c> cstList= new list<Nrich__InitialSetup__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__No_of_Days_for_Reapplication_for_SameJob__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Email_Candidate_on_submission_of_appln__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Interview_Scheduled_Email_Checkbox__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Automated_ApprovalProcess_on_JobCreation__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Feedback_for_all_previous_rounds__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Success_Email_completion_on_each_Round__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__InitialSetup__c', 'Nrich__Rejection_Email_to_Candidate__c')
          )
            cstList= [SELECT id,Nrich__No_of_Days_for_Reapplication_for_SameJob__c,Nrich__Email_Candidate_on_submission_of_appln__c,Nrich__Interview_Scheduled_Email_Checkbox__c,
                      Nrich__Automated_ApprovalProcess_on_JobCreation__c,Nrich__Feedback_for_all_previous_rounds__c,Nrich__Success_Email_completion_on_each_Round__c,Nrich__Rejection_Email_to_Candidate__c FROM Nrich__InitialSetup__c];
        if(cstList.size()>0){
            List<Nrich__InitialSetup__c> updateList =new List<Nrich__InitialSetup__c>();
            for(Nrich__InitialSetup__c cstobj : cstList ){
                Nrich__InitialSetup__c obj = new Nrich__InitialSetup__c();
                obj.Id = cstobj.Id;
                obj.Nrich__No_of_Days_for_Reapplication_for_SameJob__c = Integer.valueOf(number1);
                
                //obj.Nrich__Email_Panel_Member_on_Interview_Schedule__c = boolean1!=NULL?boolean1:false;
                obj.Nrich__Interview_Scheduled_Email_Checkbox__c =boolean1!=NULL?boolean1:false;
                obj.Nrich__Email_Candidate_on_submission_of_appln__c = boolean2!=NULL?boolean2:false;
                obj.Nrich__Automated_ApprovalProcess_on_JobCreation__c = boolean3!=NULL?boolean3:false;
                obj.Nrich__Feedback_for_all_previous_rounds__c = boolean4!=NULL?boolean4:false;
                obj.Nrich__Success_Email_completion_on_each_Round__c = boolean5!=NULL?boolean5:false;
                obj.Nrich__Rejection_Email_to_Candidate__c = boolean6!=NULL?boolean6:false;
                updateList.add(obj);
            }
            update updateList;
        }
        
        else{
            Nrich__InitialSetup__c obj = new Nrich__InitialSetup__c();
            obj.Nrich__No_of_Days_for_Reapplication_for_SameJob__c = Integer.valueOf(number1);
            // obj.Nrich__Email_Panel_Member_on_Interview_Schedule__c = boolean1!=NULL?boolean1:false;
            obj.Nrich__Interview_Scheduled_Email_Checkbox__c =boolean1!=NULL?boolean1:false;
            obj.Nrich__Email_Candidate_on_submission_of_appln__c = boolean2!=NULL?boolean2:false;
            obj.Nrich__Automated_ApprovalProcess_on_JobCreation__c = boolean3!=NULL?boolean3:false;
            obj.Nrich__Feedback_for_all_previous_rounds__c = boolean4!=NULL?boolean4:false;
            obj.Nrich__Success_Email_completion_on_each_Round__c = boolean5!=NULL?boolean5:false;
            obj.Nrich__Rejection_Email_to_Candidate__c = boolean6!=NULL?boolean6:false;           
            insert obj;            
        }
    } catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='InitialSetUpPageController.saveRecruitmentDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}