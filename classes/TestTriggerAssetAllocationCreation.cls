@isTest()
public with sharing class TestTriggerAssetAllocationCreation {
    
    @isTest()
    public static void creation()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0);  
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u1=new User(Alias = 'sr12356', Email='standardusedfdr@testorg.com', 
                         EmailEncodingKey='UTF-8', LastName='Testing1222', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p1.Id, 
                         TimeZoneSidKey='America/Los_Angeles', UserName='standarduseefer@testorg.com123');
        
        User u = new User(Alias = 's12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123',ManagerId =u1.id);
        
         Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                          
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        insert lleave;
        cleave.Nrich__Is_Active__c=True;
        update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                        
                                        Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
      
        
        Nrich__Asset__c assett=new Nrich__Asset__c ();
        assett.Name='Test11';
        insert assett;
        
        list<Nrich__Asset_Request__c>AssetRequestList=new list<Nrich__Asset_Request__c>();
        Nrich__Asset_Request__c Asq1=new Nrich__Asset_Request__c(Nrich__Asset_Category__c =assett.id, Nrich__Reason__c='Testingg',Nrich__Employeee__c=emp.id,Nrich__Status__c='Approved');
        insert Asq1;
        Nrich__Asset_Request__c   Asq =new Nrich__Asset_Request__c(Nrich__Asset_Category__c =assett.id, Nrich__Reason__c='Testingg',Nrich__Employeee__c=emp.id,Nrich__Status__c='Applied ');
        insert Asq;
        
        Asq.Nrich__Status__c='Approved';
        update Asq;
        AssetRequestList.add(Asq);
        
        
        Nrich__Asset_Items__c assetitems=new Nrich__Asset_Items__c();
        assetitems.Name='Abc';
        assetitems.Nrich__Asset__c= assett.id;
        assetitems.Nrich__Status__c='Available';
        insert assetitems;
        
        Nrich__Asset_Allocation__c AssetAllocation=new Nrich__Asset_Allocation__c();
        AssetAllocation.Nrich__Asset_Items__c=assetitems.id;
        AssetAllocation.Nrich__Allocation_EndDate__c=System.today();
        AssetAllocation.Nrich__Status__c='Pending';
        list<Nrich__Asset_Allocation__c> AllocationList=new list<Nrich__Asset_Allocation__c>();
        AllocationList.add(AssetAllocation);
        try{
            Nrich__Asset_Request__c AssetAllocation1=new Nrich__Asset_Request__c();
            AssetAllocation1.Nrich__Status__c='12aaadsdsad';
            insert AssetAllocation1;
        }
        catch(exception e){
            
            
        }
              
        
        System.assertEquals('Pending', AssetAllocation.Nrich__Status__c);
        
        
        
    }
    
}