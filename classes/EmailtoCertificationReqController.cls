/********************************************************************
* Company: Absyz
* Developer: Ashish
* Created Date: 16/03/2019
* Description: EmailtoCertification Req Controller 
********************************************************************/
public with sharing class EmailtoCertificationReqController {
    public String objType {get;set;}
    public ID relatedId {get;set;}
    public list<Nrich__Certification_Request__c> CertificationAppList{get;set;}
    
    public List<Nrich__Certification_Request__c> getCertificationList (){
        try{
            List<Nrich__Certification_Request__c> CertificationAppList = new List<Nrich__Certification_Request__c>();
            if(relatedId != null){
                if(ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Approval_Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Result__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Verification_id__c'))
                    CertificationAppList = [select Id, Nrich__Certification_Name__r.Name ,Nrich__Employeee__r.Name , Nrich__Approval_Status__c ,Nrich__Result__c ,Nrich__Verification_id__c,Name from Nrich__Certification_Request__c where id = : relatedId];
                system.debug('CertificationAppList' +CertificationAppList);
            }
            return CertificationAppList;
        }
        catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmailtoCertificationReqController.getCertificationList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return CertificationAppList;
        }
    }
}