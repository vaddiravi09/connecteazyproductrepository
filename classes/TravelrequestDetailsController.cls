/********************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 12/02/2019
* Description: Controller for Travel Request Details Component
********************************************************************/
public class TravelrequestDetailsController {
    @AuraEnabled
    public static Employee__c fetchEmployeeDetails(){
       try{
        string empUserId = userinfo.getUserId();
        list<Nrich__Employee__c> EmployeeList =New list<Nrich__Employee__c>();
        IF(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Related_User__c')){
            EmployeeList = [Select Id, Name from Nrich__Employee__c where Nrich__Related_User__c=:empUserId];
        }
        return EmployeeList[0];
       }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TravelrequestDetailsController.fetchEmployeeDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
          return null;
       }
    }
    
    @AuraEnabled
    public static list<string> fetchreasonfortraveloptions(){
       try{
        list<String> options=new list<string>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Reason_For_Travel__c')){
            Schema.DescribeFieldResult fieldResult = Nrich__Travel_Request__c.Nrich__Reason_For_Travel__c.getDescribe();
            options = Optionshelper(fieldResult, true);
        }
          return options;
        }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TravelrequestDetailsController.fetchreasonfortraveloptions', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
           return null;
       }
      }
    
    @AuraEnabled
    public static list<string> fetchRequestType(){
       try{ 
        list<String> options=new list<string>();
         if(ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Request_Type__c')){
            Schema.DescribeFieldResult fieldResult = Nrich__Travel_Request__c.Nrich__Request_Type__c.getDescribe();
           options = Optionshelper(fieldResult, true);
         }
        return options;
       }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TravelrequestDetailsController.fetchRequestType', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
          return null;
       }
    }
    
    @AuraEnabled
    public static list<string> fetchStatus(){
      try{ 
        list<String> options=new list<string>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Status__c')){
        Schema.DescribeFieldResult fieldResult = Nrich__Travel_Request__c.Nrich__Status__c.getDescribe();
        options = Optionshelper(fieldResult, false);
        system.debug('options===>'+options);
        }
        return options;
      }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TravelrequestDetailsController.fetchStatus', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
          return null;
       }
    }
    
    @AuraEnabled
    public static list<string> fetchModeofTransport(){
      try{
        list<String> options=new list<string>();
         if(ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Mode_of_Transport__c')){
            Schema.DescribeFieldResult fieldResult = Nrich__Travel_Request__c.Nrich__Mode_of_Transport__c.getDescribe();
            options = Optionshelper(fieldResult, true);
         }
        return options;
      }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TravelrequestDetailsController.fetchModeofTransport', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
          return null;
       }
    }
    
    
    public static list<String> Optionshelper(Schema.DescribeFieldResult fieldval, boolean empty){
        list<String> options = new list<String>();
        List<Schema.PicklistEntry> ple = fieldval.getPicklistValues();
        if(empty)
            options.add('--None--');
        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getValue());
        }       
        return options;
    }
    
    @AuraEnabled
    public static saveresultStatus saveEmployeeTravelRequest(Nrich__Travel_Request__c TravelRequest, String travellers, String empid){
        try{
            if(TravelRequest!=NULL && String.isNotBlank(travellers) && String.isNotBlank(empid) && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Employeee__c')){
                TravelRequest.Nrich__Employeee__c  = empid;
                database.saveresult result = database.insert(TravelRequest, false);
                saveresultStatus resultval = new saveresultStatus();
                resultval.isSuccess = result.isSuccess();
                resultval.recordId = result.getId();
                if(result.getErrors().size()>0)
                    resultval.message = result.getErrors()[0].message;
                list<Nrich__Employee__c> TravellersList = (List<Nrich__Employee__c>)JSON.deserialize(travellers, list<Nrich__Employee__c>.class);
                if(TravellersList!=NULL && TravellersList.size()>0 && ObjectFieldAccessCheck.checkAccessible('Nrich__Travellers_List__c', 'Nrich__Employeee__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Travellers_List__c', 'Nrich__Travel_Request__c') ){
                    list<Nrich__Travellers_List__c> TravellersInsertList = new list<Nrich__Travellers_List__c>();
                    for(Nrich__Employee__c travellist : TravellersList){
                        Nrich__Travellers_List__c tl = new Nrich__Travellers_List__c();
                        tl.Nrich__Employeee__c = travellist.Id;
                        tl.Nrich__Travel_Request__c = result.getId();
                        TravellersInsertList.add(tl);
                    }
                    list<database.saveresult> travellistresult = database.insert(TravellersInsertList, false);
                    if(travellistresult!=NULL && travellistresult.size()>0 && travellistresult[0].getErrors().size()>0)
                        resultval.message = travellistresult[0].getErrors()[0].message;
                }
                return resultval;
            }
            else
                return null;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
               Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TravelrequestDetailsController.saveEmployeeTravelRequest', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
               database.insert(errorlog, false);
            }
            saveresultStatus resultval = new saveresultStatus();
            resultval.isSuccess = false;
            resultval.message = e.getMessage();
            return resultval;
        }
    }
    
    
    @AuraEnabled
    public static boolean submitTravelRequest(String TravelRequestId){
        try{           
            System.debug('TravelRequestId' +TravelRequestId);
            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
            approvalRequest.setProcessDefinitionNameOrId('Approval_for_Travel_Request');
            approvalRequest.setComments(label.Submitted_For_Approval );
            approvalRequest.setObjectId(TravelRequestId);
            Approval.ProcessResult approvalResult = Approval.process(approvalRequest);
            System.debug('process '+approvalResult);
            return approvalResult.isSuccess();
            
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
               Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TravelrequestDetailsController.submitTravelRequest', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
               database.insert(errorlog, false);
            }
            return false;
        }
    }
    
    public class saveresultStatus{
        @AuraEnabled
        public Boolean isSuccess {get; set;}
        @AuraEnabled
        public String recordId {get; set;}
        @AuraEnabled
        public String message {get; set;}
    }
}