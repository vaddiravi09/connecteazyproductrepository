@isTest
public with sharing class CustomLookUpControllerTest {
    public static testmethod void testfetchLookUpValues(){
        test.startTest();
        List<sObject> res = customLookUpController.fetchLookUpValues('Test', 'Account', '');
        system.assertEquals('Test','Test');
        test.stopTest();
    } 
}