/********************************************************************
* Company: Absyz
* Developer: Ravi Teja
* Created Date: 30/06/2018
* Description: Checking for status of related UserStory
********************************************************************/
public class ModuleStatusRestrictHandler {
    
    // to restrict status of module to be completed without all its related user story is not completed
    public static void CheckStatusOfrelatedUserStory(Map<id,Nrich__Module__c > mapmolid)
    {
        try{
        list<Nrich__User_Story__c > Lstuserstry=new list<Nrich__User_Story__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Module__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__status__c'))
            Lstuserstry = new list<Nrich__User_Story__c >([select id,Nrich__Module__c from Nrich__User_Story__c 
                                                           where Nrich__Module__c IN:mapmolid.keySet() AND Nrich__status__c !=: Label.Completed]);
        set<id> ModuleErrorId = new set<id>();
        for(Nrich__User_Story__c  us:Lstuserstry)
        {
            mapmolid.get(us.Nrich__Module__c).adderror(label.ModuleStatusRestrict_err1);
        }
        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ModuleStatusRestrictHandler.CheckStatusOfrelatedUserStory', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}