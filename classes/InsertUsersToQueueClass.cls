/****************************************************************************************************
    * Company: Absyz
    * Developer: Ravi Teja
    * Created Date: 23/10/2018
    * Description: Inserting Users to Queue Class
*****************************************************************************************************/ 
Global class InsertUsersToQueueClass {
    @Future
    public static void insertusersQueue(String CertificationUserId,String LeaveRequestUserId, String AssetUserId, String PolicyUserId){
        try{
        list<GroupMember> grouplist=new list<GroupMember>();
        list<GroupMember> grouplist_delete=new list<GroupMember>();
        list<GroupMember> grouplist_delete2=new list<GroupMember>();
        list<GroupMember> grouplist_delete3=new list<GroupMember>();
        list<GroupMember> grouplist_delete4=new list<GroupMember>();
        Group Certificationgroup =[select id, name from group where name=:Label.Queue_for_Certification_Request and Type=:Label.Queue  limit 1];
        list<GroupMember> GroupMemberlist=[select UserOrGroupId from GroupMember where GroupId=:Certificationgroup.Id];
        for(GroupMember g:GroupMemberlist){
            //if(g.UserOrGroupId!=CertificationUserId){
            grouplist_delete.add(g);
            //}
        }
        GroupMember CertificationMember= new GroupMember();
        CertificationMember.UserOrGroupId=CertificationUserId;
        CertificationMember.GroupId=Certificationgroup.Id;
        grouplist.add(CertificationMember);
        
        Group leaveReuqestqueue =[select id, name from group where name=:Label.Queue_For_Leave_Request and Type=:Label.Queue  limit 1];
        list<GroupMember> GroupMemberleavelist=[select UserOrGroupId from GroupMember where GroupId=:leaveReuqestqueue.Id];
        for(GroupMember g:GroupMemberlist){
            // if(g.UserOrGroupId!=CertificationUserId){
            
            grouplist_delete2.add(g);
            //}
        }
        
        GroupMember LeaveRequestMember= new GroupMember();
        LeaveRequestMember.UserOrGroupId=LeaveRequestUserId;
        LeaveRequestMember.GroupId=leaveReuqestqueue.Id;
        grouplist.add(LeaveRequestMember);
        
        
        Group Assetgroup=[select id,name from group where name=:Label.Queue_For_Asset_Request  and Type=:Label.Queue limit 1];
        list<GroupMember> GroupMemberAssetlist=[select UserOrGroupId from GroupMember where GroupId=:Assetgroup.id];
        for(GroupMember g:GroupMemberAssetlist){
            grouplist_delete3.add(g);
        }
        
        GroupMember AssetRequestMember = new GroupMember();
        AssetRequestMember.UserOrGroupId=AssetUserId;
        AssetRequestMember.GroupId=Assetgroup.id;
        grouplist.add(AssetRequestMember);
        
        Group Policygroup=[select id,name from group where name=:Label.Queue_For_Policy_Request and Type=:Label.Queue limit 1];
        list<GroupMember> GroupMemberPolicylist=[select UserOrGroupId from GroupMember where GroupId=:Policygroup.id];
        for(GroupMember g:GroupMemberPolicylist){
            grouplist_delete4.add(g);
        }
        
        GroupMember PolicyRequestMember=new GroupMember();
        PolicyRequestMember.UserOrGroupId=PolicyUserId;
        PolicyRequestMember.GroupId=Policygroup.id;
        grouplist.add(PolicyRequestMember);
        
        system.debug(grouplist_delete);
        system.debug(grouplist_delete2);
        system.debug(grouplist_delete3);
        system.debug(grouplist_delete4);
        delete grouplist_delete;
        //delete grouplist_delete2;
        insert grouplist;
        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='InsertUsersToQueueClass.insertusersQueue', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}