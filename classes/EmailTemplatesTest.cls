@isTest
public class EmailTemplatesTest {
    
    
    public static testMethod void SampleData(){
        
        test.startTest();
        InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Candidate__c cand = UtilityForTest.Candidate_Utility();
        Employee__c emp = UtilityForTest.Employee_Utility();
        Department__c dept = UtilityForTest.Department_Utility();
        Job__c job = UtilityForTest.Job_Utility(dept.Id);
        job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
		Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);        
        
        EmailtoCandidateBeforeJoiningDate joiningInstance=new EmailtoCandidateBeforeJoiningDate();
        joiningInstance.ObjType='Job_Application__c'; 
        joiningInstance.RelatedId=jobAppl.id;
        joiningInstance.getJobApplicationList();
        
        
        EmailToHROnRejectingScheduleController hrscheduleInstance =new EmailToHROnRejectingScheduleController();
        hrscheduleInstance.ObjType='Interview_Panel_Member__c';
        hrscheduleInstance.RelatedId=empPanelMemb.id;
        hrscheduleInstance.getPanelList();
        
        emailTemplatePreferredSlotsController PreferredSlotInstance =new emailTemplatePreferredSlotsController();
        PreferredSlotInstance.ObjType='Employee__c';
        PreferredSlotInstance.RelatedId=emp.id;
        PreferredSlotInstance.getEmployeeSlotList();
        
        EmailToCandidateRejectedController CandidateRejectedInstance=new EmailToCandidateRejectedController();
        CandidateRejectedInstance.ObjType='Interview_Round__c';
        CandidateRejectedInstance.RelatedId=irRecord.id;
        CandidateRejectedInstance.getInterviewRoundList();
        
        
        EmailtoCandidateOfferAcceptedController OfferAcceptedinstance=new EmailtoCandidateOfferAcceptedController();
        OfferAcceptedinstance.ObjType='Job_Application__c';
        OfferAcceptedinstance.RelatedId=jobAppl.id;
        OfferAcceptedinstance.getJobApplicationList();
        
        EmailtoCandidateonCompletitonController CandidateInstance=new EmailtoCandidateonCompletitonController();
		CandidateInstance.ObjType='Interview_Round__c';
        CandidateInstance.RelatedId=irRecord.id;
        CandidateInstance.getInterviewRoundList();
        
		EmailtoCandidateBeforeJoiningDate CandidatejoiningInstance=new EmailtoCandidateBeforeJoiningDate();
        CandidatejoiningInstance.ObjType='Candidate__c';
        CandidatejoiningInstance.RelatedId=cand.id;
        CandidatejoiningInstance.getJobApplicationList();
                
        EmailCandidateScheduledOrRescheduled ScheduledOrRescheduledInstance=new EmailCandidateScheduledOrRescheduled();
        ScheduledOrRescheduledInstance.ObjType='Interview_Round__c';
        ScheduledOrRescheduledInstance.RelatedId=irRecord.id;
        ScheduledOrRescheduledInstance.getPanelList();
        
        
        EmailtoCandidateonCompletitonController CandidateOnCompletionInstance=new EmailtoCandidateonCompletitonController();
       	CandidateOnCompletionInstance.ObjType='Interview_Round__c';
        CandidateOnCompletionInstance.RelatedId=irRecord.id;
        CandidateOnCompletionInstance.getInterviewRoundList();
        
        EmailtoCandidateCreateApplication instance= new EmailtoCandidateCreateApplication();
        instance.ObjType='Job_Application__c';
        instance.RelatedId=jobAppl.id;
        instance.getJobApplicationList();
        
        
        system.assertEquals(cand.id,CandidatejoiningInstance.RelatedId);
        test.stopTest(); 
    }
    
    
}