/********************************************************************
* Company: Absyz
* Developer: Ashish Nag
* Created Date: 30/06/2018
* Description: Getting the status of related sprint
********************************************************************/
public class MilestoneStatusRestrictHandler {
    
    // restrict Milestone status to be completed without all its related Sprint is not completed
    public static void checkStatusOfRelatedSprint(set<id> milesid, list<Nrich__Milestone__c> mllist)
    {
        try{
        Map<Id, Nrich__Milestone__c> LstMilestone = new Map<Id, Nrich__Milestone__c>([select id from Nrich__Milestone__c where id IN:milesid]);
        list<Nrich__Sprint__c> lstSprint=new list<Nrich__Sprint__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Milestone__c'))
            lstSprint = new list<Nrich__Sprint__c>([select id,Nrich__Status__c,Nrich__Milestone__c from Nrich__Sprint__c where Nrich__Milestone__c IN:milesid
                                                    AND Nrich__status__c !=:Label.Completed]);
        set<id> mileErrorId  = new set<id>();
        
        for(Nrich__Sprint__c sp:lstSprint)
        {
            if(LstMilestone.containsKey(sp.Nrich__Milestone__c))
                LstMilestone.get(sp.Nrich__Milestone__c).adderror(label.Milestoneststusrestrict_err1);
        }
        }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='MilestoneStatusRestrictHandler.checkStatusOfRelatedSprint', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
        
    }
}