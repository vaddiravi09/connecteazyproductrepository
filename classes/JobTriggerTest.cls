@isTest
public class JobTriggerTest {
    
    public static testMethod void Method1(){ 
        test.startTest();
        InitialSetup__c iniSetup =  UtilityForTest.InitialSetup_Utility();
        iniSetup.Automated_ApprovalProcess_on_JobCreation__c = true;
        update iniSetup;
        Department__c dept = UtilityForTest.Department_Utility();
        Job__c testjob=UtilityForTest.Job_Utility(dept.id);
        system.assertEquals('Test','Test');
        test.stopTest();
    }
    
}