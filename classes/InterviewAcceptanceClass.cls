/********************************************************************
* Company: Absyz
* Developer: Vivek
* Created Date: 22/06/2018
* Description: Accepting/Rejecting to take Interview by Interviewer.
* Test Class: InterviewAcceptanceClassTest
********************************************************************/
public with sharing class InterviewAcceptanceClass {
    public Nrich__Interview_Panel_Member__c currentRound{get;set;} // To get the Interview Panel Member record.
    public string currentRecordId;                          // Variable to get recordid from url.
    public boolean displayPopUp{get;set;}                   // Variable to display Modal.
    public boolean showerror{get;set;}                      // Variable to display error message when description is not clicked.
    public boolean disableRejectbutton{get;set;}            // Variable to disable buttons.
    public boolean disableApprovebutton{get;set;}           // Variable to disable buttons. 
    public boolean Showdescription{get;set;}                // Variable to show Reason of Rejection.
    public string SuccessMessage{get;set;}                  // Variable to show Success Message.
    public boolean showMessage{get;set;}                    // Variable to show or hide success message.
    public InterviewAcceptanceClass(){ 
        try{
            currentRecordId=Apexpages.currentPage().getParameters().get('Id');
            Schema.DescribeSObjectResult objectDescription = Nrich__Interview_Panel_Member__c.getSObjectType().getDescribe();
            if(objectDescription.isQueryable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Description__c') 
              ){
                  currentRound=[select id,Name,Nrich__Status__c,Nrich__Interview_Round__r.name,Nrich__Employee__r.name,Nrich__Description__c from Nrich__Interview_Panel_Member__c where id=:currentRecordId  ];
              }
            if(currentRound.Nrich__Status__c=='Rejected'){
                disableRejectbutton=true;
                disableApprovebutton=true;
            }
            else{
                disableRejectbutton=false;
                disableApprovebutton=false;
            }
            displayPopUp=false;
            Showdescription=false;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='InterviewAcceptanceClass.InterviewAcceptanceClass', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
        }
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This method is used for Rejection of Interview Scheduled by Interviewer
    //InputParams     : NA
    //OutputParams    : NA
    //*******************************************************************************************   
    public void Reject(){
        
        displayPopUp=true;
        Showdescription=true;
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This method is used for Approval of Interview Scheduled by Interviewer
    //InputParams     : NA
    //OutputParams    : NA
    //*******************************************************************************************
    public void Approve(){
        disableRejectbutton=true;
        disableApprovebutton=true;
        showMessage=true;
        SuccessMessage=system.label.Approval_updated_successfully;
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This method is used to Save the details of Rejection.
    //InputParams     : NA
    //OutputParams    : NA
    //*******************************************************************************************
    public void save(){
        if(currentRound.Nrich__Description__c==null){
            showerror=true;
        }
        else{
            currentRound.Nrich__Status__c='Rejected';
            Schema.DescribeSObjectResult objectDescription = Nrich__Interview_Panel_Member__c.getSObjectType().getDescribe();
            if(objectDescription.isUpdateable() && currentRound!=NULL)
                database.update(currentRound);
            //update currentRound; 
            displayPopUp=false;
            Showdescription=false;
            disableRejectbutton=true;
            disableApprovebutton=true;
            showMessage=true;
            SuccessMessage=system.label.Rejection_updated_successfully;
        }
        
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This method is used for closing the modal.
    //InputParams     : NA
    //OutputParams    : NA
    //*******************************************************************************************
    public void close(){
        displayPopUp=false;
    }
}