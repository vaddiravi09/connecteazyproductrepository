/****************************************************************************************************
* Company: Absyz
* Developer: Ashish Nag K
* Created Date: 29/10/2018
* Description: Test class covering Support Request Modal.
*****************************************************************************************************/  
@isTest()
public class TestSupportDetails {
    public static string strEmployeeID;
    
    @isTest static void insertrecords(){
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                                           
                                                                                          );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                                          Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                                          Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                                          Nrich__Carry_Forward_Frequency__c=0
                                                                         );
            
            //  insert lleave;
            cleave.Nrich__Is_Active__c=True;
            // update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                                          
                                                          Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            Nrich__Support_Ticket__c support= new Nrich__Support_Ticket__c();
            support.Nrich__Subject__c='Test';
            support.Nrich__Status__c='New';
            // support.Employee__c=emp.id;
            //  insert support;
             Nrich__Support_Ticket__c support1= new Nrich__Support_Ticket__c();
            //support.Nrich__Subject__c='';
            support1.Nrich__Status__c='New';
            
            SupportDetailsController.insertnewsupportrecords(support);
            SupportDetailsController.insertnewsupportrecords(support1);
            list<Nrich__Support_Ticket__c> cerlist2=SupportDetailsController.viewSupportRecords();
            SupportDetailsController.fetchPriorityoptions();
            SupportDetailsController.fetchCategoryoptions();
            SupportDetailsController.fetchStatus();
            
            System.assertEquals('New', support.Nrich__Status__c);
            
            Test.StopTest();
            
        }
        
    }
    
    @isTest static void supporttickettrigger(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
        System.runAs(u){
            Test.startTest();
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                                           
                                                                                          );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                                          Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                                          Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                                          Nrich__Carry_Forward_Frequency__c=0
                                                                         );
            
            //  insert lleave;
            cleave.Nrich__Is_Active__c=True;
            // update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                                          
                                                          Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            
            
            Nrich__InitialSetup__c Setup=new Nrich__InitialSetup__c();
            Setup.Nrich__Automatic_Assignment_of_Support_Tickets__c=TRUE;
            insert Setup;
            
            Nrich__SupportTeam__c supTeam=new Nrich__SupportTeam__c();
            supTeam.Name='TestTeam';
            supTeam.Nrich__Category__c='Admin Facilities';
            supTeam.Nrich__Last_Support_Person_Assigned__c='Test user';
            insert supTeam;
            
            Nrich__Support_Team_Member__c supTeamMember=new Nrich__Support_Team_Member__c();
           // supTeamMember.Name='TestTeamMember';
            supTeamMember.Nrich__Active__c=true;
            //supTeamMember.Nrich__Member_Name__c='TestMember';
            supTeamMember.Nrich__Support_team__c=supTeam.Id;
            insert supTeamMember;
            
            
            
           
            Nrich__Support_Ticket__c suppticket=new Nrich__Support_Ticket__c();
            suppticket.Nrich__Subject__c='Test';
            suppticket.Nrich__Status__c='New';
            suppticket.Nrich__IssueCategory__c=	'Admin Facilities';
            suppticket.Nrich__Assigned_Too__c=supTeamMember.Id;
            insert suppticket;
            list<Nrich__Support_Ticket__c>Supporticketlist=new list<Nrich__Support_Ticket__c>();
            Supporticketlist.add(suppticket);
            set<string>SuppCategory=new set<string>();
            SuppCategory.add(suppticket.Nrich__IssueCategory__c);
            suppticket.Nrich__IssueCategory__c='Insurance';
            update suppticket;
            SupportDetailsController.assignSupportTickets(Supporticketlist,SuppCategory);
            
            System.assertEquals('New', suppticket.Nrich__Status__c);
            Test.stopTest();
        }
        
        
    }
    
    
    
}