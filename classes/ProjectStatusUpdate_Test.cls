@isTest
public class ProjectStatusUpdate_Test {
    
    @istest public static void statusupdate(){
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        
         Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                          
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        insert lleave;
        cleave.Nrich__Is_Active__c=True;
        update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser1' , Nrich__EmployeeDesignation__c='Developer',
                                        
                                        Nrich__Total_Experience__c=11,
                                        Nrich__Related_User__c=u1.id );
        insert emp;
        
        
        Nrich__Project__c  Prj=new Nrich__Project__c();
        Prj.Name='TestPrj';
        Prj.Nrich__Project_Start_Date__c=System.today();
        Prj.Nrich__Project_End_Date__c=System.today()+7;
        Prj.Nrich__ProjectManagerr__c=emp.id;
        Prj.Nrich__Status__c='Active';
        insert prj;
        
        
        Prj.Nrich__Status__c='Completed';
        update prj;
        
        Nrich__Project__c  Prj1=new Nrich__Project__c();
        Prj1.Name='TestPrj1';
        Prj1.Nrich__Project_Start_Date__c=System.today();
        Prj1.Nrich__Project_End_Date__c=System.today()+7;
        Prj1.Nrich__ProjectManagerr__c=emp.id;
        Prj1.Nrich__Status__c='Active';
        insert Prj1;
        
        
        
        Nrich__Sprint__c  spr=new Nrich__Sprint__c();
        spr.Name='Sprint1';
        spr.Nrich__Status__c='New';
        spr.Nrich__Project__c =prj1.id;
        insert spr;
        
        
        Nrich__User_Story__c  userstry=new Nrich__User_Story__c();
        userstry.Name='Userstry1';
        userstry.Nrich__Project__c=prj1.id;
        userstry.Nrich__Status__c='New';
        insert userstry;
        
        Nrich__User_Story__c  userstry1=new Nrich__User_Story__c();
        userstry1.Name='Userstry12';
        userstry1.Nrich__Project__c=prj1.id;
        userstry1.Nrich__Status__c='Ready for Design';
        insert userstry1;
        
        
        Nrich__Task__c  tsk=new Nrich__Task__c();
        tsk.Nrich__Project__c=prj1.id;
        
        tsk.Nrich__Status__c='New'; 
        insert tsk;
        
        System.assertEquals('New',  tsk.Nrich__Status__c);
        
    }
}