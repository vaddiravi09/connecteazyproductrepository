@isTest
public class CloneTemplateControllerTest {
    
    public static TestMethod void method1(){
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__InitialSetup__c iniSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__Job_Round__c jobRound = UtilityForTest.Job_Round_Utility(job.Id);
        Nrich__Template__c temp = UtilityForTest.Template_Utility(job.Id,jobRound.Id);
        temp.Nrich__Type_of_Template__c='Feedback';
        Nrich__Section__c sec = UtilityForTest.Section_Utility(temp.Id);
        List<Nrich__Section_Fields__c> sfList = UtilityForTest.Section_Fields_Utility(sec.Id);
        CloneTemplateController.fetchTemplateDetails(temp.Id); 
        
        Nrich__Department__c  testDepartment = new Nrich__Department__c();
        testDepartment.Name='testDepartment';
        insert testDepartment;
        Nrich__Job__c job1 = UtilityForTest.Job_Utility2(testDepartment.Id);
        Nrich__Job_Round__c jobRound1 = UtilityForTest.Job_Round_Utility1(job1.Id);
        
        CloneTemplateController.cloneTemplate(temp.Id,'test template',job1.Id,jobRound1.Id,false);  
        system.assertEquals('Test Description','Test Description');
    }
}