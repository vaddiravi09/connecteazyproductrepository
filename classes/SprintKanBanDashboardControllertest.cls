@isTest
public class SprintKanBanDashboardControllertest {
    @isTest
    public static void testdata(){      
        
        Date startdate = Date.newInstance(2019, 04, 10);
        Date enddate = Date.newInstance(2019, 04, 10);
        
        Nrich__Project__c proj = new Nrich__Project__c();
        proj.name = 'abc';
        proj.Nrich__Project_Type__c = 'Paid Project';
        proj.Nrich__Status__c = 'Active';
        insert proj;
        
        Nrich__Milestone__c  milestonestest= new Nrich__Milestone__c(Name = 'Name854', Nrich__Status__c = 'New', Nrich__Project__c = proj.Id);
        Insert milestonestest;       
        
        Nrich__Project_Team_Member__c PTM = new Nrich__Project_Team_Member__c();
        PTM.Nrich__Project__c = proj.Id;
        PTM.Nrich__Project_Role__c='Senior Management';
        insert PTM;
        
        Nrich__Sprint__c sp1 = new Nrich__Sprint__c();
        sp1.name = 'vf page';
        sp1.Nrich__Project__c  = proj.Id;
        sp1.Nrich__Milestone__c = milestonestest.id;
        sp1.Nrich__Status__c = Label.In_Progress;
        //sp1.Total_Actual_Effort__c = 10;
        //sp1.Total_Estimated_Effort__c = 10;
        insert sp1;
        
        Nrich__Sprint__c sp2 = new Nrich__Sprint__c();
        sp2.name = 'vf page';
        sp2.Nrich__Project__c  = proj.Id;
        sp2.Nrich__Milestone__c = milestonestest.id;
        sp2.Nrich__Status__c = Label.In_Progress;
        //sp1.Total_Actual_Effort__c = 10;
        //sp1.Total_Estimated_Effort__c = 10;
        insert sp2;
        
        Nrich__User_Story__c userstr = new Nrich__User_Story__c(Name = 'Story 1', Nrich__Project__c = proj.Id, Nrich__Status__c = 'New');
        insert userstr;  
        
        List<Nrich__Task__c> lstofTask = new List<Nrich__Task__c>();
        Nrich__Task__c tsk = new Nrich__Task__c( Nrich__Project__c = proj.Id, Nrich__Status__c= 'New');
        lstofTask.add(tsk);
        
        Nrich__Task__c tsk1 = new Nrich__Task__c( Nrich__Project__c = proj.Id, Nrich__Status__c= 'New');
        lstofTask.add(tsk1);
        insert lstofTask;
        
        test.startTest();
        SprintKanBanDashboardController.getSprintUserstorydetails(proj.id,sp1.id,userstr.id,startdate,enddate);
        test.stopTest();
        System.assertEquals('New', tsk1.Nrich__Status__c);
    }
    
    @isTest
    public static void testSprintKanbanmethods(){
        Nrich__InitialSetup__c InitalSetup=new Nrich__InitialSetup__c();
        InitalSetup.Nrich__Cancelled_Color__c='#123';
        InitalSetup.Nrich__Completed_Color__c='#234';
        insert InitalSetup;
        
        Nrich__Project__c proj = new Nrich__Project__c();
        proj.name = 'abc';
        proj.Nrich__Project_Type__c = 'Paid Project';
        proj.Nrich__Status__c = 'Active';
        insert proj;
        
        Nrich__Milestone__c  milestonestest= new Nrich__Milestone__c(Name = 'Name854', Nrich__Status__c = 'New', Nrich__Project__c = proj.Id);
        Insert milestonestest;
        
        Nrich__User_Story__c userstr = new Nrich__User_Story__c(Name = 'Story 1', Nrich__Project__c = proj.Id, Nrich__Status__c = 'New');
        insert userstr;
        
        Nrich__Sprint__c sp1 = new Nrich__Sprint__c();
        sp1.name = 'vf page';
        sp1.Nrich__Project__c = proj.Id;
        sp1.Nrich__Milestone__c = milestonestest.id;
        sp1.Nrich__Status__c = Label.In_Progress;
        insert sp1;
        
        sp1.Name='vf page1';
        update sp1;
        
        test.startTest();
        SprintKanBanDashboardController.UpdateUserStory(userstr.Id, sp1.Id);
        SprintKanBanDashboardController.UpdateUserStory(userstr.Id, NULL);
        SprintKanBanDashboardController.GetDetails();
        test.stopTest();
        System.assertEquals('vf page1', sp1.Name);
    }
}