@isTest
public class TimeSheetTableDisplayController_Test {
    
    @isTest
    public static void testdata(){
        string strEmpID = userinfo.getuserid();
        list<String> datetimelist = new list<String>();
        datetimelist.add('2019');
        
        datetimelist.add('31');
        datetimelist.add('12');
        
        list<Nrich__Timesheet_Detail__c> tdlist = new list<Nrich__Timesheet_Detail__c>();
        
       Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                          
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
      //  insert lleave;
        cleave.Nrich__Is_Active__c=True;
       // update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        
        Nrich__Employee__c e = new Nrich__Employee__c();
        e.Name = 'Employee 1';
        e.Nrich__EmployeeDesignation__c = 'Developer';
        
        e.Nrich__Active__c = true;
        e.Nrich__Related_User__c = strEmpID;
        insert e;
        
        
        Nrich__Project__c p = new Nrich__Project__c();
        p.Name = 'New Project';
        insert p;
        
        Nrich__Timesheet__c t = new Nrich__Timesheet__c();
        t.Name = '2019/12/31';
        t.Nrich__Date__c = Date.newInstance(2019,12,31);
        t.Nrich__Employeee__c = t.Nrich__Employeee__r.id;
        
        insert t;
        
        Nrich__Timesheet_Detail__c td = new Nrich__Timesheet_Detail__c();
        td.Nrich__Timesheet__c= t.Id;
        td.Nrich__Project__c = p.id;
        insert td;
        
        list<Nrich__Timesheet__c>TimeSheetList=new list<Nrich__Timesheet__c>();
        TimeSheetList.add(t);
        
        System.assertEquals('New Project',  p.Name);
        
        TimeSheetTableDisplayController.ViewTimeSheetDetails();
    }
    
    @isTest
    public static void testelsecondition(){
        
        TimeSheetTableDisplayController.ViewTimeSheetDetails();
        System.assertEquals('test', 'test');
    }
    
    
    
}