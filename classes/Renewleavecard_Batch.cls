/****************************************************************************************************
* Company: Absyz
* Developer: Thadeus Ronn Thomas
* Created Date: 22/10/2018
* Description: Deactivating Legacy Leave cards 
*****************************************************************************************************/   

public class Renewleavecard_Batch implements Database.Batchable < sObject > {
    public List<String> CompanyLeaveStructureIdList;
    public string query = 'select Id, Name from Nrich__Employee_Leave_Card__c where Nrich__Is_Active__c=True AND Nrich__Leave_Structure__c IN: CompanyLeaveStructureIdList';
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List < Nrich__Employee_Leave_Card__c > LCard) {
        try{
        list<Nrich__Employee_Leave_Card__c> emplcardList = new list<Nrich__Employee_Leave_Card__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Card__c', 'Nrich__Is_Active__c'))
            for(Nrich__Employee_Leave_Card__c LC: LCard){
                LC.Nrich__Is_Active__c=False;
                emplcardList.add(LC);
            }
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Employee_Leave_Card__c.getSObjectType().getDescribe();
        if(objectDescriptioncontent.isUpdateable() && emplcardList.size()>0)
            database.update(emplcardList);
    }catch(exception e){
        e.getLineNumber();
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='Renewleavecard_Batch', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    public void finish(Database.BatchableContext BC) {}
    
}