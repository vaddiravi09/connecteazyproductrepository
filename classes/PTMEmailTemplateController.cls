/********************************************************************
* Company: Absyz
* Developer: Seshu Varma
* Created Date: 30/06/2018
* Description: fetching Details List
********************************************************************/
public with sharing class PTMEmailTemplateController {
    
    public String objType {get;set;}
    public ID relatedId {get;set;}
    public list<Nrich__Project_Team_Member__c  > ProjectTeamMemberAppList{get;set;}
    
    public list<Nrich__Project_Team_Member__c> getdetailslist(){
        try{
        System.debug('relatedId' +relatedId);
        List<Nrich__Project_Team_Member__c> ProjectTeamMemberAppList = new List<Nrich__Project_Team_Member__c>();
        if(relatedId != null && ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Employee_Name__c') && (ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Project_Role__c'))){
            ProjectTeamMemberAppList = [select Id, Nrich__Employee_Name__c ,Nrich__Project__r.Name ,Nrich__Project_Role__c,Nrich__ProjectManagerName__c ,Nrich__Employee_End_Date__c,Nrich__Employee_Start_Date__c from Nrich__Project_Team_Member__c where id = : relatedId ];
            system.debug('ProjectTeamMemberAppList' +ProjectTeamMemberAppList);
        }
        return ProjectTeamMemberAppList;
        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='PTMEmailTemplateController.getdetailslist', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    
}