public with sharing class CustomLookUpController {
    
    
    /****************************************************************************************************
  * Company:Absyz
  * Developer:Sai kesa
  * Created Date:13/11/18
  * Description:Fetching the lookup values 
*****************************************************************************************************/
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName, String jobidval) {
        try{
        String searchKey = searchKeyWord + '%';
        System.debug('Object name' +ObjectName);
        List < sObject > returnList = new List < sObject > ();
        String sQuery =  'select id, Name from '+ObjectName;      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        sQuery =  sQuery+ ' where Name LIKE: searchKey';
         if(String.isNotBlank(jobidval))
            //sQuery =  sQuery+' AND Job__c=: jobidval';
        sQuery =  sQuery+ ' order by createdDate DESC limit 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CustomLookUpController.fetchLookUpValues', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}