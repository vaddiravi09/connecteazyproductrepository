/********************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 22/05/2019
* Description: Generic class to check the field access.
********************************************************************/
public with sharing class ObjectFieldAccessCheck {
    public static Boolean checkCreateAccess(String objectName, String fieldName){
            SObjectType schemaType = Schema.getGlobalDescribe().get(objectName);
            Map<String, SObjectField> fields = schemaType.getDescribe().fields.getMap();
            DescribeFieldResult fieldDescribe = fields.get(fieldName).getDescribe();
            return fieldDescribe.isCreateable();
        }
        
        public static Boolean checkUpdateAccess(String objectName, String fieldName){
            SObjectType schemaType = Schema.getGlobalDescribe().get(objectName);
            Map<String, SObjectField> fields = schemaType.getDescribe().fields.getMap();
            DescribeFieldResult fieldDescribe = fields.get(fieldName).getDescribe();
            return fieldDescribe.isUpdateable();
        }
        
        public static Boolean checkAccessible(String objectName, String fieldName)
        {
            SObjectType schemaType = Schema.getGlobalDescribe().get(objectName);
            Map<String, SObjectField> fields = schemaType.getDescribe().fields.getMap();
            DescribeFieldResult fieldDescribe = fields.get(fieldName).getDescribe();
            return fieldDescribe.isAccessible();
        }
    }