public class PolicyDetailComponentController {
    /****************************************************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 26/12/2018
* Description: Updating Employee Policy
*****************************************************************************************************/
    @AuraEnabled
    public static List<Nrich__EmployeePolicy__c> AcceptorRejectPolicy(String employeepolicy){
        try{
        if(String.isNotBlank(employeepolicy)){
            List<Nrich__EmployeePolicy__c> EmployeePolicyList = (List<Nrich__EmployeePolicy__c>)System.JSON.deserialize(employeepolicy, list<Nrich__EmployeePolicy__c>.class);
            List<Nrich__EmployeePolicy__c> EmployeePolicyUpdateList = new List<Nrich__EmployeePolicy__c>(); 
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__EmployeePolicy__c','Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__EmployeePolicy__c', 'Nrich__Date_Responded__c'))
                for(Nrich__EmployeePolicy__c emplist: EmployeePolicyList){          
                    emplist.Nrich__Status__c =Label.Accepted;
                    emplist.Nrich__Date_Responded__c = system.today();
                    EmployeePolicyUpdateList.add(emplist);
                }
            if(EmployeePolicyUpdateList.size()>0)
                database.update(EmployeePolicyUpdateList, false);
        }
        return EmployeeDashboardController.fetchEmployeePolicyMethod();
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='PolicyDetailComponentController.AcceptorRejectPolicy', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    @AuraEnabled
    public static string getPolicyDocument(String policyId) {
        try{
        if(String.isNotBlank(policyId)){
            list<ContentDocumentLink> documentLinkList =[select id,LinkedEntityId,ContentDocumentId from ContentDocumentLink where LinkedEntityId=:policyId ];
            if(documentLinkList!=NULL && documentLinkList.size()>0){
                list<ContentDocument> contentDocList=[select id from ContentDocument where id=:documentLinkList[0].ContentDocumentId order By createddate desc];
                if(contentDocList!=NULL && contentDocList.size()>0){
                    contentVersion s=[select id,islatest from contentVersion where isLatest=true and ContentDocumentId=:contentDocList[0].Id];
                    system.debug('Contenet version--'+s);
                    system.debug('content Document id'+contentDocList[0].Id);
                    return contentDocList[0].Id;
                }
                else
                    return null;
            }
            else
                return null;
            
        }
        else
            return null;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='PolicyDetailComponentController.getPolicyDocument', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
}