/*************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 22/06/2018
* Class Name : CreationOfFirstRoundInterviewController
* Purpose : Update Dynamic Picklist value for each Job Application from related Job Rounds  
Create a new Interview Round from the Picklist value that is selected 
* Testclass Name : creationOfFirstRoundInterviewTest
************************************/
public with sharing class CreationOfFirstRoundInterviewController {
    /*************************************
// Method Name : getselectOptions
// Purpose : Update Dynamic Picklist value for each Job Application from related Job Rounds 
************************************/
    @AuraEnabled
    public static List < Nrich__Job_Round__c > getselectOptions(Id record_Id) {
        try{
        //Query the Current Job Application from the record id
        Nrich__Job_Application__c jobApplicationList= new Nrich__Job_Application__c();
        List<Nrich__Job_Round__c> jobRoundList= new list<Nrich__Job_Round__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Job__c'))
            jobApplicationList = [SELECT id, Name, Nrich__Job__c FROM Nrich__Job_Application__c where id=: record_Id];
        Id jobId = jobApplicationList.Nrich__Job__c;
        //Query the Job Rounds to display in a picklist as values
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job_Round_Order_Number__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job__c'))
            jobRoundList = [select id,Name,Nrich__Job_Round_Order_Number__c from Nrich__Job_Round__c where Nrich__Job__c =: jobId   order by Nrich__Job_Round_Order_Number__c ASC ];
        //Return a List of Job Rounds
        return jobRoundList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CreationOfFirstRoundInterviewController.getselectOptions', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    @AuraEnabled
    public static Boolean getInterviewRound(Id record_Id) {
        try{
        //Query the Current Job Application from the record id
        Nrich__Job_Application__c jobApplicationList= new Nrich__Job_Application__c();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Job__c'))
            jobApplicationList = [SELECT id, Name, Nrich__Job__c, (select Id, Name from Nrich__Interview_Rounds__r) FROM Nrich__Job_Application__c where id=: record_Id ];
        Id jobId = jobApplicationList.Nrich__Job__c;
        Boolean val = false;
        if(jobApplicationList.Nrich__Interview_Rounds__r.size() == 0){
            val = true;           
        }
        return val;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CreationOfFirstRoundInterviewController.getInterviewRound', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    /*************************************
// Method Name : storeSelectedValue
// Purpose : Create a new Interview Round from the Picklist value that is selected 
************************************/
    @AuraEnabled
    public static Id storeSelectedValue(Integer selected, Id record_Id) {
        try{
        List<Nrich__Job_Application__c> jobApplicationRecordList= new list<Nrich__Job_Application__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Job_Order_Number__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Job__c'))
            jobApplicationRecordList = [select id, Name, Nrich__Job_Order_Number__c, Nrich__Job__c, Nrich__Job__r.Name from Nrich__Job_Application__c where  id=:record_Id ];
        List<Nrich__Job_Application__c> jobApplicationUpdatedList = new List<Nrich__Job_Application__c>();
        Nrich__Interview_Round__c interviewList = new Nrich__Interview_Round__c();
        String jobName;
        Id jobApplicationId;
        Id jobId;
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Job_Order_Number__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Application_Status__c') 
          )
            for(Nrich__Job_Application__c ja : jobApplicationRecordList){
                ja.Nrich__Job_Order_Number__c = selected;
                ja.Nrich__Application_Status__c = System.Label.Application_Status_to_Eligible;
                jobApplicationUpdatedList.add(ja);
                jobName = ja.Nrich__Job__r.Name;
                jobApplicationId = ja.Id;
                jobId = ja.Nrich__Job__c;
            }
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Job_Application__c.getSObjectType().getDescribe();
        if(objectDescriptioncontent.isUpdateable())
            database.update(jobApplicationUpdatedList);
        Decimal selectedVal = Decimal.valueOf(selected);
        list<Nrich__Job_Round__c> jobValueList= new list<Nrich__Job_Round__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Interview_Level__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job_Round_Description__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job_Round_Mode__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Round__c', 'Nrich__Job_Round_Order_Number__c')
          )
            jobValueList = [SELECT Id, Name, Nrich__Interview_Level__c, Nrich__Job_Round_Description__c, Nrich__Job_Round_Mode__c FROM Nrich__Job_Round__c where Nrich__Job_Round_Order_Number__c =: selected AND Nrich__Job__c=:jobId ];
        if(jobValueList!=NULL && jobValueList.size()>0 && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Is_Active__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Type_of_Template__c') 
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job_Round__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Interview_Level__c') &&
           ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Interview_Mode__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Interview_Round_Description__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Feedback_URL__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Status__c')
          ){
              list<Nrich__Template__c> templatelist = [select id from Nrich__Template__c where Nrich__Is_Active__c=TRUE AND Nrich__Type_of_Template__c=:Label.Feedback AND Nrich__Job_Round__c=: jobValueList[0].Id];           
              interviewList = new Nrich__Interview_Round__c(Name=jobValueList[0].Name, Nrich__Job_Order__c=selected, Nrich__Job_Application__c=jobApplicationId);
              interviewList.Nrich__Interview_Level__c = jobValueList[0].Nrich__Interview_Level__c;
              interviewList.Nrich__Interview_Mode__c = jobValueList[0].Nrich__Job_Round_Mode__c;
              interviewList.Nrich__Interview_Round_Description__c = jobValueList[0].Nrich__Job_Round_Description__c;
              interviewList.RecordTypeId = Schema.SObjectType.Nrich__Interview_Round__c.getRecordTypeInfosByName().get(Label.ToBeScheduled_Interview).getRecordTypeId();
              if(templatelist!=NULL && templatelist.size()>0){
                  interviewList.Nrich__Feedback_URL__c = System.Label.SitePageURL+'/apex/'+Label.FeedbackURL+'?templateId='+templatelist[0].Id;
              }
              interviewList.Nrich__Status__c = System.Label.InterviewToBeSchedule;
              Schema.DescribeSObjectResult objectInterviewcontent = Nrich__Interview_Round__c.getSObjectType().getDescribe();
              
              if(objectInterviewcontent.iscreateable())
                  database.insert(interviewList);
          }      
        return interviewList.Id;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CreationOfFirstRoundInterviewController.storeSelectedValue', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}