@isTest
public with sharing class EmailTempPreferredSlotsControllerTest {
    private static testmethod void testgetpanelList() {
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        Nrich__Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        EmailTemplatePreferredSlotsController inst = new EmailTemplatePreferredSlotsController();
        inst.RelatedId = empPanelMemb.Id;
        inst.getEmployeeSlotList();
        system.assertEquals('Test', 'Test');
        test.stopTest();
    }   
}