/****************************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 22/06/2018
* Description: Apex Controller for Configuration page  
* Test Class: TemplateCreationControllerTest
****************************************************************************/
public with sharing class TemplateCreationController {
    
    //*******************************************************************************************
    //Created by      : Raviteja
    //Method Name     : getTemplateType
    //Overall Purpose : This method is used for fetching the Template Types
    //InputParams     : NA
    //OutputParams    : N/A
    //*******************************************************************************************        
    @AuraEnabled
    public static List < String > getTemplateType() {
        try{
        List < String > allOpts = new list < String > ();
        Schema.DescribeSObjectResult objDescribe = Nrich__Template__c.getSObjectType().getDescribe();
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: fieldMap.get('Nrich__Type_of_Template__c').getDescribe().getPickListValues()) {
            allOpts.add(a.getLabel());
        }
        allOpts.sort();
        return allOpts;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TemplateCreationController.getTemplateType', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    //*******************************************************************************************
    //Created by      : Raviteja
    //Method Name     : fetchTemplaterecord
    //Overall Purpose : This method is used for fetching the Template Details
    //InputParams     : templateId
    //OutputParams    : template__c
    //*******************************************************************************************
    @AuraEnabled
    public static TemplateVal fetchTemplaterecord(String templateId) {
        try{        
        if(string.isNotBlank(templateId) &&ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Background_Color__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job_Round_Name__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Is_Active__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Job_Round__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Type_of_Template__c') 
          ){
              list<Nrich__Template__c> template = [Select Id, Nrich__Background_Color__c, Nrich__Job_Name__c, Nrich__Job_Round_Name__c, Nrich__Is_Active__c,Nrich__Job__c, Nrich__Job_Round__c, Name, Nrich__Type_of_Template__c from Nrich__Template__c where Id=:templateId ];
              if(template!=NULL && template.size()>0){
                  TemplateVal temp = new TemplateVal();
                  temp.template = template[0];
                  list<Nrich__Section__c> sectionvallist = new list<Nrich__Section__c>();
                  map<String, Decimal> sectionOrder = new map<String, Decimal>();
                  if(ObjectFieldAccessCheck.checkAccessible('Nrich__Section__c', 'Nrich__Order__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section__c', 'Nrich__Template__c'))
                      for(Nrich__Section__c sec: [Select Id, Name, Nrich__Order__c from Nrich__Section__c where Nrich__Template__c=:templateId order by Nrich__Order__c]){
                          sectionvallist.add(sec);
                          sectionOrder.put(sec.Id, sec.Nrich__Order__c);
                      }
                  list<FieldVal> FieldVallist = new list<FieldVal>();
                  map<String, String> sectionfieldMap = new map<String, String>();
                  if(ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Coloumn__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Object_API_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Mapping_Field_API_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Field_Length__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Mandatory__c') && 
                     ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Order__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Regex__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Section__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Validation_Type__c')
                    )
                      for(Nrich__Section_Fields__c sectionfields: [Select Id, Nrich__Coloumn__c, Nrich__Object_API_Name__c, Nrich__Mapping_Field_API_Name__c, Nrich__Field_Length__c, Nrich__Field_Type__c, Nrich__Mandatory__c, Nrich__Order__c, Nrich__Regex__c, Nrich__Section__c, Name, Nrich__Validation_Type__c, Nrich__Section__r.Nrich__Order__c, Nrich__Section__r.Name from Nrich__Section_Fields__c where Nrich__Section__r.Nrich__Template__c=: templateId order by Nrich__Order__c]){
                          FieldVal fl = new FieldVal();
                          fl.sectiondfieldId = sectionfields.Id;
                          fl.fieldName = sectionfields.Nrich__Mapping_Field_API_Name__c;
                          fl.fieldLabel = sectionfields.Name;
                          fl.fieldType = sectionfields.Nrich__Field_Type__c;
                          fl.fieldObjectName = sectionfields.Nrich__Object_API_Name__c;
                          fl.mandatory = sectionfields.Nrich__Mandatory__c;
                          fl.fieldValidationType = sectionfields.Nrich__Validation_Type__c;
                          fl.fieldValidationLength = sectionfields.Nrich__Field_Length__c;
                          fl.sectionColumn = sectionfields.Nrich__Coloumn__c;
                          fl.fieldValidationRegex = sectionfields.Nrich__Regex__c;
                          fl.sectionOrder  = Integer.valueof(sectionOrder.get(sectionfields.Nrich__Section__c));
                          fl.sectiondfieldOrder = Integer.valueof(sectionfields.Nrich__Order__c);
                          FieldVallist.add(fl);     
                          sectionfieldMap.put(fl.fieldName, fl.fieldName);
                      }
                  list<FieldVal> FieldValAvailableList = getFields(template[0].Nrich__Type_of_Template__c);
                  list<FieldVal> FieldValAvailableListNew  = new list<FieldVal>();
                  for(FieldVal fl: FieldValAvailableList){
                      if(sectionfieldMap.size()>0){
                          if(!sectionfieldMap.containsKey(fl.fieldName)){
                              FieldValAvailableListNew.add(fl);
                          }
                      }
                      else
                          FieldValAvailableListNew.add(fl);
                  }
                  Nrich__Job__c jobval = new Nrich__Job__c();
                  jobval.id= template[0].Nrich__Job__c;
                  Nrich__Job_Round__c jobroundval = new Nrich__Job_Round__c();
                  jobroundval.id= template[0].Nrich__Job_Round__c;
                  // jobval.Job_Title__c = template[0].Job__r.Job_Title__c;
                  temp.job = jobval;
                  temp.jobround = jobroundval;
                  temp.sectionList = sectionvallist;
                  temp.sectionfieldList = FieldVallist;
                  temp.availablefieldList = FieldValAvailableListNew;
                  return temp;
              }          
          }
        return null;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TemplateCreationController.fetchTemplaterecord', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    //*******************************************************************************************
    //Created by      : Raviteja
    //Method Name     : getFields
    //Overall Purpose : This method is used for fetching the picklist field values
    //InputParams     : templatetype
    //OutputParams    : fieldlist
    //*******************************************************************************************
    @AuraEnabled
    public static list<FieldVal> getFields(string templateType){
        try{
        list<FieldVal> fieldList = new list<FieldVal>();
        string objName;
        if(templateType.equalsIgnoreCase('Application')){
            list<FieldVal> fieldListobj = fieldhelper('Nrich__Job_Application__c');
            fieldList.addAll(fieldListobj);
            list<FieldVal> fieldListAddobj = fieldhelper('Nrich__Additional_Candidate_Details__c');
            fieldList.addAll(fieldListAddobj);
            list<FieldVal> uploadfields = uploadfieldhelper('Nrich__Job_Application__c');
            fieldList.addAll(uploadfields);
        }
        if(templateType.equalsIgnoreCase('On Boarding')){
            list<FieldVal> fieldListobj = fieldhelper('Nrich__Candidate__c');
            fieldList.addAll(fieldListobj);
            list<FieldVal> uploadfields = uploadfieldhelper('Nrich__Candidate__c');
            fieldList.addAll(uploadfields);
        }
        else if(templateType.equalsIgnoreCase('Feedback')){
            list<FieldVal> fieldListobj = fieldhelper('Nrich__Feedback__c');
            fieldList.addAll(fieldListobj);                        
        }       
        return fieldList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TemplateCreationController.getFields', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    //*******************************************************************************************
    //Created by      : Raviteja
    //Method Name     : fieldhelper
    //Overall Purpose : This method is helper for fetching the picklist field values
    //InputParams     : objectname
    //OutputParams    : fieldlist
    //*******************************************************************************************
    public static list<FieldVal> uploadfieldhelper(String objname){
        try{
        list<FieldVal> fieldList = new list<FieldVal>();         
        for(Nrich__Upload_Field_Types__mdt uploadfile: [Select Id, MasterLabel, DeveloperName from Nrich__Upload_Field_Types__mdt where Id!=NULL]){
            FieldVal fl = new FieldVal();
            fl.fieldName = uploadfile.DeveloperName;
            fl.fieldLabel = uploadfile.MasterLabel;
            fl.fieldType = Label.Upload;
            fl.fieldObjectName = objname;
            fl.resume = false;
            fieldList.add(fl);
            
        }
        return fieldList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TemplateCreationController.uploadfieldhelper', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    //*******************************************************************************************
    //Created by      : Raviteja
    //Method Name     : fieldhelper
    //Overall Purpose : This method is helper for fetching the picklist field values
    //InputParams     : objectname
    //OutputParams    : fieldlist
    //*******************************************************************************************
    public static list<FieldVal> fieldhelper(String objName){
        try{
        list<FieldVal> fieldList = new list<FieldVal>();
        for (Schema.SObjectField sfield: Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap().values()) {
            if (sfield.getDescribe().getName() != 'Id' && sfield.getDescribe().getName() != 'IsDeleted' && sfield.getDescribe().getName() != 'CreatedDate' && sfield.getDescribe().getName() != 'CreatedById' && sfield.getDescribe().getName() != 'LastModifiedDate' && sfield.getDescribe().getName() != 'LastModifiedById' && sfield.getDescribe().getName() != 'SystemModstamp' && sfield.getDescribe().getName() != 'LastViewedDate' && sfield.getDescribe().getName() != 'LastReferencedDate' && !sfield.getDescribe().getType().name().equalsIgnorecase('Reference') && !sfield.getDescribe().getType().name().equalsIgnorecase('Url') && !sfield.getDescribe().getType().name().equalsIgnorecase('MultiPicklist')){
                if(sfield.getDescribe().isUpdateable()){
                    FieldVal fl = new FieldVal();
                    fl.fieldName = sfield.getDescribe().getName();
                    fl.fieldLabel = sfield.getDescribe().getLabel();
                    fl.fieldType = sfield.getDescribe().getType().name();
                    fl.fieldObjectName = objName;
                    fieldList.add(fl); 
                }
            }                
        }
        return fieldList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TemplateCreationController.fieldhelper', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    //*******************************************************************************************
    //Created by      : Raviteja
    //Method Name     : saveTemplate
    //Overall Purpose : This method is to Save Template, Section and Section Fields
    //InputParams     : filename, filecontent, filetype, Template, Section, sectionfields
    //OutputParams    : templateId
    //*******************************************************************************************
    @AuraEnabled
    public static templateSaveResponse saveTemplate(String fileName, String base64Data, String contentType, Nrich__Template__c template, String sections, String fieldlist){
        try{
            database.upsertresult templateresult;
            if(template!=NULL && ObjectFieldAccessCheck.checkAccessible('Nrich__Template__c', 'Nrich__Type_of_Template__c')){
                if(template.Nrich__Type_of_Template__c=='Application')
                    template.Nrich__Mapping_Object_API_Name__c = 'Nrich__Job_Application__c';
                else if(template.Nrich__Type_of_Template__c=='Feedback')
                    template.Nrich__Mapping_Object_API_Name__c = 'Nrich__Feedback__c';
                else if(template.Nrich__Type_of_Template__c=='On Boarding')
                    template.Nrich__Mapping_Object_API_Name__c = 'Nrich__Candidate__c';
                Id jobid = template.Nrich__job__c;
                //template.job__c = jobid;
                templateresult = database.upsert(template, true);
                system.debug('templatere'+templateresult);
            }
            if(templateresult!=NULL && templateresult.isSuccess()){
                if(string.isnotBlank(fileName) && string.isNotBlank(base64Data)){
                    base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
                    
                    ContentVersion cv = new ContentVersion();
                    cv.ContentLocation = 'S';
                    cv.VersionData =EncodingUtil.base64Decode(base64Data);
                    cv.Title = fileName;
                    cv.PathOnClient = fileName;
                    database.SaveResult contentResult = database.insert(cv);
                    if(contentResult!=NULL && contentResult.isSuccess()){
                        list<ContentDocumentLink> ContentDocumentLinklist = new list<ContentDocumentLink>();
                        list<ContentDistribution> ContentDistributionList = new list<ContentDistribution>();
                        for(ContentVersion ContentVersionlist : [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id =: contentResult.getId()]){
                            ContentDocumentLink cdl = new ContentDocumentLink();
                            cdl.ContentDocumentId = ContentVersionlist.ContentDocumentId;
                            cdl.LinkedEntityId = templateresult.getId();
                            cdl.ShareType = 'V';
                            ContentDocumentLinklist.add(cdl); 
                        }
                        Schema.DescribeSObjectResult objectDescriptioncontentdoc = ContentDocumentLink.getSObjectType().getDescribe();
                        if(objectDescriptioncontentdoc.isCreateable() && ContentDocumentLinklist.size()>0){
                            database.saveresult[] res = database.insert(ContentDocumentLinklist);
                        }
                        
                        Nrich__Template__c templatecontent = new Nrich__Template__c();
                        templatecontent.Id=templateresult.getId();
                        templatecontent.Nrich__Background_Image__c = '/sfc/servlet.shepherd/version/download/'+contentResult.getId();
                        database.update(templatecontent);                                   
                    }
                }
                
                list<Nrich__Section__c> sectionsList = (list<Nrich__Section__c>)Json.deserialize(sections, list<Nrich__Section__c>.class);
                system.debug('sectionsList'+sectionsList);
                list<Nrich__Section__c> SectionInsertList = new list<Nrich__Section__c>();
                map<String, String> sectionExistingMap = new map<String, String>();
                for(Nrich__Section__c section: sectionsList){ 
                    if(string.isnotBlank(section.Id))
                        sectionExistingMap.put(section.Id, section.Id);
                    Nrich__Section__c se = new Nrich__Section__c();
                    se = section;
                    if(!string.isNotBlank(se.Id))
                        se.Nrich__Template__c = templateresult.getId();
                    SectionInsertList.add(se);
                }             
                list<Nrich__Section__c> sectionDeleteList = new list<Nrich__Section__c>();
                if(template!=NULL && template.Id!=NULL){               
                    for(Nrich__Section__c sec: [select Id from Nrich__Section__c where Nrich__template__c=: template.Id]){
                        if(sectionExistingMap!=NULL && sectionExistingMap.size()>0 && !sectionExistingMap.containsKey(sec.Id))
                            sectionDeleteList.add(sec);
                    }
                }
                
                database.upsertresult[] result;
                if(SectionInsertList.size()>0)
                    result = database.upsert(SectionInsertList, true);
                
                if(sectionDeleteList.size()>0)
                    database.delete(sectionDeleteList, true);
                
                if(result!=NULL && result.size()>0){
                    list<FieldVal> sectionFieldList = (list<FieldVal>)Json.deserialize(fieldlist, list<FieldVal>.class);
                    map<String, FieldVal> fieldMap = new map<String, FieldVal>();
                    for(FieldVal sectionfield: sectionFieldList){
                        fieldMap.put(sectionfield.fieldName, sectionfield);
                    }
                    system.debug('fieldsList'+fieldMap);
                    list<Nrich__Section_Fields__c> sectionfieldsList = new list<Nrich__Section_Fields__c>();
                    map<String, String> sectionFieldExistingMap = new map<String, String>();
                    if(ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Coloumn__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Object_API_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Mapping_Field_API_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Field_Length__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Mandatory__c') && 
                       ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Order__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Regex__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Section__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Section_Fields__c', 'Nrich__Validation_Type__c')
                      )
                        for(FieldVal sectionfield: fieldMap.values()){
                            Nrich__Section_Fields__c secfld = new Nrich__Section_Fields__c();
                            secfld.Name                      = sectionfield.fieldLabel;
                            secfld.Id                        = sectionfield.sectiondfieldId;
                            if(string.isnotBlank(sectionfield.sectiondfieldId))
                                sectionFieldExistingMap.put(secfld.Id, secfld.Id);
                            secfld.Nrich__Mapping_Field_API_Name__c = sectionfield.fieldName;
                            if(sectionfield.fieldType.equalsIgnoreCase('String'))
                                secfld.Nrich__Field_Type__c             = 'String';
                            else if(sectionfield.fieldType.equalsIgnoreCase('Picklist'))
                                secfld.Nrich__Field_Type__c             = 'Picklist';
                            else if(sectionfield.fieldType.equalsIgnoreCase('Double'))
                                secfld.Nrich__Field_Type__c             = 'Decimal';
                            else if(sectionfield.fieldType.equalsIgnoreCase('Currency'))
                                secfld.Nrich__Field_Type__c             = 'Decimal';
                            else if(sectionfield.fieldType.equalsIgnoreCase('MultiPicklist'))
                                secfld.Nrich__Field_Type__c             = 'Multi Picklist';
                            else if(sectionfield.fieldType.equalsIgnoreCase('Date'))
                                secfld.Nrich__Field_Type__c             = 'Date';
                            else if(sectionfield.fieldType.equalsIgnoreCase('DateTime'))
                                secfld.Nrich__Field_Type__c             = 'DateTime';
                            else if(sectionfield.fieldType.equalsIgnoreCase('Boolean'))
                                secfld.Nrich__Field_Type__c             = 'Boolean';
                            else if(sectionfield.fieldType.equalsIgnoreCase('TextArea'))
                                secfld.Nrich__Field_Type__c             = 'Text Area'; 
                            else if(sectionfield.fieldType.equalsIgnoreCase('LongTextArea'))
                                secfld.Nrich__Field_Type__c             = 'Long Text Area';
                            else if(sectionfield.fieldType.equalsIgnoreCase('Email'))
                                secfld.Nrich__Field_Type__c             = 'Email';
                            else if(sectionfield.fieldType.equalsIgnoreCase('Phone'))
                                secfld.Nrich__Field_Type__c             = 'Phone';
                            else if(sectionfield.fieldType.equalsIgnoreCase('Percent'))
                                secfld.Nrich__Field_Type__c             = 'Percent';
                            else
                                secfld.Nrich__Field_Type__c             = sectionfield.fieldType;
                            secfld.Nrich__Mandatory__c              = sectionfield.mandatory!=NULL?sectionfield.mandatory:FALSE;
                            if(sectionfield.fieldValidationType!=NULL)
                                secfld.Nrich__Validation_Type__c        = sectionfield.fieldValidationType;
                            if(sectionfield.fieldValidationLength!=NULL)
                                secfld.Nrich__Field_Length__c           = sectionfield.fieldValidationLength;
                            if(sectionfield.fieldValidationRegex!=NULL)
                                secfld.Nrich__Regex__c                  = sectionfield.fieldValidationRegex;
                            if(sectionfield.sectionColumn=='1')
                                secfld.Nrich__Coloumn__c                = 'Column-1'; 
                            else if(sectionfield.sectionColumn=='2')
                                secfld.Nrich__Coloumn__c                = 'Column-2'; 
                            secfld.Nrich__Order__c                  = sectionfield.sectiondfieldOrder;
                            if(!String.isNotBlank(sectionfield.sectiondfieldId))
                                secfld.Nrich__Section__c                = result[Integer.valueof(sectionfield.sectionOrder-1)].getId();
                            secfld.Nrich__Object_API_Name__c         = sectionfield.fieldObjectName;
                            if(sectionfield.resume!=NULL)
                                secfld.Nrich__Resume__c                  = sectionfield.resume;
                            sectionfieldsList.add(secfld);
                        }
                    system.debug('inserylist' + sectionfieldsList);
                    list<Nrich__Section_Fields__c> sectionfieldDeleteList = new list<Nrich__Section_Fields__c>();
                    if(template!=NULL && template.Id!=NULL){               
                        for(Nrich__Section_Fields__c sec: [select Id from Nrich__Section_Fields__c where Nrich__Section__r.Nrich__template__c=: template.Id]){
                            if(sectionFieldExistingMap!=NULL && sectionFieldExistingMap.size()>0 && !sectionFieldExistingMap.containsKey(sec.Id))
                                sectionfieldDeleteList.add(sec);
                        }
                    }
                    if(sectionfieldsList.size()>0)
                        database.upsert(sectionfieldsList, true);
                    
                    if(sectionfieldDeleteList.size()>0)
                        database.delete(sectionfieldDeleteList, true);
                }
                templateSaveResponse responseval = new templateSaveResponse();
                responseval.templateIdMessage = templateresult.getId();
                responseval.isError = false;
                return responseval;
            }
            else{
                templateSaveResponse responseval = new templateSaveResponse();
                responseval.isError = true;
                responseval.templateIdMessage = templateresult.getErrors()[0].getMessage();
                return responseval;
            }
        }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TemplateCreationController.saveTemplate', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    //Start of response Class
    public class templateSaveResponse{
        @AuraEnabled public String templateIdMessage {get; set;}
        @AuraEnabled public Boolean isError {get; set;}
    }
    
    //Start of Wrapper Class
    public class TemplateVal{
        @AuraEnabled public list<Nrich__Section__c> sectionList {get; set;}
        @AuraEnabled public Nrich__Template__c template {get; set;}
        @AuraEnabled public Nrich__Job__c job {get; set;}
        @AuraEnabled public Nrich__Job_Round__c jobround {get; set;}
        @AuraEnabled public list<FieldVal> sectionfieldList {get; set;}
        @AuraEnabled public list<FieldVal> availablefieldList {get; set;}
        public TemplateVal(){
            sectionList =  new list<Nrich__Section__c>();
            sectionfieldList = new list<FieldVal>();
            availablefieldList = new list<FieldVal>();
        }
    }
    
    public class FieldVal{
        @AuraEnabled public String fieldName {get; set;}
        @AuraEnabled public String fieldLabel {get; set;}
        @AuraEnabled public String fieldObjectName {get; set;}
        @AuraEnabled public String fieldType {get; set;}
        @AuraEnabled public Boolean mandatory {get; set;}
        @AuraEnabled public String fieldValidationType {get; set;}
        @AuraEnabled public Decimal fieldValidationLength {get; set;}
        @AuraEnabled public String fieldValidationRegex {get; set;}
        @AuraEnabled public Integer sectionOrder {get; set;}
        @AuraEnabled public String sectionColumn {get; set;}
        @AuraEnabled public Integer sectiondfieldOrder {get; set;}
        @AuraEnabled public String sectiondfieldId {get; set;}
        @AuraEnabled public Boolean resume {get; set;}
    }
    //End of Wrapper Class
}