@isTest
public class TestPolicyTrigger {
    
    @isTest static void policy(){
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        User u1 = new User(Alias = 'st167', Email='standarduser@test.com', 
                           EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p.Id, 
                           TimeZoneSidKey='America/Los_Angele', UserName='standarer@testorg.com12');
        
        
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
          //  insert lleave;
            cleave.Nrich__Is_Active__c=True;
          //  update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            
            Nrich__Employee__c emp1=new Nrich__Employee__c(Name='TestUser11' , Nrich__EmployeeDesignation__c ='Developer',
                                             
                                             Nrich__Total_Experience__c=2,Nrich__Related_User__c=u1.id);
            insert emp1;
            
            
            Nrich__Policy__c policy=new Nrich__Policy__c();
            policy.Nrich__Policy_Applicable_From__c=System.today();
            policy.Nrich__Pulished_Date__c=System.today()+1;
            policy.Nrich__status__c='Draft';
            insert policy;
            
            
            policy.Nrich__status__c='Published';
            update policy;
            
            System.assertEquals('Published', policy.Nrich__status__c);
            
            
        }
        
    }
}