/********************************************************************
* Company: Absyz
* Developer: Seshu Varma
* Created Date: 30/06/2018
* Description: fetching Asset List
********************************************************************/
public  with sharing class EmailToAssetRequestController {
    
    public String objType {get;set;}
    public ID relatedId {get;set;}
    
    public list<Nrich__Asset_Request__c  > AssetApplist{get;set;}
    
    public List<Nrich__Asset_Request__c> getAssetsList (){
        try{
        list<Nrich__Asset_Request__c> AssetApplist=new list<Nrich__Asset_Request__c>();
        if(relatedId != null && ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__Date_Approved__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__Status__c')){
            
            System.debug('RelatedId' +relatedId);
            AssetApplist=[select id,Name,Nrich__Date_Approved__c,Nrich__Employeee__r.Name , Nrich__Status__c  from Nrich__Asset_Request__c where id=:relatedId ];
            
            system.debug('AssetApplist' +AssetApplist);
        }
        return AssetApplist;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmailToAssetRequestController.getAssetsList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}