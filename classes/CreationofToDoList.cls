/****************************************************************************************************
    * Company: Absyz
    * Developer: Seshu Varma
    * Created Date: 23/10/2018
    * Description: Methods creates and updates ToDoList
*****************************************************************************************************/
public class CreationofToDoList {
    
    public static void CreationofToDoListMethod(list<Sobject> sobjectList, String objType){
        try{
            list<Nrich__ToDoList__c> ToDoList = new list<Nrich__ToDoList__c>();
            if(sobjectList!=NULL && sobjectList.size()>0 && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Due_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Record_Id__c') &&
               ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Related_User__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Title__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ToDoList__c', 'Nrich__Body__c')
              ){
                  for(Sobject obj: sobjectList){
                      Nrich__ToDoList__c toDo = new Nrich__ToDoList__c();
                      toDo.Nrich__Status__c = Label.Open;
                      date duedate;
                      if(Test.isRunningTest()){
                          duedate =system.today();
                      }
                      else{
                          duedate = date.valueof(obj.get('createddate'));                       
                      }
                      toDo.Nrich__Due_Date__c = duedate;
                      toDo.Nrich__Record_Id__c = obj.Id;
                      if(objType.equalsIgnoreCase('Nrich__Certification_Request__c') ){
                          toDo.Nrich__Related_User__c = string.valueof(obj.get('Nrich__Approved_Id_Technical__c'));                
                          toDo.Nrich__Title__c = Label.CertificationRequestForEmployee +string.valueof(obj.get('Nrich__EmployeeName__c'));
                          toDo.Nrich__Body__c = string.valueof(obj.get('Nrich__EmployeeName__c'))+Label.CertificationBody +String.Valueof(obj.get('Nrich__CertName__c'))+Label.Onthedateof +date.valueof(obj.get('Nrich__Expected_Ceritifcation_Date__c'));                 
                      }
                      else if(objType.equalsIgnoreCase('Nrich__Leave_Request__c')){                
                          toDo.Nrich__Related_User__c = string.valueof(obj.get('Nrich__Approved_Id_Technical__c'));                
                          toDo.Nrich__Title__c = Label.LeaveRequestForEmployee +string.valueof(obj.get('Nrich__RequestorName__c'));
                          toDo.Nrich__Body__c = string.valueof(obj.get('Nrich__RequestorName__c'))+Label.hasapplied +String.valueof(obj.get('Nrich__Leave_Type__c')) +Label.From +Date.Valueof(obj.get('Nrich__Start_Date__c'))+Label.To+date.valueof(obj.get('Nrich__End_Date__c'))+Label.forthefollowingreason +String.valueof(obj.get('Nrich__Comments__c'));                 
                      }
                      else if(objType.equalsIgnoreCase('Nrich__Travel_Request__c')){                
                          toDo.Nrich__Related_User__c = string.valueof(obj.get('Nrich__Approved_Id_Technical__c'));                
                          toDo.Nrich__Title__c = Label.TravelRequestForEmployee +string.valueof(obj.get('Nrich__Requestor_Name_Technical__c'));
                          toDo.Nrich__Body__c = string.valueof(obj.get('Nrich__Requestor_Name_Technical__c'))+Label.hasappliedtravelrequestfrom +String.valueof(obj.get('Nrich__Source_Location__c')) +Label.To+String.valueof(obj.get('Nrich__Destination_Location__c'))+Label.From+Date.Valueof(obj.get('Nrich__Start_Date__c'))+Label.To+date.valueof(obj.get('Nrich__End_Date__c'))+Label.forthefollowingreason+String.valueof(obj.get('Nrich__Reason_For_Travel__c'));                 
                      }
                      else if(objType.equalsIgnoreCase('Nrich__Asset_Request__c')){                
                          toDo.Nrich__Related_User__c = string.valueof(obj.get('Nrich__Approver_Id__c'));                
                          toDo.Nrich__Title__c = Label.AssetRequestForTheEmployee +string.valueof(obj.get('Nrich__RequestorName__c'));
                          toDo.Nrich__Body__c = string.valueof(obj.get('Nrich__RequestorName__c'))+Label.hasappliedAssetRequest +String.valueof(obj.get('Nrich__Asset_Category__c'))+Label.forthefollowingreason+String.valueof(obj.get('Nrich__Reason__c'));                 
                      }
                      else if(objType.equalsIgnoreCase('Nrich__Approval__c')){                
                          toDo.Nrich__Related_User__c = string.valueof(obj.get('Nrich__ManagerId__c'));                
                          toDo.Nrich__Title__c = Label.TimeSheetSubmittedForTheEmployee +string.valueof(obj.get('Nrich__RequestorName__c'));
                          toDo.Nrich__Body__c = string.valueof(obj.get('Nrich__RequestorName__c'))+Label.HasSubmittedTimeSheet +String.valueof(obj.get('Nrich__Start_Date__c'))+Label.To+String.valueof(obj.get('Nrich__End_Date__c'));                 
                      }
                      ToDoList.add(toDo);                                 
                  }
                  
              }
            
            if(ToDoList.size()>0)
                list<Database.SaveResult> SaveResult = database.insert(ToDoList, false);
        }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CreationofToDoList.CreationofToDoListMethod', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    
    public static void updateToDoListMethod(Set<Id> recordIdSet){
        try{        
        if(recordIdSet!=NULL && recordIdSet.size()>0){
            list<Nrich__ToDoList__c> ToDoList = new list<Nrich__ToDoList__c>();
          
                for(Nrich__ToDoList__c todo: [Select Id, Nrich__Status__c from Nrich__ToDoList__c where Record_Id__c IN: recordIdSet AND Nrich__Status__c=:Label.Open ]){
                    todo.Nrich__Status__c = Label.Closed;
                    ToDoList.add(todo);
                }
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ToDoList__c.getSObjectType().getDescribe();
            Map<String, Schema.SObjectField> maps = Schema.SObjectType.Nrich__ToDoList__c.fields.getMap();
            for(String fieldName : maps.keySet()) {
                if(maps.get(fieldName).getDescribe().isUpdateable() && objectDescriptioncontent.isUpdateable() && ToDoList.size()>0) {
                    // custom1.put(fieldName , 'some value');
                    Database.update(ToDoList);
                }
            }
        }
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CreationofToDoList.updateToDoListMethod', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}