@isTest
public class TestEmailtoCertificationReqController {
    
    
    @isTest static void  Certificationlist()
    {
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'sta12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id,ManagerId =u1.id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com1234');
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
          //  insert lleave;
            cleave.Nrich__Is_Active__c=True;
          //  update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            
            Nrich__Employee_Leave_Category__c emleavecategory = new Nrich__Employee_Leave_Category__c();
            emleavecategory.Nrich__Leave_Card__c=empleavecard.id;
            
            insert emleavecategory;
            
            Nrich__Certification__c certification=new Nrich__Certification__c(Name='Test12', Nrich__Active__c=true);
            certification.Nrich__Cost__c=50;
            certification.Nrich__Employee__c=emp.id;
            certification.Nrich__Applicable_Designation__c='Developer'; 
            insert certification;
            Nrich__Certification_Request__c  cer =new Nrich__Certification_Request__c(Nrich__Certification_Name__c=certification.id, Nrich__Employeee__c =emp.id,Nrich__Approval_Status__c='Applied');
            insert cer;
            list<Nrich__Certification_Request__c> clist=new list<Nrich__Certification_Request__c>();
            clist.add(cer);
            
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Hii Every one' ); 
            req.setObjectId(cer.Id); 
            req.setNextApproverIds(new Id[] {cer.Nrich__Employeee__c});
            
            
            // CertificationDetailsController.submitforapproval(cer.id); 
            
            EmailtoCertificationReqController em= new EmailtoCertificationReqController();
            em.objType='Nrich__Certification_Request__c';
            em.CertificationAppList=clist;
            em.getCertificationList();
            System.assertEquals('Applied', cer.Nrich__Approval_Status__c);
            CertificationDetailsController.submitforapproval(cer.id); 
            
        }   
    }
    
}