/********************************************************************
* Company: Absyz
* Developer: Anivesh
* Created Date: 16/03/2019
* Description: Updating hours 
********************************************************************/
public class UserStoryUpdateTriggerHandler {
    
    // custom roll up at sprint level
    public static void usrstryHourUpdate(list < string > sprintids) {
        try {
        Map < Id, Nrich__Sprint__c  > mapofSprint = new Map < Id, Nrich__Sprint__c  > ();
        Map < Id, Decimal > MapActualCount = new Map < Id, Decimal > ();
        Map < Id, Decimal > MapEstimatedCount = new Map < Id, Decimal > ();
        list < Nrich__Sprint__c  > sprintupdate = new list < Nrich__Sprint__c  > ();
        
        if(sprintids.size()>0 && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Actual_Effort__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__sprint__c') &&
           ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Estimated_Effort__c')){        
              for (Nrich__User_Story__c  usstryrec: [select Id, name, Nrich__Actual_Effort__c, Nrich__Estimated_Effort__c, Nrich__Sprint__c from Nrich__User_Story__c  where Nrich__Sprint__c in: sprintids]) {
                  system.debug(usstryrec);
                  Decimal sumofacteff = 0, sumofesteff = 0;
                  
                  if (usstryrec.Nrich__Actual_Effort__c != null) {
                      if (MapActualCount.containsKey(usstryrec.Nrich__sprint__c)) {                          
                          sumofacteff = MapActualCount.get(usstryrec.Nrich__sprint__c) + usstryrec.Nrich__Actual_Effort__c;
                          MapActualCount.put(usstryrec.Nrich__Sprint__c, sumofacteff);                          
                      } else {                         
                          MapActualCount.put(usstryrec.Nrich__Sprint__c, usstryrec.Nrich__Actual_Effort__c);                          
                      }                   
                  }
                  
                  if (usstryrec.Nrich__Estimated_Effort__c != null) {
                      
                      If(MapEstimatedCount.containsKey(usstryrec.Nrich__sprint__c)) {
                          
                          sumofesteff = MapEstimatedCount.get(usstryrec.Nrich__sprint__c) + usstryrec.Nrich__Estimated_Effort__c;
                          MapEstimatedCount.put(usstryrec.Nrich__Sprint__c, sumofesteff);
                          
                      }
                      else {
                          
                          MapEstimatedCount.put(usstryrec.Nrich__Sprint__c, usstryrec.Nrich__Estimated_Effort__c);
                          
                      }
                  }
                  
                  mapofSprint.put(usstryrec.Nrich__Sprint__c, new Nrich__Sprint__c (id = usstryrec.Nrich__Sprint__c, Nrich__Estimated_Effort__c = 0, Nrich__Actual_Effort__c = 0));
              }
          }
        
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Estimated_Effort__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Actual_Effort__c') ){
            for (Id sprntid: SprintIds) {
                
                Nrich__Sprint__c  sprintRec = new Nrich__Sprint__c (id = sprntid, Nrich__Estimated_Effort__c = 0, Nrich__Actual_Effort__c = 0);
                sprintRec.Nrich__Estimated_Effort__c = MapEstimatedCount.get(sprntid) != Null ? MapEstimatedCount.get(sprntid) : sprintRec.Nrich__Estimated_Effort__c;
                sprintRec.Nrich__Actual_Effort__c = MapActualCount.get(sprntid) != Null ? MapActualCount.get(sprntid) : sprintRec.Nrich__Actual_Effort__c;
                sprintupdate.add(sprintRec);
                
            }
        }
        
        
            system.debug('sprint ===' + sprintupdate);
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Sprint__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isUpdateable() && sprintupdate.size()>0)
                database.update(sprintupdate);
            // update sprintupdate;
        } catch (exception e) {
           Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='UserStoryUpdateTriggerHandler.usrstryHourUpdate', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        }
        
    }
    
    
    // custom rollup at sprint on delete of user story
    public static void usstryUpdateOnRemove(list < string > SprintIds) {       
        usrstryHourUpdate(SprintIds);        
    }
    
    // for custom roll up at project level
    public static void projectRollUp(set < Id > UserStryProjId) {
        try{
        list < Nrich__Project__c > projectrollupdate = new list < Nrich__Project__c > ();
        Map < Id, Nrich__Project__c > mapProject = new Map < Id, Nrich__Project__c > ();
        Map < Id, Decimal > MapActualCount = new Map < Id, Decimal > ();
        Map < Id, Decimal > MapEstimatedCount = new Map < Id, Decimal > ();
        
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Project__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Actual_Effort__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Estimated_Effort__c')){
               for (Nrich__User_Story__c  UserStryRecord: [select Id, Name, Nrich__Project__c, Nrich__Actual_Effort__c, Nrich__Estimated_Effort__c, Nrich__Sprint__c from Nrich__User_Story__c where Nrich__Project__c in: UserStryProjId]) {                  
                  Decimal sumofacteff = 0, sumofesteff = 0;
                  mapProject.put(UserStryRecord.Nrich__Project__c, new Nrich__Project__c(id = UserStryRecord.Nrich__Project__c, Nrich__Estimated_Effort_from_User_Story__c = 0, Nrich__Actual_Effort_from_User_Story__c = 0));
                  if (UserStryRecord.Nrich__Actual_Effort__c != null) {
                      if (MapActualCount.containsKey(UserStryRecord.Nrich__Project__c)) {
                          sumofacteff = MapActualCount.get(UserStryRecord.Nrich__Project__c) + UserStryRecord.Nrich__Actual_Effort__c;
                          MapActualCount.put(UserStryRecord.Nrich__Project__c, sumofacteff);
                      } else {                     
                          MapActualCount.put(UserStryRecord.Nrich__Project__c, UserStryRecord.Nrich__Actual_Effort__c);
                      }
                  }
                  if (UserStryRecord.Nrich__Estimated_Effort__c != null) {
                      If(MapEstimatedCount.containsKey(UserStryRecord.Nrich__Project__c)) {
                          sumofesteff = MapEstimatedCount.get(UserStryRecord.Nrich__Project__c) + UserStryRecord.Nrich__Estimated_Effort__c;
                          System.debug(sumofesteff +'sumofacteff =='+UserStryRecord.Nrich__Estimated_Effort__c);
                          MapEstimatedCount.put(UserStryRecord.Nrich__Project__c, sumofesteff);
                      }
                      else {
                          System.debug(sumofacteff +'before sumofacteff =='+UserStryRecord.Nrich__Estimated_Effort__c);
                          MapEstimatedCount.put(UserStryRecord.Nrich__Project__c, UserStryRecord.Nrich__Estimated_Effort__c);
                      }
                  }
              }
          }
        
        if( ObjectFieldAccessCheck.checkAccessible('Nrich__Project__c', 'Nrich__Estimated_Effort_from_User_Story__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Project__c', 'Nrich__Actual_Effort_from_User_Story__c')){
            for (Nrich__Project__c ProjRec: [select id, Nrich__Estimated_Effort_from_User_Story__c, Nrich__Actual_Effort_from_User_Story__c from Nrich__Project__c where id =: UserStryProjId]) {
                projRec.Nrich__Estimated_Effort_from_User_Story__c = MapEstimatedCount.get(ProjRec.id) != Null ? MapEstimatedCount.get(ProjRec.id) : projRec.Nrich__Estimated_Effort_from_User_Story__c;
                projRec.Nrich__Actual_Effort_from_User_Story__c = MapActualCount.get(ProjRec.id) != Null ? MapActualCount.get(ProjRec.id) : projRec.Nrich__Actual_Effort_from_User_Story__c;
                projectrollupdate.add(projRec);                
            }
        }
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Project__c.getSObjectType().getDescribe();
        if(objectDescriptioncontent.isUpdateable() && projectrollupdate.size()>0)
            database.update(projectrollupdate);
        }
        catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='UserStoryUpdateTriggerHandler.projectRollUp', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
        }
    
    
    // update sprint status if user story is completed
    public static void changeSprintStatus(set < id > sprntids) { //Map < Id, List < Manage_User_Stories__c >> MapSprntId_LstUserstry) {
        try{
        Map < id, Nrich__User_Story__c  > MapUserNotCompleted = new Map < id, Nrich__User_Story__c  > ();
        list < Nrich__Sprint__c > LstOfsprnt = new list <Nrich__Sprint__c > ();
        // list < Manage_User_Stories__c > UsrStry = new list < Manage_User_Stories__c > ([select id, name, status__c, Sprint__c from Manage_User_Stories__c where Sprint__c In: MapSprntId_LstUserstry.keyset() AND Status__c != 'Completed']);
        Map < Id, List < Nrich__Task__c >> mapTask = new Map < Id, List < Nrich__Task__c >> ();
        Map < id, List < Nrich__User_Story__c  > > MapSprntIdUser = new Map < id, List < Nrich__User_Story__c  >> ();
        // integer sizeofusrstry = us.size();
        boolean check1 = true;
        boolean check2 = true;
        //Map<string, Project_Pick_Values__c> Settingvalues =  Project_Pick_Values__c.getall();
        
        
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Sprint__c') ){
            for (Nrich__User_Story__c  SprintRecrd: [select id, name, Nrich__status__c, Nrich__Sprint__c from Nrich__User_Story__c  where Nrich__Sprint__c IN: sprntids AND Nrich__Status__c != Null  ]) {
                
                if (SprintRecrd.Nrich__status__c == System.Label.Completed) {
                    
                    if (MapSprntIdUser.containskey(SprintRecrd.Nrich__Sprint__c)) {
                        
                        List < Nrich__User_Story__c  > lstUserStry = MapSprntIdUser.get(SprintRecrd.Nrich__Sprint__c);
                        lstUserStry.add(SprintRecrd);
                        MapSprntIdUser.put(SprintRecrd.Nrich__Sprint__c, lstUserStry);
                        
                    } else {
                        MapSprntIdUser.put(SprintRecrd.Nrich__Sprint__c, new List < Nrich__User_Story__c  > {
                            SprintRecrd
                                });
                    }
                    
                } else if (SprintRecrd.Nrich__status__c != System.Label.Completed) {
                    MapUserNotCompleted.put(SprintRecrd.Nrich__Sprint__c, SprintRecrd);
                }
            }
        }
        system.debug(MapUserNotCompleted.size() + '<== incomplete am at us delete  completed==>' + MapSprntIdUser);
        
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__Sprint__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__User_Story__c')){
            list < Nrich__Task__c > lsttask = new list < Nrich__Task__c > ([select id, name, Nrich__Status__c, RecordType.Name, Nrich__Sprint__c from Nrich__Task__c
                                                                            where Nrich__Sprint__c IN: sprntids AND Nrich__User_Story__c = null AND Nrich__Status__c!= Null AND Nrich__Status__c!=: Label.Working_Fine  
                                                                           ]);
            If(lsttask.size() > 0) {
                For(Nrich__Task__c tskrecrd: lsttask) {
                    if (mapTask.containsKey(tskrecrd.Nrich__Sprint__c)) {
                        list < Nrich__Task__c > addlist = mapTask.get(tskrecrd.Nrich__Sprint__c);
                        addlist.add(tskrecrd);
                        mapTask.put(tskrecrd.Nrich__Sprint__c, addlist);
                        
                    } else {
                        mapTask.put(tskrecrd.Nrich__Sprint__c, new List < Nrich__Task__c > {tskrecrd});                        
                    }                    
                }
            }
        }
        
        if (MapSprntIdUser.size() > 0 && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__status__c')) {
            
            system.debug(sprntids.size() + ' i am at ticket' + sprntids);
            
            for (Id SprintId: sprntids) {
                
                system.debug(mapTask.size() + 'sprint record status' + sprntids.size());
                
                if (mapTask.size() == 0 && (MapSprntIdUser.get(SprintId).size() > 0 && MapUserNotCompleted.size() == 0)) {
                    Nrich__Sprint__c  spt = new Nrich__Sprint__c (Id = SprintId, Nrich__status__c = Label.Completed);
                    LstOfsprnt.add(spt);
                } else if (MapUserNotCompleted.size() > 0) {
                    Nrich__Sprint__c  newsprint = new Nrich__Sprint__c (id = SprintId, Nrich__status__c = Label.Progress);
                    LstOfsprnt.add(newsprint);
                }
                
            }
            
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Sprint__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isUpdateable() && LstOfsprnt.size()>0)
                Database.update(LstOfsprnt);
            
        }        
        } catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='UserStoryUpdateTriggerHandler.changeSprintStatus', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    
    public static void checkSprintStatus(set < id > userstoryIdSet) {
        if( ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Sprint__c')){
             for (Nrich__User_Story__c  userstory: [Select Id, Nrich__Sprint__c, Nrich__Sprint__r.Nrich__status__c from Nrich__User_Story__c where Id IN: userstoryIdSet]) {
                  if(String.isNotBlank(userstory.Nrich__Sprint__c) && String.isNotBlank(userstory.Nrich__Sprint__r.Nrich__status__c) && userstory.Nrich__Sprint__r.Nrich__status__c.equalsIgnoreCase(Label.Completed)) 
                     userstory.adderror(label.tickettrigger_er2);
                }               
         }      
    }
    
    public static void checkStatusOfTicket(set < id > usid, list < Nrich__User_Story__c  > usrstry) {
        try{
        set < id > ErrorUsId = new set < id > ();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__User_Story__c')){
            
            list < Nrich__Task__c > LstTask = new list < Nrich__Task__c > ([select id, Nrich__Status__c, Nrich__User_Story__c from Nrich__Task__c
                                                                            where Nrich__User_Story__c IN: usid AND(Nrich__Status__c!= Null AND Nrich__Status__c!=: System.Label.Working_Fine )
                                                                             ]);
            
            for (Nrich__Task__c tk: LstTask) {
                ErrorUsId.add(tk.Nrich__User_Story__c);
            }
        }
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Name') ){
            Map < Id, Nrich__User_Story__c  > LstUsrStry = new Map < Id, Nrich__User_Story__c  > ([select id, Name  from Nrich__User_Story__c  where id IN: ErrorUsId]);
            
            for (Nrich__User_Story__c  us: usrstry) {                
                if (LstUsrStry != NULL && LstUsrStry.containsKey(us.Id))
                    us.adderror(label.triggeruponuserstory_er3);                
            }
        }
    } catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='UserStoryUpdateTriggerHandler.checkStatusOfTicket', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      } 
    }
    
    public static void updateTeamMemberValues(Set < Id > TeamIds, list < Nrich__User_Story__c  > usrstry) {
        Map < Id, Nrich__Project_Team_Member__c > MapTeamMebers=new  Map < Id, Nrich__Project_Team_Member__c >();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Employee_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Project_Role__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Project_Team_Member__c', 'Nrich__Employee_Email__c') ){
               MapTeamMebers = new Map < Id, Nrich__Project_Team_Member__c > ([Select Id, Name, Nrich__Employee_Name__c, Nrich__Project_Role__c, Nrich__Employee_Email__c From Nrich__Project_Team_Member__c Where Id IN: TeamIds]);
           }
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Resource_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Resource_Email__c') &&
           ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Assigned_Too__c') ){
               for (Nrich__User_Story__c  UsRec: usrstry) {
                   if (MapTeamMebers.size()>0 && MapTeamMebers.containsKey(UsRec.Nrich__Assigned_Too__c)) {                       
                       UsRec.Nrich__Resource_Name__c = MapTeamMebers.get(UsRec.Nrich__Assigned_Too__c).Nrich__Employee_Name__c;
                       UsRec.Nrich__Resource_Email__c = MapTeamMebers.get(UsRec.Nrich__Assigned_Too__c).Nrich__Employee_Email__c;                       
                   }
               }                             
           }        
    }
    // for custom roll up at project level
    public static void projectRollout(set < Id > UserStryProjId, set < Id > SprintIds) {
        try{
        system.debug('delete Called  here ');
        list < Nrich__Project__c > projectrollupdate = new list < Nrich__Project__c > ();
        Map < Id,Nrich__Project__c > mapProject = new Map < Id, Nrich__Project__c > ();
        Map < Id, Decimal > MapActualCount = new Map < Id, Decimal > ();
        Map < Id, Decimal > MapEstimatedCount = new Map < Id, Decimal > ();
       if(ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Project__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Actual_Effort__c') &&
           ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Estimated_Effort__c') &&  ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Sprint__c')  
          )
        {
              List < Nrich__User_Story__c  > listofdata = new List < Nrich__User_Story__c  > ([select Id, Name, Nrich__Project__c, Nrich__Actual_Effort__c, Nrich__Estimated_Effort__c, Nrich__Sprint__c, (select Id, Nrich__User_Story__c, Nrich__Project__c from Nrich__Tasks__r) from Nrich__User_Story__c  where Nrich__Project__c in: UserStryProjId AND Id IN: SprintIds ALL ROWS] );
              
              for (Nrich__User_Story__c  UserStryRecord: listofdata) {
                  
                  if (UserStryRecord.Nrich__Tasks__r.size() == 0) {
                      
                      Decimal sumofacteff = 0, sumofesteff = 0;
                      mapProject.put(UserStryRecord.Nrich__Project__c, new Nrich__Project__c(id = UserStryRecord.Nrich__Project__c, Nrich__Estimated_Effort_from_User_Story__c = 0, Nrich__Actual_Effort_from_User_Story__c = 0));
                      if (UserStryRecord.Nrich__Actual_Effort__c != null) {
                          
                          if (MapActualCount.containsKey(UserStryRecord.Nrich__Project__c)) {
                              sumofacteff = MapActualCount.get(UserStryRecord.Nrich__Project__c) + UserStryRecord.Nrich__Actual_Effort__c;
                              MapActualCount.put(UserStryRecord.Nrich__Project__c, sumofacteff);
                              
                          } else {
                              MapActualCount.put(UserStryRecord.Nrich__Project__c, UserStryRecord.Nrich__Actual_Effort__c);
                          }
                      }
                      
                      if (UserStryRecord.Nrich__Estimated_Effort__c != null) {
                          If(MapEstimatedCount.containsKey(UserStryRecord.Nrich__Project__c)) {
                              sumofesteff = MapEstimatedCount.get(UserStryRecord.Nrich__Project__c) + UserStryRecord.Nrich__Estimated_Effort__c;
                              MapEstimatedCount.put(UserStryRecord.Nrich__Project__c, sumofesteff);
                          }
                          else {
                              MapEstimatedCount.put(UserStryRecord.Nrich__Project__c, UserStryRecord.Nrich__Estimated_Effort__c);
                          }
                      }
                      
                  }
              }
          }
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Project__c', 'Nrich__Estimated_Effort_from_User_Story__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Project__c', 'Nrich__Actual_Effort_from_User_Story__c')){
            list < Nrich__Project__c > projectupdate = new list < Nrich__Project__c > ([select id, Nrich__Estimated_Effort_from_User_Story__c,Nrich__Actual_Effort_from_User_Story__c from Nrich__Project__c where id =: UserStryProjId   ]);
            for (Nrich__Project__c ProjRec: projectupdate) {
                system.debug(MapActualCount.get(ProjRec.id) + 'delete called ' + MapEstimatedCount.get(ProjRec.id));
                projRec.Nrich__Estimated_Effort_from_User_Story__c = MapEstimatedCount.get(ProjRec.id) != Null ? projRec.Nrich__Estimated_Effort_from_User_Story__c - MapEstimatedCount.get(ProjRec.id) : projRec.Nrich__Estimated_Effort_from_User_Story__c;
                projRec.Nrich__Actual_Effort_from_User_Story__c = MapActualCount.get(ProjRec.id) != Null ? projRec.Nrich__Actual_Effort_from_User_Story__c - MapActualCount.get(ProjRec.id) : projRec.Nrich__Actual_Effort_from_User_Story__c;
                projectrollupdate.add(projRec);
                
            }
        }
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Project__c.getSObjectType().getDescribe();
        if(objectDescriptioncontent.isUpdateable() && projectrollupdate.size()>0)
            database.update(projectrollupdate);
        
    }
        catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='UserStoryUpdateTriggerHandler.projectRollout', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
}
    
    public static void ValidateUsrstryLoe (List< Nrich__User_Story__c  > UserStryRec) {
        map< id,Nrich__User_Story__c  > mapuserstory = new map< id,Nrich__User_Story__c  >();
        set<Id> usrId = new set<id>();
        for(Nrich__User_Story__c  usrstry : UserStryRec){
            usrId.add(usrstry.id);
            mapuserstory.put(usrstry.id,usrstry);        
        }
        
        for(Nrich__User_Story__c  usrrec: [select Id, Name, Nrich__Project__c, Nrich__Actual_Effort__c, Nrich__Estimated_Effort__c, Nrich__Sprint__c, (select Id, Nrich__User_Story__c, Nrich__Project__c from Nrich__Tasks__r ) from Nrich__User_Story__c  where Id IN: usrId ]){
            if(usrrec.Nrich__Tasks__r.size()>0){               
                system.debug(usrrec.Nrich__Estimated_Effort__c +'error to display'+usrrec.Nrich__Actual_Effort__c);
                mapuserstory.get(usrrec.id).adderror(Label.UserStoryWithTaskCannotBeDeleted);
            }
        }        
    }         
}