@RestResource(urlMapping='/Onboarding')
Global class CallOutToGetOnBoardingDetails {
    
    //*******************************************************************************************
    //Created by      : Prasad Vievk
    //Method Name 	  : doPost
    //Overall Purpose : This method is used Update Job Application On boarding Details.
    //InputParams     : N/A
    //OutputParams    : N/A
    //*******************************************************************************************
    @HttpPost
    global static void doPost(){
        
        RestRequest req = RestContext.request;
        RestResponse res= RestContext.response;
        try{
            system.debug('-----------data--------' + req);
            System.debug('jjk'+ req.requestBody.toString());
            object data1 = (object) JSON.deserializeUntyped(req.requestBody.toString());
            Map < String, Object > result = (Map < String, Object > ) data1;
            map<string,string> apptocandMap =new map<string,string>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Applicant_to_Candidate__mdt', 'Nrich__Mapping_Applicant_Field__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Applicant_to_Candidate__mdt', 'Nrich__Mapping_Candidate_Field__c'))
            { for(Nrich__Applicant_to_Candidate__mdt atoc: [Select Id, Nrich__Mapping_Applicant_Field__c, Nrich__Mapping_Candidate_Field__c from Nrich__Applicant_to_Candidate__mdt where Id!=NULL]){
                apptocandMap.put(atoc.Nrich__Mapping_Applicant_Field__c, atoc.Nrich__Mapping_Candidate_Field__c);
            }
            }
            Nrich__Job_Application__c jobApp=new Nrich__Job_Application__c();
            for( string s:result.Keyset()){
                system.debug('jobApp--'+jobApp);
                jobApp.put(s,result.get(s));
                system.debug('jobApp--'+jobApp);
            }
            system.debug('jobApp--'+jobApp);
            Database.SaveResult jobAppInsert=Database.update(jobApp,false);
            if(jobAppInsert.isSuccess()){
                system.debug(jobAppInsert.getId());
                res.statusCode=200;
                res.responseBody=blob.valueOf('Successfully Updated-- '+jobAppInsert.id);
            }
            else{
                res.statusCode=406;
                for(Database.Error e:jobAppInsert.getErrors()){
                    res.responseBody=Blob.valueOf(e.getStatusCode()+'--'+e.getFields()+'--'+e.message);
                }
            }
            system.debug('jobAppInsert-->'+jobAppInsert);
        }
        catch(Exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CallOutToGetOnBoardingDetails.doPost', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        }
    }
}