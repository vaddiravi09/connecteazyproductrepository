public class EmailtoWorkAnniversaryController {
    
    public String objType {get;set;}
    public ID relatedId {get;set;}
    public string imageURL{get{ 
      string URLval =URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id=';
      List< document > documentList=[select name from document where
                                       Name=:Label.WorkAnniversaryBackground];
      if(documentList.size()>0)
        {
            URLval=URLval+documentList[0].id+'&oid='+UserInfo.getOrganizationId();
        }  
      return URLval;                               
    }
    set;}
        
}