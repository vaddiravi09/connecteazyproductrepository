public with sharing class Profilerec {
    
    /****************************************************************************************************
* Company:Absyz
* Developer:Ashish
* Created Date:29/10/18
* Description:Fetching the Profile Details 
*****************************************************************************************************/
    public static string  EmployyeID;
    
    //for viewing records in a modal
    @AuraEnabled
    public static Nrich__Employee__c viewrecords(){
        try{
        string strEmpID = userinfo.getuserid();
        Nrich__Employee__c objEmp=new Nrich__Employee__c();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__ProfilePic__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__EmployeeDesignation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Employee_Id__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Mentor__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Email__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Experience_in_Organization__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Total_Experience__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Phone__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Alternate_Phone__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact_Person__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact_Relation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Alternate_Email__c') 
          )
            objEmp = [select id,
                      Name,Nrich__ProfilePic__c,
                      Nrich__EmployeeDesignation__c,
                      Nrich__Employee_Id__c,
                      Nrich__Primary_Phone__c,
                      Nrich__Alternate_Phone__c,
                      Nrich__Emergency_Contact_Person__c ,
                      Nrich__Emergency_Contact__c,
                      Nrich__Emergency_Contact_Relation__c,
                      Nrich__Alternate_Email__c,
                      Nrich__Primary_Email__c,
                      Nrich__Blood_Group__c,
                      Nrich__Mentor__c,
                      Nrich__Mentor__r.Name,
                      Nrich__Mentor__r.Nrich__Employee_Id__c,
                      Nrich__Mentor__r.Nrich__EmployeeDesignation__c,
                      Nrich__Experience_in_Organization__c,
                      Nrich__Total_Experience__c,
                      Nrich__Previous_Experience__c
                      from Nrich__Employee__c 
                      where Nrich__Related_User__c =: strEmpID
                      limit 1]; 
        system.debug('objEmp****'+objEmp);
        EmployyeID=objEmp.id;
        system.debug('EmployyeID in view' +EmployyeID);
        return objEmp;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='Profilerec.viewrecords', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    
    
    //For inserting record in edit modal
    @AuraEnabled
    public static Nrich__Employee__c  insertrec(Nrich__Employee__c empdetails ){
        try{
        string strEmpID = userinfo.getuserid();
        Nrich__Employee__c objEmp=new Nrich__Employee__c();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__ProfilePic__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__EmployeeDesignation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Employee_Id__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Mentor__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Email__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Experience_in_Organization__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Total_Experience__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Phone__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Alternate_Phone__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact_Person__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact_Relation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Alternate_Email__c') 
          )
            
            objEmp = [select id,
                      Name,Nrich__ProfilePic__c,
                      Nrich__EmployeeDesignation__c,
                      Nrich__Employee_Id__c,
                      Nrich__Mentor__c,
                      Nrich__Mentor__r.Name,
                      Nrich__Mentor__r.Nrich__Employee_Id__c,
                      Nrich__Mentor__r.Nrich__EmployeeDesignation__c,
                      Nrich__Experience_in_Organization__c,
                      Nrich__Total_Experience__c,
                      Nrich__Previous_Experience__c
                      from Nrich__Employee__c 
                      where Nrich__Related_User__c =: strEmpID
                      limit 1]; 
        system.debug('objEmp****'+objEmp);
        EmployyeID=objEmp.id;
        system.debug('EmployyeID'+EmployyeID);
        empdetails.id=EmployyeID;
        update empdetails;
        return empdetails;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='Profilerec.insertrec', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    //For Display or geting image
    
    @Auraenabled
    public static string getProfilePicture() {
        try{
        string strEmpID = userinfo.getuserid();
        Nrich__Employee__c objEmp=new Nrich__Employee__c();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__ProfilePic__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__EmployeeDesignation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Employee_Id__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Mentor__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Email__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Experience_in_Organization__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Total_Experience__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Phone__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Alternate_Phone__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact_Person__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact_Relation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Alternate_Email__c') 
          )
            
            objEmp = [select id,
                      Name,Nrich__ProfilePic__c,
                      Nrich__EmployeeDesignation__c,
                      Nrich__Employee_Id__c,
                      Nrich__Mentor__c,
                      Nrich__Mentor__r.Name,
                      Nrich__Mentor__r.Nrich__Employee_Id__c,
                      Nrich__Mentor__r.Nrich__EmployeeDesignation__c,
                      Nrich__Experience_in_Organization__c,
                      Nrich__Total_Experience__c,
                      Nrich__Previous_Experience__c
                      
                      from Nrich__Employee__c 
                      where Nrich__Related_User__c =: strEmpID
                      limit 1]; 
        system.debug('objEmp****'+objEmp);
        
        EmployyeID=objEmp.id;
        system.debug('parentId---'+EmployyeID);
        ContentDocumentLink Docs=[select id,LinkedEntityId,ContentDocumentId from ContentDocumentLink where LinkedEntityId=:EmployyeID AND ContentDocument.FileType !='SNOTE'  limit 1];
        ContentDocument dc=[select id from ContentDocument where id=:Docs.ContentDocumentId order By createddate desc];
        contentVersion s=[select id,islatest from contentVersion where isLatest=true and ContentDocumentId=:dc.Id];
        system.debug('Contenet version--'+s);
        system.debug('content Document id'+dc.Id);
        return s.Id;
        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='Profilerec.getProfilePicture', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    
    
    //For inserting image in a modal
    @AuraEnabled
    public static void saveImage(String fileName, String base64Data, String contentType){
        try{
        system.debug('fileName' +fileName);
        string strEmpID = userinfo.getuserid();
        Nrich__Employee__c objEmp=new  Nrich__Employee__c();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__ProfilePic__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__EmployeeDesignation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Employee_Id__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Mentor__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Email__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Experience_in_Organization__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Total_Experience__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Primary_Phone__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Alternate_Phone__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact_Person__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Emergency_Contact_Relation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Alternate_Email__c') 
          )
            
            objEmp = [select id,
                      Name,Nrich__ProfilePic__c,
                      Nrich__EmployeeDesignation__c,
                      Nrich__Employee_Id__c,
                      Nrich__Mentor__c,
                      Nrich__Mentor__r.Name,
                      Nrich__Mentor__r.Nrich__Employee_Id__c,
                      Nrich__Mentor__r.Nrich__EmployeeDesignation__c,
                      Nrich__Experience_in_Organization__c,
                      Nrich__Total_Experience__c,
                      Nrich__Previous_Experience__c
                      from Nrich__Employee__c 
                      where Nrich__Related_User__c =: strEmpID
                      limit 1]; 
        system.debug('objEmp****'+objEmp);
        
        EmployyeID=objEmp.id;
        String contentDocumentIdval;
        system.debug('EmployyeID' +EmployyeID);
        if(string.isnotBlank(fileName) && string.isNotBlank(base64Data)){
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            if(objEmp!=NULL && objEmp.Nrich__ProfilePic__c!=NULL && objEmp.Nrich__ProfilePic__c.contains('/sfc/servlet.shepherd/version/download/')){
                String Idval = objEmp.Nrich__ProfilePic__c.substringbetween('/sfc/servlet.shepherd/version/download/','"></img>');
                if(String.isNotBlank(Idval)){
                    list<ContentVersion> cnvlist = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id =: Idval];
                    if(cnvlist.size()>0){
                        ContentVersion cv = new ContentVersion();
                        cv.VersionData =EncodingUtil.base64Decode(base64Data);
                        cv.ContentDocumentId = cnvlist[0].ContentDocumentId;
                        cv.Title = fileName;
                        cv.ContentLocation = 'S';
                        cv.PathOnClient = fileName;
                        database.SaveResult contentResult = database.insert(cv);
                        if(contentResult!=NULL && contentResult.isSuccess()){
                            contentDocumentIdval = contentResult.getId();
                        }
                    }
                }
            }else{
                ContentVersion cv = new ContentVersion();
                cv.ContentLocation = 'S';
                cv.VersionData =EncodingUtil.base64Decode(base64Data);
                cv.Title = fileName;
                cv.PathOnClient = fileName;
                database.SaveResult contentResult = database.insert(cv);
                
                if(contentResult!=NULL && contentResult.isSuccess()){
                    list<ContentDocumentLink> ContentDocumentLinklist = new list<ContentDocumentLink>();
                    list<ContentDistribution> ContentDistributionList = new list<ContentDistribution>();
                    for(ContentVersion ContentVersionlist : [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id =: contentResult.getId()]){
                        ContentDocumentLink cdl = new ContentDocumentLink();
                        cdl.ContentDocumentId = ContentVersionlist.ContentDocumentId;
                        cdl.LinkedEntityId = EmployyeID;
                        cdl.ShareType = 'V';
                        ContentDocumentLinklist.add(cdl); 
                        System.debug('Content documentlist' +ContentDocumentLinklist);
                        contentDocumentIdval = contentResult.getId();
                    }
                    Schema.DescribeSObjectResult objectDescriptioncontentdoc = ContentDocumentLink.getSObjectType().getDescribe();
                    if(objectDescriptioncontentdoc.isCreateable() && ContentDocumentLinklist.size()>0){
                        insert ContentDocumentLinklist;
                        
                    }
                }
            }
        }
        if(String.isNotBlank(contentDocumentIdval)){
            objEmp.Nrich__ProfilePic__c = '<img alt="'+fileName+'" src="'+URL.getSalesforceBaseUrl().toExternalForm()+'/sfc/servlet.shepherd/version/download/'+contentDocumentIdval+'"></img>';
            update objemp;
        }
        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='Profilerec.saveImage', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}