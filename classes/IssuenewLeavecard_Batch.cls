/****************************************************************************************************
    * Company: Absyz
    * Developer: Thadeus Ronn Thomas
    * Created Date: 23/10/2018
    * Description: Issuing new Leave cards and populating employee leave categories based on leave categories
*****************************************************************************************************/   

public class IssuenewLeavecard_Batch implements Database.Batchable < sObject > {
  
  public string query = 'select id, Name from Nrich__Employee__c where Nrich__Active__c=True';
    list<Nrich__Leave_Category__c> currentlcat= new list<Nrich__Leave_Category__c>();
    list<Nrich__Leave_Category__c> prevlcat= new list<Nrich__Leave_Category__c>();
    list<Nrich__Employee_Leave_Category__c> PrevEmployeeLeaveCategory= new list<Nrich__Employee_Leave_Category__c>();
    list<Nrich__Employee_Leave_Card__c> leavecardlist= new list<Nrich__Employee_Leave_Card__c>();
    list<Nrich__Employee_Leave_Category__c> ELClistInsert =new list<Nrich__Employee_Leave_Category__c>();
    Set<String> lcnames=new Set<String>();
    map<String,decimal> CFList=new map<String,decimal>();
    Nrich__Company_Leave_Structure__c currcls= new Nrich__Company_Leave_Structure__c();   

    public IssuenewLeavecard_Batch(Nrich__Company_Leave_Structure__c cls){
        if(cls!=NULL && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__LossofPay__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__Leave_category_code__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__Default__c')
            && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__Applicable_Leave_Structure__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Company_Leave_Structure__c', 'Nrich__Applicable_From__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Company_Leave_Structure__c', 'Nrich__Applicable_Till__c')
          ){ 
            currcls=cls;
            currentlcat = [Select Id, Name, Nrich__LossofPay__c, Nrich__Leave_category_code__c, Nrich__Default__c from Nrich__Leave_Category__c where Nrich__Applicable_Leave_Structure__c=:cls.Id];
            date fromdate = date.newinstance(cls.Nrich__Applicable_From__c.year()-1, cls.Nrich__Applicable_From__c.month(), cls.Nrich__Applicable_From__c.day());
            date tilldate = date.newinstance(cls.Nrich__Applicable_Till__c.year()-1, cls.Nrich__Applicable_Till__c.month(), cls.Nrich__Applicable_Till__c.day());  
            list<Nrich__Company_Leave_Structure__c> previousCompanyLeaveStru = [Select Id, Name, Nrich__Applicable_Till__c, Nrich__Applicable_From__c from Nrich__Company_Leave_Structure__c where Nrich__Applicable_From__c>=:fromdate AND Nrich__Applicable_Till__c<=:tilldate];
            if(previousCompanyLeaveStru.size()>0){
                prevlcat = [Select Id, Nrich__Leave_category_code__c from Nrich__Leave_Category__c where Nrich__Applicable_Leave_Structure__c =: previousCompanyLeaveStru[0].Id AND Nrich__Will_Carry_Forward__c=TRUE ];
                for(Nrich__Leave_Category__c lc: prevlcat)
                    lcnames.add(lc.Nrich__Leave_category_code__c);
                
                PrevEmployeeLeaveCategory = [Select Id, Name, Nrich__Leave_Card__c, Nrich__Leave_Card__r.Nrich__Employee__c, Nrich__Leave_category_code__c, Nrich__Leaves_Obtained__c, Nrich__Leaves_Remaining__c from Nrich__Employee_Leave_Category__c where Nrich__Leave_Card__r.Nrich__Leave_Structure__c =:previousCompanyLeaveStru[0].Id AND Nrich__Leave_category_code__c IN:lcnames
                                           ];
                for(Nrich__Employee_Leave_Category__c elc: PrevEmployeeLeaveCategory){
                    CFList.put(elc.Nrich__Leave_Card__r.Nrich__Employee__c+elc.Nrich__Leave_category_code__c, elc.Nrich__Leaves_Remaining__c);
                }
            }
        }
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List < Nrich__Employee__c > EmpList) {
        try{
        list<database.SaveResult> result;
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Card__c', 'Nrich__Employee__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Card__c', 'Nrich__Applicable_From__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Card__c', 'Nrich__Applicable_Till__c') && 
              ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Card__c', 'Nrich__Is_Active__c') 
              
              )
        for(Nrich__Employee__c Emp:EmpList){
            Nrich__Employee_Leave_Card__c elcard = new Nrich__Employee_Leave_Card__c();
            elcard.Name=Emp.Name+' - '+currcls.Nrich__Applicable_From__c.year();
            elcard.Nrich__Employee__c=Emp.id;
            elcard.Nrich__Applicable_From__c=currcls.Nrich__Applicable_From__c;
            elcard.Nrich__Applicable_Till__c=currcls.Nrich__Applicable_Till__c;
            elcard.Nrich__Is_Active__c=True;
            elcard.Nrich__Leave_Structure__c=currcls.Id;
            leavecardlist.add(elcard);
        }
         Schema.DescribeSObjectResult objectcontent = Nrich__Employee_Leave_Card__c.getSObjectType().getDescribe();
         if(objectcontent.isCreateable() && leavecardlist.size()>0)
           result = database.insert(leavecardlist, false);
             
        Set<String> employeeLeaveCardIDSet = new set<String>();
        for(database.SaveResult res: result){
            if(res.isSuccess())
                employeeLeaveCardIDSet.add(res.getId());
        }
        list<Nrich__Employee_Leave_Category__c> empcategorylist = new list<Nrich__Employee_Leave_Category__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Category__c', 'Nrich__Leave_category_code__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Category__c', 'Nrich__Loss_of_Pay__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Category__c', 'Nrich__Leaves_Obtained__c'))
        for(Nrich__Leave_Category__c leavecat: currentlcat){
            Nrich__Employee_Leave_Category__c elc = new Nrich__Employee_Leave_Category__c();
            elc.Name = leavecat.Name;
            elc.Nrich__Leave_category_code__c = leavecat.Nrich__Leave_category_code__c;
            elc.Nrich__Loss_of_Pay__c = leavecat.Nrich__LossofPay__c;
            elc.Nrich__Leaves_Obtained__c = leavecat.Nrich__Default__c;
            ELClistInsert.add(elc);
        }
        map<Integer, Nrich__Employee_Leave_Card__c> empLeavecardMap = new map<Integer, Nrich__Employee_Leave_Card__c>();
        integer i=0;
        
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Card__c', 'Nrich__Employee__c'))
        for(Nrich__Employee_Leave_Card__c empLeaveCard: [Select Id, Nrich__Employee__c from Nrich__Employee_Leave_Card__c where Id IN: employeeLeaveCardIDSet ]){           
            empcategorylist.addall(ELClistInsert);
            empLeavecardMap.put(i, empLeaveCard);
            i=i+1;
        }
        integer j=0;
        integer k=0;
        list<Nrich__Employee_Leave_Category__c> employeeleavecategoryInsertList = new list<Nrich__Employee_Leave_Category__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Category__c', 'Nrich__Leave_Card__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Category__c', 'Nrich__CarryForward__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Category__c', 'Nrich__Leaves_Obtained__c'))
        for(Nrich__Employee_Leave_Category__c leavecategory: empcategorylist){
            if(empLeavecardMap.size()>0 && empLeavecardMap.containskey(k)){
               leavecategory.Nrich__Leave_Card__c = empLeavecardMap.get(k).Id;
               if(CFList.size()>0 && CFList.containsKey(empLeavecardMap.get(k).Nrich__Employee__c+leavecategory.Nrich__Leave_category_code__c)){
                leavecategory.Nrich__CarryForward__c = CFList.get(empLeavecardMap.get(k).Nrich__Employee__c+leavecategory.Nrich__Leave_category_code__c);
                leavecategory.Nrich__Leaves_Obtained__c = leavecategory.Nrich__Leaves_Obtained__c+CFList.get(empLeavecardMap.get(k).Nrich__Employee__c+leavecategory.Nrich__Leave_category_code__c); 
               }    
               if(j<currentlcat.size())
                   j=j+1;
               else{
                   j=0;
                   k=k+1;
                }
                employeeleavecategoryInsertList.add(leavecategory);
            }
        }
        
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Employee_Leave_Category__c.getSObjectType().getDescribe();
        if(objectDescriptioncontent.isCreateable() && employeeleavecategoryInsertList.size()>0)
            database.insert(employeeleavecategoryInsertList, false);

    }catch(exception e){
          e.getLineNumber();
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='IssuenewLeavecard_Batch', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    public void finish(Database.BatchableContext BC) {}

}