/****************************************************************************************************
    * Company: Absyz
    * Developer: Raviteja
    * Created Date: 24/12/2018
    * Description: Batch class to create Employee Policy when a new policy is published
    *****************************************************************************************************/  
 global with sharing class EmployeePolicyCreation_Batch implements Database.Batchable < sObject > { 
     
    public list<String> policyIdList;
    global Database.QueryLocator start(Database.BatchableContext BC) {
        string query = 'select Id From Nrich__Employee__c where Nrich__Active__c=TRUE LIMIT 1';
        return Database.getQueryLocator(query);        
    }    
    global void execute(Database.BatchableContext BC, List < Nrich__Employee__c > employeeList) {
        try{
        if(employeeList!=NULL && employeeList.size()>0){
            list<Nrich__EmployeePolicy__c> EmployeePolicyInsertList = new list<Nrich__EmployeePolicy__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__EmployeePolicy__c', 'Nrich__Date_Issued__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__EmployeePolicy__c', 'Nrich__Employeee__c')
              && ObjectFieldAccessCheck.checkAccessible('Nrich__EmployeePolicy__c', 'Nrich__Policy__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__EmployeePolicy__c', 'Nrich__status__c')
              
              )
            for(String policyId: policyIdList){
                Nrich__EmployeePolicy__c emp = new Nrich__EmployeePolicy__c();
                emp.Nrich__Date_Issued__c = system.today();
                emp.Nrich__Employeee__c = employeeList[0].Id;
                emp.Nrich__Policy__c = policyId;
                emp.Nrich__status__c = Label.Pending;
                EmployeePolicyInsertList.add(emp);
            }
             Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Employee__c.getSObjectType().getDescribe();
                if(objectDescriptioncontent.isCreateable() && EmployeePolicyInsertList.size()>0)
                database.insert(EmployeePolicyInsertList, false);
        }
    }catch(exception e){
        e.getLineNumber();
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeePolicyCreation_Batch', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    
    global void finish(Database.BatchableContext BC) {}
}