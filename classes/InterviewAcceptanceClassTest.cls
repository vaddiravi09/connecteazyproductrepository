@isTest
public class InterviewAcceptanceClassTest{
    
    @testSetup public static  void SampleDate(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
      Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                          
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
     // insert lleave;
        cleave.Nrich__Is_Active__c=True;
      //  update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                        
                                        Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
        
        
        insert emp;
         
        
        /*Employee__c testemployee=new Employee__c();
testemployee.Name='Test Employee';
testemployee.Interview_Level__c='1';
testemployee.Gender__c='Male';
testemployee.Email__c='testemployee@gmail.com';
insert testemployee;
*/
        Nrich__Candidate__c testcandidate =new Nrich__Candidate__c();
        testcandidate.name='testCandidate';
        testcandidate.Nrich__Email__c='testabc@gmail.com';
        testcandidate.Nrich__Candidate_Last_Name__c='CandidateLastName';
        insert testcandidate;
        
        Nrich__Department__c  testDepartment = new Nrich__Department__c();
        testDepartment.Name='testDepartment';
        insert testDepartment;
        
        Nrich__InitialSetup__c cstObj = new Nrich__InitialSetup__c();
       // cstObj.Nrich__No_of_Days_before_Job_EndDate_Email__c = 20;
       // cstObj.Nrich__Email_Panel_Member_on_Interview_Schedule__c = false;
        cstObj.Nrich__No_of_Days_for_Reapplication_for_SameJob__c = 30;
        cstObj.Nrich__Email_Candidate_on_submission_of_appln__c = true;
        insert cstObj;     
        
        Nrich__Job__c testJob =new Nrich__Job__c();
        testJob.Nrich__Department__c=testDepartment.Id;
        testJob.Nrich__End_Date__c=system.today().adddays(4);
        testJob.Nrich__Start_Date__c=system.today();
        testJob.Nrich__Open_Positions__c=1;
        insert testJob;
        
        Nrich__job_Application__c testjobapplication=new Nrich__Job_Application__c();
        testjobapplication.Nrich__Candidate__c=testcandidate.Id;
        testjobapplication.Nrich__Job__c=testJob.Id;
        testjobapplication.Nrich__Source__c='Direct';
        insert testjobapplication;
        
        Nrich__Interview_Round__c testInterviewRound=new Nrich__Interview_Round__c();
        testInterviewRound.Name='Technical1';
        testInterviewRound.Nrich__Job_Application__c=testjobapplication.Id;
        insert testInterviewRound;
        
        Nrich__Interview_Panel_Member__c testPanelMember= new Nrich__Interview_Panel_Member__c();
        testPanelMember.Nrich__Status__c='Accepted';
        testPanelMember.Nrich__Employee__c=emp.Id;
        testPanelMember.Nrich__Interview_Round__c=testInterviewRound.Id;
        insert testPanelMember;
        system.assertEquals('Test Description','Test Description');
    }
    
    public static testmethod void  rejectInterviewSlot(){
        
        Nrich__Interview_Panel_Member__c testPanelMember=[select id,name from Nrich__Interview_Panel_Member__c limit 1];
        
        apexpages.currentPage().getParameters().put('id', testPanelMember.id);
        
        test.startTest();
        InterviewAcceptanceClass classinstance=new InterviewAcceptanceClass();
        classinstance.showerror=false;
        classinstance.showMessage=false;
        system.assertEquals('Test Description','Test Description');
        test.stopTest();
        
        
    }
    public static testmethod void  alreadyRejectedSlot(){
        
        Nrich__Interview_Panel_Member__c testPanelMember=[select id,name from Nrich__Interview_Panel_Member__c limit 1];
        testPanelMember.Nrich__Status__c='Rejected';
        update testPanelMember;
        
        apexpages.currentPage().getParameters().put('id', testPanelMember.id);
        
        test.startTest();
        InterviewAcceptanceClass classinstance=new InterviewAcceptanceClass();
        classinstance.showerror=false;
        classinstance.showMessage=false;
        system.assertEquals('Test Description','Test Description');
        test.stopTest();
    }
    public static testmethod void  RejectMethod(){
        
        Nrich__Interview_Panel_Member__c testPanelMember=[select id,name from Nrich__Interview_Panel_Member__c limit 1];
        testPanelMember.Nrich__Status__c='Rejected';
        update testPanelMember;
        
        apexpages.currentPage().getParameters().put('id', testPanelMember.id); 
        
        test.startTest();
        InterviewAcceptanceClass classinstance=new InterviewAcceptanceClass();
        classinstance.showerror=false;
        classinstance.showMessage=false;
        classinstance.Reject();
        system.assertEquals('Test Description','Test Description');
        test.stopTest();
    }
    public static testmethod void  closeModal(){
        
        Nrich__Interview_Panel_Member__c testPanelMember=[select id,name from Nrich__Interview_Panel_Member__c limit 1];
        testPanelMember.Nrich__Status__c='Rejected';
        update testPanelMember;
        
        apexpages.currentPage().getParameters().put('id', testPanelMember.id); 
        
        test.startTest();
        InterviewAcceptanceClass classinstance=new InterviewAcceptanceClass();
        classinstance.showerror=false;
        classinstance.showMessage=false;
        classinstance.close();
        system.assertEquals('Test Description','Test Description');
        test.stopTest();
    }
    public static testmethod void  ApproveInterviewSlot(){
        
        Nrich__Interview_Panel_Member__c testPanelMember=[select id,name from Nrich__Interview_Panel_Member__c limit 1];
        
        apexpages.currentPage().getParameters().put('id', testPanelMember.id); 
        
        test.startTest();
        InterviewAcceptanceClass classinstance=new InterviewAcceptanceClass();
        classinstance.showerror=false;
        classinstance.showMessage=false;
        classinstance.Approve();
        system.assertEquals('Test Description','Test Description');
        test.stopTest();
    }
    public static testmethod void  saveMethodWithError(){
        
        Nrich__Interview_Panel_Member__c testPanelMember=[select id,name from Nrich__Interview_Panel_Member__c limit 1];
        testPanelMember.Nrich__Description__c=null;
        update testPanelMember;
        
        apexpages.currentPage().getParameters().put('id', testPanelMember.id); 
        
        test.startTest();
        InterviewAcceptanceClass classinstance=new InterviewAcceptanceClass();
        classinstance.showerror=false;
        classinstance.showMessage=false;
        classinstance.save();
        System.assertEquals('TestDescription','TestDescription');
        test.stopTest();
    }
    public static testmethod void  saveMethod(){
        
        Nrich__Interview_Panel_Member__c testPanelMember=[select id,name from Nrich__Interview_Panel_Member__c limit 1];
        testPanelMember.Nrich__Description__c='Not Interested';
        update testPanelMember;
        
        apexpages.currentPage().getParameters().put('id', testPanelMember.id); 
        
        test.startTest();
        InterviewAcceptanceClass classinstance=new InterviewAcceptanceClass();
        classinstance.showerror=false;
        classinstance.showMessage=false;
        classinstance.save();
        System.assertEquals('TestDescription','TestDescription');
        test.stopTest();
    }
    
}