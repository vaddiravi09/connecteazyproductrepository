/****************************************************************************
* Company: Absyz
* Developer: Hrishikesh
* Created Date: 26/06/2018
* Description: Apex Controller for Interview Schedule Page  
* Test Class: HRInterviewScheduleControllerTest
****************************************************************************/
public with sharing class HRInterviewScheduleController {
    //*******************************************************************************************
    //Created by      : Hrishikesh
    //Method Name     : fetchApplicantDetails
    //Overall Purpose : This method is used for Rejection of Interview Scheduled by Interviewer
    //InputParams     : Interview Round ID
    //OutputParams    : Appliucant Details
    //*******************************************************************************************   
    
    @AuraEnabled
    public static applicantdetails fetchApplicantDetails(String interviewRoundId){
        try{
        Schema.DescribeSObjectResult objectDescription = Nrich__Interview_Round__c.getSObjectType().getDescribe();
       list<Nrich__Interview_Round__c> interviewRound = new list<Nrich__Interview_Round__c>();
        if(objectDescription.isQueryable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Interview_Start_Time__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Interview_End_Time__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Job_Application__c') 
 &&  ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Status__c')
          ){
           interviewRound = [Select Id, Nrich__Interview_Start_Time__c, Nrich__Interview_End_Time__c, Nrich__Job_Application__c, Nrich__Job_Application__r.Name, Nrich__Job_Application__r.Nrich__Email__c,Nrich__Job_Application__r.Nrich__Primary_Phone__c,Nrich__Status__c from Nrich__Interview_Round__c where Id=:interviewRoundId ];                       
            if(interviewRound!=NULL && interviewRound.size()>0){
                applicantdetails applicant = new applicantdetails(); 
                applicant.applicantName = interviewRound[0].Nrich__Job_Application__r.Name;
                applicant.applicantEmail = interviewRound[0].Nrich__Job_Application__r.Nrich__Email__c;
                applicant.applicantMobile = interviewRound[0].Nrich__Job_Application__r.Nrich__Primary_Phone__c;
                applicant.applicantId = interviewRound[0].Nrich__Job_Application__c;
                applicant.interviewStartDate = interviewRound[0].Nrich__Interview_Start_Time__c;
                applicant.interviewEndDate = interviewRound[0].Nrich__Interview_End_Time__c;
                applicant.applicationStatus = interviewRound[0].Nrich__Status__c;
                return applicant;
            }
        }     
        return null;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='HRInterviewScheduleController.fetchApplicantDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    //*******************************************************************************************
    //Created by      : Hrishikesh
    //Method Name     : fetchPanelMembers
    //Overall Purpose : Fetch Panel Member Details for Specific Time Slots
    //InputParams     : Interview Round ID
    //OutputParams    : List of Panel Members 
    //*******************************************************************************************   
    @AuraEnabled
    public static list<employeelist> fetchPanelMembers(String interviewRoundId){
        try{
        if(string.isNotBlank(interviewRoundId)){
            list<employeelist> empList = new list<employeelist>();
            Schema.DescribeSObjectResult objectDescription = Nrich__Interview_Panel_Member__c.getSObjectType().getDescribe();
              if(objectDescription.isQueryable()  && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Feedback__c') 
          && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Employee__c')  && 
          ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Status__c')
            ){
                    for(Nrich__Interview_Panel_Member__c imp: [Select Id, Nrich__Interview_Round__r.Nrich__Interview_Start_Time__c, Nrich__Interview_Round__r.Nrich__Interview_End_Time__c, Nrich__Feedback__c, Nrich__Employee__c, Nrich__Employee__r.Nrich__Department__c, Nrich__Employee__r.Nrich__Department__r.name, Nrich__Employee__r.Name, Nrich__Employee__r.Nrich__Employee_Id__c, Nrich__Employee__r.Nrich__Interview_Level__c, Nrich__Status__c from Nrich__Interview_Panel_Member__c where Nrich__Interview_Round__c =: interviewRoundId 
                                                               ]){
                    string timecheck='';
                    if(imp.Nrich__Interview_Round__r.Nrich__Interview_Start_Time__c!=NULL && imp.Nrich__Interview_Round__r.Nrich__Interview_End_Time__c!=NULL){
                        timecheck = timeval(imp.Nrich__Interview_Round__r.Nrich__Interview_Start_Time__c, imp.Nrich__Interview_Round__r.Nrich__Interview_End_Time__c);
                    }                    
                    employeelist emp = new employeelist();
                    emp.employeeName = imp.Nrich__Employee__r.Name;
                    emp.employeeId = imp.Nrich__Employee__c;
                    emp.employeeNumber = imp.Nrich__Employee__r.Nrich__Employee_Id__c;
                    emp.employeeDepartment = imp.Nrich__Employee__r.Nrich__Department__r.name;
                    emp.employeeLevel = imp.Nrich__Employee__r.Nrich__Interview_Level__c;
                    emp.employeeAvailability = timecheck;
                    emp.employeefeedback = imp.Nrich__Feedback__c;
                    emp.employeeStatus = imp.Nrich__Status__c.equalsIgnoreCase('Accepted')?TRUE:FALSE;
                    emp.panelMembId = imp.Id;
                    emplist.add(emp);
                }
                return empList;
            }else
                return null;
        }
        return null;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='HRInterviewScheduleController.fetchPanelMembers', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    //*******************************************************************************************
    //Created by      : Hrishikesh
    //Method Name     : fetchEmployeeDetails
    //Overall Purpose : Fetch Employee Details (Panel Member Details)
    //InputParams     : search Keyword, Interview Round ID,Start Date and End Date           
    //OutputParams    : List of Employee 
    //*******************************************************************************************   
    
    @AuraEnabled
    public static list<employeelist> fetchEmployeeDetails(string searchword, String interviewRoundId, Datetime starttime, Datetime endtime, integer offsetval){
        try{
        if(string.isNotBlank(interviewRoundId)){
            map<String, String> employeeMap = new map<String, String>();
            map<String, String> fullemployeeMap = new map<String, String>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Employee__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Interview_Round__c'))
            {
                for(Nrich__Interview_Panel_Member__c imp: [Select Id, Nrich__Employee__c from Nrich__Interview_Panel_Member__c where Nrich__Interview_Round__c =: interviewRoundId]){
                employeeMap.put(imp.Nrich__Employee__c, imp.Nrich__Employee__c);
                fullemployeeMap.put(imp.Nrich__Employee__c, imp.Nrich__Employee__c);
            }
        }
            list<employeelist> empList = new list<employeelist>();
            list<Nrich__Interview_Round__c> interviewRound= new list<Nrich__Interview_Round__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Interview_Start_Time__c') 
      )
             interviewRound = [Select Id, Nrich__Interview_Start_Time__c, Nrich__Interview_End_Time__c, Nrich__Job_Application__c, Nrich__Job_Application__r.Nrich__Applicant_First_Name__c , Nrich__Job_Application__r.Nrich__Email__c from Nrich__Interview_Round__c where Id=:interviewRoundId ];     
            if(interviewRound!=NULL && interviewRound.size()>0 && ObjectFieldAccessCheck.checkAccessible('Nrich__Preffered__c', 'Nrich__Available_Slots__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Preffered__c', 'Nrich__Start_Date_Time__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Preffered__c', 'Nrich__End_Date_Time__c') && 
              ObjectFieldAccessCheck.checkAccessible('Nrich__Preffered__c', 'Nrich__Employee__c')  
              ){
                String slotQuery = 'select Id, Nrich__Available_Slots__c, Nrich__Start_Date_Time__c, Nrich__End_Date_Time__c, Nrich__Employee__c, Nrich__Employee__r.Name, Nrich__Employee__r.Nrich__Employee_Id__c, Nrich__Employee__r.Nrich__Primary_Email__c, Nrich__Employee__r.Nrich__Interview_Level__c, Nrich__Employee__r.Nrich__Department__c, Nrich__Employee__r.Nrich__Department__r.Name from Nrich__Preffered__c where Nrich__Employee__r.Nrich__Eligible_to_take_Interviews__c=TRUE';
                Datetime intStartTime = interviewRound[0].Nrich__Interview_Start_Time__c;
                Datetime intEndTime = interviewRound[0].Nrich__Interview_End_Time__c;    
                if(starttime!=NULL && endtime!=NULL){   
                    date sdt= date.newInstance(starttime.yearGmt(), starttime.month(), starttime.dayGmt());
                    date edt = date.newInstance(endtime.yearGmt(), endtime.month(), endtime.dayGmt());
                    slotQuery = slotQuery+' AND Nrich__Start_Date_Time__c>=:sdt AND Nrich__End_Date_Time__c<=:edt';
                }
                else if(intStartTime!=NULL && intEndTime!=NULL){
                    date intsdt= date.newInstance(intStartTime.yearGmt(), intStartTime.month(), intStartTime.dayGmt());
                    date intedt = date.newInstance(intEndTime.yearGmt(), intEndTime.month(), intEndTime.dayGmt());
                    slotQuery = slotQuery+' AND Nrich__Start_Date_Time__c>=:intsdt AND Nrich__End_Date_Time__c<=:intedt';
                }
                else{
                    date intsdt= date.newInstance(System.now().yearGmt(), System.now().month(), System.now().dayGmt());
                    date intedt = date.newInstance(System.now().yearGmt(), System.now().month(), System.now().dayGmt());
                    slotQuery = slotQuery+' AND Nrich__Start_Date_Time__c>=:intsdt AND Nrich__End_Date_Time__c<=:intedt';
                }
                
                if(string.isNotBlank(searchword)){
                    searchword = '%'+searchword+'%';
                    slotQuery = slotQuery+ ' AND (Nrich__Employee__r.Name LIKE: searchword OR Nrich__Employee__r.Nrich__Employee_Id__c LIKE: searchword OR Nrich__Employee__r.Nrich__Interview_Level__c LIKE: searchword OR Nrich__Employee__r.Nrich__Primary_Email__c LIKE: searchword OR Nrich__Employee__r.Nrich__Department__r.Name LIKE: searchword)';
                }
                slotQuery = slotQuery+' Order by Nrich__Employee__r.Nrich__Employee_Id__c LIMIT 20';
                system.debug('SlotQuery==> '+slotQuery);
                list<Nrich__Preffered__c> PrefferedList = database.query(slotQuery);  
                if(PrefferedList==NULL || PrefferedList.size()<=0){                    
                    empList.addAll(allEmployees(searchword, offsetval, employeeMap)); 
                    return empList;
                }
                else{
                    for(Nrich__Preffered__c prefer: prefferedlist){
                        if(employeeMap!=NULL && !employeeMap.containsKey(prefer.Nrich__Employee__c)){                            
                            employeelist emp = new employeelist();
                            emp.employeeName = prefer.Nrich__Employee__r.Name;
                            emp.employeeId = prefer.Nrich__Employee__c;
                            emp.employeeNumber = prefer.Nrich__Employee__r.Nrich__Employee_Id__c;
                            emp.employeeDepartment = prefer.Nrich__Employee__r.Nrich__Department__r.Name;
                            emp.employeeLevel = prefer.Nrich__Employee__r.Nrich__Interview_Level__c;
                            emp.employeeAvailability = prefer.Nrich__Available_Slots__c;
                            emp.employeefeedback = false;
                            empList.add(emp);
                            fullemployeeMap.put(prefer.Nrich__Employee__c, prefer.Nrich__Employee__c);                           
                        }                                              
                    }
                    empList.addAll(allEmployees(searchword, offsetval, fullemployeeMap));
                    return empList;
                }                  
            }
        }
        return null;       
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='HRInterviewScheduleController.fetchEmployeeDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    //*******************************************************************************************
    //Created by      : Hrishikesh
    //Method Name     : allEmployees
    //Overall Purpose : Used for Getting List Of Employees with searchkeyword 
    //InputParams     : search Keyword, Offset value,Map of Employee with Employee           
    //OutputParams    : List of Employee 
    //*******************************************************************************************   
    
    @AuraEnabled
    public static list<employeelist> allEmployees(string searchword, integer offsetval, Map<String, String> empMap){
        try{        
        list<employeelist> empList = new list<employeelist>();
        string slotQuery = 'select Id, Name, Nrich__Department__c, Nrich__Department__r.Name, Nrich__Primary_Email__c,Nrich__Employee_Id__c, Nrich__Interview_Level__c from Nrich__Employee__c where Nrich__Eligible_to_take_Interviews__c=TRUE';        
        if(string.isNotBlank(searchword)){
            searchword = '%'+searchword+'%';
            slotQuery = slotQuery+ ' AND (Name LIKE: searchword OR Nrich__Employee_Id__c LIKE: searchword OR Nrich__Interview_Level__c LIKE: searchword OR Nrich__Primary_Email__c LIKE: searchword OR Nrich__Department__r.Name LIKE: searchword)';
        }
        slotQuery = slotQuery+ ' order by Nrich__Employee_Id__c LIMIT 20';
        for(Nrich__Employee__c employee: Database.query(slotQuery)){
            if(empMap!=NULL && !empMap.containsKey(employee.Id)){
                employeelist emp = new employeelist();
                emp.employeeName = employee.Name;
                emp.employeeId = employee.Id;
                emp.employeeNumber = employee.Nrich__Employee_Id__c;
                emp.employeeDepartment = employee.Nrich__Department__r.Name;
                emp.employeeLevel = employee.Nrich__Interview_Level__c;
                emp.employeeAvailability = '';
                emp.employeefeedback = false;
                empList.add(emp);
            }
        } 
        return emplist;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='HRInterviewScheduleController.allEmployees', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    //*******************************************************************************************
    //Created by      : Hrishikesh
    //Method Name     : schedule
    //Overall Purpose : Used for Schedule Interview Round 
    //InputParams     : List of Panel Members,Interview Round Id,Start Time, End Time, Rescheduled or Not 
    //OutputParams    : True or False(Scheduled or Not) 
    //*******************************************************************************************   
    @AuraEnabled
    public static String schedule(string employeeval, String interviewRoundId, Datetime starttime, Datetime endtime, boolean reschedule){
        try{    
        list<employeelist> employees = (list<employeelist>)Json.deserialize(employeeval, list<employeelist>.class);
            map<String, String> panelMap = new map<String, String>();            
            if(employees!=NULL && employees.size()>0){               
                list<Nrich__Interview_Panel_Member__c> intpnlMemberList = new list<Nrich__Interview_Panel_Member__c>();
                Set<Id> employeeSet = new Set<Id>();
                for(employeelist emp: employees){
                    if(string.isNotBlank(emp.panelMembId)){
                        panelMap.put(emp.panelMembId, emp.panelMembId);
                    }
                    Nrich__Interview_Panel_Member__c imp = new Nrich__Interview_Panel_Member__c();
                    if(string.isNotBlank(emp.panelMembId)){
                        imp.Id = emp.panelMembId;                     
                    }
                    If((Schema.sObjectType.Nrich__Interview_Panel_Member__c.fields.Nrich__Employee__c.isCreateable() || Schema.sObjectType.Nrich__Interview_Panel_Member__c.fields.Nrich__Employee__c.isUpdateable()) && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Employee__c')){
                        imp.Nrich__Employee__c = emp.employeeId;
                    }
                    If((Schema.sObjectType.Nrich__Interview_Panel_Member__c.fields.Nrich__Interview_Round__c.isCreateable() || Schema.sObjectType.Nrich__Interview_Panel_Member__c.fields.Nrich__Interview_Round__c.isUpdateable()) && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Interview_Round__c')) {
                        imp.Nrich__Interview_Round__c = interviewRoundId;
                    }
                    If((Schema.sObjectType.Nrich__Interview_Panel_Member__c.fields.Nrich__Status__c.isCreateable() || Schema.sObjectType.Nrich__Interview_Panel_Member__c.fields.Nrich__Status__c.isUpdateable()) &&  ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Status__c')){
                        imp.Nrich__Status__c = 'Accepted';
                    }
                    If((Schema.sObjectType.Nrich__Interview_Panel_Member__c.fields.Nrich__Feedback__c.isCreateable() || Schema.sObjectType.Nrich__Interview_Panel_Member__c.fields.Nrich__Feedback__c.isUpdateable()) && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Feedback__c')){
                        imp.Nrich__Feedback__c = emp.employeefeedback;
                    }
                    If((Schema.sObjectType.Nrich__Interview_Panel_Member__c.fields.Nrich__Rescheduled_to_date_time__c.isCreateable() || Schema.sObjectType.Nrich__Interview_Panel_Member__c.fields.Nrich__Rescheduled_to_date_time__c.isUpdateable()) && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Rescheduled_to_date_time__c')){
                        imp.Nrich__Rescheduled_to_date_time__c = system.now();
                    }
                    intpnlMemberList.add(imp);
                    employeeSet.add(emp.employeeId);
                }
                string errormsg='An interview is already scheduled for these Selected Panel Members ';
                boolean error = false;
                for(Nrich__Interview_Panel_Member__c panelList : [Select Id, Nrich__Employee__c, Nrich__Employee__r.Name from Nrich__Interview_Panel_Member__c where Nrich__Interview_Round__r.Nrich__Interview_Start_Time__c<=:starttime AND Nrich__Interview_Round__r.Nrich__Interview_End_Time__c>=:endtime AND Nrich__Employee__c IN: employeeSet AND Nrich__Interview_Round__c!=:interviewRoundId ]){
                    errormsg = errormsg+panelList.Nrich__Employee__r.Name+', ';
                    error = true;
                }
                errormsg = errormsg+'in this timerange. So Please Select different Members';
                system.debug('error '+errormsg);
                if(error)
                    return errormsg;
                else{
                    list<Nrich__Interview_Panel_Member__c> intpnlMemberDeleteList = new list<Nrich__Interview_Panel_Member__c>();
                    Schema.DescribeSObjectResult objectDescription = Nrich__Interview_Panel_Member__c.getSObjectType().getDescribe();
                    if(objectDescription.isQueryable()){
                        for(Nrich__Interview_Panel_Member__c imp: [Select Id from Nrich__Interview_Panel_Member__c where Nrich__Interview_Round__c=:interviewRoundId]){
                            if(panelMap!=NULL && panelMap.size()>0 && !panelMap.containsKey(imp.Id)){
                                intpnlMemberDeleteList.add(imp);
                            }
                        }
                    }
                    
                    Schema.DescribeSObjectResult objectDescriptionround = Nrich__Interview_Round__c.getSObjectType().getDescribe();
                    if(objectDescriptionround.isQueryable()){
                        list<Nrich__Interview_Round__c> interviewList = [Select Id, Nrich__Reschedule__c, Nrich__Interview_Start_Time__c, Nrich__Interview_End_Time__c, Nrich__Status__c from Nrich__Interview_Round__c where id=: interviewRoundId];
                        if(interviewList.size()>0){
                            Nrich__Interview_Round__c interview = interviewList[0];
                            if(reschedule && (starttime!=interview.Nrich__Interview_Start_Time__c) || endtime!=interview.Nrich__Interview_End_Time__c)
                                If((Schema.sObjectType.Nrich__Interview_Round__c.fields.Nrich__Reschedule__c.isUpdateable()) &&(ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Reschedule__c')) ){
                                    interview.Nrich__Reschedule__c = true;
                                }
                            interview.Id = interviewRoundId;
                            If((Schema.sObjectType.Nrich__Interview_Round__c.fields.Nrich__Interview_Start_Time__c.isUpdateable()) && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Interview_Start_Time__c') ){
                                interview.Nrich__Interview_Start_Time__c = starttime;
                            }
                            If((Schema.sObjectType.Nrich__Interview_Round__c.fields.Nrich__Interview_End_Time__c.isUpdateable()) && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Interview_End_Time__c')) {
                                interview.Nrich__Interview_End_Time__c = endtime;   
                            }
                            If((Schema.sObjectType.Nrich__Interview_Round__c.fields.Nrich__Status__c.isUpdateable()) && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Status__c')){
                                interview.Nrich__Status__c = 'Scheduled';
                            }
                            if(objectDescriptionround.isUpdateable())
                                database.update(interview);
                        }
                        
                        if(starttime!=NULL && endtime!=NULL){
                            date dt = starttime.date();
                            string timecheck = timeval(starttime, endtime);
                            
                            if(dt!=NULL && string.isnotBlank(timecheck)){
                                list<Nrich__Preffered__c> preferList = new list<Nrich__Preffered__c>();
                                for(Nrich__Preffered__c prefer: [Select Id, Nrich__Booked_Slots__c, Nrich__Available_Slots__c from Nrich__Preffered__c where Nrich__Employee__c IN: employeeSet AND Nrich__End_Date_Time__c=:dt AND Nrich__Start_Date_Time__c=:dt]){
                                    if(Schema.sObjectType.Nrich__Preffered__c.fields.Nrich__Booked_Slots__c.isUpdateable())
                                        prefer.Nrich__Booked_Slots__c = timecheck;
                                    if(prefer.Nrich__Available_Slots__c!=NULL && Schema.sObjectType.Nrich__Preffered__c.fields.Nrich__Available_Slots__c.isUpdateable())                               
                                        prefer.Nrich__Available_Slots__c =  prefer.Nrich__Available_Slots__c.replace(timecheck,'');
                                    preferList.add(prefer);
                                }
                                
                                if(preferList.size()>0 && Nrich__Preffered__c.sObjectType.getDescribe().isUpdateable())
                                    database.update(preferList);
                            }
                        }
                        
                        
                        if((objectDescription.isCreateable() || objectDescription.isUpdateable()) && intpnlMemberList.size()>0)
                            database.upsert(intpnlMemberList, false);
                        
                        if(objectDescription.isDeletable() && intpnlMemberDeleteList.size()>0)
                            database.delete(intpnlMemberDeleteList, false);
                        
                        return 'success';
                    } 
                    
                }
            }
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='HRInterviewScheduleController.schedule', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
    }
        return null;
      }

    public static string timeval(datetime starttime, datetime endtime){
        try{
        if(starttime!=NULL && endtime!=NULL){
            integer starthourval = starttime.hour();
            integer startminval = starttime.minute();
            integer endhourval = endtime.hour();
            integer endminval = endtime.minute();
            date dt = starttime.date();
            string starthour;
            string endhour;
            if(starthourval>=12){
                if(starthourval!=12)
                    starthourval = starthourval-12;
                starthour = String.valueof(starthourval)+':'+String.valueOf(startminval)+' PM';
            }else{
                if(starthourval==0)
                    starthourval = 12;
                starthour = String.valueof(starthourval)+':'+String.valueOf(startminval)+' AM';
            }
            
            if(endhourval>=12){
                if(endhourval!=12)
                    endhourval = endhourval-12;
                endhour = String.valueof(endhourval)+':'+String.valueOf(endminval)+' PM';
            }else{
                if(endhourval==0)
                    endhourval = 12;
                endhour = String.valueof(endhourval)+':'+String.valueOf(endminval)+' AM';
            }
            string timecheck = starthour+'-'+endhour;
            return timecheck;
        }      
        return '';
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='HRInterviewScheduleController.timeval', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    //*******************************************************************************************
    //Created by      : Hrishikesh
    //Class Name      : employeelist
    //Overall Purpose : Used for Wrap details of Employee
    //*******************************************************************************************   
    public class employeelist{
        @AuraEnabled public String employeeName {get; set;}
        @AuraEnabled public String employeeId {get; set;}
        @AuraEnabled public String employeeNumber {get; set;}
        @AuraEnabled public String employeeDepartment {get; set;}
        @AuraEnabled public String employeeLevel {get; set;}
        @AuraEnabled public String employeeAvailability {get; set;}
        @AuraEnabled public Boolean employeefeedback {get; set;}
        @AuraEnabled public Boolean employeeStatus {get; set;}
        @AuraEnabled public String panelMembId {get; set;}
    }
    
    //*******************************************************************************************
    //Created by      : Hrishikesh
    //Class Name      : applicantdetails
    //Overall Purpose : Used for Wrap details of Applicant
    //*******************************************************************************************   
    
    public class applicantdetails{
        @AuraEnabled public String applicantName {get; set;}
        @AuraEnabled public String applicantEmail {get; set;}
        @AuraEnabled public String applicantMobile {get; set;}
        @AuraEnabled public String applicantId {get; set;}
        @AuraEnabled public datetime interviewStartDate {get; set;}
        @AuraEnabled public datetime interviewEndDate {get; set;}
        @AuraEnabled public string applicationStatus {get; set;}
    }
}