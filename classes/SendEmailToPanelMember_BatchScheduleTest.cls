@isTest
public class SendEmailToPanelMember_BatchScheduleTest {
    private static testMethod void testMethod1(){
        Test.startTest();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        SendEmailToPanelMember_BatchSchedule sh1 = new SendEmailToPanelMember_BatchSchedule();
        String sch = '0 0 2 * * ?'; 
        system.schedule('Test Check1', sch, sh1); 
        Test.stopTest();
        system.assertEquals('Test','Test');
    }
}