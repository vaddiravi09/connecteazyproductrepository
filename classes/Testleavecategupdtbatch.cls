@isTest
public class Testleavecategupdtbatch {
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Thadeus Ronn Thomas 
* Created Date: 31/10/2018
* Description: Test class covering Leavecategupdt.
*****************************************************************************************************/  
    
    @istest  static void testupdtlcateg(){
        //Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 
        User u = new User(Alias = 'st12356', Email='standarduser1@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u;
        User u1 = new User(Alias = 'at12356', Email='standardusers@testorg.com',EmailEncodingKey='UTF-8', LastName='Testings', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorgs.com1234');
        insert u1;
       User u2 = new User(Alias = 'at1236', Email='standardusers@testorg.com',EmailEncodingKey='UTF-8', LastName='Testings', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='standaruser@testorgs.com1234');
        insert u2;
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleaves=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =System.today(),Nrich__Applicable_Till__c=System.today()+365,Nrich__Is_Active__c= false);
            insert cleaves;
            Nrich__Leave_Category__c lleaves =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,Nrich__Will_Carry_Forward__c =False,Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleaves.id,Nrich__Lapse_Frequency__c=1);
           // insert lleaves;
            Nrich__Leave_Category__c lleaves1 =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,Nrich__Will_Carry_Forward__c =False,Nrich__Will_Lapse__c =True,Nrich__Applicable_Leave_Structure__c =cleaves.id,Nrich__Lapse_Frequency__c=1);
           // insert lleaves1;
            cleaves.Nrich__Is_Active__c=true;
           // update cleaves;
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Project Manager',Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id );
            insert emp;
            Nrich__Employee__c emp1=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Project Manager',Nrich__Total_Experience__c=10,Nrich__Related_User__c=u1.id,Nrich__Date_of_Joining__c=date.parse('1/1/2017') );
            insert emp1;
            Nrich__Employee__c emp2=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Project Manager',Nrich__Total_Experience__c=10,Nrich__Related_User__c=u2.id,Nrich__Date_of_Joining__c=date.parse('3/20/2018') );
            insert emp2;
            // Test.stopTest();
            System.assertEquals('TestUser', emp2.Name);
            database.executebatch(new LeavecategoryUpdate_Batch());
          
        }
    }
}