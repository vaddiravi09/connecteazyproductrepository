@isTest
public class ModuleStatusRestrict_Test {
    
    @isTest public static void modulestatusrestricttest(){
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        
        Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                         
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        insert lleave;
        cleave.Nrich__Is_Active__c=True;
        update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser1' , Nrich__EmployeeDesignation__c='Developer',
                                        
                                        Nrich__Total_Experience__c=11,
                                        Nrich__Related_User__c=u1.id );
        
        insert emp;
        
        Nrich__Project__c  Prj=new Nrich__Project__c();
        Prj.Name='TestPrj';
        Prj.Nrich__Project_Start_Date__c=System.today();
        Prj.Nrich__Project_End_Date__c=System.today()+7;
        Prj.Nrich__ProjectManagerr__c=emp.id;
        Prj.Nrich__Status__c='Proposed';
        insert prj;
        
        Nrich__Module__c Module=new Nrich__Module__c ();
        Module.Nrich__Project__c=prj.Id;
        Module.Name='TestModule';
        Module.Nrich__Status__c='New';
        insert Module;
        
        
        Module.Nrich__Status__c='Completed';
        update Module;
        
        
        Nrich__Module__c Module1=new Nrich__Module__c ();
        Module1.Nrich__Project__c=prj.Id;
        Module1.Name='TestModule';
        Module1.Nrich__Status__c='New';
        insert Module1;
        
        Map<id,Nrich__Module__c>ModuleMap=new Map<id,Nrich__Module__c>();
        ModuleMap.put(Module1.id,Module1);
        
        
        Nrich__User_Story__c Userstry=new Nrich__User_Story__c();
        Userstry.Name='Usrstry1';
        Userstry.Nrich__Module__c=Module1.Id;
        Userstry.Nrich__Status__c='New';
        insert Userstry;
        
        ModuleStatusRestrictHandler.CheckStatusOfrelatedUserStory(ModuleMap);
        System.assertEquals('Usrstry1', Userstry.Name);
        
        
    }
    
}