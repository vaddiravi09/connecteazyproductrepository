/********************************************************************
* Company: Absyz
* Developer: Anivesh
* Created Date: 16/03/2019
* Description: Email to Travel Request
********************************************************************/
public with sharing class EmailtoTravelReqController {
    public String objType {get;set;}
    public ID relatedId {get;set;}
    public list<Nrich__Travel_Request__c > TravelRequestAppList{get;set;}
    
    public List<Nrich__Travel_Request__c> getTravelList (){
        try{
        System.debug('relatedId' +relatedId);
        List<Nrich__Travel_Request__c> TravelRequestAppList = new List<Nrich__Travel_Request__c>();
        if(relatedId != null){
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Source_Location__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Destination_Location__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Start_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Number_of_Travellers__c')
              && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Reason_For_Travel__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Requestor_Name_Technical__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Status__c')
              && ObjectFieldAccessCheck.checkAccessible('Nrich__Travel_Request__c', 'Nrich__Approver_Name__c')
              )
            TravelRequestAppList = [select Id, Nrich__Source_Location__c ,Nrich__Destination_Location__c , Nrich__Start_Date__c ,Nrich__End_Date__c , Nrich__Number_of_Travellers__c ,Name, Nrich__Reason_For_Travel__c, Nrich__Employeee__r.Name, Nrich__Requestor_Name_Technical__c, Nrich__Status__c, Nrich__Approver_Name__c    from Nrich__Travel_Request__c where id = : relatedId ];
            system.debug('TravelRequestAppList' +TravelRequestAppList);
        }
        return TravelRequestAppList;
    }
      catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmailtoTravelReqController.getTravelList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
          return TravelRequestAppList;
      }  
    }
}