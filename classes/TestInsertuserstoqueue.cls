@isTest
public class TestInsertuserstoqueue {
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Sai kesa 
* Created Date: 02/11/2018
* Description: Test class covering The Usercreation
*****************************************************************************************************/ 
    @isTest static void testmeth2(){  
        group g=new group(name='Queue_For_Certification_Request',type='Queue');
        insert g;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
        System.runAs(u){
            Nrich__InitialSetup__c Ins1= new Nrich__InitialSetup__c(Nrich__Is_Achievements_Tile_Required__c = true,Nrich__Is_Certification_Tile_Required__c=true,Nrich__Is_LeaveChart_Tile_Require__c=true,Nrich__Is_MentorMentee_Tile_Required__c=true,Nrich__Is_OrgChart_Tile_Required__c=false,Nrich__Is_Profile_Tile_Required__c=true,  Nrich__Is_Asset_Tile_Required__c =true,Nrich__Is_Support_Request_Tile_Required__c=true,Nrich__Is_Travel_Request_Tile_Required__c=true, Nrich__Is_Policy_Tile_Required__c=true ,Nrich__Number_of_DueDays_For_Certification_Req__c=10,Nrich__Number_Of_DueDays_For_Leave_Request__c=5);
            List<Nrich__InitialSetup__c> setuplist= new List<Nrich__InitialSetup__c>();
            setuplist.add(Ins1);
            insert setuplist;
            InsertUsersToQueueClass.insertusersQueue(u.id,u.id,u.id,u.id);
            System.assertEquals(1, setuplist.size());
            
        }
    }
}