/****************************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 26/06/2018
* Description: Send an Email Alert to Candidate regaring Accepted Offer
* Test Class: EmailTemplatesTest
****************************************************************************/
public with sharing class EmailtoCandidateOfferAcceptedController {
    public String objType {get;set;}
    public ID relatedId {get;set;}
    
    /*******************************************************************************************
Created by      : Pushmitha
Overall Purpose : Fetch Job Application Details
InputParams     : NA
OutputParams    : List of Job Application
*******************************************************************************************/
    public List<Nrich__Job_Application__c> getJobApplicationList (){
        List<Nrich__Job_Application__c> jobAppList = new List<Nrich__Job_Application__c>();
        if(relatedId != null){
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Candidate__c' ) &&ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Pre_Joining_URL__c'))
            jobAppList = [select Id, Nrich__Candidate__r.Name,Nrich__Pre_Joining_URL__c, Name from Nrich__Job_Application__c where id = : relatedId];
        }
        return jobAppList;
    }
}