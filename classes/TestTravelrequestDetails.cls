/****************************************************************************************************
* Company: Absyz
* Developer:Ashish Nag K 
* Created Date: 2/1/2019
* Description: Test class covering The TravelRequestdetailController
*****************************************************************************************************/ 



@isTest
public class TestTravelrequestDetails {
    
    @isTest static void fetchrecords(){
      Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        User u1 = new User(Alias = 'st1236', Email='standarduserestorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Teng', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@terg.com123');
     
        System.runAs(u){
       Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                          
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        //insert lleave;
        cleave.Nrich__Is_Active__c=True;
       // update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                        
                                        Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
        
        
        insert emp;
     
            Nrich__Travel_Request__c  travelrequest=new Nrich__Travel_Request__c();
            travelrequest.Nrich__Start_Date__c=System.today();
            travelrequest.Nrich__End_Date__c=System.today()+1;
            travelrequest.Nrich__Source_Location__c='Hyderbad';
            travelrequest.Nrich__Destination_Location__c='Banglore';
            travelrequest.Nrich__Trip_Description__c='Test';
            travelrequest.Nrich__Reason_For_Travel__c='Relocation';
            travelrequest.Nrich__Visiting_Addresses__c='Sample Address';
            travelrequest.Nrich__Associated_Clients__c='Test1';
            travelrequest.Nrich__Request_Type__c=' International';
            travelrequest.Nrich__Employeee__c =emp.id;
            travelrequest.Nrich__Mode_of_Transport__c='Train';
            travelrequest.Nrich__Status__c='Processed';
            insert travelrequest;
            list<Nrich__Travel_Request__c> travelList=new list<Nrich__Travel_Request__c>();
            travelList.add(travelrequest);
            string travell=JSON.serialize(travelList);
            
            travelrequest.Nrich__Status__c='Submitted For Approval';
            update travelrequest;
            
            travelrequest.Nrich__Status__c='TR to be Processed';
            update travelrequest;
            
            //string travellers;
            TravelrequestDetailsController.fetchEmployeeDetails();
            Nrich__Employee__c emp11=new Nrich__Employee__c(Name='TestUser11' , Nrich__EmployeeDesignation__c='Project Manager',
                                              
                                              Nrich__Total_Experience__c=1,
                                              Nrich__Related_User__c=u1.id );
            
            insert emp11;
            
            string message;
            TravelrequestDetailsController.saveEmployeeTravelRequest(travelrequest, travell, emp11.id);
            TravelrequestDetailsController.fetchreasonfortraveloptions();
            TravelrequestDetailsController.fetchRequestType();
            TravelrequestDetailsController.fetchModeofTransport();
            TravelrequestDetailsController.fetchStatus();
            System.assertEquals('TR to be Processed',travelrequest.Nrich__Status__c);
            
            
       }
        
    }
    
    @isTest static void FrApproval(){
        
        
        string message;
        Boolean isSuccess;
        
        
        Test.startTest();
       /* Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 
        User u = new User(Alias = 'sta12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, ManagerId =u1.id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com1234');
        //   System.runAs(u){
        Company_Leave_Structure__c cleave=new Company_Leave_Structure__c(Applicable_From__c =system.today(),Applicable_Till__c=system.today(),Is_Active__c= true);
        insert cleave;
        Leave_Category__c lleave =new Leave_Category__c(Name='Test',Increment_Frequency__c=1,Increment_Step__c =2,
                                                        Will_Carry_Forward__c =False,
                                                        Will_Lapse__c =False,Applicable_Leave_Structure__c =cleave.id
                                                        
                                                       );
        
        insert lleave;
        Employee__c emp=new Employee__c(Name='TestUser1' , EmployeeDesignation__c='Project Manager',
                                        
                                        Total_Experience__c=11,
                                        Related_User__c=u.id );
        
        insert emp;*/
        
        
        Nrich__Travel_Request__c travelrequest=new Nrich__Travel_Request__c();
        travelrequest.Nrich__Start_Date__c=System.today();
        travelrequest.Nrich__End_Date__c=System.today()+1;
        travelrequest.Nrich__Source_Location__c='Hyderbad';
        travelrequest.Nrich__Destination_Location__c='Banglore';
        travelrequest.Nrich__Trip_Description__c='Test';
        travelrequest.Nrich__Reason_For_Travel__c='Relocation';
        travelrequest.Nrich__Visiting_Addresses__c='Sample Address';
        travelrequest.Nrich__Associated_Clients__c='Test1';
        travelrequest.Nrich__Request_Type__c=' International';
        travelrequest.Nrich__Employeee__c =UtilityForTest.Employee_Utility().Id;
        travelrequest.Nrich__Mode_of_Transport__c='Train';
        travelrequest.Nrich__Status__c='Processed';
        
        insert travelrequest;
        
        
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Hii Every one' ); 
        req.setObjectId(travelrequest.Id); 
        // req.setProcessDefinitionNameOrId('New_Certification_Request');
        req.setNextApproverIds(new Id[] {travelrequest.Nrich__Employeee__c });
        
        TravelrequestDetailsController.submitTravelRequest(travelrequest.id); 
        System.assertEquals('Processed',travelrequest.Nrich__Status__c);
        
        Test.StopTest();
        
        
        
    }
    
    
}