/****************************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 26/06/2018
* Description: Rollup Summary for status
* Test Class: JobApplicationTriggerHandlerTest
****************************************************************************/
public with sharing class JobApplicationTriggerHandler {
    public static void totalOfStatus(set<id> jobApplicationIdSet){
        //Initialization
        try{
        list<Nrich__Job__c> sumListRejected = new list<Nrich__Job__c>();
        list<Nrich__Job__c> sumListInprocess = new list<Nrich__Job__c>();
        list<Nrich__Job__c> sumListAccepted = new list<Nrich__Job__c>();
        list<Nrich__Job__c> sumListRolledOut = new list<Nrich__Job__c>();
        list<Nrich__Job__c> sumListPositionFilled = new list<Nrich__Job__c>();
        Double countRejected;
        Double countInprocess;
        Double countNegotiation;
        Double countRolledOut;
        Double countPositionFilled;
        
        //Aggregate function to count the records from Job Application Object
        String statusValueRejected = System.Label.JobApplicationRejected;
        String statusValueInProgress = System.Label.JobApplicationInProgress;
        String statusValueOfferAccepted = System.Label.JobApplicationOfferAccepted;
        String statusValueRolledOut = System.Label.JobApplicationOfferRolledOut;
        String statusValuePositionFilled = System.Label.JobApplicationOnboardCompleted;
        AggregateResult[] groupedResultsRejected,groupedResultsInProcess,groupedResultsAccepted,groupedResultsRolledOut,groupedResultsPositionFilled;
        
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Application_Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__job__c'))
            groupedResultsRejected = [SELECT COUNT(Name), Nrich__Application_Status__c ,Nrich__job__c  FROM Nrich__Job_Application__c where Nrich__Job__c IN :jobApplicationIdSet AND Nrich__Application_Status__c =: statusValueRejected  GROUP BY Nrich__Application_Status__c ,Nrich__job__c  ];
        groupedResultsInProcess = [SELECT COUNT(Name), Nrich__Application_Status__c ,Nrich__job__c  FROM Nrich__Job_Application__c where Nrich__Job__c IN :jobApplicationIdSet AND Nrich__Application_Status__c =: statusValueInProgress  GROUP BY Nrich__Application_Status__c ,Nrich__job__c  ];
        groupedResultsAccepted = [SELECT COUNT(Name), Nrich__Application_Status__c ,Nrich__job__c  FROM Nrich__Job_Application__c where Nrich__Job__c IN :jobApplicationIdSet AND Nrich__Application_Status__c =: statusValueOfferAccepted  GROUP BY Nrich__Application_Status__c ,Nrich__job__c  ];
        groupedResultsRolledOut = [SELECT COUNT(Name), Nrich__Application_Status__c ,Nrich__job__c  FROM Nrich__Job_Application__c where Nrich__Job__c IN :jobApplicationIdSet AND Nrich__Application_Status__c =: statusValueRolledOut  GROUP BY Nrich__Application_Status__c ,Nrich__job__c  ];
        groupedResultsPositionFilled = [SELECT COUNT(Name), Nrich__Application_Status__c ,Nrich__job__c  FROM Nrich__Job_Application__c where Nrich__Job__c IN :jobApplicationIdSet AND Nrich__Application_Status__c =: statusValuePositionFilled GROUP BY Nrich__Application_Status__c ,Nrich__job__c  ];
        
        //Get the count of status field that contains Rejected
        if(groupedResultsRejected.size()==0){
            for(id ab:jobApplicationIdSet){
                Nrich__Job__c order = new Nrich__Job__c(Id=ab);
                If(Schema.sObjectType.Nrich__Job__c.fields.Nrich__Total_Candidates_Rejected__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Job__c', 'Nrich__Total_Candidates_Rejected__c')){
                    order.Nrich__Total_Candidates_Rejected__c = 0;
                }
                sumListRejected.add(order);
            }
        }
        else{
            for(AggregateResult ar:groupedResultsRejected) {
                Id itemId = (ID)ar.get('Nrich__job__c');
                countRejected = (DOUBLE)ar.get('expr0');
                Nrich__Job__c order = new Nrich__Job__c(Id=itemId);
                If(Schema.sObjectType.Nrich__Job__c.fields.Nrich__Total_Candidates_Rejected__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Job__c', 'Nrich__Total_Candidates_Rejected__c')){
                    order.Nrich__Total_Candidates_Rejected__c = countRejected;
                }
                sumListRejected.add(order);
            }
        }
        //Get the count of status field that contains In process
        if(groupedResultsInProcess.size()==0){
            for(id ab:jobApplicationIdSet){
                Nrich__Job__c order = new Nrich__Job__c(Id=ab);
                If(Schema.sObjectType.Nrich__Job__c.fields.Nrich__Total_Candidates_In_Process__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Job__c', 'Nrich__Total_Candidates_In_Process__c')){
                    order.Nrich__Total_Candidates_In_Process__c = 0;
                }
                sumListInprocess.add(order);
            }
        }
        else{
            for(AggregateResult ar:groupedResultsInProcess) {
                Id itemId = (ID)ar.get('Nrich__job__c');
                countInprocess = (DOUBLE)ar.get('expr0');
                Nrich__Job__c order = new Nrich__Job__c(Id=itemId);
                If(Schema.sObjectType.Nrich__Job__c.fields.Nrich__Total_Candidates_In_Process__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Job__c', 'Nrich__Total_Candidates_In_Process__c')){
                    order.Nrich__Total_Candidates_In_Process__c    = countInprocess;
                }
                sumListInprocess.add(order);
            }
        }
        //Get the count of status field that contains Offer Accepted
        if(groupedResultsAccepted.size()==0){
            for(id ab:jobApplicationIdSet){
                Nrich__Job__c order = new Nrich__Job__c(Id=ab);
                If(Schema.sObjectType.Nrich__Job__c.fields.Nrich__Total_Candidates_Offered_Accepted__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Job__c', 'Nrich__Total_Candidates_Offered_Accepted__c')){
                    order.Nrich__Total_Candidates_Offered_Accepted__c = 0;
                }
                sumListAccepted.add(order);
            }
        }
        else{
            for(AggregateResult ar:groupedResultsAccepted) {
                Id itemId = (ID)ar.get('Nrich__job__c');
                countNegotiation = (DOUBLE)ar.get('expr0');
                Nrich__Job__c order = new Nrich__Job__c(Id=itemId);
                If(Schema.sObjectType.Nrich__Job__c.fields.Nrich__Total_Candidates_Offered_Accepted__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Job__c', 'Nrich__Total_Candidates_Offered_Accepted__c')){
                    order.Nrich__Total_Candidates_Offered_Accepted__c = countNegotiation;
                }
                sumListAccepted.add(order);
            }
        }
        //Get the count of status field that contains Offer Rolled Out
        if(groupedResultsRolledOut.size()==0){
            for(id ab:jobApplicationIdSet){
                Nrich__Job__c order = new Nrich__Job__c(Id=ab);
                If(Schema.sObjectType.Nrich__Job__c.fields.Nrich__Total_Candidates_Offered__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Job__c', 'Nrich__Total_Candidates_Offered__c')){
                    order.Nrich__Total_Candidates_Offered__c = 0;
                }
                sumListRolledOut.add(order);
            }
        }
        else{
            for(AggregateResult ar:groupedResultsRolledOut) {
                Id itemId = (ID)ar.get('Nrich__job__c');
                countRolledOut = (DOUBLE)ar.get('expr0');
                Nrich__Job__c order = new Nrich__Job__c(Id=itemId);
                If(Schema.sObjectType.Nrich__Job__c.fields.Nrich__Total_Candidates_Offered__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Job__c', 'Nrich__Total_Candidates_Offered__c')){
                    order.Nrich__Total_Candidates_Offered__c = countRolledOut;
                }
                sumListRolledOut.add(order);
            }
        }
        //Get the count of status field that contains Onboarded
        if(groupedResultsPositionFilled.size()==0){
            for(id ab:jobApplicationIdSet){
                Nrich__Job__c order = new Nrich__Job__c(Id=ab);
                If(Schema.sObjectType.Nrich__Job__c.fields.Nrich__Total_Positions_Filled__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Job__c', 'Nrich__Total_Positions_Filled__c')){
                    order.Nrich__Total_Positions_Filled__c = 0;
                }
                sumListPositionFilled.add(order);
            }
        }
        else{
            for(AggregateResult ar:groupedResultsPositionFilled) {
                Id itemId = (ID)ar.get('Nrich__job__c');
                countPositionFilled = (DOUBLE)ar.get('expr0');
                Nrich__Job__c order = new Nrich__Job__c(Id=itemId);
                If(Schema.sObjectType.Nrich__Job__c.fields.Nrich__Total_Positions_Filled__c.isUpdateable()){
                    order.Nrich__Total_Positions_Filled__c = countPositionFilled;
                }
                sumListPositionFilled.add(order);
            }
        }
        //Finally update the fields
        Schema.DescribeSObjectResult objectDescription = Nrich__Job__c.getSObjectType().getDescribe();
        if(objectDescription.isUpdateable()){
            if(sumListRejected.size()>0)
                database.update(sumListRejected);
            if(sumListInprocess.size()>0)
                database.update(sumListInprocess);
            if(sumListAccepted.size()>0)
                database.update(sumListAccepted);
            if(sumListRolledOut.size()>0)
                database.update(sumListRolledOut);
            if(sumListPositionFilled.size()>0)
                database.update(sumListPositionFilled);
        }
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='JobApplicationTriggerHandler.totalOfStatus', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
}
}