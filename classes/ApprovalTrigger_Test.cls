@isTest
public class ApprovalTrigger_Test {
    
    @isTest
    public static void testmethodsample(){
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        System.runAs(u1){
            
            Nrich__Employee__c e = new Nrich__Employee__c();
            e.Name = 'Employee 1';
            
            e.Nrich__EmployeeDesignation__c = 'Software Engineer';
            //e.BasEnrich__Active__c = true;
            //e.BasEnrich__Related_User__c = strEmpID;
            insert e;
            
            Nrich__Employee__c e2 = new Nrich__Employee__c();
            e2.Name = 'Employee 2';
            
            e2.Nrich__EmployeeDesignation__c = 'Software Engineer';
            e2.Nrich__Active__c = true;
            e2.Nrich__Related_User__c = u1.id;
            insert e2;
            
            Nrich__Project__c p = new Nrich__Project__c();
            p.Name = 'New Project';
            p.Nrich__ProjectManagerr__c = e2.Id;
            p.Nrich__Project_Start_Date__c =System.today();
            p.Nrich__Project_End_Date__c =System.today()+7;
            insert p;
            
            
            Nrich__Timesheet__c timesheet1 = new Nrich__Timesheet__c();
            timesheet1.Nrich__Date__c=System.today()+2;
            timesheet1.Name=string.valueOf(timesheet1.Date__c);
            timesheet1.Nrich__Employeee__c =e.id;
            insert timesheet1;
            
            
            
            Nrich__Timesheet_Detail__c td = new Nrich__Timesheet_Detail__c();
            
            td.Nrich__Timesheet__c = timesheet1.id;
            td.Nrich__Project__c = p.id;
            td.Nrich__No_of_hours__c = 5;
            
            td.Nrich__Activity__c = 'True';
            td.Nrich__CopyTask__c = True;
            
            insert td;
            
            Nrich__Approval__c app = new Nrich__Approval__c();
            app.Name = 'Approval1';
            app.Nrich__Start_Date__c = System.today();
            app.Nrich__End_Date__c = System.today()+4;
            app.Nrich__ManagerId__c=u1.Id;
            app.Nrich__ProjectId__c=p.id;    
            insert app;
            
            app.Nrich__Status__c='Approved';
            update app;
            System.assertEquals(app, app);
            
        }
    }
}