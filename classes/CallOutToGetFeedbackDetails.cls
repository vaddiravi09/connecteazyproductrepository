@RestResource(UrlMapping='/Feedbackclass')
global class CallOutToGetFeedbackDetails {
    
    //*******************************************************************************************
    //Created by      : Prasad Vievk
    //Method Name 	  : doPost
    //Overall Purpose : This method is used Insert Feedback Details.
    //InputParams     : N/A
    //OutputParams    : N/A
    //*******************************************************************************************
    @HttpPost
    global static void doPost(){
        RestRequest req = RestContext.request;
        RestResponse res=RestContext.response;
        try{
            object data1 = (object) JSON.deserializeUntyped(req.requestBody.toString());
            Map < String, Object > result = (Map < String, Object > ) data1;
            Nrich__Feedback__c feedbackInstance= new Nrich__Feedback__c();
            list<String> lst=new list<string>();
            map<string,SObjectType> schemaMap=Schema.getGlobalDescribe();
            map<string, Schema.SobjectField> fieldmap=schemaMap.get('Nrich__Feedback__c').getDescribe().fields.getMap();
            for(Schema.SObjectField field:fieldmap.values()){
                schema.DescribeFieldResult  fld=field.getDescribe();
                lst.add(fld.getname());
            }
            for( string s:result.Keyset()){
                if(lst.contains(s)){
                    feedbackInstance.put(s,result.get(s));
                }
            }
            Database.SaveResult feedbackInsert=Database.insert(feedbackInstance,false);
            if(feedbackInsert.isSuccess()){
                system.debug(feedbackInsert.getId());
                res.statusCode=200;
                res.responseBody=blob.valueOf('Successfully Updated-- '+feedbackInsert.id);
            }
            else{
                res.statusCode=406;
                system.debug(feedbackInsert.getErrors());
                for(Database.Error e:feedbackInsert.getErrors()){
                    system.debug('error code-->'+e.getStatusCode()+'-----'+e.message+'------'+e.getFields()+e.statuscode);
                    res.responseBody=Blob.valueOf(e.getStatusCode()+'--'+e.getFields()+'--'+e.message);
                }
            }
        }
        catch(Exception e){
           Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CallOutToGetFeedbackDetails.doPost', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        }
    }   
}