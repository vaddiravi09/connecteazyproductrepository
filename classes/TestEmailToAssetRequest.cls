@isTest
public class TestEmailToAssetRequest {
    
    @isTest static void AssetList(){
        Test.startTest();
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 
        User u = new User(Alias = 'sta12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id,ManagerId =u1.id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com1234');
       // System.runAs(u){
             Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                          
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        insert lleave;
        cleave.Nrich__Is_Active__c=True;
        update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser1' , Nrich__EmployeeDesignation__c ='Developer',
                                                                  
                                                                  Nrich__Total_Experience__c=11,
                                                                  Nrich__Related_User__c=u.id );
            
            insert emp;
            
            Nrich__Asset_Request__c assetreq=new Nrich__Asset_Request__c();
            
            assetreq.Nrich__Date_Approved__c=System.today();
            assetreq.Nrich__Employeee__c=emp.id;
            assetreq.Nrich__Status__c='Applied';
            insert assetreq;
            
            list<Nrich__Asset_Request__c> assetlist=new list<Nrich__Asset_Request__c>();
            assetlist.add(assetreq);
            
            EmailToAssetRequestController emas=new EmailToAssetRequestController();
            emas.objType='Nrich__Asset_Request__c';
            emas.AssetApplist=assetlist;
            emas.getAssetsList();
            
            System.assertEquals('Applied', assetreq.Nrich__Status__c);
            Test.stopTest();
            
       // }
        
        
    }
    
}