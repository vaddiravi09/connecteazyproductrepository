/********************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 30/06/2018
* Description: Email to Candidate on completion of the interview.
* Test Class: EmailTemplatesTest
********************************************************************/
public with sharing  class EmailtoCandidateonCompletitonController {
    public String objType {get;set;}
    public ID relatedId {get;set;}
    
    /*******************************************************************************************
    Created by      : Pushmitha
    Overall Purpose : Fetch Interview Round Details
    InputParams     : NA
    OutputParams    : List of Interview Round
    *******************************************************************************************/
    public List<Nrich__Interview_Round__c> getInterviewRoundList (){
        List<Nrich__Interview_Round__c> interviewList = new List<Nrich__Interview_Round__c>();
        if(relatedId != null){
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Job_Application__c'))
            interviewList = [select Id, Nrich__Job_Application__r.Nrich__Candidate__r.Name, Name from Nrich__Interview_Round__c where id = : relatedId ];
        }
        return interviewList;
    }
}