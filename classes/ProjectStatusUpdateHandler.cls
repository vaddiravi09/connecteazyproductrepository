/****************************************************************************************************
* Company:Absyz
* Developer:Seshu Varma
* Created Date:29/10/18
* Description:Fetching details for Project status update
*****************************************************************************************************/
public class ProjectStatusUpdateHandler {
    public static void checkstatus(Map<Id,Nrich__Project__c > mapproj)
    {
        try{
        Map<String, Boolean> userCheck = new map<String, Boolean>(); 
        List<Nrich__User_Story__c > lstuserstry=new list<Nrich__User_Story__c>();
        List<Nrich__Task__c> lsttask= new list<Nrich__Task__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Project__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Status__c'))
            lstuserstry = [Select Id, Name, Nrich__Project__c, Nrich__Status__c From Nrich__User_Story__c  Where Nrich__Project__c IN: mapproj.keyset() AND Nrich__status__c !=: Label.Completed ];
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__Project__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__Status__c'))
            lsttask = [Select Id, Name, Nrich__Project__c, Nrich__Status__c From Nrich__Task__c Where Nrich__Project__c IN: mapproj.keyset() AND (Nrich__Status__c!=:Label.Completed
                                                                                                                                                  AND Nrich__Status__c!= Null) ];
        System.debug('lstuserstry' +lstuserstry);               
        if(lstuserstry.size()>0){                      
            for(Nrich__User_Story__c  usrstry : lstuserstry){
                mapproj.get(usrstry.Nrich__Project__c).adderror(Label.UserStory_Yet_To_Be_Completed);
                
            }
        }
        if(lsttask.size()>0){
            for(Nrich__Task__c tsk : lsttask){
                mapproj.get(tsk.Nrich__Project__c).adderror(Label.Task_Yet_To_Be_Completed);
            }                                                                                                       
        }                                                                                                                                        
        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ProjectStatusUpdateHandler.checkstatus', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}