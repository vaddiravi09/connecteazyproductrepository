public with sharing class CertificationDetailsController {
    
    /****************************************************************************************************
* Company:Absyz
* Developer:Ashish
* Created Date:29/10/18
* Description:Fetching the Certification Details 
*****************************************************************************************************/
    
    public static string strEmployeeID;
    
    //Inserting new Certification request
    @AuraEnabled
    public static Id insertnewrecords(Nrich__Certification_Request__c certificationdetails, Id CerId)
    {
        try{
            Nrich__Employee__c objEmp = new Nrich__Employee__c();
            string strEmpID = userinfo.getuserid();
            objEmp = [select id,
                      Name
                      from Nrich__Employee__c 
                      where Nrich__Related_User__c =: strEmpID
                      limit 1]; 
            system.debug('objEmp****'+objEmp);
            
            strEmployeeID = objEmp.id;
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Approval_Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Employeee__c') && 
               ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Certification_Name__c')
              )
                certificationdetails.Nrich__Approval_Status__c='';
            certificationdetails.Nrich__Employeee__c =strEmployeeID;
            certificationdetails.Nrich__Certification_Name__c = CerId;
            //certificationdetails.Certification_Name__r. EmployeeDesignation__c='Developer';
            insert certificationdetails;
            system.debug(certificationdetails);
            return certificationdetails.Id;
            
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CertificationDetailsController.insertnewrecords', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    // Displaying records in a certification modal
    @AuraEnabled
    public static list<Nrich__Certification_Request__c > viewrecords()
    {
        try{
            string strEmpID = userinfo.getuserid();
            list<Nrich__Certification_Request__c > CertificationRequestDetails= new list<Nrich__Certification_Request__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Verification_id__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Expected_Ceritifcation_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Actual_Certification_Date__c') && 
               ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Result__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Approval_Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Employeee__c')
              )
                CertificationRequestDetails=[select Nrich__Certification_Name__r.Name, Nrich__Verification_id__c, Nrich__Expected_Ceritifcation_Date__c, Nrich__Actual_Certification_Date__c, Nrich__Result__c,Name,Nrich__Approval_Status__c ,Nrich__Certification_Name__r.Nrich__Cost__c ,Nrich__Certification_Name__r.Nrich__Certification_Name__c,Nrich__Certification_Name__r.Nrich__Number_of_Successful_Certification_Exams__c, Nrich__Employeee__r.id from Nrich__Certification_Request__c where Nrich__Employeee__r.Nrich__Related_User__c =:strEmpID
                                             
                                            ] ;
            system.debug('details' +CertificationRequestDetails);
            return CertificationRequestDetails;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CertificationDetailsController.viewrecords', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    //For submitting approval through button
    @AuraEnabled
    public static void submitforapproval(string cer)
    {
        
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments(System.Label.ApprovalMessage); 
        req.setObjectId(cer); 
        req.setProcessDefinitionNameOrId('New_Certification_Request');
        // req.setNextApproverIds(new Id[] {cer.Employee__c   });
        if(!Test.isRunningTest()){
            Approval.process(req);
        }
        system.debug('req'+req);
        
    }
    
    
    
    //Fetching records in a picklist of certification
    @AuraEnabled
    public static list<Nrich__Certification__c > fetchoptions(){
        try{
            string strEmpID = userinfo.getuserid();
            list<Nrich__Employee__c > EmplList=new list<Nrich__Employee__c>();
            string Designation;
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__EmployeeDesignation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Related_User__c'))
                EmplList=[Select id ,Nrich__EmployeeDesignation__c from Nrich__Employee__c where Nrich__Related_User__c =: strEmpID ];
            
            if(EmplList.size()>0)
                Designation = EmplList[0].Nrich__EmployeeDesignation__c;
            
            
            Map<String,String > CertificationReqMap=new Map<String ,String >();
            list<Nrich__Certification_Request__c> CertificationList= new list<Nrich__Certification_Request__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Approval_Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Result__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Verification_id__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification_Request__c', 'Nrich__Certification_Name__c'))
                CertificationList=[select name,Nrich__Approval_Status__c ,Nrich__Result__c, Nrich__Certification_Name__r.Name,Nrich__Verification_id__c  from Nrich__Certification_Request__c where Nrich__Result__c=:Label.Pass];
            
            for(Nrich__Certification_Request__c cer:CertificationList)
            {
                CertificationReqMap.put(cer.Nrich__Certification_Name__r.Name,cer.Nrich__Result__c);
            }
            list<Nrich__Certification__c> Certlist=new list<Nrich__Certification__c>();
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Certification__c', 'Nrich__Certification_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification__c', 'Nrich__Applicable_Designation__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Certification__c', 'Nrich__Active__c'))
                Certlist=[Select Name,Nrich__Certification_Name__c,Id,Nrich__Applicable_Designation__c from Nrich__Certification__c where Nrich__Active__c = TRUE];
            list<Nrich__Certification__c> CertificationReturnList = new list<Nrich__Certification__c>();
            for(Nrich__Certification__c certification:Certlist){
                if(!(CertificationReqMap.ContainsKey(certification.Name)) && (certification.Nrich__Applicable_Designation__c!=NULL && certification.Nrich__Applicable_Designation__c.contains(Designation)))
                    CertificationReturnList.add(certification);
                
            }
            System.debug('CertificationList'+CertificationList);
            System.debug('CertificationReturnList' +CertificationReturnList);
            system.debug('Certlist'+Certlist);
            System.debug('Map'+CertificationReqMap);
            return CertificationReturnList;
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='CertificationDetailsController.fetchoptions', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
    }
    
    
    
}