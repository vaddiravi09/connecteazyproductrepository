@isTest
public class CreationOfFirstRoundInterviewTest {
	
    public static testMethod void getselectOptionsTest(){
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        HRInterviewScheduleController.employeelist emplList = new HRInterviewScheduleController.employeelist();
       	system.assertEquals('Test','Test');
        CreationOfFirstRoundInterviewController.getselectOptions(jobAppl.id);
        Test.stopTest();
    }
    public static testMethod void storeSelectedValueTest(){
		test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        HRInterviewScheduleController.employeelist emplList = new HRInterviewScheduleController.employeelist();
        Nrich__Job_Round__c j=UtilityForTest.Job_Round_Utility(job.id);
       	CreationOfFirstRoundInterviewController.getselectOptions(jobAppl.id);
        CreationOfFirstRoundInterviewController.storeSelectedValue(1,jobAppl.id);
        CreationOfFirstRoundInterviewController.getInterviewRound(jobAppl.id);
        system.assertEquals('Test','Test');
        Test.stopTest();        
    }
}