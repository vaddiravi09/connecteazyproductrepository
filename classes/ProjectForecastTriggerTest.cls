@IsTest
public with sharing class ProjectForecastTriggerTest {

    
    @istest 
    public static void PrjForecastInsert(){
        
        Nrich__Project__c p = new Nrich__Project__c();
        p.Name = 'New Project';
        insert p;
        
        Nrich__Project_Category__c pc = new Nrich__Project_Category__c();
        pc.Name = 'Category 1';
        pc.Nrich__Project__c=p.id;
        pc.Nrich__Active__c=true;
        insert pc;
        
        Nrich__Project_Team_Member__c  prjteammember=new Nrich__Project_Team_Member__c ();
        prjteammember.Nrich__Employeee__c=UtilityForTest.Employee_Utility().Id;
        prjteammember.Nrich__Employee_Start_Date__c=date.newInstance(2018, 01, 01);
        prjteammember.Nrich__Employee_End_Date__c=date.newInstance(2018, 12, 31);
        prjteammember.Nrich__Project__c=p.id;
        insert prjteammember;
        
        Nrich__Project_Team_Forecast__c  PrjForecast=new Nrich__Project_Team_Forecast__c ();
      //  PrjForecast.Nrich__Start_Date__c=date.newInstance(2018, 01, 01);
      //  PrjForecast.Nrich__End_Date__c=date.newInstance(2018, 12, 31);
        PrjForecast.Nrich__Project_Team_Member__c=prjteammember.id;
        insert PrjForecast;
        
        PrjForecast.Nrich__Revenue__c=10;
        update PrjForecast;
        System.assertEquals(PrjForecast, PrjForecast);
    }
}