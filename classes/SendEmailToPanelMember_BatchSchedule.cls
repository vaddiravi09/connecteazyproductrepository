/****************************************************************************
* Company: Absyz
* Developer: Hrishikesh
* Created Date: 27/06/2018
* Description: Schedule class for Batch Class for controlling sending Email to Employee 
*              regarding interview slots
* Test Class: SendEmailToPanelMember_BatchScheduleTest
****************************************************************************/
public with sharing class SendEmailToPanelMember_BatchSchedule implements Schedulable {
    public void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled
        SendEmailToPanelMember_Batch b = new SendEmailToPanelMember_Batch (false);
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,200);
    }
}