/****************************************************************************************************
	* Company: Absyz
	* Developer: Raviteja
	* Created Date: 18/10/2018
	* Description: Controller for Rejection Email Template
	*****************************************************************************************************/
public with sharing  class timesheetemailtemplatecontroller 
{
    
    public String objType {get;set;}
    public ID relatedId {get;set;}
    public list<Nrich__TimeSheet_Detail__c> Timesheetdetailslist{get;set;}
    public list<Nrich__Approval__c > ApprovalDetailList{get;set;}
    list < ProcessInstance > listProcess =  new list <ProcessInstance>();  
    //  public set<id>ProjectIds{get;set;}
    
    
    public list<Nrich__TimeSheet_Detail__c> getdetailslist(){
        system.debug('controller'+relatedId);
        wrapperclass wrap=new wrapperclass();
        list<Nrich__TimeSheet_Detail__c>Timesheetdetailslist=new list<Nrich__TimeSheet_Detail__c>();
        list<Nrich__Approval__c>ApprovalDetailList=new list<Nrich__Approval__c>();
        
        set<id>ProjectIds=new set<id>();
        
        map<Id,wrapperclass>ApprovalMap=new map<id,wrapperclass>();
        list<Nrich__TimeSheet_Detail__c>Timesheetdetailslist1=new list<Nrich__TimeSheet_Detail__c>();
        
        if(relatedId !=null)
        {
            
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Approval__c', 'Nrich__End_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Approval__c', 'Nrich__Start_Date__c')){
                
                ApprovalDetailList=[select id,name,Nrich__End_Date__c ,Nrich__Start_Date__c ,Nrich__ProjectId__c
                                    from Nrich__Approval__c where id=:relatedId];
                for(Nrich__Approval__c t:ApprovalDetailList){
                    wrap.StDate=t.Nrich__Start_Date__c;
                    wrap.EdDate=t.Nrich__End_Date__c;
                    ApprovalMap.put(t.Nrich__ProjectId__c,wrap);
                }
            }
            
            
            
            
            if( ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Project__c') ){
                Timesheetdetailslist=[select id,Name ,Nrich__Status__c , Nrich__Project__c,Nrich__Employee_Name__c ,Nrich__Project_Name__c ,Nrich__Comments__c ,Nrich__Project__r.Nrich__ProjectManagerr__r.Name,Nrich__No_of_hours__c ,
                                      Nrich__Description__c ,
                                      Nrich__Date__c   from Nrich__TimeSheet_Detail__c   where Nrich__Project__c =:ApprovalMap.keyset()];
                for(Nrich__TimeSheet_Detail__c tdetail:Timesheetdetailslist){
                    if(tdetail.Nrich__Date__c>=ApprovalMap.get(tdetail.Nrich__Project__c).StDate ){
                        if(tdetail.Nrich__Date__c<=ApprovalMap.get(tdetail.Nrich__Project__c).EdDate){
                            tdetail.Nrich__Status__c = Label.Approved;
                            Timesheetdetailslist1.add(tdetail);
                        }
                        
                    }
                }
            }
            //For Comments
            /* listProcess = [SELECT Id, createdDate, (SELECT Id, StepStatus, Comments FROM Steps) FROM ProcessInstance order by createdDate DESC LIMIT 1];
System.debug('ListProcess' +listProcess);
for(ProcessInstanceStep ps : listProcess[0].Steps){
if(ps.Comments != null){
Timesheetdetailslist1[0].Comments__c = ps.Comments;
}
}
*/
            
        }
        
        return Timesheetdetailslist1;
        
    }
    
    /* public List<BasEnrich__Timesheet_Detail__c > getdetailslist (){
List<BasEnrich__Timesheet_Detail__c > Timesheetdetailslist = new List<BasEnrich__Timesheet_Detail__c >();

list < ProcessInstance > listProcess =  new list <ProcessInstance>();  
if(relatedId != null){
Timesheetdetailslist = [select Id, Name, BasEnrich__Comments__c, BasEnrich__Project_Name__c ,BasEnrich__No_of_hours__c, BasEnrich__Employee_Name__c  ,BasEnrich__Status__c , BasEnrich__Description__c, BasEnrich__Project__r.Id 
from BasEnrich__Timesheet_Detail__c  where id =: relatedId];
system.debug('Timesheetdetailslist');
system.debug('BasEnrich__Status__c=====>>>'+Timesheetdetailslist);
listProcess = [SELECT Id, createdDate, (SELECT Id, StepStatus, Comments FROM Steps) FROM ProcessInstance order by createdDate DESC LIMIT 1];

for(ProcessInstanceStep ps : listProcess[0].Steps){
if(ps.Comments != null){
Timesheetdetailslist[0].BasEnrich__Comments__c = ps.Comments;
}
}
}
return Timesheetdetailslist;
}*/
    
    /*
public List<Employee__c> getempdetailslist(){
list<Employee__c> Employeedetaillist = new List<Employee__c>();
if(relatedId != null){
Employeedetaillist = [Select Id, Manager__c  from Employee__c where id = : relatedId];
}
return Employeedetaillist;
}
*/
    
    public class wrapperclass{
        @AuraEnabled
        public Date StDate;
        @AuraEnabled
        public Date EdDate;
    }
}