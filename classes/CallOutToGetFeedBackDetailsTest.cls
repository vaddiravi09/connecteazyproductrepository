@isTest
public class CallOutToGetFeedBackDetailsTest {
    
    static  TestMethod void method1(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        user intuser = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Integration User', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='intuser@practo.com');
        insert intuser;
        test.startTest();
        Nrich__Feedback__c feedbackInstance= new Nrich__Feedback__c();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__Employee__c emp=UtilityForTest.Employee_Utility();
        Nrich__Job_Application__c jobapp=UtilityForTest.job_Application_Utility(cand.id,job.id);
        Nrich__Interview_Round__c intRound=UtilityForTest.Interview_Round_Utility(jobapp.id);
        
        feedbackInstance.Nrich__Communication_Skills_Feedback__c='good';
        feedbackInstance.Nrich__Communication_Skills_Rating__c='1';
        feedbackInstance.Nrich__Feedback_Description__c='Good';
        feedbackInstance.Nrich__Feedback_Status__c='Selected';
        feedbackInstance.Nrich__Job_Specific_Knowledge_Feedback__c='good';
        feedbackInstance.Nrich__Job_Specific_Knowledge_Rating__c='1';
        feedbackInstance.Nrich__Overall_Rating__c='1';
        feedbackInstance.Nrich__Timeliness_Feedback__c='Good';
        feedbackInstance.Nrich__Timeliness_Rating__c='1';
        feedbackInstance.Nrich__Interview_Panel_Member__c=emp.Id;
        feedbackInstance.Nrich__Interview_Round__c=intRound.Id;
        
        String JsonMsg=JSON.serialize(feedbackInstance);
        system.runAs(intuser){
            RestRequest req= new RestRequest();
            RestResponse res=new RestResponse();
            req.requestURI='/services/apexrest/Feedbackclass';
            req.httpMethod='POST';
            req.requestBody=Blob.valueOf(JsonMsg);
            RestContext.request=req;
            RestContext.response=res;
            Test.setMock(HttpCalloutMock.class,  new MockHttpResponseGenerator());
            
            CallOutToGetFeedbackDetails.doPost(); 
        }
        System.assertEquals('Selected',feedbackInstance.Nrich__Feedback_Status__c );
        
        test.stopTest();
    }   
}