/****************************************************************************************************
* Company: Absyz
* Developer: Ashish Nag
* Created Date: 22/10/2018
* Description: Class for uploading signature
*****************************************************************************************************/
public with sharing class SupportNotificationController {
    public String objType {get;set;}
    public ID relatedId {get;set;}
    
    public list<Nrich__Support_Ticket__c > SupportApplist{get;set;}
    
    public List<Nrich__Support_Ticket__c> getSupportList (){
        try{
        list<Nrich__Support_Ticket__c> SupportApplist=new list<Nrich__Support_Ticket__c>();
        if(relatedId != null && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Issue_Description__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Department_Name__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Department__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Proposed_solution__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Status__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Subject__c')
          ){
              
              SupportApplist=[select id,Name,Nrich__Issue_Description__c,Nrich__Department_Name__c,Nrich__Department__c, Nrich__Employeee__r.Name ,Nrich__Proposed_solution__c , Nrich__Status__c, Nrich__Subject__c  from Nrich__Support_Ticket__c where id=:relatedId
                              ];
              
              //system.debug('SupportApplist' +SupportApplist);
          }
        return SupportApplist;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='SupportNotificationController.getSupportList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}