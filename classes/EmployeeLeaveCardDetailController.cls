public class EmployeeLeaveCardDetailController {
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 16/10/2018
* Description: Fetching Details of Employee leave card
*****************************************************************************************************/   
    @AuraEnabled
    public static list<Nrich__Employee_Leave_Category__c> fetchEmployeeLeaveCard(){
        try{
        string empUserId = userinfo.getUserId();
        list<Nrich__Employee_Leave_Category__c> empleaveCategoryList=new list<Nrich__Employee_Leave_Category__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Category__c', 'Nrich__Loss_of_Pay__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Category__c', 'Nrich__Leave_Card__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Category__c', 'Nrich__Leaves_Availed__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Category__c', 'Nrich__Leaves_Obtained__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_Leave_Category__c', 'Nrich__Leaves_Remaining__c')
      )
            
            empleaveCategoryList = [Select id, Nrich__Loss_of_Pay__c, Nrich__Leave_Card__r.Nrich__Employee__c, Nrich__Leave_Card__c, Name, Nrich__Leaves_Availed__c, Nrich__Leaves_Obtained__c, Nrich__Leaves_Remaining__c from Nrich__Employee_Leave_Category__c where Nrich__Leave_Card__r.Nrich__Employee__r.Nrich__Related_User__c=:empUserId AND Nrich__Leave_Card__r.Nrich__Is_Active__c=TRUE ];
        if(empleaveCategoryList!=NULL && empleaveCategoryList.size()>0)
            return empleaveCategoryList;
        else
            return null;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeLeaveCardDetailController.fetchEmployeeLeaveCard', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 16/10/2018
* Description: Saving Employee Leave Request
*****************************************************************************************************/   
    @AuraEnabled
    public static saveresultStatus saveEmployeeLeaveRequest(Nrich__Leave_Request__c leaveRequest, String employeeId){
        try{
            System.debug('employeeIdINLeaveRQ' +employeeId);
            leaveRequest.Nrich__Requested_By__c = employeeId;
            database.saveresult result = database.insert(leaveRequest, false);
            saveresultStatus resultval = new saveresultStatus();
            resultval.isSuccess = result.isSuccess();
            resultval.recordId = result.getId();
            if(result.getErrors().size()>0)
                resultval.message = result.getErrors()[0].message;
            return resultval;
        }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeLeaveCardDetailController.saveEmployeeLeaveRequest', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
            return null;
      }
    }
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 17/10/2018
* Description: Submiting Employee Leave Request for Approval
*****************************************************************************************************/   
    @AuraEnabled
    public static boolean submitLeaveRequest(String leaveRequestId){
        try{
            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
            approvalRequest.setComments('Leave Request Submitted for Approval');
            approvalRequest.setObjectId(leaveRequestId);
            Approval.ProcessResult approvalResult = Approval.process(approvalRequest);
            System.debug('proces '+approvalResult);
            return approvalResult.isSuccess();
        }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeLeaveCardDetailController.submitLeaveRequest', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
            return null;
      }
    }
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 17/10/2018
* Description: Fetching Employee leave requests
*****************************************************************************************************/   
    @AuraEnabled
    public static list<Nrich__Leave_Request__c> fetchEmployeeLeaveRequests(String listType){
        try{
        list<String> statusList = new list<String>();
        if(listType==Label.Availed){
            statusList.add(Label.Applied);
            statusList.add(Label.Pending);
            statusList.add(Label.Approved);
            statusList.add(Label.Rejected);
        }
        System.debug('statusList' +statusList);
        string empUserId = userinfo.getUserId();
        list<Nrich__Leave_Request__c> empleaveRequestList= new list<Nrich__Leave_Request__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Leave_Type__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Approver_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Comments__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__End_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Start_Date__c')
     )
            empleaveRequestList = [Select Id, Name, Nrich__Status__c, Nrich__Leave_Type__c, Nrich__Approver_Name__c, Nrich__Comments__c, Nrich__End_Date__c,Nrich__Start_Date__c from Nrich__Leave_Request__c where Nrich__Requested_By__r.Nrich__Related_User__c=:empUserId AND Nrich__Employee_Leave_Category__r.Nrich__Leave_Card__r.Nrich__Is_Active__c=TRUE AND Nrich__Status__c IN: statusList];
        System.debug(empleaveRequestList);
        if(empleaveRequestList!=NULL && empleaveRequestList.size()>0)
            return empleaveRequestList;
        else
            return null;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeLeaveCardDetailController.fetchEmployeeLeaveRequests', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    public class saveresultStatus{
        @AuraEnabled
        public Boolean isSuccess {get; set;}
        @AuraEnabled
        public String recordId {get; set;}
        @AuraEnabled
        public String message {get; set;}
    }
}