@isTest
public class TemplateCreationControllerTest {
    
    
    public static TestMethod void getTemplateApplicationTypeMethod(){
        TemplateCreationController.getTemplateType();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__InitialSetup__c iniSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__Job_Round__c jobRound = UtilityForTest.Job_Round_Utility(job.Id);
        Nrich__Template__c temp = UtilityForTest.Template_Utility(job.Id,jobRound.Id);
        Nrich__Section__c sec = UtilityForTest.Section_Utility(temp.Id);
        //List<Section_Fields__c> sfList = UtilityForTest.Section_Fields_Utility(sec.Id);
        TemplateCreationController.fetchTemplaterecord(temp.id);
        TemplateCreationController.getFields(temp.id);
        String sections='[{"sobjectType":"Nrich__Section__c","Nrich__Order__c":1,"Name":"Test Section"}]';
        //String fieldlist='';
        String fieldlist= ' [{"fieldLabel": "Job Application Name","fieldName": "Name","fieldObjectName": "Nrich__Job_Application__c","fieldType": "STRING","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 1},{"fieldLabel": "LongTextArea","fieldName": "LongTextArea","fieldObjectName": "Nrich__Job_Application__c","fieldType": "LongTextArea","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 2}, {"fieldLabel": "Email","fieldName": "Email","fieldObjectName": "Nrich__Job_Application__c","fieldType": "Email","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 3},  {"fieldLabel": "Phone,"fieldName": "Phone","fieldObjectName": "Nrich__Job_Application__c","fieldType": "Phone","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 4},{"fieldLabel": "Applicant Permanent Address","fieldName": "Nrich__Applicant_Permanent_Address__c","fieldObjectName": "Nrich__Job_Application__c","fieldType": "TEXTAREA","sectionOrder": "0","sectionColumn": "2","sectiondfieldOrder": 2}]';
        TemplateCreationController.saveTemplate('Test Files','14346','Pdf',temp,sections,fieldlist); 
        system.assertEquals('Test Description','Test Description');
    }
    public static TestMethod void getTemplateFeedbackTypeMethod(){
        
        Department__c dept = UtilityForTest.Department_Utility();
        InitialSetup__c iniSetup =  UtilityForTest.InitialSetup_Utility();
        Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Job_Round__c jobRound = UtilityForTest.Job_Round_Utility(job.Id);
        Template__c temp = UtilityForTest.Template_Utility(job.Id,jobRound.Id);
        temp.Type_of_Template__c='Feedback';
        String sections='[{"sobjectType":"Section__c","Order__c":1,"Name":"Test Section"}]';
        
        String fieldlist= ' [{"fieldLabel": "Job Application Name","fieldName": "Name","fieldObjectName": "Feedback__c","fieldType": "STRING","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 1}, {"fieldLabel": "Picklist","fieldName": "PicklistName","fieldObjectName": "Feedback__c","fieldType": "Picklist","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 2}, {"fieldLabel": "Double Lable","fieldName": "DoubleName","fieldObjectName": "Feedback__c","fieldType": "Double","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 3} ,{"fieldLabel": "CurrencyLable","fieldName": "CurrencyName","fieldObjectName": "Feedback__c","fieldType": "Currency","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 4}, {"fieldLabel": "MultiPicklistLabel","fieldName": "MultiPicklistName","fieldObjectName": "Feedback__c","fieldType": "MultiPicklist","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 5} ,{"fieldLabel": "JDateTimeName","fieldName": "DateTimeName","fieldObjectName": "Feedback__c","fieldType": "DateTime","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 6}, {"fieldLabel": "JBooleaname","fieldName": "DooleanName","fieldObjectName": "Feedback__c","fieldType": "Boolean","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 7},{"fieldLabel": "Applicant Permanent Address","fieldName": "Applicant_Permanent_Address__c","fieldObjectName": "Job_Application__c","fieldType": "TEXTAREA","sectionOrder": "0","sectionColumn": "2","sectiondfieldOrder": 1},{"fieldLabel": "Applicant Permanent Address","fieldName": "Applicant_Permanent_Address__c","fieldObjectName": "Job_Application__c","fieldType": "TEXTAREA","sectionOrder": "0","sectionColumn": "2","sectiondfieldOrder": 2}]';
        TemplateCreationController.saveTemplate('Test Files','14346','Pdf',temp,sections,fieldlist); 
        system.assertEquals('Test Description','Test Description');
    }
    public static TestMethod void getTemplateOnboardingTypeMethod(){
        
        Department__c dept = UtilityForTest.Department_Utility();
        InitialSetup__c iniSetup =  UtilityForTest.InitialSetup_Utility();
        Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Job_Round__c jobRound = UtilityForTest.Job_Round_Utility(job.Id);
        Template__c temp = UtilityForTest.Template_Utility(job.Id,jobRound.Id);
        temp.Type_of_Template__c='On Boarding';
        String sections='[{"sobjectType":"Section__c","Order__c":1,"Name":"Test Section"}]';
        
        String fieldlist= ' [{"fieldLabel": "Job Application Name","fieldName": "Name","fieldObjectName": "Candidate__c","fieldType": "STRING","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 1},{"fieldLabel": "Picklist ","fieldName": "Name","fieldObjectName": "Candidate__c","fieldType": "Picklist","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 2}, {"fieldLabel": "Job Application Name","fieldName": "Name","fieldObjectName": "Candidate__c","fieldType": "Picklist","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 3},{"fieldLabel": "Job Application Name","fieldName": "Name","fieldObjectName": "Candidate__c","fieldType": "Currency","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 4}, {"fieldLabel": "Job Application Name","fieldName": "Name","fieldObjectName": "Job_Application__c","fieldType": "Picklist","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 5},{"fieldLabel": "Job Application Name","fieldName": "Name","fieldObjectName": "Candidate__c","fieldType": "Double","sectionOrder": "0","sectionColumn": "1","sectiondfieldOrder": 4},,{"fieldLabel": "Applicant Permanent Address","fieldName": "Applicant_Permanent_Address__c","fieldObjectName": "Job_Application__c","fieldType": "TEXTAREA","sectionOrder": "0","sectionColumn": "2","sectiondfieldOrder": 2}]';
        TemplateCreationController.saveTemplate('Test Files','14346','Pdf',temp,sections,fieldlist); 
        system.assertEquals('Test Description','Test Description');
    }
    
    
    
    
    
    
    
}