/********************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 30/06/2018
* Description: Email Alert to  panel Member Regarding Interview Slots.
* Test Class: emailTempPreferredSlotsControllerTest
********************************************************************/
public with sharing class EmailTemplatePreferredSlotsController {
	public ID relatedId {get;set;}
    public String objType {get;set;}
    
    /*******************************************************************************************
    Created by      : Pushmitha
    Overall Purpose : Fetch Employee Details
    InputParams     : NA
    OutputParams    : List of Employees
    *******************************************************************************************/
    public List<Nrich__Employee__c> getEmployeeSlotList (){
        List<Nrich__Employee__c> employeeList = new List<Nrich__Employee__c>();
        if(employeeList != null){    
            employeeList = [select Id,Name from Nrich__Employee__c where id = : relatedId];
        }
        return employeeList;
    }
}