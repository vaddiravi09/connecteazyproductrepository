/****************************************************************************************************
* Company: Absyz
* Developer: Ashish Nag K
* Created Date: 22/10/2018
* Description: Creating new support records
*****************************************************************************************************/
public with sharing class SupportDetailsController {
    
    public static string strEmployeeID;
    
    // For Inserting Support Request 
    @AuraEnabled
    public static void insertnewsupportrecords(Nrich__Support_Ticket__c   supportdetails){
        try{
        Nrich__Employee__c objEmp = new Nrich__Employee__c();
        string strEmpID = userinfo.getuserid();
        objEmp = [select id
                  from Nrich__Employee__c 
                  where Nrich__Related_User__c =: strEmpID
                  limit 1]; 
        system.debug('objEmp****'+objEmp);
        //objWrap.objEmployeeDetails = objEmp;
        strEmployeeID = objEmp.id;
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Employeee__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Status__c'))
            supportdetails.Nrich__Employeee__c=strEmployeeID;
        supportdetails.Nrich__Status__c=Label.New;
        
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Support_Ticket__c.getSObjectType().getDescribe();
        if(objectDescriptioncontent.isCreateable())
            database.insert(supportdetails);
        //insert supportdetails;
        system.debug(supportdetails);
        // return supportdetails.Id;
        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='SupportDetailsController.insertnewsupportrecords', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    //Fetching Details For Support Ticket
    @AuraEnabled
    public static list<Nrich__Support_Ticket__c> viewSupportRecords(){
        try{
        list<Nrich__Support_Ticket__c> SupportDetails= new list<Nrich__Support_Ticket__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Department_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Department__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Issue_Description__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Issue_Description__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Location__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Priority__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Proposed_solution__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__IssueCategory__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Status__c') &&
           ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__Subject__c')
          )
            SupportDetails=[select id,name,Nrich__Department_Name__c, Nrich__Department__c, Nrich__Issue_Description__c ,Nrich__Location__c ,Nrich__Priority__c ,Nrich__Proposed_solution__c,Nrich__IssueCategory__c,Nrich__Status__c , 
                            Nrich__Department__r.Name, Nrich__Assigned_Too__r.Name, Nrich__Subject__c  from Nrich__Support_Ticket__c  ];
        // System.debug('Assetdetails' +AssetDetails);
        return SupportDetails;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='SupportDetailsController.viewSupportRecords', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    @AuraEnabled
    public static list<string> fetchCategoryoptions(){
        Schema.DescribeFieldResult fieldResult = Nrich__Support_Ticket__c.Nrich__IssueCategory__c.getDescribe();
        list<String> options = Optionshelper(fieldResult, true);
        return options;
    }
    
    @AuraEnabled
    public static list<string> fetchPriorityoptions(){
        Schema.DescribeFieldResult fieldResult = Nrich__Support_Ticket__c.Nrich__Priority__c.getDescribe();
        list<String> options = Optionshelper(fieldResult, true);
        return options;
    }
   
   
    public static list<String> Optionshelper(Schema.DescribeFieldResult fieldval, boolean empty){
        list<String> options = new list<String>();
        List<Schema.PicklistEntry> ple = fieldval.getPicklistValues();
        if(empty)
            options.add('--None--');
        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getValue());
        }       
        return options;
    }
    
    @AuraEnabled
    public static list<string> fetchStatus(){
        Schema.DescribeFieldResult fieldResult = Nrich__Support_Ticket__c.Nrich__Status__c.getDescribe();
        list<String> options = Optionshelper(fieldResult, false);
        system.debug('options===>'+options);
        return options;
    }
    
    public static void assignSupportTickets(list<Nrich__Support_Ticket__c> supportTicketList, Set<String> issueCategorySet){
        try{
            System.debug('supportTicketList'+supportTicketList);
             System.debug('issueCategorySet'+issueCategorySet);
        map<String, Nrich__SupportTeam__c> CategoryMap = new map<String, Nrich__SupportTeam__c>();
        map<Id, list<String>> supportTeamMemberMap = new map<Id, list<String>>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__SupportTeam__c', 'Nrich__Category__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__SupportTeam__c', 'Nrich__Last_Support_Person_Assigned__c'))
             //System.debug('ObjectFieldAccessCheck'+ObjectFieldAccessCheck);
            for(Nrich__SupportTeam__c supTeam: [Select Id, Name, Nrich__Category__c, Nrich__Last_Support_Person_Assigned__c from Nrich__SupportTeam__c where Nrich__Category__c IN: issueCategorySet]){
                System.debug('supTeam'+supTeam);
                CategoryMap.put(supTeam.Nrich__Category__c, supTeam);      
            }
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Team_Member__c', 'Nrich__Support_team__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Team_Member__c', 'Nrich__Active__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Team_Member__c','Nrich__Member_Name__c'))
            for(Nrich__Support_Team_Member__c steam: [Select Id, Name, Nrich__Support_team__c, Nrich__Active__c, Nrich__Member_Name__c from Nrich__Support_Team_Member__c where Nrich__Active__c=TRUE AND Nrich__Support_team__r.Nrich__Category__c IN: issueCategorySet order by Name]){
                if(supportTeamMemberMap.size()>0 && supportTeamMemberMap.containsKey(steam.Nrich__Support_team__c)){
                    list<string> stringlist = supportTeamMemberMap.get(steam.Nrich__Support_team__c);
                    stringlist.add(steam.Id);
                    supportTeamMemberMap.put(steam.Nrich__Support_team__c, stringlist);
                }else{
                    supportTeamMemberMap.put(steam.Nrich__Support_team__c, new list<String>{steam.Id});
                }
            }
        list<Nrich__SupportTeam__c> supportTeamUpdatelist = new list<Nrich__SupportTeam__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Support_Ticket__c', 'Nrich__IssueCategory__c'))
            for(Nrich__Support_Ticket__c st: supportTicketList){
                if(CategoryMap.size()>0 && CategoryMap.containsKey(st.Nrich__IssueCategory__c) && supportTeamMemberMap.containsKey(CategoryMap.get(st.Nrich__IssueCategory__c).Id)){
                    
                    list<String> suppMembersList = supportTeamMemberMap.get(CategoryMap.get(st.Nrich__IssueCategory__c).Id);
                    if(suppMembersList.size()>0 && CategoryMap.get(st.Nrich__IssueCategory__c).Nrich__Last_Support_Person_Assigned__c!=NULL){
                        Nrich__SupportTeam__c suppTeam = CategoryMap.get(st.Nrich__IssueCategory__c);
                        Integer indexval = suppMembersList.indexOf(suppTeam.Nrich__Last_Support_Person_Assigned__c);
                        if((indexval+2)>suppMembersList.size()){
                            st.Nrich__Assigned_Too__c = suppMembersList[0];
                            suppTeam.Nrich__Last_Support_Person_Assigned__c = suppMembersList[0];
                            supportTeamUpdatelist.add(suppTeam);
                        }else{
                            st.Nrich__Assigned_Too__c = suppMembersList[(indexval+1)];
                            suppTeam.Nrich__Last_Support_Person_Assigned__c = suppMembersList[(indexval+1)];
                            supportTeamUpdatelist.add(suppTeam);
                        }
                    }else if(suppMembersList.size()>0){
                        st.Nrich__Assigned_Too__c = suppMembersList[0];
                        Nrich__SupportTeam__c suppTeam = CategoryMap.get(st.Nrich__IssueCategory__c);
                        suppTeam.Nrich__Last_Support_Person_Assigned__c = suppMembersList[0];
                        supportTeamUpdatelist.add(suppTeam);
                    }
                }
            }
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__SupportTeam__c.getSObjectType().getDescribe();
        if(objectDescriptioncontent.isCreateable() && supportTeamUpdatelist.size()>0)
            database.update(supportTeamUpdatelist);
        //if(supportTeamUpdatelist.size()>0)
        //database.update(supportTeamUpdatelist);
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='SupportDetailsController.assignSupportTickets', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}