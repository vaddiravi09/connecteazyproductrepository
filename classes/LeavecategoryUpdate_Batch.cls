/****************************************************************************************************
* Company: Absyz
* Developer: Thadeus Ronn Thomas
* Created Date: 15/10/2018
* Description: Batch class to calculate the no. of Leaves available at any instant of time.
*****************************************************************************************************/  

public class LeavecategoryUpdate_Batch implements Database.Batchable < sObject >, Schedulable{
    
    public Integer Year = System.today().year();
    public integer Month = System.today().month();
    map<string,Nrich__leave_category__c> name2lcmap=new map<String,Nrich__leave_category__c>();
    Date Applicable_date, Valid_from, calculate_from_date;
    integer mcount, totalfreq, totalleave, tmonths, lapse_remaining, last_lapsed;
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        string query;
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_leave_category__c', 'Nrich__CarryForward__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_leave_category__c', 'Nrich__Leaves_Obtained__c' ) && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee_leave_category__c', 'Nrich__Leave_category_code__c'))
            query = 'select id, Nrich__CarryForward__c, Nrich__Leave_Card__r.Name, Name, Nrich__Leave_Card__r.Nrich__Date_of_Joining__c, Nrich__Leave_Card__r.Nrich__Applicable_From__c, Nrich__Leave_category_code__c, Nrich__Leaves_Obtained__c from Nrich__Employee_leave_category__c where Nrich__Leave_Card__r.Nrich__Is_Active__c=True';      
        return Database.getQueryLocator(query); 
    }
    
    public void execute(Database.BatchableContext BC, List < Nrich__Employee_leave_category__c > LCateg) {
        try{
        Nrich__leave_category__c leavecat= new Nrich__leave_category__c();
        list<Nrich__Employee_leave_category__c> leavecategoryUpdateList = new list<Nrich__Employee_leave_category__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__Increment_Frequency__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__Increment_Step__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__will_lapse__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__Default__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__Will_Carry_Forward__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__Carry_Forward_Frequency__c') ) 
            
            for(Nrich__Leave_Category__c lcategory:[select Nrich__Increment_Frequency__c, Nrich__Increment_Step__c, Nrich__Will_Carry_Forward__c, Nrich__Carry_Forward_Frequency__c, Nrich__will_lapse__c, Nrich__lapse_frequency__c, Nrich__Default__c,Nrich__Leave_category_code__c from Nrich__Leave_Category__c where Nrich__Applicable_Leave_Structure__r.Nrich__Is_Active__c=True])
            name2lcmap.put(lcategory.Nrich__Leave_category_code__c,lcategory);
        for (Nrich__Employee_leave_category__c LC: LCateg) {
            mcount=totalfreq=totalleave=tmonths=lapse_remaining=last_lapsed=0;
            if(LC.Nrich__Leave_Card__r.Nrich__Date_of_Joining__c>LC.Nrich__Leave_Card__r.Nrich__Applicable_From__c)
                Valid_from=LC.Nrich__Leave_Card__r.Nrich__Date_of_Joining__c;
            else
                Valid_from=LC.Nrich__Leave_Card__r.Nrich__Applicable_From__c;
            if((month>valid_from.month())&&(year==valid_from.year()))
                mcount=month-valid_from.month();
            else if(year>valid_from.year())
                mcount=((12-valid_from.month())+month+((year-valid_from.year()-1)*12));
            if((valid_from.day() > 15)&&(mcount>0))
                mcount-=1;
            leavecat=name2lcmap.get(LC.Nrich__Leave_category_code__c);
            if(leavecat.Nrich__Will_Lapse__c==True){
                if((month>LC.Nrich__Leave_Card__r.Nrich__Applicable_From__c.month())&&(year==LC.Nrich__Leave_Card__r.Nrich__Applicable_From__c.year()))
                    tmonths=Month-LC.Nrich__Leave_Card__r.Nrich__Applicable_From__c.month();
                else if(year>LC.Nrich__Leave_Card__r.Nrich__Applicable_From__c.year())
                    tmonths=((12-LC.Nrich__Leave_Card__r.Nrich__Applicable_From__c.month())+month+((year-LC.Nrich__Leave_Card__r.Nrich__Applicable_From__c.year()-1)*12));
                lapse_remaining=(integer)math.mod(tmonths,(integer)math.floor(leavecat.Nrich__lapse_frequency__c));
                last_lapsed=(integer)math.mod(mcount-lapse_remaining,(integer)math.floor(leavecat.Nrich__Increment_Frequency__c));
                valid_from=valid_from.addMonths(mcount-(last_lapsed+lapse_remaining));
                mcount=(last_lapsed+lapse_remaining+1);
            }
            totalfreq=(integer)math.floor(mcount/leavecat.Nrich__Increment_Frequency__c);
            if(leavecat.Nrich__Will_Lapse__c==False)
                totalleave=(integer)math.floor((totalfreq+1)*leavecat.Nrich__Increment_Step__c);
            else
                totalleave=(integer)math.floor((totalfreq*leavecat.Nrich__Increment_Step__c));
            LC.Nrich__Leaves_Obtained__c=totalleave+LC.Nrich__CarryForward__c+leavecat.Nrich__Default__c;
            
            leavecategoryUpdateList.add(LC);
        }
        
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Employee_Leave_Category__c.getSObjectType().getDescribe();
        if(objectDescriptioncontent.isUpdateable() && leavecategoryUpdateList.size()>0)
            database.update(leavecategoryUpdateList, false);
    }catch(exception e){
        e.getLineNumber();
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='LeavecategoryUpdate_Batch', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    public void finish(Database.BatchableContext BC) {}
    public void execute(SchedulableContext sc)
    {     
        LeavecategoryUpdate_Batch b = new LeavecategoryUpdate_Batch();
        database.executebatch(b,10);
    }
}