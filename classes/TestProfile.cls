@isTest()
public class TestProfile {
    
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Ashish Nag K
* Created Date: 30/10/2018
* Description: Test class covering Profile Modal.
*****************************************************************************************************/  
    
    @isTest static void viewrec (){
        list<Nrich__Employee__c> emplist=new list<Nrich__Employee__c>();
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        System.runAs(u){
        Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                         
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
       // insert lleave;
        cleave.Nrich__Is_Active__c=True;
       // update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                        
                                        Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
        
        
        insert emp;
        // generalmethod();
        // UtilityForTest.Employee_Utility();
        Profilerec.viewrecords();
        
        System.assertEquals('emp2', 'emp2');
        
        
        
          }
        Test.StopTest();
    }
    
    @isTest static void insertrecord(){
        list<Nrich__Employee__c> emplist=new list<Nrich__Employee__c>();
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
          //  insert lleave;
            cleave.Nrich__Is_Active__c=True;
          //  update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            // UtilityForTest.Employee_Utility
            
            Profilerec.insertrec(emp);
            System.assertEquals('emp', 'emp');
            Test.StopTest();
        }
        
    }
    
    @isTest static void getprofile()
    {
        
        
        list<Nrich__Employee__c> emplist=new list<Nrich__Employee__c>();
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
         System.runAs(u){
       Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                          
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
       // insert lleave;
        cleave.Nrich__Is_Active__c=True;
        //update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                        
                                        Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
        
        
        insert emp;
        ContentVersion cv = new ContentVersion();
        cv.title ='test';      
        cv.PathOnClient ='test.png';           
        cv.VersionData =Blob.valueOf('Testing base 64 encode'); 
        cv.IsMajorVersion = true;
        insert cv;         
        
        
        list<contentDocument> doclist = [select id,FileType from contentdocument];
        system.debug('doclist '+doclist);
        ContentDocumentLink newFileShare = new ContentDocumentLink();
        newFileShare.contentdocumentid = doclist[0].Id;
        newFileShare.LinkedEntityId = emp.Id;
        newFileShare.ShareType= 'V';
        insert newFileShare;
        Profilerec.getProfilePicture();
        System.assertEquals('test',cv.title);
        
        Test.stopTest();
          }
    }
    
    @isTest static void insertimage(){
        string filename='abc';
        string filetype='jpeg';
        string version='testing';
        list<Nrich__Employee__c> emplist=new list<Nrich__Employee__c>();
        Test.startTest();
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
         System.runAs(u){
       Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                          
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        //insert lleave;
        cleave.Nrich__Is_Active__c=True;
       // update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                        
                                        Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
        
        
        insert emp;
        ContentVersion cv = new ContentVersion();
        cv.title = filename;      
        cv.PathOnClient =filename;           
        cv.VersionData =EncodingUtil.base64Decode(version); 
        cv.IsMajorVersion = true;
        insert cv;       
        list<contentDocument> doclist = [select id,FileType from contentdocument];
        system.debug('doclist '+doclist);
        ContentDocumentLink newFileShare = new ContentDocumentLink();
        newFileShare.contentdocumentid = doclist[0].Id;
        newFileShare.LinkedEntityId = emp.id;
        newFileShare.ShareType= 'V';
        insert newFileShare;
        Profilerec.saveImage(filename,version,filetype);
        System.assertEquals('abc',cv.title);
        Test.stopTest();
        
        
    }
    }
    
    
}