/****************************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 26/06/2018
* Description: Send an Email Alert to HR on Rejecting Scheduled job
* Test Class: EmailTemplatesTest
****************************************************************************/
public with sharing class EmailToHROnRejectingScheduleController {
    public String objType {get;set;}
    public ID relatedId {get;set;}
    public String startDateTime {get;set;}
    public String endDateTime {get;set;}
    /*******************************************************************************************
Created by      : Pushmitha
Overall Purpose : Fetch Interview Panel Member Details
InputParams     : NA
OutputParams    : List of Interview Panel Members
*******************************************************************************************/
    public List<Nrich__Interview_Panel_Member__c> getPanelList (){
        try{
        List<Nrich__Interview_Panel_Member__c> InterviewPanelList = new List<Nrich__Interview_Panel_Member__c>();
        if(InterviewPanelList != null && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Panel_Member__c', 'Nrich__Description__c')){    
            InterviewPanelList = [select Id, Nrich__Interview_Round__r.Nrich__Interview_Start_Time__c, Nrich__Interview_Round__r.Nrich__Interview_End_Time__c, Nrich__Interview_Round__r.Nrich__Job_Application__r.Nrich__Candidate__r.Name, Nrich__Interview_Round__r.Name, Name, Nrich__Description__c from Nrich__Interview_Panel_Member__c where id = : relatedId ];
        }
        for(Nrich__Interview_Panel_Member__c inc : InterviewPanelList){
            startDateTime = inc.Nrich__Interview_Round__r.Nrich__Interview_Start_Time__c.format();
            endDateTime = inc.Nrich__Interview_Round__r.Nrich__Interview_End_Time__c.format();
        }
        return InterviewPanelList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmailToHROnRejectingScheduleController.getPanelList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}