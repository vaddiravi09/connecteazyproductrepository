@isTest
public with sharing class TimeSheetEmailTemplateControllerTest {
    
    @isTest
    public static void myTest(){
        Test.StartTest();
        Id relatedId;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 
        User u = new User(Alias = 'sta12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com1234');
        
        Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                         
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        insert lleave;
        cleave.Nrich__Is_Active__c=True;
        update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        
        
        Nrich__Employee__c e = new Nrich__Employee__c();
        e.Name = 'New Employee';
        e.Nrich__EmployeeDesignation__c = 'Developer';
       
        // e.BasEnrich__Related_User__r.ManagerId=u.Id;
        insert e;
        
        Nrich__Project__c project = new Nrich__Project__c();
        project.Name = 'Project1';
        project.Nrich__Project_Start_Date__c =System.today();
        project.Nrich__Project_End_Date__c =System.today()+7;
        insert project;
        
        
        
        list<Nrich__Approval__c>TApprovalList=new list<Nrich__Approval__c>();
        Nrich__Approval__c ta = new Nrich__Approval__c();
        ta.Name = 'Approval 1';
        ta.Nrich__Employeee__c = e.id;
        ta.Nrich__Start_Date__c=System.today();
        ta.Nrich__End_Date__c=System.today()+4;
        ta.Nrich__ProjectId__c=project.Id;
        //ta.TimEnrich__End_Date__c = Date.newInstance(2019,12,31);
        ta.Nrich__ManagerId__c =u.Id;
        ta.Nrich__Status__c ='Approved';
        //ta.Project_Name__c='Project1';
        TApprovalList.add(ta);
        // insert TApprovalList;
        //  system.debug('TApprovalList' +TApprovalList[0].Id);
        insert ta;
        
        
        Nrich__Timesheet__c t = new Nrich__Timesheet__c();
        t.Name ='Timesheet';
        t.Nrich__Date__c=System.today()+2;
        insert t;
        
        list<Nrich__TimeSheet_Detail__c >TimeSheetDetailList=new list<Nrich__TimeSheet_Detail__c>();
        Nrich__TimeSheet_Detail__c  td = new Nrich__TimeSheet_Detail__c();
        td.Nrich__Timesheet__c = t.Id;
        td.Nrich__Project__c = ta.Nrich__ProjectId__c;
        
        TimeSheetDetailList.add(td);
        // insert TimeSheetDetailList;
        insert td;
        
        
        timesheetemailtemplatecontroller.wrapperclass wc = new timesheetemailtemplatecontroller.wrapperclass();
        wc.StDate=ta.Nrich__Start_Date__c;
        wc.EdDate=ta.Nrich__End_Date__c;
        
        
        
        
      /*  Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(ta.id);
        req1.setProcessDefinitionNameOrId('Approval_Process_for_Approval');
        req1.setSkipEntryCriteria(true);
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        Approval.ProcessResult result = Approval.process(req1);*/
        
        
        
        
        timesheetemailtemplatecontroller testAccPlan = new timesheetemailtemplatecontroller();
        testAccPlan.relatedId=ta.Id;
        testAccPlan.objType='Nrich__Approval__c';
        testAccPlan.ApprovalDetailList=TApprovalList;
        testAccPlan.Timesheetdetailslist=TimeSheetDetailList;
        testAccPlan.getdetailslist();
        
        
        timesheetRejectemailtemplatecontroller timesheetreject=new timesheetRejectemailtemplatecontroller();
        timesheetreject.objType='Nrich__Approval__c';
        timesheetreject.relatedId=ta.Id;
        timesheetreject.ApprovalDetailList=TApprovalList;
        timesheetreject.Timesheetdetailslist=TimeSheetDetailList;
        timesheetreject.getdetailslist();
        
        System.assertEquals('Timesheet', t.Name );
        
        Test.StopTest();
    }
    
    
    
    
}