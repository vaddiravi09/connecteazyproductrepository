public class TalentSearchClass {
    @AuraEnabled
    Public static List<Nrich__Employee__c> searchEmp(Nrich__Employee__c search,Id Dept, Integer minExp, Integer maxExp, String phone, String email){
        System.debug('search====:'+search);
        
        String skill = search.Nrich__Skills__c;
        System.debug('skill====:'+skill);
        List<Nrich__Employee__c> searchemployee = new List<Nrich__Employee__c>();
        List<Nrich__Employee__c> searchemp;
        System.debug('expr ===>  : + '+search.Nrich__Total_Experience__c);
        System.debug('MInexpr ===>  : + '+minExp );
        System.debug('Maxexpr ===>  : + '+maxExp);
        
        if(skill==''){
            if( minExp != null && maxExp!=null)
            {
                searchemp = [select id,Nrich__Employee_Id__c,Name,Nrich__Total_Experience__c,Nrich__Alternate_Email__c,Nrich__Primary_Email__c,Nrich__Alternate_Phone__c, 
                             Nrich__Primary_Phone__c,Nrich__Skills__c,Nrich__Employee_Type__c,Nrich__Department__r.Name,Nrich__Location__c from Nrich__Employee__c 
                             where (Name!=null and Name=:search.Name) 
                             or (Nrich__Employee_Id__c!=null and Nrich__Employee_Id__c=:search.Nrich__Employee_Id__c) 
                             or (Nrich__Employee_Type__c!=null and Nrich__Employee_Type__c=:search.Nrich__Employee_Type__c) 
                             or (Nrich__Department__c!=null and Nrich__Department__c=:Dept) 
                             or (Nrich__Location__c!=null and Nrich__Location__c=:search.Nrich__Location__c) 
                             or ((Nrich__Primary_Email__c!=null and Nrich__Primary_Email__c=:email) or (Nrich__Alternate_Email__c!=null and Nrich__Alternate_Email__c=:email)) 
                             or ((Nrich__Alternate_Phone__c!=null and Nrich__Alternate_Phone__c=:phone) or (Nrich__Primary_Phone__c!=null and Nrich__Primary_Phone__c=:phone)) 
                             or (Nrich__Total_Experience__c!=null and (Nrich__Total_Experience__c >=: minExp and Nrich__Total_Experience__c <=: maxExp))];
                
            }
            else
            {
                searchemp = [select id,Nrich__Employee_Id__c,Name,Nrich__Total_Experience__c,Nrich__Alternate_Email__c,Nrich__Primary_Email__c,Nrich__Alternate_Phone__c, 
                             Nrich__Primary_Phone__c,Nrich__Skills__c,Nrich__Employee_Type__c,Nrich__Department__r.Name,Nrich__Location__c from Nrich__Employee__c 
                             where (Name!=null and Name=:search.Name) 
                             or (Nrich__Employee_Id__c!=null and Nrich__Employee_Id__c=:search.Nrich__Employee_Id__c) 
                             or (Nrich__Employee_Type__c!=null and Nrich__Employee_Type__c=:search.Nrich__Employee_Type__c) 
                             or (Nrich__Department__c!=null and Nrich__Department__c=:Dept) 
                             or (Nrich__Location__c!=null and Nrich__Location__c=:search.Nrich__Location__c) 
                             or ((Nrich__Primary_Email__c!=null and Nrich__Primary_Email__c=:email) or (Nrich__Alternate_Email__c!=null and Nrich__Alternate_Email__c=:email)) 
                             or ((Nrich__Alternate_Phone__c!=null and Nrich__Alternate_Phone__c=:phone) or (Nrich__Primary_Phone__c!=null and Nrich__Primary_Phone__c=:phone)) 
                            ];
                
            }
            System.debug('searchemp:==='+searchemp);
        }else{
            searchemp = [select id,Nrich__Employee_Id__c,Name,Nrich__Total_Experience__c,Nrich__Alternate_Email__c,Nrich__Primary_Email__c,Nrich__Alternate_Phone__c, 
                         Nrich__Primary_Phone__c,Nrich__Skills__c,Nrich__Employee_Type__c,Nrich__Department__r.Name,Nrich__Location__c from Nrich__Employee__c];
            
        }
        
        for(Nrich__Employee__c emp : searchemp) {
            System.debug('emp.Nrich__Skills__c:==='+emp.Nrich__Skills__c);
            if(skill!='' && (emp.Nrich__Skills__c !=null && (emp.Nrich__Skills__c.contains(skill)))) {
                searchemployee.add(emp);               
            }
        }
        if(skill!=''){
            System.debug('searchemployee====='+searchemployee);
            return searchemployee; 
        }else{
            System.debug('searchemp====='+searchemp);
            return searchemp;
        }
        
    } 
    
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord) {
        
        String searchKey = searchKeyWord + '%';
        system.debug('searchKey-->' + searchKey);
        List < sObject > returnList = new List < sObject > ();
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from Nrich__Department__c where Name LIKE: searchKey order by createdDate DESC limit 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
}