/********************************************************************
* Company: Absyz
* Developer: Ashish Nag
* Created Date: 30/06/2018
* Description: fetching Achievements List
********************************************************************/
public with sharing class EmailtoAchievementsReqController {
    
    public String objType {get;set;}
    public ID relatedId {get;set;}
    
    public list<Nrich__Achievements__c > AchievementsApplist{get;set;}
    
    public List<Nrich__Achievements__c> getAchievementsList (){
        try{
        list<Nrich__Achievements__c> AchievementsApplist=new list<Nrich__Achievements__c>();
        // if(relatedId != null){
        
        System.debug('RelatedId' +relatedId);
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Achievements__c', 'Nrich__Approval_Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Achievements__c', 'Nrich__Category__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Achievements__c', 'Nrich__Employeee__c')
           
          ) 
            AchievementsApplist=[select id,Name,Nrich__Approval_Status__c,Nrich__Category__c ,Nrich__Employeee__r.Name from Nrich__Achievements__c where id=:relatedId 
                                 ];
        
        system.debug('AchievementsApplist' +AchievementsApplist);
        // }
        return AchievementsApplist;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmailtoAchievementsReqController.getAchievementsList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}