@isTest
public class InitialSetupPageControllerTest {
    @isTest
    public static void testMethod1(){
        Boolean selectResult1 = FALSE;
        Boolean selectResult2 = TRUE;
        
        Nrich__InitialSetup__c is = new Nrich__InitialSetup__c();
        if(selectResult1 == True){
            is.Nrich__Timesheet_Type__c ='Monthly';
        }
        else if(selectResult1 == FALSE){
            is.Nrich__Timesheet_Type__c ='WEEKLY';
        }
        insert is;
        
        Test.startTest();
        //update is1;
        InitialSetUpPageController.savedetails(selectResult1,selectResult2,'Approved','Submitted','Holiday','Leave',20);
        
        Test.stopTest();
        
        System.assertEquals('test', 'test');
    }
    
    @isTest
    public static void testMethod2(){
        Boolean selectResult3 = TRUE;
        Boolean selectResult4 = FALSE;  
        
        Nrich__InitialSetup__c is = new Nrich__InitialSetup__c();
        if(selectResult3 == True){
            is.Nrich__Timesheet_Type__c ='Monthly';
        }
        else {
            is.Nrich__Timesheet_Type__c ='WEEKLY';
        }
        insert is;
        
        Test.startTest();
        InitialSetUpPageController.savedetails(selectResult3,selectResult4,'Approved','Submitted','Holiday','Leave',20);
        InitialSetUpPageController.GetDetails();
        Test.stopTest();
        System.assertEquals('test', 'test');
    }
    
    
    @isTest
    public static void EmpSetupsavetest(){
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser1' , Nrich__EmployeeDesignation__c ='Developer',
                                        
                                        Nrich__Total_Experience__c=11,
                                        Nrich__Related_User__c=u1.id );
        
        Nrich__InitialSetup__c Initial=new Nrich__InitialSetup__c();
        Initial.Nrich__Is_Achievements_Tile_Required__c=True;
        Initial.Nrich__Is_Certification_Tile_Required__c=True;
        insert Initial;
        
        InitialSetUpPageController.EmpSetupsaveDetails(True, True, True, True,True, True, True ,True  ,True  ,True , True, u1.Id,u1.Id, u1.Id,u1.Id ,TRUE ,TRUE,10,10,TRUE);
        System.assertEquals(True, Initial.Nrich__Is_Certification_Tile_Required__c);
    }
    
    @isTest
    public static void EmpSetupElse(){
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        
        Nrich__InitialSetup__c Initial=new Nrich__InitialSetup__c();
        Initial.Nrich__Is_Achievements_Tile_Required__c=True;
        Initial.Nrich__Is_Certification_Tile_Required__c=True;  
        string userid=u1.Id;
        InitialSetUpPageController.EmpSetupsaveDetails(True,True,True,True,True,True,True,True,True,True,True,u1.Id,u1.Id,u1.Id,u1.Id,True,True,10,10,TRUE);
        System.assertEquals(True, Initial.Nrich__Is_Achievements_Tile_Required__c);
        
    }
    
    @isTest
    public static void PrjInitialSetup(){
        Nrich__InitialSetup__c Initial=new Nrich__InitialSetup__c();
        Initial.Nrich__Cancelled_Color__c='#322';
        Initial.Nrich__Completed_Color__c='#312';
        Initial.Nrich__Inprogress_Color__c='#313';
        insert Initial; 
        
        InitialSetUpPageController.PrjInitialSetupSavedetails('#317','#316','#315','#314','#316','#112','#221','#311','#211','#312');
        System.assertEquals('#313',  Initial.Nrich__Inprogress_Color__c);
    }
    
    @isTest
    public static void RecruitmentInitialSetup(){
        InitialSetup__c Initial=new InitialSetup__c();
        Initial.Nrich__No_of_Days_for_Reapplication_for_SameJob__c=10;
       // Initial.Nrich__Email_Panel_Member_on_Interview_Schedule__c=True;
        Initial.Nrich__Feedback_for_all_previous_rounds__c=True;
        insert Initial;
        
        Initial.Nrich__Feedback_for_all_previous_rounds__c=False;
        update Initial;
        
        InitialSetUpPageController.saveRecruitmentDetails(12,True, False, True, True , True , False );
        System.assertEquals(False, Initial.Nrich__Feedback_for_all_previous_rounds__c);
        
    }
    
    @isTest
    public static void RecruitmentIfSetup(){
        Nrich__InitialSetup__c Initial=new Nrich__InitialSetup__c();
        Initial.Nrich__No_of_Days_for_Reapplication_for_SameJob__c=10;
        //Initial.Nrich__Email_Panel_Member_on_Interview_Schedule__c=True;
        Initial.Nrich__Feedback_for_all_previous_rounds__c=True;
        
        InitialSetUpPageController.saveRecruitmentDetails(2,True, False, True, True , False , False );
        System.assertEquals(True,Initial.Nrich__Feedback_for_all_previous_rounds__c);
        

        
    }
    
    
}