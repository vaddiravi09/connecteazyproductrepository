/****************************************************************************************************
* Company: Absyz
* Developer: Ashish Nag
* Created Date: 22/10/2018
* Description: Fetching Details of Timesheet
*****************************************************************************************************/
public class TimeSheetTableDisplayController {
    
    @AuraEnabled
    public static list<Nrich__Timesheet_Detail__c>ViewTimeSheetDetails(){
        
        try{
            list<Nrich__Timesheet_Detail__c> TimeSheetDetails=new list<Nrich__Timesheet_Detail__c>();
            string empUserId = userinfo.getUserId();
            
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__CopyTask__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Activity__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Comments__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Description__c')
               && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Project_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Employee_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__No_of_hours__c')
               && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Project__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__TimeSheet_Detail__c', 'Nrich__Project_Category__c')
              ) 
                TimeSheetDetails=[select Nrich__Activity__c,Nrich__Status__c ,Nrich__Employee_Name__c,Nrich__Timesheet__r.Name,Nrich__Timesheet__r.Nrich__Employeee__r.Name,
                                  Nrich__Timesheet__r.Nrich__Date__c,Nrich__Timesheet__r.Nrich__TotalHours__c from Nrich__Timesheet_Detail__c
                                  where Nrich__Timesheet__r.Nrich__Employeee__r.Nrich__Related_User__c =:empUserId
                                  
                                 ];
            System.debug('TimeSheetDetails' +TimeSheetDetails);
            if(TimeSheetDetails.size()>0 && TimeSheetDetails!=NULL )
                return TimeSheetDetails;
            else
                return null;
            
        }
        catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='TimeSheetTableDisplayController.ViewTimeSheetDetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
            return null;
        }
        
        
        
    }
    
}