public class LeaveCardCreation {
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Thadeus Ronn Thomas
* Created Date: 16/10/2018
* Description: Create Leave cards on Employee insertion
*****************************************************************************************************/   
    
    public static void LeaveCardonCreationEmployee(map<id,string> Emplist)
    {
        try{
      set<id> keyset=Emplist.keySet();
     list<Nrich__Employee_Leave_Card__c> leavecardlist= new list<Nrich__Employee_Leave_Card__c>();
     list<Nrich__Company_Leave_Structure__c> cls= new list<Nrich__Company_Leave_Structure__c>();
     if(ObjectFieldAccessCheck.checkAccessible('Nrich__Company_Leave_Structure__c', 'Nrich__Applicable_From__c') &&  ObjectFieldAccessCheck.checkAccessible('Nrich__Company_Leave_Structure__c', 'Nrich__Applicable_Till__c')
        && ObjectFieldAccessCheck.checkAccessible('Nrich__Company_Leave_Structure__c', 'Nrich__Is_Active__c')
       )
         cls= [select Nrich__Applicable_From__c,Nrich__Applicable_Till__c, Name from Nrich__Company_Leave_Structure__c where Nrich__Is_Active__c= true limit 1];
     Nrich__Employee_Leave_Card__c elc;
     if(cls.size()>0){
         for(string ids:keyset){
             elc = new Nrich__Employee_Leave_Card__c();
             elc.Name=Emplist.get(ids);
             elc.Nrich__Employee__c=ids;
             elc.Nrich__Applicable_From__c=cls[0].Nrich__Applicable_From__c;
             elc.Nrich__Applicable_Till__c=cls[0].Nrich__Applicable_Till__c;
             elc.Nrich__Is_Active__c=True;
             elc.Nrich__Leave_Structure__c=cls[0].Id;
             leavecardlist.add(elc);
         }
         Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Employee_Leave_Card__c.getSObjectType().getDescribe();
         if(objectDescriptioncontent.isCreateable() && leavecardlist.size()>0)
             database.insert(leavecardlist);
         // insert leavecardlist;
         LeaveCardCreation.LCCategoryCardCreation(leavecardlist,cls[0].id);
     }
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='LeaveCardCreation.LeaveCardonCreationEmployee', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Raviteja
* Created Date: 16/10/2018
* Description: Create Employee Leave category based on Employee, Leave card and Leave category
*****************************************************************************************************/   
    
    
    public static void LCCategoryCardCreation(list<Nrich__Employee_Leave_Card__c> LClist,id Comp_l_Struct){
        try{
        list<Nrich__Leave_Category__c> lcatlist= new list<Nrich__Leave_Category__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__Default__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__Leave_category_code__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__LossofPay__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Category__c', 'Nrich__Applicable_Leave_Structure__c') ) 
            lcatlist =[select name,Nrich__Default__c,Nrich__Leave_category_code__c,Nrich__LossofPay__c from Nrich__Leave_Category__c where Nrich__Applicable_Leave_Structure__c=:Comp_l_Struct];
        list<Nrich__Employee_Leave_Category__c> ELClistInsert =new list<Nrich__Employee_Leave_Category__c>();
        
        integer i=0,j=0,k=0;     
        integer compsize = lcatlist.size()*LClist.size();
        for(i=0;i<compsize;++i){            
            Nrich__Employee_Leave_Card__c elcard=new Nrich__Employee_Leave_Card__c();
            j=(integer)math.floor(i/lcatlist.size());           
            elcard=LClist[j];
            system.debug('testid '+elcard.Id);
            Nrich__Employee_Leave_Category__c elcc = new Nrich__Employee_Leave_Category__c();   
            elcc.Nrich__Leave_Card__c=elcard.Id;
            if(k>=lcatlist.size()){
                k=0;
            }
            elcc.Name=lcatlist[k].Name;
            elcc.Nrich__Loss_of_Pay__c = lcatlist[k].Nrich__LossofPay__c;
            elcc.Nrich__Leave_category_code__c=lcatlist[k].Nrich__Leave_category_code__c;
            if(lcatlist[k].Nrich__Default__c!=NULL)
                elcc.Nrich__Leaves_Obtained__c=lcatlist[k].Nrich__Default__c;
            k=k+1;
            ELClistInsert.add(elcc);
        }
        Schema.DescribeSObjectResult objectcontent = Nrich__Employee_Leave_Category__c.getSObjectType().getDescribe();
        Map<String, Schema.SObjectField> maps = Schema.SObjectType.Nrich__Employee_Leave_Category__c.fields.getMap();
        for(String fieldName : maps.keySet()) {
            if(maps.get(fieldName).getDescribe().isCreateable() && objectcontent.isCreateable() && ELClistInsert.size()>0) {
                // custom1.put(fieldName , 'some value');
                database.insert(ELClistInsert);
            }
        }
        /* if(objectcontent.isCreateable() && ELClistInsert.size()>0){
//   insert ELClistInsert;
database.insert(ELClistInsert);
}*/
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='LeaveCardCreation.LCCategoryCardCreation', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
}