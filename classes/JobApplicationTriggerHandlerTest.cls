@isTest
public class JobApplicationTriggerHandlerTest {
    public static testMethod void totalOfStatus(){
        
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        //Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Candidate__c Candidate =new Nrich__Candidate__c();
        Candidate.Name='testCandidate';
        Candidate.Nrich__Email__c='testabc@gmail.com';
        ///Candidate.Candidate_Last_Name__c='CandidateLastName1';
        insert Candidate;
        
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility1(Candidate.Id,job.Id);
        Nrich__job_Application__c jobAppl1 = UtilityForTest.job_Application_Utility2(Candidate.Id,job.Id);
        Nrich__job_Application__c jobAppl2 = UtilityForTest.job_Application_Utility3(Candidate.Id,job.Id);
        
        jobAppl2.Nrich__Application_Status__c = System.Label.Application_Status_to_Eligible;
        update jobAppl2;
        jobAppl2.Nrich__Application_Status__c = System.Label.JobApplicationInProgress;
        update jobAppl2;
        jobAppl2.Nrich__Application_Status__c = System.Label.JobApplicationRejected;
        //update jobAppl2;
        
        jobAppl2.Nrich__Reason_for_Offer_Rejection__c = 'Others';
        update jobAppl2;
        
        delete jobAppl;
        undelete jobAppl;
        List<Nrich__job_Application__c> jobApplList= [select id from Nrich__job_Application__c]; 
        system.assertEquals(3,jobApplList.size());
        
        test.stopTest();
        
    }
    
    public static testMethod void totalOfStatus1(){
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        //job_Application__c jobAppl = UtilityForTest.job_Application_Utility1(cand.Id,job.Id);
        Nrich__job_Application__c jobAppl1 = UtilityForTest.job_Application_Utility2(cand.Id,job.Id);
        //job_Application__c jobAppl2 = UtilityForTest.job_Application_Utility3(cand.Id,job.Id);
        jobAppl1.Name ='Test1';
        jobAppl1.Nrich__Application_Status__c = System.Label.Application_Status_to_Eligible;
        update jobAppl1;
        jobAppl1.Nrich__Application_Status__c = System.Label.JobApplicationInProgress;
        update jobAppl1;
        jobAppl1.Nrich__Application_Status__c = System.Label.JobApplicationInterviewCompleted;
        update jobAppl1;
        jobAppl1.Nrich__Joining_Date__c = system.today();
        jobAppl1.Nrich__Application_Status__c = System.Label.JobApplicationOfferRolledOut;
        update jobAppl1;
        jobAppl1.Nrich__Application_Status__c = System.Label.JobApplicationOfferAccepted;
        update jobAppl1;
        jobAppl1.Nrich__Application_Status__c = System.Label.JobApplicationPre_JoiningInProgress;
        update jobAppl1;
        jobAppl1.Nrich__Application_Status__c = System.Label.JobApplicationPre_JoiningCompleted;
        update jobAppl1;
        jobAppl1.Nrich__Background_Check__c = true;
        jobAppl1.Nrich__Application_Status__c = System.Label.JobApplicationOnboardCompleted;
        update jobAppl1;
        
        List<job_Application__c> jobApplList= [select id from Nrich__job_Application__c]; 
        system.assertEquals(1,jobApplList.size());
        
        test.stopTest();
        
    }
    
    public static testMethod void totalOfStatus2(){
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility1(cand.Id,job.Id);
        Nrich__job_Application__c jobAppl1 = UtilityForTest.job_Application_Utility2(cand.Id,job.Id);
        Nrich__job_Application__c jobAppl2 = UtilityForTest.job_Application_Utility3(cand.Id,job.Id);
        test.stopTest();
        
        delete jobAppl2;
        List<Nrich__job_Application__c> jobApplList= [select id from Nrich__job_Application__c]; 
        system.assertEquals(2,jobApplList.size());
        
        delete [select Id from Nrich__job_Application__c];
        List<Nrich__job_Application__c> jobApplList1= [select id from Nrich__job_Application__c]; 
        System.assertEquals(0,jobApplList1.size());
        
        undelete [select Id from Nrich__job_Application__c where Id IN :jobApplList ALL ROWS];
        List<Nrich__job_Application__c> jobApplList2= [select id from Nrich__job_Application__c]; 
        System.assertEquals(2,jobApplList2.size());
       
    }
    
     
}