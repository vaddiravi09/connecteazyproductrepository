@isTest
public class PTMEmailTemplateController_Test {
    
    @isTest public static void prjteamlist(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        System.runAs(u){
            
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
           // insert lleave;
            cleave.Nrich__Is_Active__c=True;
          //  update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            
            
            /*ist<Employee_Leave_Card__c> leavecardlist= new list<Employee_Leave_Card__c>();
Company_Leave_Structure__c cls= [select Applicable_From__c,Applicable_Till__c, Name from Company_Leave_Structure__c where Is_Active__c= true limit 1];
Employee_Leave_Card__c elc;
for(string ids:keyset){
elc = new Employee_Leave_Card__c();
elc.Name=Emplist.get(ids);
elc.Employee__c=ids;
elc.Applicable_From__c=cls.Applicable_From__c;
elc.Applicable_Till__c=cls.Applicable_Till__c;
elc.Is_Active__c=True;
elc.Leave_Structure__c=cls.Name;
leavecardlist.add(elc);
}
insert leavecardlist;*/
            
            
            
            
            
            Nrich__Project__c  Prj=new Nrich__Project__c();
            Prj.Name='TestPrj';
            Prj.Nrich__Project_Start_Date__c=System.today();
            Prj.Nrich__Project_End_Date__c=System.today()+7;
            Prj.Nrich__ProjectManagerr__c=emp.id;
            Prj.Nrich__Status__c='Proposed';
            insert prj;
            
            list<Nrich__Project_Team_Member__c>prjteamlist=new list<Nrich__Project_Team_Member__c>();
            Nrich__Project_Team_Member__c  prjteam=new Nrich__Project_Team_Member__c();
            prjteam.Nrich__Employeee__c=emp.Id;
            prjteam.Nrich__Employee_Start_Date__c=System.today();
            prjteam.Nrich__Employee_End_Date__c=System.today()+2;
            prjteam.Nrich__Project__c=prj.Id;
            insert prjteam;
            prjteamlist.add(prjteam);
            
            
            
            PTMEmailTemplateController ptmemail=new PTMEmailTemplateController();
            ptmemail.objType='Nrich__Project_Team_Member__c';
            ptmemail.ProjectTeamMemberAppList=prjteamlist;
            ptmemail.relatedId=prjteam.id;
            ptmemail.getdetailslist();
            
            System.assertEquals('Proposed', Prj.Nrich__Status__c);   
            
        }
        
        
    }
}