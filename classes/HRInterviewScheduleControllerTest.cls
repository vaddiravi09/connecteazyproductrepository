@isTest
public with sharing class HRInterviewScheduleControllerTest {
    private static testmethod void testFetchDetails() {
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        HRInterviewScheduleController.applicantdetails appDetail = new HRInterviewScheduleController.applicantdetails();  
        test.stopTest();
        system.assertEquals('Test','Test');
        appDetail = HRInterviewScheduleController.fetchApplicantDetails(irRecord.Id);
    }
    
    private static testmethod void testfetchPanelMembers() {
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        HRInterviewScheduleController.employeelist emplList = new HRInterviewScheduleController.employeelist();
        Nrich__Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        test.stopTest();
        system.assertEquals('Test','Test');
        HRInterviewScheduleController.fetchPanelMembers(irRecord.Id);
    }
    
    private static testmethod void testfetchEmployeeDetails() {
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        HRInterviewScheduleController.employeelist emplList = new HRInterviewScheduleController.employeelist();
        // Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        
        system.assertEquals('Test','Test');
        HRInterviewScheduleController.fetchEmployeeDetails('Test',irRecord.Id,system.today(),system.today()+1,7);
        test.stopTest();
    }
    
    private static testmethod void testfetchEmployeeDetails1() {
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        irRecord.Interview_Start_Time__c = System.today();
        irRecord.Interview_End_Time__c = System.today()+1;
        update irRecord;
        HRInterviewScheduleController.employeelist emplList = new HRInterviewScheduleController.employeelist();
        Nrich__Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        Nrich__Preffered__c pref = UtilityForTest.Preffered_Utility(emp.Id);
        test.stopTest();
        system.assertEquals('Test','Test');
        HRInterviewScheduleController.fetchEmployeeDetails('Test',irRecord.Id,NULL,NULL,1);
    }
    
     private static testmethod void testfetchEmployeeDetails2() {
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Employee__c testemployee = new Nrich__Employee__c();
        testemployee.Name='Test Employee1';
        testemployee.Nrich__Interview_Level__c='1';
        testemployee.Nrich__Gender__c='Male';
        testemployee.Nrich__Primary_Email__c ='abstest@gmail.com';
        testemployee.Nrich__Eligible_to_take_Interviews__c = true;
        insert testemployee;
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        irRecord.Nrich__Interview_Start_Time__c = System.today();
        irRecord.Nrich__Interview_End_Time__c = System.today()+1;
        update irRecord;
        HRInterviewScheduleController.employeelist emplList = new HRInterviewScheduleController.employeelist();
        Nrich__Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        Nrich__Preffered__c pref = UtilityForTest.Preffered_Utility(testemployee.Id);
        test.stopTest();
        system.assertEquals('Test','Test');
        HRInterviewScheduleController.fetchEmployeeDetails('Test',irRecord.Id,NULL,NULL,1);
    }
    
    private static testmethod void testschedule() {
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        List<Nrich__Employee__c> empList = new List<Nrich__Employee__c>();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        empList.add(emp);
        String emplStr = JSON.serialize(empList);
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
        Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        irRecord.Nrich__Interview_Start_Time__c = System.today();
        irRecord.Nrich__Interview_End_Time__c = System.today()+1;
        update irRecord;
        HRInterviewScheduleController.employeelist emplList = new HRInterviewScheduleController.employeelist();
        Nrich__Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        //Preffered__c pref = UtilityForTest.Preffered_Utility(testemployee.Id);
        test.stopTest();
        system.assertEquals('Hr Interview','Hr Interview');
        HRInterviewScheduleController.schedule(emplStr,irRecord.Id,System.today(),System.today()+1,true);
    }

}