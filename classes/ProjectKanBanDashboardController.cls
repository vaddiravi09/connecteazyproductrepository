/****************************************************************************************************
* Company:Absyz
* Developer:Ashish Nag
* Created Date:29/10/18
* Description:Fetching details for KanBanDashboard
*****************************************************************************************************/
public with sharing class ProjectKanBanDashboardController {
    
    @AuraEnabled
    public static List <String> getStatusValues(sObject objObject, string field) {
        try{
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + field);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(field).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        
        return allOpts;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ProjectKanBanDashboardController.getStatusValues', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    @AuraEnabled
    Public static list<projectkanbanWrap> getStatusdetails(String ProjectID, String ResourceId, String statusType, String Priority,list<string>IdsList) {
        try{
        list<projectkanbanWrap> projectkanbanWrapList = new list<projectkanbanWrap>();
        if(String.isNotBlank(ProjectID) && String.isNotBlank(statusType)){          
            projectkanbanWrapList.addAll(statusHelper(ProjectID, ResourceId, statusType, Priority,IdsList)); 
            return projectkanbanWrapList;
        }
        else
            return null;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ProjectKanBanDashboardController.getStatusdetails', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    public static list<projectkanbanWrap> statusHelper(String ProjectID, String ResourceId, String statusType, String Priority, list<string>IdsList){   
        System.debug('Ids' +IdsList.size()+ ProjectID+ statusType);
        try{
        String objectName;
        String query;
        String statusFieldName;
        map<String, list<Sobject>> statusTaskMap = new map<String, list<Sobject>>();
        if(statusType==Label.Task || statusType==Label.Bug){
            objectName = 'Nrich__Task__c' ;
            statusFieldName = 'Nrich__Status__c';
            /* if(IdsList.size()>0){
query = 'Select Id, Subject__c, Name, Priority__c, Estimated_Effort__c, Assigned_To_Name__c , Status__c from Task__c where Project__c=:ProjectID AND User_Story__c IN:IdsList';
}
else
{
query = 'Select Id, Subject__c, Name, Priority__c, Estimated_Effort__c, Assigned_To_Name__c , Status__c from Task__c where Project__c=:ProjectID';
}*/
            
           if(IdsList.size()>0 && ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__Subject__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__Priority__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__Estimated_Effort__c')
               && ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__Assigned_To_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Task__c', 'Nrich__Status__c')
              ){
                  if(statusType==Label.Task){
                      query = 'Select Id, Nrich__Subject__c, Name, Nrich__Priority__c, Nrich__Estimated_Effort__c , Nrich__Assigned_To_Name__c , Nrich__Status__c from Nrich__Task__c where Nrich__Project__c=:ProjectID AND Nrich__User_Story__c IN:IdsList AND RecordType.Name=\'Task\'';
                  }
                  else if(statusType==Label.Bug)
                  {
                      query = 'Select Id, Nrich__Subject__c, Name, Nrich__Priority__c, Nrich__Estimated_Effort__c , Nrich__Assigned_To_Name__c , Nrich__Status__c from Nrich__Task__c where Nrich__Project__c=:ProjectID AND Nrich__User_Story__c IN:IdsList AND RecordType.Name=\'Bug\'';
                  }
             }
            else{
                if(statusType==Label.Task){
                    query = 'Select Id, Nrich__Subject__c, Name, Nrich__Priority__c, Nrich__Estimated_Effort__c , Nrich__Assigned_To_Name__c , Nrich__Status__c from Nrich__Task__c where Nrich__Project__c=:ProjectID AND RecordType.Name=\'Task\'';
                }
                else if(statusType==Label.Bug)
                {
                    query = 'Select Id, Nrich__Subject__c, Name, Nrich__Priority__c, Nrich__Estimated_Effort__c , Nrich__Assigned_To_Name__c , Nrich__Status__c from Nrich__Task__c where Nrich__Project__c=:ProjectID  AND RecordType.Name=\'Bug\'';
                }
            }
            
            
        }        
        else if(statusType==Label.User_Story  && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Subject__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Estimated_Effort__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Priority__c')
                && ObjectFieldAccessCheck.checkAccessible('Nrich__User_Story__c', 'Nrich__Project__c')
               ){
                   objectName = 'Nrich__User_Story__c';
                   statusFieldName = 'Nrich__Status__c';
                   if(IdsList.size()>0){
                       query = 'Select Id, Nrich__Subject__c, Name, Nrich__Estimated_Effort__c, Nrich__Priority__c, Nrich__Assigned_To_Name__c , Nrich__Status__c from Nrich__User_Story__c  where Nrich__Project__c=:ProjectID AND Nrich__Sprint__c IN:IdsList';
                   }
                   if(IdsList.size()<=0){
                       query = 'Select Id, Nrich__Subject__c, Name, Nrich__Estimated_Effort__c, Nrich__Priority__c, Nrich__Assigned_To_Name__c , Nrich__Status__c from Nrich__User_Story__c  where Nrich__Project__c=:ProjectID';
                   }
               }
        else if(statusType==Label.Sprint && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__AssignedTo_Name__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Status__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Start_Date__c') && 
                ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Total_Estimated_Effort__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__End_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Sprint__c', 'Nrich__Project__c')
               ){
                   objectName = 'Nrich__Sprint__c';
                   statusFieldName = 'Nrich__Status__c';
                   query = 'Select Id, Name, Nrich__AssignedTo_Name__c, Nrich__Status__c, Nrich__Start_Date__c, Nrich__Total_Estimated_Effort__c, Nrich__End_Date__c from Nrich__Sprint__c  where Nrich__Project__c=:ProjectID';
               }
        if(String.isNotBlank(ResourceId))
            
            query = query+' AND Nrich__Assigned_To__r.Nrich__Employeee__c=:ResourceId';
        if(String.isNotBlank(Priority) && !statusType.equalsIgnoreCase(Label.Sprint))
            query = query+' AND Nrich__Priority__c=:Priority';
        for(Sobject obj: database.query(query)){
            String status = String.valueOf(obj.get(statusFieldName));
            if(!statusTaskMap.containsKey(status)){
                statusTaskMap.put(status, new list<Sobject>{obj});
            }else{
                statusTaskMap.get(status).add(obj);
            }
        }
        
        Schema.SObjectType convertType = Schema.getGlobalDescribe().get(objectName);
        Sobject genericObject = convertType.newSObject();
        List <String> statusList = ProjectKanBanDashboardController.getStatusValues(genericObject, statusFieldName);
        
        list<projectkanbanWrap> projectkanbanWrapList = new list<projectkanbanWrap>();
        for(String status: statusList){
            if(statusTaskMap.containsKey(status)){
                projectkanbanWrap projectwrap = new projectkanbanWrap(statusTaskMap.get(status), status, objectName);
                projectkanbanWrapList.add(projectwrap);
            }else{
                projectkanbanWrap projectwrap = new projectkanbanWrap(null, status, objectName);
                projectkanbanWrapList.add(projectwrap);
            }
        }
        return projectkanbanWrapList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ProjectKanBanDashboardController.statusHelper', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    @AuraEnabled
    public static StatusUpdateResult UpdateStatus(String objectId, String Status, String statusType) {
        try{
            Id objId = objectId;
            Schema.SObjectType objType = objId.getSobjectType();
            SObject obj = objType.newSObject();
            if(String.isNotBlank(Status))
                obj.put('Nrich__Status__c',Status);
            
            obj.put('Id', objId);
            Database.SaveResult result = Database.update(obj, false);
            if(!result.isSuccess()){
                StatusUpdateResult statusresult = new StatusUpdateResult(FALSE, Label.Error, result.getErrors()[0].getMessage());
                return statusresult;
            }else{
                StatusUpdateResult statusresult = new StatusUpdateResult(TRUE, Label.Success, Label.Status_Changed_Successfully);
                return statusresult;
            }
        }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='ProjectKanBanDashboardController.UpdateStatus', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
    
    
    public class projectkanbanWrap {
        @AuraEnabled
        List < Sobject > objectRecords {
            get;
            set;
        }
        @AuraEnabled
        String Status {
            get;
            set;
        }
        
        @AuraEnabled
        String objectAPIName {
            get;
            set;
        }
        
        public projectkanbanWrap(List < Sobject > recs, String status, String apiname) {
            this.objectRecords = recs;
            this.Status = status;
            this.objectAPIName = apiname;
        }
    }
    
    public class StatusUpdateResult {
        @AuraEnabled
        public Boolean isSuccess				{ get; set; }
        
        @AuraEnabled
        public String title						{ get; set; }
        
        @AuraEnabled
        public String message					{ get; set; }
        
        public StatusUpdateResult( Boolean isSuccess, String title, String message ) {
            this.isSuccess  = isSuccess;
            this.title      = title;
            this.message    = message;
        }
    }
    
    
}