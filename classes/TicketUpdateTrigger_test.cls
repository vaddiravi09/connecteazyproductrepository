@isTest
public class TicketUpdateTrigger_test {
    
    @istest public static void updatetrigger(){
        
        Nrich__Project__c proj = new Nrich__Project__c();
        proj.name = 'abc';
        proj.Nrich__Project_Type__c = 'Paid Project';
        proj.Nrich__Status__c = 'Active';
        insert proj;
        
        Nrich__Milestone__c  milestonestest= new Nrich__Milestone__c(Name = 'Name854', Nrich__Status__c = 'New', Nrich__Project__c = proj.Id);
        Insert milestonestest;       
        
        Nrich__Project_Team_Member__c PTM = new Nrich__Project_Team_Member__c();
        PTM.Nrich__Project__c = proj.Id;
        PTM.Nrich__Project_Role__c='Senior Management';
        insert PTM;
        
        Nrich__Sprint__c sp1 = new Nrich__Sprint__c();
        sp1.name = 'vf page';
        sp1.Nrich__Project__c = proj.Id;
        sp1.Nrich__Milestone__c = milestonestest.id;
        sp1.Nrich__Status__c = Label.In_Progress;
        insert sp1;
        
        Nrich__Sprint__c sp2 = new Nrich__Sprint__c();
        sp2.name = 'vf page';
        sp2.Nrich__Project__c = proj.Id;
        sp2.Nrich__Milestone__c = milestonestest.id;
        sp2.Nrich__Status__c = Label.In_Progress;
        sp2.Nrich__Start_Date__c= Date.newInstance(2019, 05, 10);
        sp2.Nrich__End_Date__c=Date.newInstance(2019, 06, 10);
        
        insert sp2;
        
        Nrich__Sprint__c sp3 = new Nrich__Sprint__c();
        sp3.name = 'vf page';
        sp3.Nrich__Project__c = proj.Id;
        sp3.Nrich__Milestone__c = milestonestest.id;
        sp3.Nrich__Status__c = 'In Progress';
        sp3.Nrich__Start_Date__c= Date.newInstance(2019, 05, 10);
        sp3.Nrich__End_Date__c=Date.newInstance(2019, 06, 10);
        insert sp3;
        
        
        
        
        Nrich__User_Story__c userstr = new Nrich__User_Story__c(Name = 'Story 1', Nrich__Project__c = proj.Id, Nrich__Status__c = 'New');
        insert userstr;  
        
        List<Nrich__Task__c> lstofTask = new List<Nrich__Task__c>();
        Nrich__Task__c tsk = new Nrich__Task__c( Nrich__Project__c = proj.Id, Nrich__Status__c= 'New' ,Nrich__Assigned_To__c=PTM.id,Nrich__Actual_Effort__c=1,Nrich__Estimated_Effort__c=2
                                  ,Nrich__User_Story__c=userstr.id,Nrich__Sprint__c=sp2.id);
        insert tsk;
        
        
        
        tsk.Nrich__Status__c='Cancelled';
        update tsk;
        set<id>Sprintid=new set<id>();
        
        
        
        
        Nrich__Task__c tsk1 = new Nrich__Task__c( Nrich__Project__c = proj.Id, Nrich__Status__c= 'Completed' ,Nrich__Assigned_To__c=PTM.id
                                   ,Nrich__User_Story__c=null,Nrich__Sprint__c=sp1.id,Nrich__Actual_Effort__c=1,Nrich__Estimated_Effort__c=2);
        
        insert tsk1;
        tsk1.Nrich__Sprint__c=null;
        //Commented
        // update tsk1;
        tsk1.Nrich__Sprint__c=sp3.id;
        //Commented
        //update tsk1;
        
        Nrich__Task__c tsk2 = new Nrich__Task__c( Nrich__Project__c = proj.Id, Nrich__Status__c= 'Completed' ,Nrich__Assigned_To__c=PTM.id
                                   ,Nrich__User_Story__c=null,Nrich__Sprint__c=null);
        insert tsk2;
        tsk2.Nrich__User_Story__c=userstr.id;
        tsk2.Nrich__Sprint__c=sp2.id;
        // update tsk2;
        
        Nrich__Task__c tsk3 = new Nrich__Task__c( Nrich__Project__c = proj.Id, Nrich__Status__c= 'Completed' ,Nrich__Assigned_To__c=PTM.id
                                   ,Nrich__User_Story__c=userstr.id,Nrich__Sprint__c=null);  
        insert tsk3;
        tsk3.Nrich__User_Story__c=null;
        tsk3.Nrich__Sprint__c=sp3.id;
        //commented
        // update tsk3;
        
        
        Nrich__Task__c tsk4 = new Nrich__Task__c( Nrich__Project__c = proj.Id, Nrich__Status__c= 'New' ,Nrich__Assigned_To__c=PTM.id
                                   ,Nrich__User_Story__c=userstr.id); 
        insert tsk4;
        tsk4.Nrich__User_Story__c=null;
        tsk4.Nrich__Sprint__c=null;
        update tsk4;
        
        Nrich__Task__c tsk5 = new Nrich__Task__c( Nrich__Project__c = proj.Id, Nrich__Status__c= 'Completed' ,Nrich__Assigned_To__c=PTM.id
                                   ,Nrich__User_Story__c=null); 
        insert tsk5;
        tsk5.Nrich__User_Story__c=userstr.id;
        // update tsk5;
        
        
        Nrich__Task__c deltsk = new Nrich__Task__c( Nrich__Project__c = proj.Id,Nrich__Status__c= 'New',Nrich__User_Story__c=userstr.id,Nrich__Sprint__c=sp2.id);
        insert deltsk;
        
        delete deltsk;
        
        
        Nrich__Task__c deltsk1 = new Nrich__Task__c( Nrich__Project__c = proj.Id,Nrich__Status__c= 'New',Nrich__User_Story__c=null,Nrich__Sprint__c=sp3.id);
        insert deltsk1;
        
        delete deltsk1;
        
        Sprintid.add(tsk.Nrich__Sprint__c);
        TicketUpdateTriggerHandler.updatesprintstatus(Sprintid);
        System.assertEquals('Completed',tsk5.Nrich__Status__c);
        
    }
    
    @istest
    public static void triggerhandleprojectrollout(){
        Nrich__Project__c proj = new Nrich__Project__c();
        proj.name = 'abc';
        proj.Nrich__Project_Type__c = 'Paid Project';
        proj.Nrich__Status__c = 'Active';
        insert proj;
        
        Nrich__Milestone__c  milestonestest= new Nrich__Milestone__c(Name = 'Name854', Nrich__Status__c = 'New', Nrich__Project__c = proj.Id);
        Insert milestonestest;       
        
        Nrich__Project_Team_Member__c PTM = new Nrich__Project_Team_Member__c();
        PTM.Nrich__Project__c = proj.Id;
        PTM.Nrich__Project_Role__c='Senior Management';
        insert PTM;
        
        Nrich__Sprint__c sp1 = new Nrich__Sprint__c();
        sp1.name = 'vf page';
        sp1.Nrich__Project__c = proj.Id;
        sp1.Nrich__Milestone__c = milestonestest.id;
        sp1.Nrich__Status__c = Label.In_Progress;
        insert sp1;
        
        Nrich__Sprint__c sp2 = new Nrich__Sprint__c();
        sp2.name = 'vf page';
        sp2.Nrich__Project__c = proj.Id;
        sp2.Nrich__Milestone__c = milestonestest.id;
        sp2.Nrich__Status__c = Label.In_Progress;
        sp2.Nrich__Start_Date__c= Date.newInstance(2019, 05, 10);
        sp2.Nrich__End_Date__c=Date.newInstance(2019, 06, 10);
        
        insert sp2;
        
        Nrich__User_Story__c userstr = new Nrich__User_Story__c(Name = 'Story 1', Nrich__Project__c = proj.Id, Nrich__Status__c = 'New');
        insert userstr;  
        
        List<Nrich__Task__c> lstofTask = new List<Nrich__Task__c>();
        Nrich__Task__c tsk = new Nrich__Task__c( Nrich__Project__c = proj.Id, Nrich__Status__c= 'New' ,Nrich__Assigned_To__c=PTM.id,Nrich__Actual_Effort__c=1,Nrich__Estimated_Effort__c=2
                                  ,Nrich__User_Story__c=userstr.id,Nrich__Sprint__c=sp2.id);
        insert tsk;
        tsk.Nrich__Status__c='Cancelled';
        update tsk;
        
        set<id>prj=new set<id>();
        set<id>userid=new set<id>();
        
        prj.add(tsk.Nrich__Project__c);
        userid.add(tsk.Nrich__User_Story__c);
        
        map < id, Nrich__Task__c > mapTaskk=new map<id, Nrich__Task__c>();
        mapTaskk.put(tsk.id,tsk);
        TicketUpdateTriggerHandler.projectrollout(prj,mapTaskk);
        TicketUpdateTriggerHandler.onticketdelete(userid);
        
        
        Nrich__Task__c detsk = new Nrich__Task__c( Nrich__Project__c = proj.Id ,Nrich__Actual_Effort__c=1,Nrich__Estimated_Effort__c=2, Nrich__Status__c='New' 
                                    ,Nrich__User_Story__c=userstr.id,Nrich__Sprint__c=sp2.id);
        insert detsk;
        detsk.Nrich__Actual_Effort__c=0;
        update detsk;
        
        set<id>sprintid=new set<id>();
        sprintid.add(detsk.Nrich__Sprint__c);
        System.assertEquals(0, detsk.Nrich__Actual_Effort__c);
        //TicketUpdateTriggerHandler.statusupdateonSprintTicketDelete(sprintid);
        
    }
    
    @istest
    public static void Frtask(){
        
        Nrich__Project__c proj = new Nrich__Project__c();
        proj.name = 'abc';
        proj.Nrich__Project_Type__c = 'Paid Project';
        proj.Nrich__Status__c = 'Active';
        insert proj;
        
        Nrich__Sprint__c sp1 = new Nrich__Sprint__c();
        sp1.name = 'vf page';
        sp1.Nrich__Project__c = proj.Id;
        //sp1.Milestone__c = milestonestest.id;
        sp1.Nrich__Status__c = Label.In_Progress;
        insert sp1;
        
        //sp1.Name='TestSprint';
        // update sp1;
        
        set<id> sprintId=new set<id>();
        sprintId.add(sp1.Id);
        
        Nrich__Task__c tsk = new Nrich__Task__c( Nrich__Project__c = proj.Id, Nrich__Status__c= 'New' ,Nrich__Actual_Effort__c=1,Nrich__Estimated_Effort__c=2
                                  ,Nrich__User_Story__c=null,Nrich__Sprint__c=sp1.id);
        insert tsk;
        
        
        
        
        //User_Story__c userstr = new User_Story__c(Name = 'Story 1', Project__c = proj.Id, Status__c = 'New',);
        Nrich__User_Story__c Userstr=new Nrich__User_Story__c();
        Userstr.Name='Stry1';
        Userstr.Nrich__Project__c = proj.Id;
        Userstr.Nrich__Status__c='New';
        Userstr.Nrich__Sprint__c=sp1.Id;
        insert Userstr;  
        
        System.assertEquals('New', Userstr.Nrich__Status__c);
        TicketUpdateTriggerHandler.updatesprintstatus(sprintId);
        TicketUpdateTriggerHandler.statusupdateonSprintTicketDelete(sprintid);
        
        
        
    }
    
}