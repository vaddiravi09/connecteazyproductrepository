@isTest
public class CandidateTriggerHandlerTest {
    
    public static TestMethod void Method1(){
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        //Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Candidate__c Candidate =new Nrich__Candidate__c();
        Candidate.name='testCandidate';
        Candidate.Nrich__Email__c='testabc@gmail.com';
        //Candidate.Candidate_Last_Name__c='CandidateLastName1';
        insert Candidate;
        
        
        Candidate.Nrich__Candidate_Status__c='OnBoarded';
        update Candidate;
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        system.assertEquals('OnBoarded',[select id,Nrich__Candidate_Status__c from  Nrich__Candidate__c  where id=:Candidate.id].Nrich__Candidate_Status__c);
        Test.stopTest();
        
    }
}