@isTest
public class jobRoundTriggerHandlerTest {
    
    public static TestMethod void method1(){
        
        test.startTest();
        InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        //Employee__c emp = UtilityForTest.Employee_Utility();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
       Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                          
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
      //  insert lleave;
        cleave.Nrich__Is_Active__c=True;
      //  update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;  
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                        
                                        Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
        
        
        insert emp;
        
        
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__Job_Round__c jobround=UtilityForTest.Job_Round_Utility(job.id);
        Nrich__Job_Round__c jobround2=UtilityForTest.Job_Round_Utility1(job.id);
        Nrich__Job_Round__c jobround1=UtilityForTest.Job_Round_Utility2(job.id);
        test.stopTest();
        system.assertEquals('Jobnumber','Jobnumber');
    }
    public static TestMethod void method2(){
        
        test.startTest();
        try{
            Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
            Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
            Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
            Nrich__Department__c dept = UtilityForTest.Department_Utility();
            Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
            Nrich__Job__c job2 = UtilityForTest.Job_Utility(dept.Id);
            Nrich__Job_Round__c jobround=UtilityForTest.Job_Round_Utility(job.id);
            Nrich__Job_Round__c jobround1=UtilityForTest.Job_Round_Utility1(job.id);       
            Nrich__Job_Round__c jobround2=UtilityForTest.Job_Round_Utility1(job.id);           
            delete jobround1;
            update jobround2;
            test.stopTest();
        }catch(exception e){}
        system.assertEquals('Jobnumber2','Jobnumber2');
    }
    
    public static TestMethod void method3(){
        try{
            test.startTest();
            Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
            Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
            Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
            Nrich__Department__c dept = UtilityForTest.Department_Utility();
            Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
            Nrich__Job__c job2 = UtilityForTest.Job_Utility(dept.Id);
            Nrich__Job_Round__c jobround=UtilityForTest.Job_Round_Utility(job.id);
            jobRound.Nrich__Job_Round_Order_Number__c = 2;
            Nrich__Job_Round__c jobround1=UtilityForTest.Job_Round_Utility1(job.id);       
            Nrich__Job_Round__c jobround2=UtilityForTest.Job_Round_Utility1(job.id);           
            update jobround;
            //delete jobround1;
            test.stopTest();
        }catch(exception e){}
        system.assertEquals('Jobnumber2','Jobnumber2');
    }
    
    public static TestMethod void method4(){
        
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__Job_Round__c jobround=UtilityForTest.Job_Round_Utility(job.id);
        update jobround;
        test.stopTest();
        system.assertEquals('Jobnumber','Jobnumber');
    }
}