/********************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 30/06/2018
* Description: Email to Interview Panel Member.
* Test Class: EmailTemplatesTest
********************************************************************/
public with sharing class EmailCandidateScheduledOrRescheduled {
    public String objType {get;set;}
    public ID relatedId {get;set;}
    public String startDateTime {get;set;}
    public String endDateTime {get;set;}
    //public List<AB_HireBolt__Interview_Panel_Member__c> InterviewPanelList{get;set;}
    /*******************************************************************************************
Created by      : Pushmitha
Overall Purpose : Fetch Interview Panel Member Details
InputParams     : NA
OutputParams    : List of Interview Panel Members
*******************************************************************************************/
    public List<Nrich__Interview_Round__c> getPanelList (){
        try{
        List<Nrich__Interview_Round__c> InterviewPanelList = new List<Nrich__Interview_Round__c>();
        if(InterviewPanelList != null && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Interview_Mode__c' ) && ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Interview_Start_Time__c') && 
           ObjectFieldAccessCheck.checkAccessible('Nrich__Interview_Round__c', 'Nrich__Interview_End_Time__c')
           
          ){    
              InterviewPanelList = [select Id,Nrich__Interview_Mode__c, Nrich__Interview_Start_Time__c, Nrich__Interview_End_Time__c, Nrich__Job_Application__r.Nrich__Candidate__r.Name,Nrich__Job_Application__r.Name, Name from Nrich__Interview_Round__c where id = : relatedId ];
          }
        for(Nrich__Interview_Round__c inc : InterviewPanelList){
            startDateTime = inc.Nrich__Interview_Start_Time__c.format();
            endDateTime = inc.Nrich__Interview_End_Time__c.format();
        }
        return InterviewPanelList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmailCandidateScheduledOrRescheduled.getPanelList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}