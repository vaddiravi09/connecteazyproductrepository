public class EmployeeTrainingManagementHandler {
/****************************************************************************************************
* Company: Absyz
* Developer: Anivesh Muppa
* Created Date: 9/7/2018
* Description: Create Announcement records when the Training is Updated 
*****************************************************************************************************/
    Public static void createAnnouncementRcrd(Map<id,Employee_Training_Management__c > newTrainingrecords, Map<id,Employee_Training_Management__c > oldTrainingrecords){
      Set<Id> TrainingId = new Set<Id>(); 
      Map<id,Employee_Training_Management__c> filteredTraining = new Map<id,Employee_Training_Management__c>();
      try{
      for(Employee_Training_Management__c emp_training : newTrainingrecords.values()){        
          if( newTrainingrecords.get(emp_training.Id).Nrich__Is_Announcement_Required__c && 
                newTrainingrecords.get(emp_training.Id).Nrich__Status__c != oldTrainingrecords.get(emp_training.Id).Nrich__Status__c
                && newTrainingrecords.get(emp_training.Id).Nrich__Status__c!='New'){
                    
              filteredTraining.put(emp_training.Id,emp_training);                    
          }             
      }
      if(filteredTraining.size()>0)
          insertAnnouncements(filteredTraining);
      }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeTrainingManagementHandler.createAnnouncementRcrd', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    Public static void insertAnnouncements(Map<id,Employee_Training_Management__c> TrainingId){
        List<Nrich__Announcements__c> insertAnnounceRecords = new List<Nrich__Announcements__c>();
        map<string,Nrich__Announcements__c> mapAnnouncements = new map<string,Nrich__Announcements__c>();
        try{
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Announcements__c', 'Nrich__Training_Id__c') ){
                for(Nrich__Announcements__c announcement_rcd : [select Id, Nrich__Training_Id__c from Nrich__Announcements__c 
                                                                where Nrich__Training_Id__c IN:TrainingId.keyset()]){
                                                                    mapAnnouncements.put(announcement_rcd.Nrich__Training_Id__c,announcement_rcd);
                                                                }
            }
            for(id createannouce : TrainingId.keyset()){
                if(!mapAnnouncements.containsKey(createannouce)){
                    Nrich__Announcements__c announce = new Nrich__Announcements__c();
                    announce.Nrich__Start_Date__c = TrainingId.get(createannouce).Nrich__Start_Date__c;
                    announce.Nrich__End_Date__c = TrainingId.get(createannouce).Nrich__End_Date__c;
                    announce.Nrich__Title__c = TrainingId.get(createannouce).Name;
                    announce.Nrich__Training_Id__c = createannouce;
                    announce.Nrich__Type_of_Announcement__c = 'Public';
                    announce.Nrich__Publication_Status__c = 'Published';
                    announce.Nrich__Body__c = TrainingId.get(createannouce).Nrich__Trainer_Details__c;
                    insertAnnounceRecords.add(announce);
                }
            }
            if(insertAnnounceRecords.size()>0){
                Database.insert(insertAnnounceRecords);
            }
            
        }catch(exception e){
            Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
            if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
                Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmployeeTrainingManagementHandler.insertAnnouncements', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
                database.insert(errorlog, false);
            }
      }
        
    }
  
}