/********************************************************************
* Company: Absyz
* Developer: Anivesh
* Created Date: 16/03/2019
* Description: Email to Leave Request 
********************************************************************/
public with sharing class EmailtoLeaveReqController {
    public String objType {get;set;}
    public ID relatedId {get;set;}
    public list<Nrich__Leave_Request__c> LeaveAppList{get;set;}
    //public list<Leave_Request__c> LvList{get;set;}
    public List<Nrich__Leave_Request__c> getLeaveList(){
        try{
        List<Nrich__Leave_Request__c> LeaveAppList = new List<Nrich__Leave_Request__c>();
      
        if(relatedId != null){
            if(ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Start_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__End_Date__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Comments__c')
              && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__Approver_Name__c')  && ObjectFieldAccessCheck.checkAccessible('Nrich__Leave_Request__c', 'Nrich__No_of_working_days__c')
             )
            LeaveAppList=[select id,Nrich__Start_Date__c,Nrich__End_Date__c ,Nrich__Comments__c ,Nrich__Approver_Name__c,Nrich__Employee_Leave_Category__r.Name,Nrich__No_of_working_days__c ,Nrich__Requested_By__r.Name from Nrich__Leave_Request__c where id = : relatedId ];
            
        }
        return LeaveAppList;
        
    }
       catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmailtoLeaveReqController.getLeaveList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
           return LeaveAppList;
      } 
    }
    
}