@isTest
public class TestEmployeePolicyCreation_Batch {
    
    
    @isTest static void insertrecords(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
        System.runAs(u) {
            String policyId;
            // The following code runs as user 'u' 
            
            //insert cer;
            
            /* Company_Leave_Structure__c cleave=new Company_Leave_Structure__c(Applicable_From__c =system.today(),Applicable_Till__c=system.today(),Is_Active__c= true);
insert cleave;
Leave_Category__c lleave =new Leave_Category__c(Name='Test',Increment_Frequency__c=1,Increment_Step__c =2,
Will_Carry_Forward__c =False,
Will_Lapse__c =False,Applicable_Leave_Structure__c =cleave.id

);

insert lleave;*/
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
           // insert lleave;
            cleave.Nrich__Is_Active__c=True;
           // update cleave;
            
            /*Employee_Leave_Card__c EmpLeaveCard=new Employee_Leave_Card__c();
EmpLeaveCard.Is_Active__c=TRUE;
EmpLeaveCard.Name='Test';
insert EmpLeaveCard;*/
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Active__c=true,
                                            Nrich__Related_User__c=u.id );
            
            insert emp;
            
            Nrich__Certification__c certification=new Nrich__Certification__c(Name='Test');
            certification.Nrich__Cost__c=50;
            insert certification;
            Nrich__Certification_Request__c  cer =new Nrich__Certification_Request__c(Nrich__Certification_Name__c=certification.id);
            
            list<Nrich__Employee__c> emplist=new list<Nrich__Employee__c>();
            emplist.add(emp);
            
            Nrich__Policy__c  plcy=new Nrich__Policy__c ();
            plcy.Nrich__Policy_Applicable_From__c =System.today();
            plcy.Name='Abc';
            plcy.Nrich__Pulished_Date__c =System.today();
            insert plcy;
            list<string>PolicyIdList1=new list<string>();
            
            Nrich__EmployeePolicy__c emppolicy=new Nrich__EmployeePolicy__c();
            emppolicy.Nrich__Date_Issued__c = system.today();
            emppolicy.Nrich__Employeee__c = emp.Id;
            emppolicy.Nrich__Policy__c = plcy.id;
            emppolicy.Nrich__status__c ='Pending';
            
            insert emppolicy;
            PolicyIdList1.add(emppolicy.Id);
            
            System.assertEquals('Pending', emppolicy.Nrich__status__c);
            
            EmployeePolicyCreation_Batch obj = new EmployeePolicyCreation_Batch();
            obj.policyIdList=PolicyIdList1;
            DataBase.executeBatch(obj);
            
        }
    }
}