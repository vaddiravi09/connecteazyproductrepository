/****************************************************************************
* Company: Absyz
* Developer: Hrishikesh
* Created Date: 27/06/2018
* Description: Batch class for controlling sending Email to Employee 
*			   regarding interview slots 
* Test Class: SendEmailToPanelMember_BatchScheduleTest 
****************************************************************************/
global with sharing class SendEmailToPanelMember_Batch implements Database.Batchable<sObject>, Database.Stateful {
    
    Boolean strParameter;
    
    public SendEmailToPanelMember_Batch(Boolean Param) {
        strParameter = Param;
        system.debug('----------strParameter: '+strParameter);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        return Database.getQueryLocator([select id,Name,Nrich__Eligible_to_take_Interviews__c FROM Nrich__Employee__c where Nrich__Eligible_to_take_Interviews__c = true]);
    }
    
    global void execute(Database.BatchableContext bc, List<Nrich__Employee__c> empList){
        try{
        List<Nrich__Employee__c> empListUpdate = new List<Nrich__Employee__c>();
        system.debug('----------strParameter: '+strParameter);
        
        if(strParameter == false){
            for(Nrich__Employee__c emp : empList){
                If(Schema.sObjectType.Nrich__Employee__c.fields.Nrich__Tech_Email_for_Preffered_Slot__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Tech_Email_for_Preffered_Slot__c')){
                    emp.Nrich__Tech_Email_for_Preffered_Slot__c = true;
                }
                empListUpdate.add(emp);
            }  
        }
        if(strParameter == true){
            
            for(Nrich__Employee__c emp : empList){
                If(Schema.sObjectType.Nrich__Employee__c.fields.Nrich__Tech_Email_for_Preffered_Slot__c.isUpdateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__Employee__c', 'Nrich__Tech_Email_for_Preffered_Slot__c')){
                    emp.Nrich__Tech_Email_for_Preffered_Slot__c = false;
                }
                empListUpdate.add(emp);
            }   
        }
        
        Schema.DescribeSObjectResult objectDescription = Nrich__Employee__c.getSObjectType().getDescribe();
        if(objectDescription.isUpdateable() && empListUpdate.size()>0)
            database.update(empListUpdate);
        system.debug('empListUpdate -->'+empListUpdate);
        
    }catch(exception e){
        e.getLineNumber();
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='SendEmailToPanelMember_Batch', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
    }
    global void finish(Database.BatchableContext bc){
        Boolean b = true;
        system.debug('strParameter:'+strParameter);
        if(strParameter==false)
            Database.executeBatch(new SendEmailToPanelMember_Batch(b));
    }
    
    
}