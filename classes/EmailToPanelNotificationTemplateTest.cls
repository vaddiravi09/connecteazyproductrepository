@isTest
public  with sharing class EmailToPanelNotificationTemplateTest {

    public static TestMethod void getpanelListTest(){
        
        test.startTest();
        Nrich__InitialSetup__c initSetup =  UtilityForTest.InitialSetup_Utility();
        Nrich__Candidate__c cand = UtilityForTest.Candidate_Utility();
        Nrich__Employee__c emp = UtilityForTest.Employee_Utility();
        Nrich__Department__c dept = UtilityForTest.Department_Utility();
        Nrich__Job__c job = UtilityForTest.Job_Utility(dept.Id);
        Nrich__job_Application__c jobAppl = UtilityForTest.job_Application_Utility(cand.Id,job.Id);
    	Nrich__Interview_Round__c irRecord = UtilityForTest.Interview_Round_Utility(jobAppl.Id);
        HRInterviewScheduleController.employeelist emplList = new HRInterviewScheduleController.employeelist();
        Nrich__Interview_Panel_Member__c testPanelMember= new Nrich__Interview_Panel_Member__c();
        testPanelMember.Status__c='Accepted';
        testPanelMember.Employee__c=emp.Id;
        testPanelMember.Interview_Round__c=irRecord.Id;
        insert testPanelMember;
        Nrich__Interview_Panel_Member__c empPanelMemb = UtilityForTest.Interview_Panel_Member_Utility(emp.Id,irRecord.Id);
        emailToPanelNotificationTemplate instance =new emailToPanelNotificationTemplate();
        instance.ObjType='Interview_Panel_Member__c';
        instance.RelatedId=empPanelMemb.id;
        instance.getpanelList();
        test.stopTest();
        system.assertEquals('Test','Test');
    }
}