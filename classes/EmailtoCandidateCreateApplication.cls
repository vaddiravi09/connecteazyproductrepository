/********************************************************************
* Company: Absyz
* Developer: Pushmitha
* Created Date: 30/06/2018
* Description: Email Alert to Job Applicant.
* Test Class: EmailTemplatesTest
********************************************************************/
public with sharing class EmailtoCandidateCreateApplication {
    public String objType {get;set;}
    public ID relatedId {get;set;}
    
    /*******************************************************************************************
Created by      : Pushmitha
Overall Purpose : Fetch Job Application Details
InputParams     : NA
OutputParams    : List of Job Application
*******************************************************************************************/
    public List<Nrich__Job_Application__c> getJobApplicationList (){
        try{
        List<Nrich__Job_Application__c> jobAppList = new List<Nrich__Job_Application__c>();
        if(relatedId != null && ObjectFieldAccessCheck.checkAccessible('Nrich__Job_Application__c', 'Nrich__Application_Reference_No__c')){
            jobAppList = [select Id, Nrich__Candidate__r.Name, Nrich__Job__r.Name, Nrich__Application_Reference_No__c, Name from Nrich__Job_Application__c where id = : relatedId ];
        }
        return jobAppList;
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='EmailtoCandidateCreateApplication.getJobApplicationList', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
        return null;
      }
    }
}