@isTest
public class TestIssueNewLeaveStruct {
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Thadeus Ronn Thomas 
* Created Date: 02/11/2018
* Description: Test class covering TriggcomplStruct,BatchIssuenewcard,BatchRenewlegacylcard
*****************************************************************************************************/  
    
    @istest  static void test_updtlcateg(){
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u;
        User u1 = new User(Alias = 'at12356', Email='standardusers@testorg.com',EmailEncodingKey='UTF-8', LastName='Testings', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorgs.com123');
        insert u1;
        Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                          
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        insert lleave;
        cleave.Nrich__Is_Active__c=True;
        update cleave;
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id );
        insert emp;
        Nrich__Employee__c emp1=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Total_Experience__c=10,Nrich__Related_User__c=u1.id,Nrich__Date_of_Joining__c=date.parse('1/1/2017') );
        insert emp1;
        /*cleave.Is_Active__c=false;
        update cleave;*/
        /*Company_Leave_Structure__c cleaves=new Company_Leave_Structure__c(Applicable_From__c =date.parse('1/1/2019'),Applicable_Till__c=date.parse('12/31/2019'),Is_Active__c= false);
        insert cleaves;
        Leave_Category__c lleaves =new Leave_Category__c(Name='Test',Increment_Frequency__c=1,Increment_Step__c =2,Will_Carry_Forward__c =False,Will_Lapse__c =False,Applicable_Leave_Structure__c =cleaves.id,Lapse_Frequency__c=1);
        insert lleaves;
        Leave_Category__c lleaves1 =new Leave_Category__c(Name='Test',Increment_Frequency__c=1,Increment_Step__c =2,Will_Carry_Forward__c =False,Will_Lapse__c =False,Applicable_Leave_Structure__c =cleaves.id,Lapse_Frequency__c=1);
        insert lleaves1;
        cleaves.Is_Active__c=true;
        update cleaves;*/
        System.assertEquals(true, cleave.Nrich__Is_Active__c);
        Test.stopTest();
    }
    
}