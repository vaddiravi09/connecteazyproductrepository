public with sharing class HeaderComponentVF{
   public String documentId{
      get{
         string URLval =URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id=';
         List< document> documentList=[select Id from document where Name=:Label.EmailHeaderImage];
         if(documentList.size()>0)
         {
            URLval=URLval+documentList[0].id+'&oid='+UserInfo.getOrganizationId();
         }
         return URLval;
      }
      set;
   }
}