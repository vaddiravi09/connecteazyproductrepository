@istest
public class TestCreationofToDoList {
    
    @istest static void fetchdetails(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 
        User u = new User(Alias = 'sta12356', Email='standarduser@testorg123.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com1234');
        User u1 = new User(Alias = 's12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com12');
        
        
        
         Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                          
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        insert lleave;
        cleave.Nrich__Is_Active__c=True;
        update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;

        Nrich__Employee_Leave_Category__c emleavecategory = new Nrich__Employee_Leave_Category__c();
        emleavecategory.Nrich__Leave_Card__c=empleavecard.id;
        //emleavecategory.Leave_category_code__c =100;
        insert emleavecategory;
        
        
         Nrich__Employee__c emp1=new Nrich__Employee__c(Name='TestUser11' , Nrich__EmployeeDesignation__c='Developer',
                                         
                                         Nrich__Total_Experience__c=11,
                                         Nrich__Related_User__c=u.id );
        insert emp1;
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser1' , Nrich__EmployeeDesignation__c ='Developer',
                                        Nrich__Total_Experience__c=11,Nrich__Mentor__c=emp1.id,
                                        Nrich__Related_User__c=u1.id );
        
        insert emp;
        
        Nrich__Leave_Request__c lv=new Nrich__Leave_Request__c();
        lv.Nrich__Start_Date__c=System.today();
        lv.Nrich__End_Date__c=System.today()+1;
        lv.Nrich__Comments__c='Test';
        lv.Nrich__Employee_Leave_Category__c=emleavecategory.id;
        lv.Nrich__Requested_By__c=emp.id;
        lv.Nrich__Status__c='Applied';
        Datetime todayval = Datetime.now();
        list<Sobject>lvlist1=new list<Sobject>();
        lvlist1.add(lv);
        insert lvlist1;
        Test.setCreatedDate(lv.id,todayval);
        string objtype;
        Nrich__ToDoList__c todo=new Nrich__ToDoList__c();
        todo.Nrich__Status__c='Open';
        
        todo.Nrich__Record_Id__c='asd12';
        todo.Nrich__Body__c='Test has applied for writing a certification named: abc 080907';
        Datetime yesterdayval = Datetime.now();
        
        list<Nrich__ToDoList__c> ToDoList = new list<Nrich__ToDoList__c>();
        ToDoList.add(todo);
        insert ToDoList;
        Test.setCreatedDate(todo.Id, yesterdayval);
        
        system.debug('ToDoList' +todo.CreatedDate);
        
        Nrich__ToDoList__c todofinal=new Nrich__ToDoList__c();
        todofinal.Nrich__Status__c='Open';
        todofinal.Nrich__Record_Id__c=ToDoList[0].Nrich__Record_Id__c;
        todofinal.Nrich__Body__c='Final Data';
        list<Nrich__ToDoList__c> ToDoListFinal = new list<Nrich__ToDoList__c>();
        ToDoListFinal.add(todofinal);
        insert ToDoListFinal;
        
        
        
        Nrich__Certification__c certification=new Nrich__Certification__c(Name='Test12');
        certification.Nrich__cost__c=50;
        insert certification;
        Nrich__Certification_Request__c  cer =new Nrich__Certification_Request__c(Nrich__Certification_Name__c=certification.id, Nrich__Employeee__c =emp.id,Nrich__Approval_Status__c='Applied');
        insert cer;
        list<Sobject> clist=new list<Sobject>();
        clist.add(cer);
        
        
        list<Sobject>ApprovalList=new list<Sobject>();
        Nrich__Approval__c Approv=new Nrich__Approval__c ();
        Approv.Name='Test';
        Approv.Nrich__Employeee__c=emp.Id;
        Approv.Nrich__Start_Date__c =System.today();
        Approv.Nrich__End_Date__c=System.today()+1;
        Approv.Nrich__Project_Name__c='ConnectProduct';
        insert Approv;
        ApprovalList.add(Approv);
        
        CreationofToDoList.CreationofToDoListMethod(lvlist1,'Nrich__Leave_Request__c');
        CreationofToDoList.CreationofToDoListMethod(clist,'Nrich__Certification_Request__c');
        CreationofToDoList.CreationofToDoListMethod(ApprovalList,'Nrich__Approval__c');
        
        set<id>recset=new set<id>();
        recset.add(ToDoListFinal[0].id);
        System.assertEquals('ConnectProduct',Approv.Nrich__Project_Name__c);
        CreationofToDoList.updateToDoListMethod(recset);
        
        
    }
    
}