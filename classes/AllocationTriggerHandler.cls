/****************************************************************************************************
	* Company: Absyz
	* Developer: Raviteja
	* Created Date: 18/10/2018
	* Description: Creation of AssertAllocation
	*****************************************************************************************************/
public with sharing class AllocationTriggerHandler {
    
    public static void handle(list<Nrich__Asset_Request__c> AllocationList)
    {
        try{
        list<Nrich__Asset_Allocation__c> AssetList=new list<Nrich__Asset_Allocation__c>();
        if(ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__Employeee__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__AllocationExpectedDate__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Request__c', 'Nrich__Date_Approved__c')&& ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Allocation__c', 'Nrich__Status__c') &&
           ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Allocation__c', 'Nrich__Assigned_To__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Allocation__c', 'Nrich__Expected_Date__c')
           && ObjectFieldAccessCheck.checkAccessible('Nrich__Asset_Allocation__c', 'Nrich__Approved_date__c')
          ){
              for(Nrich__Asset_Request__c Asset:AllocationList)
              {
                  System.debug('Asset' +Asset);
                  Nrich__Asset_Allocation__c  AssetAllocation=new Nrich__Asset_Allocation__c ();
                  AssetAllocation.Nrich__Status__c =Label.Pending;
                  AssetAllocation.Nrich__Assigned_To__c=Asset.Nrich__Employeee__c;
                  AssetAllocation.Nrich__Expected_Date__c=Asset.Nrich__AllocationExpectedDate__c;
                  AssetAllocation.Nrich__Approved_date__c=Asset.Nrich__Date_Approved__c;
                  AssetList.add(AssetAllocation);
              }
          }
        
        Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__Asset_Allocation__c.getSObjectType().getDescribe();
        if(objectDescriptioncontent.isCreateable() && AssetList.size()>0 && AssetList!=NULL)
            database.insert(AssetList);
        
    }catch(exception e){
          Schema.DescribeSObjectResult objectDescriptioncontent = Nrich__ErrorLog__c.getSObjectType().getDescribe();
          if(objectDescriptioncontent.isCreateable() && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__StackTrace__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__Message__c') && ObjectFieldAccessCheck.checkAccessible('Nrich__ErrorLog__c', 'Nrich__LineNumber__c')){
            Nrich__ErrorLog__c errorlog = new Nrich__ErrorLog__c(Nrich__Type__c='AllocationTriggerHandler.handle', Nrich__StackTrace__c=e.getStackTraceString(), Nrich__Message__c=e.getMessage(), Nrich__LineNumber__c=e.getLineNumber());
            database.insert(errorlog, false);
          }
      }
        
    }
    
}