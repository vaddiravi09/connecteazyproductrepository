@isTest
Global class MockHttpResponseGenerator implements HttpCalloutMock{

    Global HTTPResponse  respond(HTTPRequest req){
        
        HttpResponse res=new HttpResponse();
        res.setHeader('Content-Type','application/json');
        res.setBody('{"Name":"csd","Nrich__Email__c": "vivekfhgj@yopmail.com"}');
        res.setStatusCode(200);
        return res;
        
        
    }
    
    
}