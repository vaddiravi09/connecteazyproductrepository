@isTest()
public class TestCertificationDetails {
    
    /****************************************************************************************************
* Company: Absyz
* Developer: Ashish Nag K
* Created Date: 29/10/2018
* Description: Test class covering Certification Modal.
*****************************************************************************************************/  
    
    public static string strEmployeeID;
    
    @isTest static void insertrecords()
    {
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
        System.runAs(u) {
            // The following code runs as user 'u' 
            Nrich__Certification__c certification=new Nrich__Certification__c(Name='Test');
            certification.Nrich__Cost__c=50;
            insert certification;
            Nrich__Certification_Request__c  cer =new Nrich__Certification_Request__c(Nrich__Certification_Name__c=certification.id);
            //insert cer;
            
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
          //  insert lleave;
            cleave.Nrich__Is_Active__c=True;
         //   update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            
            
            id CertificationId=CertificationDetailsController.insertnewrecords(cer,certification.Id);
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
            System.assertEquals(CertificationId, cer.Id);
        }
    }
    
    @isTest static void viewrec (){
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
           // insert lleave;
            cleave.Nrich__Is_Active__c=True;
           // update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            
            
            Nrich__Certification__c certification=new Nrich__Certification__c(Name='Test',Nrich__Cost__c=100);
            insert certification;
            Nrich__Certification_Request__c  cer =new Nrich__Certification_Request__c(Nrich__Certification_Name__c=certification.id, Nrich__Employeee__c =emp.id);
            
            list<Nrich__Certification_Request__c> cerlist =new list<Nrich__Certification_Request__c>();
            cerlist.add(cer);
            insert cerlist;
            list<Nrich__Certification_Request__c> cerlist2=CertificationDetailsController.viewrecords();
            
            
            
            System.assertEquals(cerlist2, cerlist2);
            Test.stopTest();
            
        }
    }
    
    @isTest static void submitfrapproval(){
        Test.startTest();
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'].get(0); 
        User u = new User(Alias = 'sta12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, ManagerId =u1.id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com1234');
        System.runAs(u){
           Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
         //   insert lleave;
            cleave.Nrich__Is_Active__c=True;
          //  update cleave;
            
          /*  Employee_Leave_Card__c EmpLeaveCard=new Employee_Leave_Card__c();
            EmpLeaveCard.Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;*/
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
           
            
            Nrich__Certification__c certification=new Nrich__Certification__c(Name='Test12');
            certification.Nrich__Cost__c=510;
            insert certification;
            Nrich__Certification_Request__c  cer =new Nrich__Certification_Request__c(Nrich__Certification_Name__c=certification.id, Nrich__Employeee__c =emp.id);
            insert cer;
            
            cer.Nrich__Approval_Status__c='Applied';
            update cer;
            
            
           /* Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
req.setComments('Hi' ); 
req.setObjectId(cer.Id); 
// req.setProcessDefinitionNameOrId('New_Certification_Request');
req.setNextApproverIds(new Id[] {cer.Employeee__c});
*/
            
            CertificationDetailsController.submitforapproval(cer.id); 
            System.assertEquals('Applied', cer.Nrich__Approval_Status__c);
            
            Test.StopTest();
            
            
        }
    }  
    
    @isTest static void  viewcertificationpicklist()
    {
        Test.startTest();
        List <Nrich__Certification__c> cerlsit=new list<Nrich__Certification__c>();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
           // insert lleave;
            cleave.Nrich__Is_Active__c=True;
         //   update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            
            
            Nrich__Certification__c cer=new Nrich__Certification__c(Name='Abc',Nrich__Applicable_Designation__c=emp.Nrich__EmployeeDesignation__c,Nrich__cost__c=50);
            
            
            cerlsit.add(cer);
            insert cerlsit;
            
            Nrich__Certification_Request__c  cer1 =new Nrich__Certification_Request__c(Nrich__Certification_Name__c=cer.id, Nrich__Employeee__c =emp.id,Nrich__Approval_Status__c='Applied',Nrich__Result__c='Pass',
                                                                         Nrich__Actual_Certification_Date__c=System.today(), Nrich__Verification_id__c='Abc');
            insert cer1;
            
            cer1.Nrich__Approval_Status__c='Pending';
            update cer1;
            
            // Map<String,String > CertificationReqMap=new Map<String ,String >();
            
            
            
            
            list<Nrich__Certification__c> clist= CertificationDetailsController.fetchoptions();  
            
            System.assertEquals('Pass', cer1.Nrich__Result__c);
            Test.stopTest();
            
        }
    }
    
    @isTest static void  frtriggerupdate(){
        
        List <Nrich__Certification__c> cerlsit=new list<Nrich__Certification__c>();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
           // insert lleave;
            cleave.Nrich__Is_Active__c=True;
         //   update cleave;
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            
            
            Nrich__Certification__c cer=new Nrich__Certification__c(Name='Abc',Nrich__Applicable_Designation__c=emp.Nrich__EmployeeDesignation__c,Nrich__cost__c=50);
            
            
            cerlsit.add(cer);
            insert cerlsit;
            
            Nrich__Certification_Request__c  cer1 =new Nrich__Certification_Request__c(Nrich__Certification_Name__c=cer.id, Nrich__Employeee__c =emp.id,Nrich__Result__c='Pass',
                                                                         Nrich__Actual_Certification_Date__c=System.today(), Nrich__Verification_id__c='Abc');
            insert cer1;
            cer1.Nrich__Approval_Status__c='Rejected';
            update cer1;
            System.assertEquals('Rejected', cer1.Nrich__Approval_Status__c);
            
        }
        
    }
    
    @isTest static void frcompanyleavestructuretrigger(){
        
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
        System.runAs(u){
            Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                             
                                                                            );
            insert cleave;
            Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                            Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                            Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                            Nrich__Carry_Forward_Frequency__c=0
                                                           );
            
           // insert lleave;
            cleave.Nrich__Is_Active__c=True;
          // update cleave;
            list<String>CLlist=new list<string>();
            CLlist.add(cleave.Id);
            
            Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
            EmpLeaveCard.Nrich__Is_Active__c=TRUE;
            EmpLeaveCard.Name='Test';
            insert EmpLeaveCard;
            
            
            Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                            
                                            Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
            
            
            insert emp;
            System.assertEquals('TestUser', emp.Name);
        }
        
    }
    
    
}