@isTest
public class ProjectKanBanDashboardController_Test {
    
    @isTest
    public static void testdata(){
        Lead Ld=new Lead();
        
        ProjectKanBanDashboardController.getStatusValues(ld,'Status');
        System.assertEquals('test', 'test');
        
    }
    
    @isTest
    public static void getstats(){
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u1 = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p1.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        insert u1;
        
        Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                         
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
        insert lleave;
        cleave.Nrich__Is_Active__c=True;
        update cleave;
        
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        insert EmpLeaveCard;
        
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser1' , Nrich__EmployeeDesignation__c='Developer',
                                        
                                        Nrich__Total_Experience__c=11,
                                        Nrich__Related_User__c=u1.id );
        
        insert emp;
        
        list<Id>ProjList=new list<Id>();
        Nrich__Project__c  Prj=new Nrich__Project__c();
        Prj.Name='TestPrj';
        Prj.Nrich__Project_Start_Date__c=System.today();
        Prj.Nrich__Project_End_Date__c=System.today()+1;
        Prj.Nrich__ProjectManagerr__c=emp.id;
        Prj.Nrich__Status__c='Proposed';
        insert prj;
        ProjList.add(prj.id);
        
        Nrich__Project_Team_Member__c prjteammember=new Nrich__Project_Team_Member__c ();
        prjteammember.Nrich__Employeee__c=emp.id;
        prjteammember.Nrich__Project__c=prj.id;
        prjteammember.Nrich__Employee_Start_Date__c=System.today();
        prjteammember.Nrich__Employee_End_Date__c=System.today()+7;
        insert prjteammember;
        
        
        list<Id>SprintIdlist=new list<id>();
        Nrich__Sprint__c Sprint=new Nrich__Sprint__c ();
        Sprint.Nrich__Project__c=prj.Id;
        insert Sprint;
        SprintIdlist.add(Sprint.Id);
        
        List<id>UserstoryIdList=new list<id>();
        Nrich__User_Story__c Userstory=new Nrich__User_Story__c ();
        Userstory.Nrich__Project__c=prj.Id;
        Userstory.Nrich__Status__c='New';
        Userstory.Nrich__Sprint__c=Sprint.Id;
        insert Userstory;
        UserstoryIdList.add(Userstory.Id);
        
        
        list<id>EmptyIDlist=new list<id>();
        Nrich__Task__c task=new Nrich__Task__c ();
        task.Nrich__Assigned_To__c=prjteammember.id;
        task.Nrich__Employee__c=emp.id;
        task.Nrich__Project__c=prj.id;
        task.Nrich__Sprint__c=Sprint.id;
        task.Nrich__User_Story__c=Userstory.id;
        task.Nrich__Type__c='Original';
        task.Nrich__Status__c='New'; 
      //  insert task;
        
        Nrich__Task__c task1=new Nrich__Task__c ();
        task1.Nrich__Assigned_To__c=prjteammember.id;
        task1.Nrich__Employee__c=emp.id;
        task1.Nrich__Project__c=prj.id;
        task1.Nrich__Sprint__c=Sprint.id;
        task1.Nrich__User_Story__c=Userstory.id;
        task1.Nrich__Type__c='Original';
        task1.Nrich__Status__c='Ready for Design'; 
        Id BugId = Schema.SObjectType.Nrich__Task__c.getRecordTypeInfosByName().get('Bug').getRecordTypeId();
        task1.RecordtypeId=BugId;
       // insert task1;  
        
        System.assertEquals('Original', task1.Nrich__Type__c);
        
        ProjectKanBanDashboardController.getStatusdetails(prj.Id, emp.Id, 'Sprint','P1', ProjList);
        //ProjectKanBanDashboardController.getStatusdetails(prj.Id, emp.Id, 'UserStory','P2',EmptyIDlist);
        ProjectKanBanDashboardController.getStatusdetails(prj.Id, emp.Id, 'Task','P2',ProjList);
        ProjectKanBanDashboardController.getStatusdetails(prj.Id, emp.Id, 'Task','P2',EmptyIDlist);
        ProjectKanBanDashboardController.getStatusdetails(prj.Id, emp.Id,'Bug','P1',ProjList);
        ProjectKanBanDashboardController.getStatusdetails(prj.Id, emp.Id,'Bug','P1',EmptyIDlist);
        ProjectKanBanDashboardController.UpdateStatus(task1.id,'Status','Task');
        
        
    }
    
}