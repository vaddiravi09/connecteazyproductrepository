@isTest
public with sharing class UtilityForTest {
    public static Employee__c Employee_Utility(){
        /* Employee__c testemployee=new Employee__c();
testemployee.Name='Test Employee';
testemployee.Last_Name__c='emp';
testemployee.Interview_Level__c='1';
testemployee.Gender__c='Male';
testemployee.Eligible_to_take_Interviews__c = true;
testemployee.Email__c='testabc@gmail.com';
insert testemployee;

return testemployee;*/
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'st12356', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com123');
        
        Nrich__Company_Leave_Structure__c cleave=new Nrich__Company_Leave_Structure__c(Nrich__Applicable_From__c =system.today(),Nrich__Applicable_Till__c=system.today()+365,Nrich__Is_Active__c=False
                                                                         
                                                                        );
        insert cleave;
        Nrich__Leave_Category__c lleave =new Nrich__Leave_Category__c(Name='Test',Nrich__Increment_Frequency__c=1,Nrich__Increment_Step__c =2,
                                                        Nrich__Will_Carry_Forward__c =False,Nrich__Leave_category_code__c ='1',
                                                        Nrich__Will_Lapse__c =False,Nrich__Applicable_Leave_Structure__c =cleave.id, Nrich__Default__c=1,
                                                        Nrich__Carry_Forward_Frequency__c=0
                                                       );
        
       // insert lleave;
        cleave.Nrich__Is_Active__c=True;
       // update cleave;
        
       
       
        
        Nrich__Employee__c emp=new Nrich__Employee__c(Name='TestUser' , Nrich__EmployeeDesignation__c='Developer',Nrich__Active__c=TRUE,
                                        
                                        Nrich__Total_Experience__c=10,Nrich__Related_User__c=u.id);
        
        
        insert emp;
        Nrich__Employee_Leave_Card__c EmpLeaveCard=new Nrich__Employee_Leave_Card__c();
        EmpLeaveCard.Nrich__Employee__c=emp.id;
        EmpLeaveCard.Nrich__Is_Active__c=TRUE;
        EmpLeaveCard.Name='Test';
        EmpLeaveCard.Nrich__Applicable_From__c=cleave.Nrich__Applicable_From__c;
        EmpLeaveCard.Nrich__Applicable_Till__c=cleave.Nrich__Applicable_Till__c;
        
        insert EmpLeaveCard;
        return emp;
    }
    
    public static Nrich__Candidate__c Candidate_Utility(){
        Nrich__Candidate__c testcandidate =new Nrich__Candidate__c();
        testcandidate.name='testCandidate';
        testcandidate.Nrich__Email__c='testabc@gmail.com';
        // testcandidate.Candidate_Last_Name__c='CandidateLastName1';
        insert testcandidate;
        
        return testcandidate;
    }
    
    public static Nrich__Department__c Department_Utility(){
        Nrich__Department__c  testDepartment = new Nrich__Department__c();
        testDepartment.Name='testDepartment';
        insert testDepartment;
        
        return testDepartment;
    }
    
    public static Nrich__InitialSetup__c InitialSetup_Utility(){
        
        Nrich__InitialSetup__c cstObj = new Nrich__InitialSetup__c();
        //cstObj.Nrich__No_of_Days_before_Job_EndDate_Email__c = 20;
        //cstObj.Nrich__Email_Panel_Member_on_Interview_Schedule__c = false;
        cstObj.Nrich__Email_Candidate_on_submission_of_appln__c = true;
        cstObj.Nrich__Automated_ApprovalProcess_on_JobCreation__c = true;
        insert cstObj;  
        
        return cstObj;
    }  
    
    public static Nrich__Job__c Job_Utility(Id departId){
        
        Nrich__Job__c testJob =new Nrich__Job__c();
        testJob.Nrich__Department__c=departId;
        testJob.Nrich__Approver__c = UserInfo.getUserId();
        testJob.Nrich__End_Date__c=system.today().adddays(4);
        testJob.Nrich__Start_Date__c=system.today();
        testJob.Nrich__Open_Positions__c=1;
        insert testJob;
        
        return testJob;
    }
    
    public static Nrich__job_Application__c job_Application_Utility(Id candId,Id jobId){
        
        Nrich__job_Application__c testjobapplication=new Nrich__Job_Application__c();
        testjobapplication.Nrich__Candidate__c=candId;
        testjobapplication.Nrich__Job__c=jobId;
        testjobapplication.Nrich__Comments_Rejection__c='test data';
        testjobapplication.Nrich__Application_Status__c = 'New';
        testjobapplication.Nrich__Source__c='Direct';
        insert testjobapplication;
        
        return testjobapplication;
    }   
    
    public static Nrich__job_Application__c job_Application_Utility1(Id candId,Id jobId){
        
        Nrich__job_Application__c testjobapplication=new Nrich__Job_Application__c();
        testjobapplication.Name = 'expr0';
        testjobapplication.Nrich__Candidate__c=candId;
        testjobapplication.Nrich__Job__c=jobId;
        testjobapplication.Nrich__Comments_Rejection__c='test data';
        testjobapplication.Nrich__Source__c='Direct';
        testjobapplication.Nrich__Application_Status__c = 'New';
        insert testjobapplication;
        
        return testjobapplication;
    } 
    public static Nrich__Job__c Job_Utility2(Id departId){
        
        Nrich__Job__c testJob =new Nrich__Job__c();
        testJob.Nrich__Department__c=departId;
        testjob.Nrich__Approver__c=UserInfo.getUserId();
        testJob.Nrich__End_Date__c=system.today().adddays(4);
        testJob.Nrich__Start_Date__c=system.today();
        testJob.Nrich__Open_Positions__c=1;
        insert testJob;
        system.assertEquals('Test','Test');
        return testJob;
    }
    public static Nrich__job_Application__c job_Application_Utility2(Id candId,Id jobId){
        
        Nrich__job_Application__c testjobapplication=new Nrich__Job_Application__c();
        testjobapplication.Name = 'expr0';
        testjobapplication.Nrich__Candidate__c=candId;
        testjobapplication.Nrich__Job__c=jobId;
        testjobapplication.Nrich__Comments_Rejection__c='test data';
        testjobapplication.Nrich__Source__c='Direct';
        testjobapplication.Nrich__Application_Status__c = 'New';
        insert testjobapplication;
        
        return testjobapplication;
    } 
    
    public static Nrich__job_Application__c job_Application_Utility3(Id candId,Id jobId){
        
        Nrich__job_Application__c testjobapplication=new Nrich__Job_Application__c();
        testjobapplication.Name = 'expr0';
        testjobapplication.Nrich__Candidate__c=candId;
        testjobapplication.Nrich__Job__c=jobId;
        testjobapplication.Nrich__Application_Status__c = 'New';
        testjobapplication.Nrich__Source__c='Direct';
        insert testjobapplication;
        
        return testjobapplication;
    } 
    
    public static Nrich__Interview_Round__c Interview_Round_Utility(Id jobApplId){
        
        Nrich__Interview_Round__c testInterviewRound=new Nrich__Interview_Round__c();
        testInterviewRound.Name='Technical1';
        testInterviewRound.Nrich__Job_Application__c=jobApplId;
        testInterviewRound.Nrich__Reschedule__c = false;
        testInterviewRound.Nrich__Interview_Start_Time__c=system.today();
        testInterviewRound.Nrich__Interview_End_Time__c=system.today()+9;
        testInterviewRound.Nrich__Job_Order__c = 1;
        testInterviewRound.Nrich__Status__c = System.Label.InterviewToBeSchedule;
        insert testInterviewRound;
        return testInterviewRound;
    }
    
    public static Nrich__Interview_Panel_Member__c Interview_Panel_Member_Utility(Id empId,Id InrvRndId){
        
        Nrich__Interview_Panel_Member__c testPanelMember= new Nrich__Interview_Panel_Member__c();
        testPanelMember.Nrich__Status__c='Accepted';
        testPanelMember.Nrich__Employee__c=empId;
        testPanelMember.Nrich__Interview_Round__c=InrvRndId;
        insert testPanelMember;
        
        return testPanelMember;
    }
    
    public static Nrich__Preffered__c Preffered_Utility(Id EmpId){
        Nrich__Preffered__c pref = new Nrich__Preffered__c();
        pref.Nrich__Employee__c = EmpId;
        pref.Nrich__Start_Date_Time__c = System.today();
        pref.Nrich__End_Date_Time__c = System.today()+1;
        insert pref;
        return pref;
    }
    
    public static Nrich__Job_Round__c Job_Round_Utility(Id JobId){
        Nrich__Job_Round__c jobRound = new Nrich__Job_Round__c();
        jobRound.Name = 'Technical 1';
        jobRound.Nrich__Job__c = JobId;
        insert jobRound;
        return jobRound;
    }
    
    public static Nrich__Job_Round__c Job_Round_Utility1(Id JobId){
        Nrich__Job_Round__c jobRound = new Nrich__Job_Round__c();
        jobRound.Name = 'Technical 2';
        jobRound.Nrich__Job__c = JobId;
        insert jobRound;
        return jobRound;
    }
    public static Nrich__Job_Round__c Job_Round_Utility2(Id JobId){
        Nrich__Job_Round__c jobRound = new Nrich__Job_Round__c();
        jobRound.Name = 'Technical 2';
        jobRound.Nrich__Job__c = JobId;
        jobRound.Nrich__Job_Round_Order_Number__c = null;
        insert jobRound;
        return jobRound;
    }
    
    public static Nrich__Template__c Template_Utility(Id jobID,ID jobRoundId){
        Nrich__Template__c temp = new Nrich__Template__c();
        temp.Name = 'Application';
        temp.Nrich__Job__c =jobId;
        temp.Nrich__Job_Round__c = jobRoundId;
        temp.Nrich__Type_of_Template__c = 'Application';
        temp.Nrich__Mapping_Object_API_Name__c = 'Nrich__Job_Application__c';
        temp.Nrich__Background_Color__c = '#36D722';
        temp.Nrich__Is_Active__c = true;
        insert temp;
        return temp;
    }
    
    public static Nrich__Template__c Template_Utility1(Id jobID,ID jobRoundId){
        Nrich__Template__c temp = new Nrich__Template__c();
        temp.Name = 'On Boarding';
        temp.Nrich__Job__c =jobId;
        temp.Nrich__Job_Round__c = jobRoundId;
        temp.Nrich__Type_of_Template__c = 'On Boarding';
        temp.Nrich__Mapping_Object_API_Name__c = 'Job_Application__c';
        temp.Nrich__Background_Color__c = '#36D722';
        temp.Nrich__Is_Active__c = true;
        insert temp;
        return temp;
    }
    
    public static Nrich__Section__c Section_Utility(Id tempId){
        Nrich__Section__c sec = new Nrich__Section__c();
        sec.Name = 'Section 1';
        sec.Nrich__Order__c = 1;
        sec.Nrich__Template__c = tempId;
        insert sec;
        return sec;
    }
    
    public static List<Nrich__Section_Fields__c> Section_Fields_Utility(Id secId){
        List<Nrich__Section_Fields__c> sfList = new List<Nrich__Section_Fields__c>();
        Nrich__Section_Fields__c sf = new Nrich__Section_Fields__c();
        sf.Name = 'Section Field 1';
        sf.Nrich__Coloumn__c = 'Column-1';
        sf.Nrich__Order__c = 1;
        sf.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf.Nrich__Field_Type__c = 'String';
        sf.Nrich__Mapping_Field_API_Name__c = 'Name';
        sf.Nrich__Section__c = secId;
        sf.Nrich__Mandatory__c = false;
        sfList.add(sf);
        
        Nrich__Section_Fields__c sf1 = new Nrich__Section_Fields__c();
        sf1.Name = 'Section Field 2';
        sf1.Nrich__Coloumn__c = 'Column-1';
        sf1.Nrich__Order__c = 2;
        sf1.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf1.Nrich__Field_Type__c = 'String';
        sf1.Nrich__Mapping_Field_API_Name__c = 'Name';
        sf1.Nrich__Section__c = secId;
        sf1.Nrich__Mandatory__c = true;
        sfList.add(sf1);
        
        Nrich__Section_Fields__c sf2 = new Nrich__Section_Fields__c();
        sf2.Name = 'Section Field 3';
        sf2.Nrich__Coloumn__c = 'Column-2';
        sf2.Nrich__Order__c = 1;
        sf2.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf2.Nrich__Field_Type__c = 'Picklist';
        sf2.Nrich__Mapping_Field_API_Name__c = 'Nrich__Application_Status__c';
        sf2.Nrich__Section__c = secId;
        sf2.Nrich__Mandatory__c = false;
        sfList.add(sf2);
        
        Nrich__Section_Fields__c sf3 = new Nrich__Section_Fields__c();
        sf3.Name = 'Section Field 4';
        sf3.Nrich__Coloumn__c = 'Column-2';
        sf3.Nrich__Order__c = 2;
        sf3.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf3.Nrich__Field_Type__c = 'Picklist';
        sf3.Nrich__Mapping_Field_API_Name__c = 'Application_Status__c';
        sf3.Nrich__Section__c = secId;
        sf3.Nrich__Mandatory__c = true;
        sfList.add(sf3);    
        
        Nrich__Section_Fields__c sf4 = new Nrich__Section_Fields__c();
        sf4.Name = 'Section Field 5';
        sf4.Nrich__Coloumn__c = 'Column-1';
        sf4.Nrich__Order__c = 3;
        sf4.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf4.Nrich__Field_Type__c = 'Date';
        sf4.Nrich__Mapping_Field_API_Name__c = 'Date_of_Birth__c';
        sf4.Nrich__Section__c = secId;
        sfList.add(sf4);
        
        Nrich__Section_Fields__c sf5 = new Nrich__Section_Fields__c();
        sf5.Name = 'Section Field 6';
        sf5.Nrich__Coloumn__c = 'Column-2';
        sf5.Nrich__Order__c = 3;
        sf5.Nrich__Object_API_Name__c = 'Nrich__Job_Application__c'; 
        sf5.Nrich__Field_Type__c = 'Date';
        sf5.Nrich__Mapping_Field_API_Name__c = 'Nrich__Date_of_Birth__c';
        sf5.Nrich__Mandatory__c = true;    
        sf5.Nrich__Section__c = secId;
        sfList.add(sf5);
        
        Nrich__Section_Fields__c sf6 = new Nrich__Section_Fields__c();
        sf6.Name = 'Section Field 7';
        sf6.Nrich__Coloumn__c = 'Column-1';
        sf6.Nrich__Order__c = 4;
        sf6.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf6.Nrich__Field_Type__c = 'Boolean';
        sf6.Nrich__Mapping_Field_API_Name__c = 'Nrich__Notice_Period_Negotiable__c';
        sf6.Nrich__Section__c = secId;
        sfList.add(sf6);
        
        Nrich__Section_Fields__c sf7 = new Nrich__Section_Fields__c();
        sf7.Name = 'Section Field 8';
        sf7.Nrich__Coloumn__c = 'Column-2';
        sf7.Nrich__Order__c = 3;
        sf7.Nrich__Object_API_Name__c = 'Nrich__Job_Application__c'; 
        sf7.Nrich__Field_Type__c = 'Boolean';
        sf7.Nrich__Mapping_Field_API_Name__c = 'Nrich__Notice_Period_Negotiable__c';
        sf7.Nrich__Mandatory__c = true;    
        sf7.Nrich__Section__c = secId;
        sfList.add(sf7);
        
        Nrich__Section_Fields__c sf8 = new Nrich__Section_Fields__c();
        sf8.Name = 'Section Field 9';
        sf8.Nrich__Coloumn__c = 'Column-1';
        sf8.Nrich__Order__c = 5;
        sf8.Nrich__Object_API_Name__c = 'Nrich__Job_Application__c'; 
        sf8.Nrich__Field_Type__c = 'Long Text Area';
        sf8.Nrich__Mapping_Field_API_Name__c = 'Comments__c';
        sf8.Nrich__Section__c = secId;
        sfList.add(sf8);
        
        Nrich__Section_Fields__c sf9 = new Nrich__Section_Fields__c();
        sf9.Name = 'Section Field 10';
        sf9.Nrich__Coloumn__c = 'Column-1';
        sf9.Nrich__Order__c = 5;
        sf9.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf9.Nrich__Field_Type__c = 'Long Text Area';
        sf9.Nrich__Mapping_Field_API_Name__c = 'Comments__c';
        sf9.Nrich__Mandatory__c = true;    
        sf9.Nrich__Section__c = secId;
        sfList.add(sf9);
        
        Nrich__Section_Fields__c sf10 = new Nrich__Section_Fields__c();
        sf10.Name = 'Section Field 11';
        sf10.Nrich__Coloumn__c = 'Column-2';
        sf10.Nrich__Order__c = 5;
        sf10.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf10.Nrich__Field_Type__c = 'Text Area';
        sf10.Nrich__Mapping_Field_API_Name__c = 'Nrich__Referring_Employee_Comments__c';
        sf10.Nrich__Section__c = secId;
        sfList.add(sf10);
        
        Nrich__Section_Fields__c sf11 = new Nrich__Section_Fields__c();
        sf11.Name = 'Section Field 10';
        sf11.Nrich__Coloumn__c = 'Column-1';
        sf11.Nrich__Order__c = 6;
        sf11.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf11.Nrich__Field_Type__c = 'Text Area';
        sf11.Nrich__Mapping_Field_API_Name__c = 'Referring_Employee_Comments__c';
        sf11.Nrich__Mandatory__c = true;    
        sf11.Nrich__Section__c = secId;
        sfList.add(sf11);
        
        Nrich__Section_Fields__c sf12 = new Nrich__Section_Fields__c();
        sf12.Name = 'Section Field 12';
        sf12.Nrich__Coloumn__c = 'Column-2';
        sf12.Nrich__Order__c = 6;
        sf12.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf12.Nrich__Field_Type__c = 'Email';
        sf12.Nrich__Mapping_Field_API_Name__c = 'Nrich__Email__c';
        sf12.Nrich__Section__c = secId;
        sfList.add(sf12);
        
        Nrich__Section_Fields__c sf13 = new Nrich__Section_Fields__c();
        sf13.Name = 'Section Field 13';
        sf13.Nrich__Coloumn__c = 'Column-1';
        sf13.Nrich__Order__c = 7;
        sf13.Nrich__Object_API_Name__c = 'Nrich__Job_Application__c'; 
        sf13.Nrich__Field_Type__c = 'Email';
        sf13.Nrich__Mapping_Field_API_Name__c = 'Email__c';
        sf13.Nrich__Mandatory__c = true;    
        sf13.Nrich__Section__c = secId;
        sfList.add(sf13);
        
        Nrich__Section_Fields__c sf14 = new Nrich__Section_Fields__c();
        sf14.Name = 'Section Field 14';
        sf14.Nrich__Coloumn__c = 'Column-2';
        sf14.Nrich__Order__c = 7;
        sf14.Nrich__Object_API_Name__c = 'Job_Application__c'; 
        sf14.Nrich__Field_Type__c = 'Phone';
        sf14.Nrich__Mapping_Field_API_Name__c = 'Primary_Phone__c';
        sf14.Nrich__Section__c = secId;
        sfList.add(sf14);
        
        Nrich__Section_Fields__c sf15 = new Nrich__Section_Fields__c();
        sf15.Name = 'Section Field 15';
        sf15.Nrich__Coloumn__c = 'Column-1';
        sf15.Nrich__Order__c = 8;
        sf15.Nrich__Object_API_Name__c = 'Nrich__Job_Application__c'; 
        sf15.Nrich__Field_Type__c = 'Phone';
        sf15.Nrich__Mapping_Field_API_Name__c = 'Nrich__Primary_Phone__c';
        sf15.Nrich__Mandatory__c = true;     
        sf15.Nrich__Section__c = secId;
        sfList.add(sf15);
        
        insert(sfList);
        return sfList;
    }
}