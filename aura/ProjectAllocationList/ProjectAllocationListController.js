({
    doInit: function(component, event, helper) {
       component.set('v.columns', [
                            {label: $A.get("$Label.c.Name"), fieldName: 'Nrich__Project_Name__c', type: 'text', sortable:true},
                            {label: $A.get("$Label.c.Start_Date"), fieldName: 'Nrich__Employee_Start_Date__c', type: 'date', sortable:true},
                            {label: $A.get("$Label.c.end_date"), fieldName: 'Nrich__Employee_End_Date__c', type: 'date', sortable:true},
                            {label: $A.get("$Label.c.Project_Manager"), fieldName: 'Nrich__ProjectManagerName__c', type: 'text', sortable:true},
                        ]); 
    },
    handleColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    },
    closeinsertionleavemodal:function(component, event, helper){
        component.set("v.openmodal", false);
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'ProjectAllocations'});   
        compEvent.fire();
    },
})