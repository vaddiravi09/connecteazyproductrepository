({
    doInit : function(component, event, helper) {
    helper.handleInit(component, event);
},
 saveSignatureOnClick : function(component, event, helper){
    helper.handleSaveSignature(component, event);
},
    clearSignatureOnClick : function(component, event, helper){
        helper.clearCanvas(component, event);
    },
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
})