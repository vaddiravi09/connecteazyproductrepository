({
    handleInit : function(component, event) {
        //var canvas = document.getElementById('signature-pad');</h5>
        // Fix tab switching issue
        //alert("Controller");
        
        var canvas = component.find("signature-pad").getElement();
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio * 1.5;
        canvas.getContext("2d").scale(ratio, ratio);
        var signaturePad = new SignaturePad(canvas);
        signaturePad.clear();
        signaturePad.on();
        component.set("v.signaturePad", signaturePad);
    },
    handleSaveSignature : function(component, event) {
        var action = component.get("c.uploadSignature");
        var sig = component.get("v.signaturePad");
        if(sig.isEmpty() == false){
            action.setParams({
                "name" : component.get("v.HRInstance.Name"),
                "demoReportId": component.get("v.recordId"),
                "b64SignData": sig.toDataURL().replace(/^data:image\/(png|jpg);base64,/, "")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    var cmpEvent = component.getEvent("cmpEvent");
                    cmpEvent.setParams({
                        "isOpen" : false,
                        "isSubmitted" : true
                    });
                    cmpEvent.fire();                  
                }
            });
            $A.enqueueAction(action);        
        }else{
            alert("Signature is empty. Insert the signature.");
        }
    },
    clearCanvas : function(component, event) {
        var sig = component.get("v.signaturePad");
        sig.clear();
    },
    showToast: function(component,isSuccess, error){
        var toastEvent = $A.get("e.force:showToast");
        var type = isSuccess ? "success" : "error";
        var title = isSuccess ? "Success" : "Error";
        var message = isSuccess ? "Success" : error;
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": message,
            "mode": "pester"
        });
        toastEvent.fire();
    }
})