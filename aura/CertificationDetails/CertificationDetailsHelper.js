({
    
    validateFields: function(component,event){
        var today = new Date();
        var dd = today.getDate();
        var inputCmp = component.find("inputCmp");
        var value = inputCmp.get("v.value");
        var inputCmp1=component.find("colorId")
        var value1=inputCmp1.get("v.value");
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        today= yyyy +'-'+mm+'-'+dd;
        
        component.set("v.Ifcondition",false);
        if (((value =='')||(value==null))||((new Date(value)<=new Date(today)))||((value1==$A.get("$Label.c.Select_Required_Certification")))) {
            
            component.set("v.Ifcondition",true);
            /* var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error Message',
                message:'Please fill the required details',
                
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'Sticky '
            });
            toastEvent.fire();
            */
            
        }
    }
    
    
})