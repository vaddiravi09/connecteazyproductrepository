({
    doInit : function(component, event, helper) {
        component.set("v.IsSpinner", true);
        var alist=[];
        var completedlist=[];
        var pendinglist=[];
        var approvedlist=[];
        var approvedntsubmit=[];
        var action1 = component.get("c.viewrecords");
        /*component.set('v.mcolumns', [
            { label: 'Certification Name', fieldName: 'CertName__c ', type:'text'},
            { label: 'Cost', fieldName: 'Cost__c'},
            { label: 'Approver', fieldName: 'Certification_Name__r.Approver__r.Name'},
            { label: 'Successful Exams', fieldName: 'Certification_Name__r.Number_of_Successful_Certification_Exams__c'},
            { label: 'Result', fieldName: 'Result__c'},
            { label: 'Name', fieldName: 'Name'},
            { label: 'ApprovalStatus', fieldName: 'Approval_Status__c'} 
        ]); */
        action1.setCallback(this, function(a){
            var state = a.getState();
            if (state === "SUCCESS"){
                //var approvedntsubmit=[];
                alist=a.getReturnValue();
                var i=0;
                for( alist[i];i<alist.length;i=i+1)
                {
                    if(alist[i].Nrich__Approval_Status__c==''|| alist[i].Nrich__Approval_Status__c==null)
                    {
                        approvedntsubmit.push(alist[i]);
                    }
                    
                    else if(alist[i].Nrich__Approval_Status__c ==$A.get("$Label.c.Approved") &&  (alist[i].Nrich__Result__c!=$A.get("$Label.c.Pass")|| alist[i].Nrich__Result__c!=$A.get("$Label.c.Fail")))
                    {
                        approvedlist.push(alist[i]);
                    }
                    
                        else if(((alist[i].Nrich__Approval_Status__c ==$A.get("$Label.c.Applied")) || (alist[i].Nrich__Approval_Status__c ==$A.get("$Label.c.Pending"))) && alist[i].Nrich__Approval_Status__c!=$A.get("$Label.c.Approved") && alist[i].Nrich__Result__c!=$A.get("$Label.c.Pass"))
                        {
                            
                            pendinglist.push(alist[i]);
                        }
                    
                    if(alist[i].Nrich__Result__c==$A.get("$Label.c.Pass") )
                    {
                        completedlist.push(alist[i]); 
                    }
                }
                component.set('v.Completedcount',completedlist.length);
                component.set('v.Approvedcount',approvedlist.length); 
                component.set('v.Pendingcount',pendinglist.length); 
                console.log('completedlist****',completedlist);
                component.set("v.Viewpendingnotsubmitted",approvedntsubmit);
                component.set("v.ViewCompleteddetails",completedlist);
                component.set("v.Viewapproveddetails",approvedlist);
                component.set("v.Viewpendingdetails",pendinglist);
            }
        })
        $A.enqueueAction(action1);
        var action3 = component.get("c.fetchoptions"); 
        action3.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                
                component.set("v.options",response.getReturnValue());
            } 
            component.set("v.IsSpinner", false);
        });
        $A.enqueueAction(action3);
        
        
    },
    
    goToRecord : function(component, event, helper) {
        var recId = event.target.id;
        if(recId != undefined){        
            window.open('/' + recId,'_blank');
        }
        
    },
    
    handleComponentEvent : function(component, event, helper) {
        
        
        var selectedCertificationGetFromEvent = event.getParam("CertificationByEvent");
        
        component.set("v.SelectedCertification",selectedCertificationGetFromEvent);
    },
    
    
    
    
    insertcertificationrecord:function(component, event, helper){
        var buttonName = event.getSource().get("v.name");
        var insertiondetails=component.get("v.InsertCertificationdetails");
        
        if(buttonName==$A.get("$Label.c.Save")){
            helper.validateFields(component,event)
            var condition=component.get("v.Ifcondition");
            
            if(condition==true) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Error_Message"),
                    message:$A.get("$Label.c.Errror_In_Required_Fields"),
                    
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'dismissible'
                });
                toastEvent.fire();
                
            }
            if(condition==false){
                
                var action=component.get("c.insertnewrecords");
                
                action.setParams({'certificationdetails':insertiondetails ,'CerId':component.find("colorId").get("v.value")});
                
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS"){
                        
                        certificationId=response.getReturnValue();
                        
                        
                        component.set("v.CertId",response.getReturnValue());
                        
                        
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : $A.get("$Label.c.Success_Message "),
                            message: $A.get("$Label.c.RecordSavedSubmittedFrApproval "),
                            
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'success',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();
                        
                        
                        component.set("v.isOpenSubmit", true);
                    }
                    
                });
                $A.enqueueAction(action);
            }
        }
        
        else {
            
            var certificationId=component.get("v.CertId");
            var action1=component.get("c.submitforapproval");
            action1.setParams({'cer':certificationId});
            action1.setCallback(this, function(response){
                var state = response.getState();
                
                if (state === "SUCCESS"){
                    
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.Success_Message"),
                        message: $A.get("$Label.c.Approval_Submitted_Successfully"),
                        
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                    
                    
                }
                
            });
            
            component.set("v.IsSpinner", false);
            $A.enqueueAction(action1);  
            component.set("v.openmodal", false);
            var compEvent = component.getEvent("closemodaleventval");
            compEvent.setParams({"openclosemodal" : 'false',
                                 "typeofmodal" : 'Certificationcomp'});   
            compEvent.fire(); 
            
        }
        
        
        
    }, 
    
    submit:function(component, event, helper){
        
        var cerid = event.getSource().get("v.value");
        
        var action1=component.get("c.submitforapproval");
        
        action1.setParams({'cer':cerid});
        
        action1.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS"){
                
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Success_Message"),
                    message: $A.get("$Label.c.Approval_Submitted_Successfully"),
                    
                    duration:'5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }
            
        });
        
        $A.enqueueAction(action1);  
        component.set("v.openmodal", false);
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'Certificationcomp'});   
        compEvent.fire(); 
        // $A.get('e.force:refreshView').fire();
    },
    
    closemodal : function(component, event, helper){
        component.set("v.openmodal", false);
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'Certificationcomp'});   
        compEvent.fire(); 
        
    },
    handleColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    }
})