({
    doInit : function(component, event, helper) {
        var action = component.get("c.fetchEmployeeDetails");
        
        action.setCallback(this, function(a)
                           {
                               var state = a.getState();
                               if (state === "SUCCESS"){
                                   console.log(a.getReturnValue());
                                   component.set("v.ViewEmployeedetails",a.getReturnValue());
                                   
                               }
                           });
        $A.enqueueAction(action); 
       
    },
    
    
    opendetailsmodal:function(component, event, helper) {
        component.set("v.isOpenEmployeedetails", true);	
    },
    
    closemodal:function(component, event, helper) {
        component.set("v.isOpenEmployeedetails", false);	
    },
    
    //Code for Navigation to Record Page Start
    goToRecord : function(component, event, helper) {
        
        var recId = event.target.id;
        if(recId != undefined){
            var sObjectEvent = $A.get("e.force:navigateToSObject");
            sObjectEvent.setParams({
                "recordId": recId
            })
            sObjectEvent.fire();
            
            component.set("v.isOpenEmployeedetails", false);
        }
        
    },
})