({
    save : function(component, event, helper) {
		helper.savetravelRequest(component, event, helper);
	},
    doInit : function(component, event, helper) {
        component.set("v.IsSpinner", true);
        var action = component.get("c.fetchEmployeeDetails");
         action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.employeeId", outputresponse.Id);
                var emplist = component.get("v.employeelist");
                emplist.push(outputresponse);
                component.set("v.employeelist", emplist);
            }
        });
        $A.enqueueAction(action);
            
        component.set('v.columns', [
            {label:$A.get("$Label.c.Travel_Request_No"), fieldName: 'Name', type: 'text', sortable:true},
            {label: $A.get("$Label.c.Source_location"), fieldName: 'Nrich__Source_Location__c', type: 'text', sortable:true},
            {label: $A.get("$Label.c.Destination_Location"), fieldName: 'Nrich__Destination_Location__c', type: 'text', sortable:true},
            {label: $A.get("$Label.c.Start_Date"), fieldName: 'Nrich__Start_Date__c', type: 'date', sortable:true},
            {label: $A.get("$Label.c.end_date"), fieldName: 'Nrich__End_Date__c', type: 'date', sortable:true},
            {label: $A.get("$Label.c.No_Of_Travellers"), fieldName: 'Nrich__Number_of_Travellers__c', type: 'Roll-Up Summary', sortable:true},
            {type: 'button', typeAttributes: {label: $A.get("$Label.c.View"), name: 'View', 'iconName': 'utility:preview', iconPosition: 'right' }}
        ]);
        
	},
    submitforapproval : function(component, event, helper) {
        helper.submitTravelforapproval(component, event, helper);
    },
    savetravellers :function(component, event, helper) {
       helper.savetravellerslist(component, event, helper) ;
    },
    removetravellers :function(component, event, helper) {
       helper.removetravellerslist(component, event, helper) ;
    },
    fetchreason :function(component, event, helper) {
       var action = component.get("c.fetchreasonfortraveloptions");
         action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.reasonlist", outputresponse);
            }
        });
        $A.enqueueAction(action);
    },
    fetchmode :function(component, event, helper) {
       var action = component.get("c.fetchModeofTransport");
         action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.modelist", outputresponse);
            }
        });
        $A.enqueueAction(action);
    },
    fetchrequest :function(component, event, helper) {
       var action = component.get("c.fetchRequestType");
         action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.requesttypelist", outputresponse);
            }
        });
        $A.enqueueAction(action);
    },
    fetchstatusval :function(component, event, helper) {
       var action = component.get("c.fetchStatus");
         action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.statuslist", outputresponse);     
                var emplist = component.get("v.employeetravelRequestlist");
                var empdisplaylist = [];
                var typeval = component.get("v.type");
                for(var i=0; i<emplist.length; i=i+1){
                    if(emplist[i].Nrich__Status__c==typeval)
                       empdisplaylist.push(emplist[i]);
                }
                component.set("v.employeedisplaytravelRequestlist", empdisplaylist);
                component.set("v.IsSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    selectedtab :function(component, event, helper) {
        var emplist = component.get("v.employeetravelRequestlist");
        
        var empdisplaylist = []
        component.set("v.employeedisplaytravelRequestlist", empdisplaylist);
        var typeval = event.getParam('id');
        component.set("v.type", typeval);
        for(var i=0; i<emplist.length; i=i+1){
            if(emplist[i].Nrich__Status__c==typeval)
               empdisplaylist.push(emplist[i]);
        }
        component.set("v.employeedisplaytravelRequestlist", empdisplaylist);
    },
    closeinsertionleavemodal:function(component, event, helper){
        component.set("v.openmodal", false);
         var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'travelRequest'});   
         compEvent.fire();
    },
    handleColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    },
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');  
        switch (action.name) {
            case 'View':
                helper.goToRecord(component, row)
                break;
           }
    },
})