({
    savetravelRequest : function(component, event, helper) {
        component.set("v.IsSpinner", true);		
        var employeeCategorylist = [];
        var validationcheck = "false";
        var empid = component.get("v.selectedemployee");
        if(empid!='' && empid!=null && !component.get("v.selectedemployeenull")){
            var emplist = component.get("v.employeelist");
            emplist.push(empid);
            component.set("v.employeelist", emplist);
            component.set("v.selectedemployeenull", true);
        }
        if(component.get("v.employeelist").length<=0)
            validationcheck = "true";
        if(validationcheck=="false"){
            var action = component.get("c.saveEmployeeTravelRequest"); 
            action.setParams({
                TravelRequest: component.get("v.employeeTravelRequest"),
                travellers: JSON.stringify(component.get("v.employeelist")),
                empid: component.get("v.employeeId")
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                var outputresponse = component.get("v.saveresultStatusval");
                outputresponse = response.getReturnValue();
                if (state === "SUCCESS") {
                    if(outputresponse.isSuccess){
                        component.set("v.submitforapproval", true);
                        component.set("v.saveresultStatusval", outputresponse);
                        component.set("v.IsSpinner", false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({    
                            "type" : "success",
                            "title": $A.get("$Label.c.Success_Message"),
                            "message":$A.get("$Label.c.RecordSavedSubmittedFrApproval")  
                        });
                        toastEvent.fire();
                    }
                    else{
                        component.set("v.IsSpinner", false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "error",
                            "title":  $A.get("$Label.c.Error"),
                            "message": outputresponse.message
                        });
                        toastEvent.fire();
                    }
                }
            });
            $A.enqueueAction(action);
        }else{
            component.set("v.IsSpinner", false);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type" : "error",
                "title": $A.get("$Label.c.Error"),
                "message": $A.get("$Label.c.Error_In_Travellers")
            });
            toastEvent.fire();
        }
    },
    submitTravelforapproval : function(component, event, helper) {
        var action = component.get("c.submitTravelRequest");
        //alert('recId' +component.get("v.saveresultStatusval").recordId);
        action.setParams({
            TravelRequestId: component.get("v.saveresultStatusval").recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                if(outputresponse===true){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type" : "success",
                        "title": $A.get("$Label.c.Success_Message"),
                        "message": $A.get("$Label.c.RecordSavedSubmittedFrApproval")
                    });
                    toastEvent.fire();
                    component.set("v.openmodal", false);
                    var compEvent = component.getEvent("closemodaleventval");
                    compEvent.setParams({"openclosemodal" : 'false',
                                         "typeofmodal" : 'travelRequest'});   
                    compEvent.fire();
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type" : "error",
                        "title": $A.get("$Label.c.Error"),
                        "message": $A.get("$Label.c.Error_In_Approval")
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    savetravellerslist: function(component,event,helper){
        var empid = component.get("v.selectedemployee");
        if(empid!='' && empid!=null){
            var emplist = component.get("v.employeelist");
            emplist.push(empid);
            if(component.get("v.refreshval"))
               component.set("v.refreshval", false);
            else
               component.set("v.refreshval", true);
            component.set("v.employeelist", emplist);     
            component.set("v.selectedemployee",{});
        }
    },
    
    removetravellerslist : function(component, event, helper){
        var idval = event.target.id;
        var emplist = component.get("v.employeelist");
        var spliceval;
        for(var i=0; i<emplist.length; i=i+1){
            if(emplist[i].Id==idval){
                spliceval = i;            
            }
        }
        emplist.splice(spliceval, 1);
        component.set("v.employeelist", emplist);
    },
    
    goToRecord : function(component, row) {
        if(row != undefined){        
            window.open('/' + row.Id,'_blank');
        }        
    },
    
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.employeetravelRequestlist");
        var res;
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.employeeTravelRequestlist", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
})