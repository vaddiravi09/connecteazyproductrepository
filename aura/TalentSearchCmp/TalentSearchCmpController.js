({
    searchEmployees : function(component, event, helper) {
        var rowDept=[];
        var searchEmp = component.get('v.Employee');
        console.log('searchEmp======='+JSON.stringify(searchEmp));
        var MinExp = component.get('v.MinExperience');
        console.log('MinExp======='+JSON.stringify(MinExp));
        var MaxExp = component.get('v.MaxExperience');
        console.log('MaxExp======='+JSON.stringify(MaxExp));
        var dept = component.get("v.selectedRecord").Id;
        //alert(JSON.stringify(dept));
        console.log('selectedDept======='+JSON.stringify(dept));
        var phone = component.get('v.phone');
        var email = component.get('v.email');
        
        component.set('v.columns', [
            {label: 'Emp Id', fieldName: 'Nrich__Employee_Id__c',sortable: true},
            {label: 'Name', fieldName: 'Name',sortable: true},
            {label: 'Experience', fieldName: 'Nrich__Total_Experience__c', type: 'text',sortable: true},
            {label: 'Skill', fieldName: 'Nrich__Skills__c',sortable: true},
            {label: 'Department', fieldName: 'Nrich__Department__c', type: 'text',sortable: true},
            {label: 'Type', fieldName: 'Nrich__Employee_Type__c',sortable: true},
            {label: 'location', fieldName: 'Nrich__Location__c',sortable: true},
            {type: 'button', typeAttributes: {label: $A.get("$Label.c.View"), name: 'View', 'iconName': 'utility:preview', iconPosition: 'right' }}
        ]);
        
        var action = component.get('c.searchEmp');
        action.setParams({'search' : searchEmp,
                          'Dept' : dept,
                          'minExp' : MinExp,
                          'maxExp' : MaxExp,
                          'phone': phone,
                          'email': email});
        action.setCallback(this, function(response) {       
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                console.log('recs=====:'+JSON.stringify(response.getReturnValue()));
                //component.set('v.EmployeeRecords', response.getReturnValue());
                var rows = response.getReturnValue();
                for(var i = 0; i < rows.length; i++){
                    var row = rows[i];
                    if (row.Nrich__Department__c){ 
                        row.Nrich__Department__c = row.Nrich__Department__r.Name;
                        row.Nrich__Total_Experience__c = row.Nrich__Total_Experience__c.toString();
                    }  
                    rowDept.push(row);
                }
                console.log('rowDept=====:'+JSON.stringify(rowDept));
                component.set('v.EmployeeRecords', rowDept);
                console.log('rows=====:'+JSON.stringify(component.get('v.EmployeeRecords')));
                
                if(rows.length>0){
                    component.set('v.table',true);                
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Alert!",
                        "message": "No Employees Found!!."
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
    handleColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    },
    clearSearch : function(component, event, helper) {
        component.set("v.Employee",{'sobjectType':'Nrich__Employee__c',
                                    'Name':'','Nrich__Skills__c':'','Nrich__Employee_Type__c':'',
                                    'Nrich__Department__c':'','Nrich__Location__c':'' });
        component.set("v.selectedRecord",null);
        component.set('v.MinExperience',null);
        component.set('v.MaxExperience',null);
        component.set('v.table',false);
        component.set('v.phone',null);
        component.set('v.email',null);
        //component.set('v.pill',true);
       
    },
        
    openDetailPopup : function(component,event,helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        //var id=event.getSource().get("v.tabindex");
        component.set('v.popup',true);
        component.set('v.recId',row.Id);
    },
    closeModel: function(component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set('v.popup',false);
    },
    
})