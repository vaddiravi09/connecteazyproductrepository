({    
    doInit:function(component, event, helper) {
        component.set('v.IsSpinner',true);
        var action = component.get("c.GetDetails");        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){       
                if(component.isValid() && response !== null && response.getState() === 'SUCCESS')
                {
                    component.set("v.ApprovedColor", response.getReturnValue().Nrich__Approved__c  );
                    component.set("v.SubmittedColor", response.getReturnValue().Nrich__Submitted__c);
                    component.set("v.HolidayColor", response.getReturnValue().Nrich__Holiday__c  );
                    component.set("v.LeaveColor", response.getReturnValue().Nrich__Leave__c  );
                    component.set("v.TotalHours",response.getReturnValue().Nrich__TotalHoursCap__c);  
                    component.set("v.Is_Profile_Tile_Required", response.getReturnValue().Nrich__Is_Profile_Tile_Required__c );
                    component.set("v.Is_Achievements_Tile_Required", response.getReturnValue().Nrich__Is_Achievements_Tile_Required__c );
                    component.set("v.Is_Certification_Tile_Required", response.getReturnValue().Nrich__Is_Certification_Tile_Required__c );
                    component.set("v.Is_LeaveChart_Tile_Required", response.getReturnValue().Nrich__Is_LeaveChart_Tile_Require__c );
                    component.set("v.Is_MentorMentee_Tile_Required", response.getReturnValue().Nrich__Is_MentorMentee_Tile_Required__c 	);
                    component.set("v.Is_OrgChart_Tile_Required", response.getReturnValue().Nrich__Is_OrgChart_Tile_Required__c );
                    component.set("v.Is_Asset_Tile_Required", response.getReturnValue().Nrich__Is_Asset_Tile_Required__c );
                    component.set("v.Is_Travel_Request_Tile_Required", response.getReturnValue().Nrich__Is_Travel_Request_Tile_Required__c );
                    component.set("v.Is_Support_Request_Tile_Required", response.getReturnValue().Nrich__Is_Support_Request_Tile_Required__c );
                    component.set("v.Is_Policy_Tile_Required", response.getReturnValue().Nrich__Is_Policy_Tile_Required__c );
                    component.set("v.Due_Days_For_Leave_Request", response.getReturnValue().Nrich__Number_Of_DueDays_For_Leave_Request__c );
                    component.set("v.Due_Days_For_Certification_Request", response.getReturnValue().Nrich__Number_of_DueDays_For_Certification_Req__c );
                    component.set("v.fetchCertificationUserId", response.getReturnValue().Nrich__approver_for_certificationid__c );
                    component.set("v.fetchLeaveRequestUserId",response.getReturnValue().Nrich__approver_for_leaveid__c );
                    component.set("v.fecthAssetUserId",response.getReturnValue().Nrich__Approver_For_AssetId__c  );
                    component.set("v.fetchPolicyUserId",response.getReturnValue().Nrich__Approver_For_PolicyId__c  );
                    component.set("v.CertificationUserId", response.getReturnValue().Nrich__approver_for_certificationid__c );
                    component.set("v.LeaveRequestUserId",response.getReturnValue().Nrich__approver_for_leaveid__c);
                    component.set("v.AssetUserId",response.getReturnValue().Nrich__Approver_For_AssetId__c );
                    component.set("v.PolicyUserId",response.getReturnValue().Nrich__Approver_For_PolicyId__c );
                    component.set("v.Is_TimeSheet_Tile_Required",response.getReturnValue().Nrich__Is_TimeSheet_Tile_Required_c__c );
                    component.set("v.ESignature_for_Policy_Approval",response.getReturnValue().Nrich__ESignature_for_Policy_Approval__c );
                    component.set("v.AutomationforSupportTickets",response.getReturnValue().Nrich__Automatic_Assignment_of_Support_Tickets__c );
                    component.set("v.Is_Project_Tile_Required",response.getReturnValue().Nrich__Is_Project_Tile_Required__c);
                    component.set("v.Is_Expense_Tile_Required",response.getReturnValue().Nrich__Is_Expense_Tile_Required__c);
                    component.set("v.CancelledColor", response.getReturnValue().Nrich__Cancelled_Color__c);
                    component.set("v.CompletedColor", response.getReturnValue().Nrich__Completed_Color__c);
                    component.set("v.ProgressColor", response.getReturnValue().Nrich__Inprogress_Color__c);
                    component.set("v.NewColor", response.getReturnValue().Nrich__New_Color__c);
                    component.set("v.OnholdColor", response.getReturnValue().Nrich__Onhold_Color__c);
                    component.set("v.FixedColor", response.getReturnValue().Nrich__Fixed_Color__c);
                    component.set("v.QAFailedColor", response.getReturnValue().Nrich__QA_Failed_Color__c);
                    component.set("v.QAProgressColor", response.getReturnValue().Nrich__QA_In_Pogress_Color__c);
                    component.set("v.ReadyforBuildColor", response.getReturnValue().Nrich__Ready_for_Build_Color__c);
                    component.set("v.ReadyforDesignColor", response.getReturnValue().Nrich__Ready_for_Design_Color__c);   
                    component.set("v.number1", response.getReturnValue().Nrich__No_of_Days_for_Reapplication_for_SameJob__c);
                    // component.set("v.boolean1", response.getReturnValue().Nrich__Email_Panel_Member_on_Interview_Schedule__c);
                    component.set("v.boolean1",response.getReturnValue().Nrich__Interview_Scheduled_Email_Checkbox__c);
                    component.set("v.boolean2", response.getReturnValue().Nrich__Email_Candidate_on_submission_of_appln__c);
                    component.set("v.boolean3", response.getReturnValue().Nrich__Automated_ApprovalProcess_on_JobCreation__c);
                    component.set("v.boolean4", response.getReturnValue().Nrich__Feedback_for_all_previous_rounds__c);
                    component.set("v.boolean5", response.getReturnValue().Nrich__Success_Email_completion_on_each_Round__c);
                    component.set("v.boolean6", response.getReturnValue().Nrich__Rejection_Email_to_Candidate__c);
                    
                    /* var weekval = $A.get("$Label.c.Weekly");
                    var monthval = $A.get("$Label.c.Monthly");
                    if(response.getReturnValue().Nrich__Timesheet_Type__c!=null && response.getReturnValue().Nrich__Timesheet_Type__c==weekval)
                        component.set("v.selectResult2", true);
                    else if(response.getReturnValue().Nrich__Timesheet_Type__c!=null && response.getReturnValue().Nrich__Timesheet_Type__c==monthval)
                        component.set("v.selectResult1", true);*/
                }
            }
            component.set('v.IsSpinner',false);            
        });
        $A.enqueueAction(action);
        
    },
    save : function(component, event, helper) {
        var action = component.get("c.savedetails");       
        action.setParams({
            "selectResult1":component.get("v.selectResult1"),
            "selectResult2":component.get("v.selectResult2"),
            "Approved":component.get("v.ApprovedColor"),
            "Submitted":component.get("v.SubmittedColor"),
            "Holiday":component.get("v.HolidayColor"),
            "Leave":component.get("v.LeaveColor"),
            "TotalHrs": component.get("v.TotalHours")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var state = response.getState();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Success_Message"),
                    message : $A.get("$Label.c.Saved_Record"),
                    type : 'success',
                    mode : 'dismissible'
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    Empsave:function(component, event, helper){
        var action = component.get("c.EmpSetupsaveDetails");    
        if(component.get("v.CertificationUserId")!=null && component.get("v.CertificationUserId").Id!=undefined){
            component.set("v.fetchCertificationUserId", component.get("v.CertificationUserId").Id);
            
        }
        if(component.get("v.LeaveRequestUserId")!=null && component.get("v.LeaveRequestUserId").Id!=undefined){
            component.set("v.fetchLeaveRequestUserId", component.get("v.LeaveRequestUserId").Id);
        }
        if(component.get("v.AssetUserId")!=null && component.get("v.AssetUserId").Id!=undefined){
            component.set("v.fecthAssetUserId", component.get("v.AssetUserId").Id);
        }
        if(component.get("v.PolicyUserId")!=null && component.get("v.PolicyUserId").Id!=undefined){
            component.set("v.fetchPolicyUserId", component.get("v.PolicyUserId").Id);
        }
        action.setParams({
            "IsProfileTileRequired":component.get("v.Is_Profile_Tile_Required"),
            "IsAchievementsTileRequired":component.get("v.Is_Achievements_Tile_Required"),
            "IsCertificationTileRequired":component.get("v.Is_Certification_Tile_Required"),
            "IsLeaveChartTileRequired":component.get("v.Is_LeaveChart_Tile_Required"),
            "IsMentorMenteeTileRequired":component.get("v.Is_MentorMentee_Tile_Required"),
            "IsOrgChartTileRequired":component.get("v.Is_OrgChart_Tile_Required"),
            "IsAssetTileRequired":component.get("v.Is_Asset_Tile_Required"),
            "IsTravelTileRequired":component.get("v.Is_Travel_Request_Tile_Required"),
            "IsSupportTileRequired":component.get("v.Is_Support_Request_Tile_Required"),
            "IsPolicyTileRequired":component.get("v.Is_Policy_Tile_Required"),
            "CertificationUserId":component.get("v.fetchCertificationUserId"),
            "LeaveRequestUserId":component.get("v.fetchLeaveRequestUserId"),
            "AssetUserId":component.get("v.fecthAssetUserId"),
            "PolicyUserId":component.get("v.fetchPolicyUserId"),
            "esignature" : component.get("v.ESignature_for_Policy_Approval"),
            "IsTimeSheetTilerequired":component.get("v.Is_TimeSheet_Tile_Required"),
            "IsProjectTilerequired":component.get("v.Is_Project_Tile_Required"),
            "IsExpenseTilerequired":component.get("v.Is_Expense_Tile_Required"), 
            "Due_Days_For_Certification_Request":component.get("v.Due_Days_For_Certification_Request"),
            "Due_Days_For_Leave_Request":component.get("v.Due_Days_For_Leave_Request"),
            "AutomaticAssigment":component.get("v.AutomationforSupportTickets")
        });
        action.setCallback(this, function(a){
            var state = a.getState();
            if(state === "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Success_Message"),
                    message:$A.get("$Label.c.Saved_Record"),
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
        
    },
    
    Prjsave:function(component,event,helper){
        var action = component.get("c.PrjInitialSetupSavedetails");
        action.setParams({
            "Completed":component.get("v.CompletedColor"),
            "Cancelled":component.get("v.CancelledColor"),
            "Onhold":component.get("v.OnholdColor"),
            "Progress":component.get("v.ProgressColor"),
            "Open": component.get("v.NewColor"),
            "Fixed": component.get("v.FixedColor"),
            "QAFailed": component.get("v.QAFailedColor"),
            "QAInProgress": component.get("v.QAProgressColor"),
            "ReadyforBuild": component.get("v.ReadyforBuildColor"),
            "ReadyforDesign": component.get("v.ReadyforDesignColor"),
        });
        action.setCallback(this, function(a){
            var state = a.getState();
            if(state ==="SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Success_Message"),
                    message : $A.get("$Label.c.Saved_Record"),
                    type : 'success',
                    mode : 'dismissible'
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);        
    },
    
    inlineEditName : function(component,event,helper){   
        component.set("v.editmode", true);         
    },
    
    recruitsave : function(component, event, helper) {
        var number1 = component.get("v.number1");     
        var boolean1 = component.get("v.boolean1");
        var boolean2 = component.get("v.boolean2");
        var boolean3 = component.get("v.boolean3");
        var boolean4 = component.get("v.boolean4");
        var boolean5 = component.get("v.boolean5");
        var boolean6 = component.get("v.boolean6");        
        var action = component.get("c.saveRecruitmentDetails");
        action.setParams({
            "number1" : number1,
            "boolean1" : boolean1,
            "boolean2" : boolean2,
            "boolean3" : boolean3,
            "boolean4" : boolean4,
            "boolean5" : boolean5,
            "boolean6" : boolean6         
        });
        action.setCallback(this, function(a){
            var state = a.getState();
            if(state === "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Success_Message"),
                    message : $A.get("$Label.c.Saved_Record"),
                    type : 'success',
                    mode : 'dismissible'
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },    
})