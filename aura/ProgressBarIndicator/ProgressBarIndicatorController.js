({
	init : function(component, event, helper) {
       
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.today', today);   
        var recordid = component.get("v.recordId");
        var action = component.get("c.initMethod");       
        action.setParams({
            "projectID":recordid            
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var statusValue= response.getReturnValue();                                     
                component.set("v.mileStoneList",statusValue);
                var stepsData = statusValue;                
                var stepsPreComponents = [[
                    "lightning:progressIndicator",
                    {
                        "currentStep" : component.getReference('v.stage.id')
                    }
                ]];
                    // Add the steps
                    for (var index in stepsData) 
                    {
                        //alert('Value of index ::'+index);
                        var step = stepsData[index];
                        //alert('Value of step.Id ::'+step.Id);
                        var temp = String(step.Index);
                        var temp3 = String(step.Name);
                       component.set("v.temp3",temp3);

                        stepsPreComponents.push([
                            "lightning:progressStep",
                            {
                                "label":temp3,
                                "value":temp
                            }
                        ]);
                    }
                    
                    $A.createComponents(
                                stepsPreComponents,
                                function(components, status, errorMessage){
                                    if (status === "SUCCESS") {
                                        // Remove the father
                                        var progressIndicator = components.shift();
                                        // Add the rest to the father
                                        progressIndicator.set('v.body',components);
                                        // Assign it to the view Attribute
                                        component.set('v.progressIndicator',progressIndicator);
                                    } else {
                                       // alert('Error');
                                    }
                                }
                            ); 
                    
                    	//alert('Value of count'+count);
                    	//count = count-1;
         				//var temp1 = String(count);
                    	//alert('Value of temp1 '+temp1);
         				//component.set('v.stage.id', temp1);

                    	//alert('After the value set ::'+component.get('v.stage.id'));
                }
        });
        $A.enqueueAction(action);
   },
    
   openMilestone : function( component, event, helper ) {      
       var modalBody;
        $A.createComponent("force:recordView", {"recordId":event.getSource().get( "v.name" )},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayval').showCustomModal({
                                       header: $A.get("$Label.c.MilestoneDetails"),
                                       body: modalBody, 
                                       showCloseButton: true,
                                       closeCallback: function() {
                                       }
                                   })
                               }                               
                           });

    },
 
})