({
    doInit : function(component, event, helper) {
        component.set("v.IsSpinner", true);       
        var action2 = component.get("c.fetchCategoryoptions");  
        action2.setCallback(this, function(response2){
            if(response2.getState() === 'SUCCESS'){
                component.set("v.categoryOptions", response2.getReturnValue());
            }
        });
        
        var action3 = component.get("c.fetchPriorityoptions");  
        action3.setCallback(this, function(response2){
            if(response2.getState() === 'SUCCESS'){
                component.set("v.priorityOptions", response2.getReturnValue());
            }
        });
        
        var action = component.get("c.viewSupportRecords");  
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){      
                component.set('v.SupportDetails',response.getReturnValue());
            }
            component.set("v.IsSpinner", false);
        });
        $A.enqueueAction(action);
        $A.enqueueAction(action2);
        $A.enqueueAction(action3);
    },
    
    
    closemodal : function(component, event, helper){
        component.set("v.openmodal", false);
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'Supportcomp'});   
        compEvent.fire(); 
        
    },
    
    fetchstatusval :function(component, event, helper) {
       var action = component.get("c.fetchStatus");
         action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.statuslist", outputresponse);  
                var emplist = component.get("v.SupportDetails");
                var empdisplaylist = [];
                var typeval = component.get("v.TabId");
                alert('typeval' +typeval);
                for(var i=0; i<emplist.length; i=i+1){
                    if(emplist[i].Nrich__Status__c==typeval)
                       empdisplaylist.push(emplist[i]);
                }
                  alert('infetchstat' +JSON.stringify(empdisplaylist));
                component.set("v.SupportDisplayDetails", empdisplaylist);
                component.set("v.IsSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    selectedtab :function(component, event, helper) {
        var emplist = component.get("v.SupportDetails");   
        
        var empdisplaylist = []
        component.set("v.SupportDisplayDetails", empdisplaylist);
        var typeval = event.getParam('id');
        
        component.set("v.TabId", typeval);
        for(var i=0; i<emplist.length; i=i+1){
            if(emplist[i].Nrich__Status__c==typeval)
               empdisplaylist.push(emplist[i]);
        }
        component.set("v.SupportDisplayDetails", empdisplaylist);
        
    },
    insertsupportrecord:function(component, event, helper){
        var buttonName = event.getSource().get("v.name");
        var insertiondetails=component.get("v.InsertSupportdetails");
        
        if(buttonName==$A.get("$Label.c.Save")){
            helper.validateFields(component,event)
            var condition=component.get("v.Ifcondition");
            if(condition==true) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Error_Message"),
                    message:$A.get("$Label.c.Errror_In_Required_Fields"),
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            }
            if(condition==false){
                var action=component.get("c.insertnewsupportrecords");
                action.setParams({'supportdetails':insertiondetails});
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS"){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : $A.get("$Label.c.Success_Message"),
                            message: $A.get("$Label.c.Saved_Record"),
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'success',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();
                    }
                });
                $A.enqueueAction(action);
                component.set("v.openmodal", false);
                var compEvent = component.getEvent("closemodaleventval");
                compEvent.setParams({"openclosemodal" : 'false',
                                     "typeofmodal" : 'Supportcomp'});   
                compEvent.fire(); 
                $A.get('e.force:refreshView').fire();
            }
        }
    },
    goToRecord : function(component, event, helper) {
        var recId = event.target.id;
        if(recId != undefined){        
            window.open('/' + recId,'_blank');
        }
        
    },
    
    
    
})