({
    hideSpinner : function( component ) {
        var eleSpinner = component.find( "spinner" );
        
        $A.util.addClass( eleSpinner, "slds-hide" );
    },
    showSpinner : function( component ) {
        var eleSpinner = component.find( "spinner" );
        
        $A.util.removeClass( eleSpinner, "slds-hide" );
    },
    
    initHelper : function(component, event, helper){
        helper.showSpinner(component);
        var action2 = component.get("c.GetDetails");           
        action2.setCallback(this, function(response){
            var state = response.getState();
             //alert('state' +state);
            if(state ==="SUCCESS"){    
              // alert('response' +JSON.stringify(response.getReturnValue()));
                if(component.isValid() && response !== null && response.getState() === 'SUCCESS')
                {
                    component.set("v.CancelledColor", response.getReturnValue().Nrich__Cancelled_Color__c );
                    component.set("v.CompletedColor", response.getReturnValue().Nrich__Completed_Color__c );
                    component.set("v.ProgressColor", response.getReturnValue().Nrich__Inprogress_Color__c );
                    component.set("v.NewColor", response.getReturnValue().Nrich__New_Color__c);
                    component.set("v.OnholdColor", response.getReturnValue().Nrich__Onhold_Color__c );
                    component.set("v.FixedColor", response.getReturnValue().Nrich__Fixed_Color__c );
                    component.set("v.QAFailedColor", response.getReturnValue().Nrich__QA_Failed_Color__c );
                    component.set("v.QAProgressColor", response.getReturnValue().Nrich__QA_In_Pogress_Color__c	);
                    component.set("v.ReadyforBuildColor", response.getReturnValue().Nrich__Ready_for_Build_Color__c );
                    component.set("v.ReadyforDesignColor", response.getReturnValue().Nrich__Ready_for_Design_Color__c);

                    
                }
            }
            component.set('v.IsSpinner',false);            
        });
        $A.enqueueAction(action2);
        
        var sprintIdval = '';
        var userstoryIdval = '';
        if(component.get("v.selectedSprint")!=null && component.get("v.selectedSprint").Id!=null)
            sprintIdval = component.get("v.selectedSprint").Id;
        if(component.get("v.selectedUserStory")!=null && component.get("v.selectedUserStory").Id!=null)
            userstoryIdval = component.get("v.selectedUserStory").Id;
        var action = component.get( "c.getSprintUserstorydetails" );
        action.setParams({
            "ProjectID": component.get("v.recordId"),
            "SprintId": sprintIdval,
            "UserStoryId": userstoryIdval,
            "Startdate": component.get("v.Startdate"),
            "Enddate": component.get("v.Enddate")
        });
        component.set( "v.scriptsLoaded", true );
        
        action.setCallback( this, function( response ) {
            var state = response.getState();
            
            if ( state === "SUCCESS" ) {
                var result = response.getReturnValue();
				component.set("v.sprintUserstoryList", result);
                if(component.get( "v.sprinthandle"))
                    component.set( "v.sortableApplied", false);
                helper.hideSpinner(component);
            }
            else {
                helper.hideSpinner(component);
                helper.showToast( 
                    {
                        "title"		: $A.get("$Label.c.Error"), 
                        "message"	: $A.get("$Label.c.Error") + JSON.stringify( response.getError() ) + ", State: " + state, 
                        "isSuccess"	: "error",
                        "type" : "Error"
                    } 
                );
                
            }
        } );
        
        $A.enqueueAction( action );
    },
    showToast : function( data ) {
        var toastEvent = $A.get( "e.force:showToast" );
                                
        toastEvent.setParams(
            {
                duration	: 2000,
                title		: data.title,
                message		: data.message,
                type		: data.type ? data.type : (data.isSuccess ? "success" : "error")
            }
        );
        
        toastEvent.fire();
    },
    applySortable : function( component ) {     
        var helper = this;
        
       	jQuery( ".slds-lane" ).sortable(
            {
                revert				: true,
                connectWith			: ".slds-lane",
                handle 				: ".slds-title",
                placeholder 		: "slds-item slds-m-around--small slds-item-placeholder"
            }
		);
        
        jQuery( ".slds-lane" ).on(
            "sortstart",
            $A.getCallback(
                function( event, ui ) {                    
                    jQuery( ui.item ).addClass( "moving-card" );                   
                }
            )
        );
        
		jQuery( ".slds-lane" ).on(
            "sortstop",
            $A.getCallback(
                function( event, ui ) {                    
                    jQuery( ui.item ).removeClass( "moving-card" );                    
                    
                    var leadId       		= $( ui.item ).data( "id" );
                    var oldLeadStatus 		= $( ui.item ).data( "status" );
                    var newLeadStatus   	= $( ui.item ).parent().data( "name" );
                     helper.showSpinner( component );
                        
                        var action = component.get( "c.UpdateUserStory" );
                        var params = {
                            "userStoryId" 		: leadId,
                            "sprintId" : newLeadStatus
                        };
                        
                        /**
                         * Maintain the ordering within
                         * the lane.
                         */
                      /*  $( ui.item ).parent().children().each(
                            function() {
                                params.ordering.push( $( this ).data( "id" ) );
                            }
                        );*/
                        
                        action.setParams( params );
                        
                        action.setCallback( 
                            this, 
                            function( response ) {
                                var state = response.getState();
                                
                                helper.hideSpinner( component );
                                
                                if( state === "SUCCESS" ) {                                    
                                    var updateStatus = response.getReturnValue();
                                    
                                    /**
                                     * Show a separate message
                                     * if the cards were just
                                     * re-arranged within the
                                     * same column.
                                     */                                    
                                   if( oldLeadStatus === newLeadStatus ) {
                                        updateStatus.isSuccess  = true;
                                        updateStatus.message	= $A.get("$Label.c.SprintKanbanMessage");
                                    }
                                    
                                    $( ui.item ).attr( "data-status", newLeadStatus );
                                    
                                    helper.showToast( updateStatus );
                                    if(!updateStatus.isSuccess){
                                       component.set( "v.sprinthandle", true );
                                       helper.initHelper(component, event, helper);
                                    }
                                }                                
                            }
                        );
                        
                        $A.enqueueAction( action );

                }
            )
        );
    }
})