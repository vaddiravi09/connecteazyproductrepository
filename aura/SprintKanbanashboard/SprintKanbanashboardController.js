( {
    init : function( component, event, helper ) {
       // document.getElementsByClassName("slds-tabs--path slds-is-incomplete").setAttribute("background-color", "black");
        helper.initHelper(component, event, helper);
    },    
    showBacklog : function( component, event, helper ) {
        if(!component.get("v.backlogvisible"))
            component.set("v.backlogvisible", true);
        else
            component.set("v.backlogvisible", false);
    },    
    handleComponentEvent : function( component, event, helper ) {
        component.set( "v.sprinthandle", true );
        helper.initHelper(component, event, helper);
    },    
    applySortable : function( component, event, helper ) {        
        var sortableApplied = component.get( "v.sortableApplied" );
        var scriptsLoaded 	= component.get( "v.scriptsLoaded" );
        /**
         * Apply the jQuery Sortable
         * when the DOM is ready and 
         * the Scripts have been loaded.
         */
        if( scriptsLoaded 	&& 
           !sortableApplied && 
           jQuery( ".slds-lane" ).length > 0
		) {
            component.set( "v.sortableApplied", true );
            
            helper.applySortable( component );
        }
    },
    createRecord : function (component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Nrich__User_Story__c"
        });
        createRecordEvent.fire();
   },
    editUserStory : function( component, event, helper ) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
             "recordId": event.getSource().get( "v.name" )
       });
        editRecordEvent.fire();
    },
    movetoBacklog : function( component, event, helper ) {
        helper.showSpinner( component );
        var action = component.get( "c.UpdateUserStory" );
        var params = {
            "userStoryId" 		: event.getSource().get( "v.name" )
        };
         action.setParams( params );
                        
        action.setCallback( this, function( response ) {
                var state = response.getState();
                
                if( state === "SUCCESS" ) {                                    
                    var updateStatus = response.getReturnValue();
                    helper.showToast( updateStatus );
                    component.set( "v.sprinthandle", true );
                    helper.initHelper(component, event, helper);
                  }
            }
         );        
        $A.enqueueAction( action );
        
    },
     
} )