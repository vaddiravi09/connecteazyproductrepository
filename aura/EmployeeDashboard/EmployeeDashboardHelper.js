({
    
    fetchEmployeeDetails : function(component,event){
        
        var action = component.get("c.fetchEmployeeDetails1");
        action.setCallback(this, function(response){
            
            if(response.getState() === 'SUCCESS'){
                component.set("v.wrapperList", response.getReturnValue());
                var wrap = response.getReturnValue();
                var menteeslength=component.get("v.wrapperList").lstMentees.length;
                if(menteeslength==null || menteeslength==0)
                {
                    component.set("v.nomentees",true);
                }
                if(!wrap.objEmployeeDetails.Nrich__ProfilePic__c.includes($A.get("$Label.c.EmployeeDasboardImage")))
                    component.set("v.noimage", true);
            }
        });
        $A.enqueueAction(action);
        
        
    },
    
    
    fetchCertificationDetails:function(component,event){
        var alist=[];
        var completedlist=[];
        var pendinglist=[];
        var approvedlist=[];
        var approvedntsubmit=[];
        var action= component.get("c.viewrecords1");
        
        action.setCallback(this, function(a){
            
            var state = a.getState();
            
            if (state === "SUCCESS"){
                
                alist=a.getReturnValue();
                
                var i=0;
                for( alist[i];i<alist.length;i=i+1)
                {
                    
                    if(alist[i].Nrich__Approval_Status__c ==$A.get("$Label.c.Approved") &&  (alist[i].Nrich__Result__c!=$A.get("$Label.c.Pass") || alist[i].Nrich__Result__c!=$A.get("$Label.c.Fail")))
                    {
                        approvedlist.push(alist[i]);
                    }
                    
                    else if(((alist[i].Nrich__Approval_Status__c ==$A.get("$Label.c.Applied")) ||(alist[i].Nrich__Approval_Status__c ==$A.get("$Label.c.Pending"))) && alist[i].Nrich__Approval_Status__c!=$A.get("$Label.c.Approved") && alist[i].Nrich__Result__c!=$A.get("$Label.c.Pass"))
                    {
                        
                        pendinglist.push(alist[i]);
                    }
                    
                    if(alist[i].Nrich__Result__c==$A.get("$Label.c.Pass") )
                    {
                        completedlist.push(alist[i]); 
                    } 
                }
                component.set('v.Completedcount',completedlist.length);
                component.set('v.Approvedcount',approvedlist.length); 
                component.set('v.Pendingcount',pendinglist.length); 
            }
        })
        $A.enqueueAction(action); 
    },
    
    
    
    fetchAssetDetails:function(component,event){
        var alist=[];
        var allocatedlist=[];
        var action = component.get("c.viewAssetRecords");
        
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var i=0;
                alist=response.getReturnValue();
                for(alist[i];i<alist.length;i=i+1)
                {
                    if(alist[i].Nrich__Status__c ==$A.get("$Label.c.Allocated"))
                    {
                        allocatedlist.push(alist[i]) ;
                    }
                }
                component.set("v.ViewAssetallocateddetails",allocatedlist);
            }
        });
        $A.enqueueAction(action);        
    },
    
    fetchAssetAllocatedDetails:function(component,event){
        var pendinglist=[];
        var allocatedlist=[];
        var alist=[];
        var action = component.get("c.viewAssetRecords");  
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var i=0;
                alist=response.getReturnValue();
                for(alist[i];i<alist.length;i=i+1)
                {
                    if(alist[i].Nrich__Status__c ==$A.get("$Label.c.Allocated"))
                    {
                        allocatedlist.push(alist[i]) ;
                    }
                }
                component.set('v.AllocatedPendingCount',allocatedlist.length);
                
            }
        });
        $A.enqueueAction(action);   
        
    },
    
    fetchSupportDetails:function(component,event){
        var alist=[];
        var Completedlist=[];
        var Pendinglist=[];
        var newlist=[];
        var RequestforMorelist=[];
        var action = component.get("c.viewSupportRecords");  
        
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var i=0;
                alist=response.getReturnValue();
                for(alist[i];i<alist.length;i=i+1)
                {
                    if (alist[i].Nrich__Status__c ===$A.get("$Label.c.Pending"))
                    {
                        Pendinglist.push(alist[i]);
                    }
                    if(alist[i].Nrich__Status__c===$A.get("$Label.c.Completed"))
                    {
                        Completedlist.push(alist[i]);
                    }
                    if(alist[i].Nrich__Status__c===$A.get("$Label.c.More_Info_Required"))
                    {
                        RequestforMorelist.push(alist[i]);
                    }
                }
                
                component.set('v.SupportPendingDetailsCount',Pendinglist.length);
                component.set('v.SupportCompletedDetailsCount',Completedlist.length);
                component.set('v.SupportRequestForMoreDetailsCount',RequestforMorelist.length)
                
            }
        });
        $A.enqueueAction(action);
        
        
    },
    
    fetchExpenseDetails:function(component,event){
        var alist=[];
        var Approvedlist=[];
        var Requestedlist=[];
        var newlist=[];
        var Moreinfolist=[];
        
        var action = component.get("c.ViewExpenserecords");  
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var i=0;
                alist=response.getReturnValue();
                
               
                for(alist[i];i<alist.length;i=i+1)
                {
                    if (alist[i].Nrich__Expense_Status__c  ===$A.get("$Label.c.Approved"))
                    {
                        Approvedlist.push(alist[i]);
                       
                    }
                    if(alist[i].Nrich__Expense_Status__c ===$A.get("$Label.c.Requested"))
                    {
                        Requestedlist.push(alist[i]);
                    }
                    if(alist[i].Nrich__Expense_Status__c ==='Need More Details')
                    {
                        Moreinfolist.push(alist[i]);
                    }
                }
               
                
                component.set('v.ApprovedExpenseDetailsCount',Approvedlist.length);
                component.set('v.RequestedExpenseDetailsCount',Requestedlist.length);
                component.set('v.MoreInfoExpenseDetailsCount',Moreinfolist.length)
                
            }
        });
        $A.enqueueAction(action);
        
        
        
        
    },
    
    /*   fetchTimeSheetDetails:function(component,event){
        var alist=[];
        var Approvedlist=[];
        var Pendinglist=[];
        var Rejectedlist=[];
        
        var action = component.get("c.ViewTimeSheetDetails");  
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var i=0;
                alist=response.getReturnValue();
                
                for(alist[i];i<alist.length;i=i+1)
                {
                    if (alist[i].Nrich__Status__c ==$A.get("$Label.c.Approved"))
                    {
                        Approvedlist.push(alist[i]);
                    }
                    if(alist[i].Nrich__Status__c==$A.get("$Label.c.Rejected"))
                    {
                        Rejectedlist.push(alist[i]);
                    }
                    if(alist[i].Nrich__Status__c==$A.get("$Label.c.Submitted_For_Approval"))
                    {
                        Pendinglist.push(alist[i]);
                    }
                }
                
                component.set('v.TimeSheetApprovedCount',Approvedlist.length);
                component.set('v.TimeSheetRejectedCount',Rejectedlist.length);
                component.set('v.TimeSheetPendingCount',Pendinglist.length)
                
            }
        });
        $A.enqueueAction(action); 
    },*/
    fetchProjectDetails:function(component,event){
        component.set("v.IsSpinner", true);
        var action10 = component.get("c.ViewProjectteammemberDetails");  
        action10.setCallback(this, function(response10){
            if(response10.getState() === 'SUCCESS'){
                
                var alist=response10.getReturnValue();
                component.set('v.CurrentProjectDetailsCount',alist.CurrentProjectteamList.length);
                component.set('v.PastProjectDetailsCount',alist.PastProjectteamList.length);
                component.set('v.OwnedByMeProjectDetailsCount',alist.OwnProjectteamList.length);
                
                component.set("v.IsSpinner", false);
            }
            
        });
        $A.enqueueAction(action10);
        
    },
    
    
})