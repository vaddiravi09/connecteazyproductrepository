({
    doInit : function(component, event, helper) {
        
        var action = component.get("c.fetchDetails");
        var v=component.find("inputId1");
        
        action.setCallback(this, function(response){
           
            if(response.getState() =='SUCCESS'){
                component.set("v.IsSpinner",false);
                component.set("v.accounts", response.getReturnValue());                 
            }
        });
        
        $A.enqueueAction(action); 
        helper.fetchAssetAllocatedDetails(component,event);
        helper.fetchAssetDetails(component,event);
        helper.fetchSupportDetails(component,event);
        helper.fetchEmployeeDetails(component, event);
        helper.fetchExpenseDetails(component,event);
       
        
        window.verticalScroller2 = function($elem) {
            var top = parseInt($elem.css("top2"));
            var temp = -1 * $('#verticalScroller2 >table').height();
            if(top < temp) {
                top = $('#verticalScroller2').height()
                $elem.css("top2", top);
            }
            $elem.animate({ top: (parseInt(top)-40) }, 1400, function () {
                window.verticalScroller2($(this))
            });
        }
        
        $(document).ready(function() {
            var i = 0;
            $("#verticalScroller2 >table").last().addClass("last2");
            $("#verticalScroller2 >table").each(function() {
                var $this = $(this);
                $this.css("top2", i);
                i += $this.height();
                doScroll2($this);
                $this.mouseover(function() {
                    $("#verticalScroller2 >table").each(function() {
                        $(this).stop();
                    });
                }).mouseout(function() {
                    $("#verticalScroller2 >table").each(function() {
                        doScroll2($(this));
                    });
                });
            });
        });
        
        function doScroll($ele) {
            var top = parseInt($ele.css("top"));
            if (top < -80) { //bit arbitrary!
                var $lastEle = $(".last");
                $lastEle.removeClass("last");
                $ele.addClass("last");
                var top = (parseInt($lastEle.css("top")) + $lastEle.height());
                $ele.css("top", top);
            }
            $ele.animate({
                top: (parseInt(top) - 60)
            }, 1500, 'linear', function() {
                doScroll($(this))
            });
        } 
        
        function doScroll2($ele) {
            var top = parseInt($ele.css("top2"));
            if (top < -80) { //bit arbitrary!
                var $lastEle = $(".last2");
                $lastEle.removeClass("last2");
                $ele.addClass("last2");
                var top = (parseInt($lastEle.css("top2")) + $lastEle.height());
                $ele.css("top2", top);
            }
            $ele.animate({
                top: (parseInt(top) - 100)
            }, 1500, 'linear', function() {
                doScroll($(this))
            });
        } 
        
    },
    
    getcustomsettingdata : function(component, event) {
        var action = component.get("c.fetchcustomSetting"); 
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){                
                component.set("v.customsettingrec", response.getReturnValue());
            }
        });
        
        $A.enqueueAction(action);
    },
    fetchProjectrecords : function(component, event, helper) {
        helper.fetchProjectDetails(component,event);
    },
    
    fetchCertificationRecords:function(component, event, helper){
         helper.fetchCertificationDetails(component, event);
    },
    
    fetchTimeSheetRecords:function(component, event, helper){
        
    	var alist=[];
        var Approvedlist=[];
        var Pendinglist=[];
        var Rejectedlist=[];
        
        var action = component.get("c.ViewTimeSheetDetails");  
        action.setCallback(this, function(timesheetresponse){
           
            if(timesheetresponse.getState() === 'SUCCESS'){
                var i=0;
                alist=timesheetresponse.getReturnValue();
                
                for(alist[i];i<alist.length;i=i+1)
                {
                    if (alist[i].Nrich__Status__c ==$A.get("$Label.c.Approved"))
                    {
                        Approvedlist.push(alist[i]);
                    }
                    if(alist[i].Nrich__Status__c==$A.get("$Label.c.Rejected"))
                    {
                        Rejectedlist.push(alist[i]);
                    }
                    if(alist[i].Nrich__Status__c==$A.get("$Label.c.Submitted_For_Approval"))
                    {
                        Pendinglist.push(alist[i]);
                    }
                }
                
                component.set('v.TimeSheetApprovedCount',Approvedlist.length);
                component.set('v.TimeSheetRejectedCount',Rejectedlist.length);
                component.set('v.TimeSheetPendingCount',Pendinglist.length)
                
            }
        });
        $A.enqueueAction(action); 
    },
    
    
    
    fetchTravelRequestrecords : function(component, event) {
        var action = component.get("c.fetchTravelRequestMethod"); 
        action.setCallback(this, function(response){
            if(response.getState() ==='SUCCESS'){                
                var travellist = response.getReturnValue();
                
                component.set("v.totaltraveldetails", travellist);
                var approvedlist = component.get("v.Viewapprovedtraveldetails");
                var processedlist = component.get("v.ViewProcessedtraveldetails");
                var rejectedlist = component.get("v.ViewRejectedtraveldetails");
                for(var i=0; i<travellist.length; i=i+1){
                    if(travellist[i].Nrich__Status__c===$A.get("$Label.c.Approved"))
                        approvedlist.push(travellist[i]);
                    else if(travellist[i].Nrich__Status__c===$A.get("$Label.c.Processed"))
                        processedlist.push(travellist[i]);
                        else if(travellist[i].Nrich__Status__c===$A.get("$Label.c.Rejected"))
                            rejectedlist.push(travellist[i]);
                }
                
                component.set("v.Viewapprovedtraveldetails", approvedlist);
                component.set("v.ViewProcessedtraveldetails", processedlist);
                component.set("v.ViewRejectedtraveldetails", rejectedlist);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    fetchEmployeePolicyrecords : function(component, event) {
        var action = component.get("c.fetchEmployeePolicyMethod"); 
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){                
                var travellist = response.getReturnValue();
                var approvedlist = component.get("v.ViewAcceptedpolicydetails");
                var pendingpolicylist = component.get("v.ViewPendingpolicydetails");
                var rejectedlist = component.get("v.ViewRejectedpolicydetails");
                for(var i=0; i<travellist.length; i=i+1){
                    if(travellist[i].Nrich__status__c===$A.get("$Label.c.Accepted"))
                        approvedlist.push(travellist[i]);
                    else if(travellist[i].Nrich__status__c===$A.get("$Label.c.Pending"))
                        pendingpolicylist.push(travellist[i]);
                        else if(travellist[i].Nrich__status__c===$A.get("$Label.c.Rejected"))
                            rejectedlist.push(travellist[i]);
                }
                component.set("v.ViewAcceptedpolicydetails", approvedlist);
                component.set("v.ViewPendingpolicydetails", pendingpolicylist);
                component.set("v.ViewRejectedpolicydetails", rejectedlist);
            }
        });
        
        $A.enqueueAction(action);
    },
 
    //Code for Profile End
    openAnnouncement : function( component, event, helper ) {   
        var modalBody;
        $A.createComponent("force:recordView", {"recordId":event.target.id},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayval').showCustomModal({
                                       header: $A.get("$Label.c.AnnouncementDetails"),
                                       body: modalBody, 
                                       showCloseButton: true,
                                       closeCallback: function() {
                                       }
                                   })
                               }                               
                           });
        
    },
    //Code for Navigation to Record Page Start
    goToRecord : function(component, event, helper) {
        var recId = event.target.id;
        //  alert('recId' +recId);
        if(recId != undefined){        
            window.open('/' + recId,'_blank');
        }      
    },
    //Code for Navigation to Record Page End
    
    //code for Certificate
    openinsertionmodal:function(component, event, helper){
        component.set("v.isOpen", true);
    },
    
    opensupportreqmodal:function(component, event, helper){
        component.set("v.isOpensupport", true);
    },
    openexpensemodal:function(component, event, helper){
        component.set("v.expenseInsert",true);
    },
    opentravelinsertmodal:function(component, event, helper){
        component.set("v.travelRequestInsert",true);
    },
    openassetinsertmodal:function(component, event, helper){
        component.set("v.isopenasset",true);
    },
    
    openachivementmodal:function(component, event, helper){
        component.set("v.isOpenAchievement", true);
    },
    closeachivementmodal:function(component, event, helper){
        component.set("v.isOpenAchievement", false);
    },
    opentodomodal:function(component, event, helper){
        component.set("v.isOpentodolist", true);
    },
    openspecifictodomodal:function(component, event, helper){
        var recId = event.target.id;
        var todolist = component.get("v.todorecords");
        var listval = [];
        for(var i=0; i<todolist.length; i=i+1){
            if(todolist[i].Id===recId)
                listval.push(todolist[i]);
        }
        component.set("v.todospecificrecord", listval);
        component.set("v.isOpenspecifictdolist", true);
    },
    closetodomodal:function(component, event, helper){
        component.set("v.isOpentodolist", false);
    },
    openinsertionleavemodal:function(component, event, helper){
        component.set("v.isOpenleaverequest", true);
    }, 
    openobtainedleavemodal:function(component, event, helper){
        component.set("v.isOpenobtainedrequest", true);
    }, 
    openavailedleavemodal:function(component, event, helper){
        component.set("v.isOpenavailedrequest", true);
    }, 
    openremainingleavemodal:function(component, event, helper){
        component.set("v.isOpenremainingrequest", true);
    }, 
    closeinsertionmodal:function(component, event, helper){
        component.set("v.isOpen", false);
    },
    openemppolicypendingmodal:function(component, event, helper){
        component.set("v.employeepending", true);
    }, 
    openemppolicyacceptedmodal:function(component, event, helper){
        component.set("v.employeeaccepted", true);
    }, 
    openemppolicyrejectedmodal:function(component, event, helper){
        component.set("v.employeerejected", true);
    }, 
    opentravelApprovedmodal:function(component, event, helper){
        component.set("v.travelRequestApproved", true);
    }, 
    opentravelRejectedmodal:function(component, event, helper){
        component.set("v.travelRequestRejected", true);
    }, 
    opentravelProcessedmodal:function(component, event, helper){
        component.set("v.travelRequestProcessed", true);
    }, 
    OpenTimeSheetDetailsModal:function(component, event, helper){
        var idx= event.currentTarget.dataset.row;
        component.set("v.TabId",idx);
        
        component.set("v.OpenTimeSheetTable",true);
    },
    
    OpenProjectDetailsModal:function(component, event, helper){
        var idx= event.currentTarget.dataset.row;
        component.set("v.TabId",idx);
        component.set("v.OpenProjectDetailsTable" ,true);
    },
    
    OpenExpenseDetailsModal:function(component,event,helper){
        var idx=event.currentTarget.dataset.row;
        alert('idx' +idx);
         component.set("v.TabId",idx);
        component.set("v.OpenExpenseDetailsTable",true);
        alert("madoal" +component.get("v.OpenExpenseDetailsTable"));
    },
    
    showempinfo : function(component, event, helper){
        console.log('showempinfo success');
        var cmpTarget=component.find('empinfo');
        $A.util.removeClass(cmpTarget, 'slds-fall-into-ground');
        $A.util.addClass(cmpTarget, 'slds-rise-from-ground');
    },
    hideempinfo : function(component, event, helper){
        console.log('hideempinfo success');
        var cmpTarget=component.find('empinfo');
        $A.util.removeClass(cmpTarget, 'slds-rise-from-ground');
        $A.util.addClass(cmpTarget, 'slds-fall-into-ground');
    },
    
    getMenteesDetails : function(component, event, helper){
        component.set("v.isMenteesPopup", true);
    },
    closeMenteesModel: function(component, event, helper){
        component.set("v.isMenteesPopup", false);
    },
    
    openProfileModel: function(component, event, helper) {
        component.set("v.isProfilePopup", true);
    },
    
    closeProfileModel: function(component, event, helper) {
        component.set("v.isProfilePopup", false);
    },
    CloseCertificationsModel : function(component, event, helper) {
        component.set("v.isCerificationPopup", false);
    },
    openCertificationsModel: function(component, event, helper) {
        component.set("v.isCerificationPopup", true);
    },
    
    handleComponentEvent : function(component, event, helper) {
        
        // get the selected Account record from the COMPONENT event 	 
        var selectedCertificationGetFromEvent = event.getParam("CertificationByEvent");
        //  alert("select cer "+JSON.stringify(selectedCertificationGetFromEvent));
        component.set("v.SelectedCertification",selectedCertificationGetFromEvent);
    },
    
    handleAssetEvent:function(component, event, helper){
        var selectedCertificationGetFromEvent = event.getParam("AssetByEvent");
        //  alert("select cer "+JSON.stringify(selectedCertificationGetFromEvent));
        component.set("v.SelectedAsset",selectedCertificationGetFromEvent);
        
    },
    closemodaleventmethod :  function(component, event, helper) {
        var typeofevent  = event.getParam("typeofmodal");
        var openclose  = event.getParam("openclosemodal");
        if(typeofevent==="LeaveRequest"){
            component.set("v.isOpenleaverequest", openclose);
            component.set("v.isOpenobtainedrequest", openclose);
            component.set("v.isOpenavailedrequest", openclose);
            component.set("v.isOpenremainingrequest", openclose);
        }        
        else if(typeofevent==="ProfileComp")
        {
            component.set("v.isProfilePopup",openclose);
        }  
            else if(typeofevent==="travelRequest"){
                component.set("v.travelRequestInsert",openclose); 
                component.set("v.travelRequestRejected",openclose);
                component.set("v.travelRequestApproved",openclose);
                component.set("v.travelRequestProcessed",openclose);
            }
                else if(typeofevent==="policy"){
                    component.set("v.employeerejected",openclose);
                    component.set("v.employeeaccepted",openclose);
                    component.set("v.employeepending",openclose);
                }
        
                    else if(typeofevent==="Assetcomp"){
                        component.set("v.isOpenAssetPending",openclose); 
                        component.set("v.isopenasset",openclose);
                    }
        
                        else if(typeofevent==="Supportcomp") 
                        {
                            component.set("v.isOpensupport",openclose);
                            component.set("v.isOpenSupportDetails",openclose);
                            
                        }
                            else if(typeofevent==="Certificationcomp")
                            {
                                component.set("v.isOpenCompleted",openclose);
                                component.set("v.isOpenapproved",openclose);
                                component.set("v.isOpenpending",openclose);
                                component.set("v.isOpen",openclose);
                            }
                                else if(typeofevent==="achivementcomp")
                                {
                                    component.set("v.isOpenAchievement",openclose);
                                }
                                    else if(typeofevent==="todolistcomp"){
                                        component.set("v.isOpentodolist",openclose);
                                        component.set("v.isOpenspecifictdolist", openclose);
                                    }
                                        else if (typeofevent==='TimeSheetcomp'){
                                            component.set("v.OpenTimeSheetTable",openclose); 
                                        }
                                            else if (typeofevent==='Projectcomp'){
                                                component.set("v.OpenProjectDetailsTable",openclose); 
                                            }
                                                else if(typeofevent==='ExpenseModal'){
                                                    component.set("v.expenseInsert",openclose);
                                                     component.set("v.OpenExpenseDetailsTable",openclose);
                                                }
        
        
        
    },
    opencompletedmodal: function(component, event, helper) {
        component.set("v.isOpenCompleted", true);
        var completed=component.get("v.Completedcount");
        if(completed<=0)
        {
            component.set("v.displaynull",true); 
        }
        
    },
    
    OpenSupportDetailsModal:function(component, event, helper) {
        // var idx = event.target.id; 
        var idx= event.currentTarget.dataset.row;
        component.set("v.TabId",idx);        
        component.set("v.isOpenSupportDetails", true);
       
    },
    
    closemodal : function(component, event, helper){
        component.set("v.isOpenCompleted", false);
    },
    openapprovedmodal:function(component, event, helper) {
        component.set("v.isOpenapproved", true);
        var approved=component.get("v.Approvedcount");
        if(approved<=0)
        {
            component.set("v.displaynull",true); 
            
        }
        
        
    },
    openassetpending:function(component, event, helper){
        component.set("v.isOpenAssetPending",true);
        var pending=component.get("v.AssetPendingCount");
        if(pending<=0)
        {
            component.set("v.displaynull",true); 
            
        }
    },
    
    closeapprovedmodal:function(component, event, helper){
        component.set("v.isOpenapproved", false);
    },
    openpendingmodal:function(component, event, helper) {
        component.set("v.isOpenpending", true);
        var pending=component.get("v.Pendingcount");
        
        if(pending<=0)
        {
            component.set("v.displaynull",true); 
        }
        
    },
    closependingmodal:function(component, event, helper){
        component.set("v.isOpenpending", false);
    },
    fetchLeaveCardData: function(component, event, helper){
        var action = component.get("c.fetchEmployeeLeaveCardDetails");
        action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.employeeleavecard", outputresponse);
            }
        });
        $A.enqueueAction(action);
    },
    fetchToDoList : function(component, event, helper){
        var action = component.get("c.fetchToDoListMethod");
        action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.todorecords", outputresponse);
            }
        });
        $A.enqueueAction(action);
    },
    fetchOrgChart : function(component, event, helper){
        var action = component.get("c.getOrgChartData");
        action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                
                component.set("v.orgChartval", outputresponse);
            }
        });
        $A.enqueueAction(action);
    },
    fetchAchivementrecords : function(component, event, helper){
        var action = component.get("c.fetchAchievements");
        action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.totalachivementslist", outputresponse);
                var listval = [];
                if(outputresponse.length>3){
                    for(var i=0; i<3; i=i+1){
                        listval.push(outputresponse[i]);                  
                    }
                    component.set("v.achivementslist", listval);
                }
                else
                    component.set("v.achivementslist", outputresponse);
            }
        });
        $A.enqueueAction(action);
    },
    navigatetotimesheet:function(component, event, helper){
        var hostname = window.location.hostname;
        var arr = hostname.split(".");
        var instance = arr[0];
        window.open('https://'+instance+$A.get("$Label.c.TimeSheetDashboard_URL_Link"));
        $A.get('e.force:refreshView').fire();
    },
   })