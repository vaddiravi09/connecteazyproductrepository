({
    doInit: function( component, event, helper ) {
    	
        // Get the list of the products from the source opportunity
        var action = component.get("c.fetchTemplateDetails");
        action.setParams({
            "templateId": component.get("v.recordId")	
        });
        
        action.setCallback( this, function( response ) { 
        	var state = response.getState();
            console.log(state);
			if (state === "SUCCESS") {
                console.log( response.getReturnValue() );
				component.set( "v.template", response.getReturnValue() );
                if(component.get("v.template").Nrich__Type_of_Template__c==$A.get("$Label.c.Feedback"))
                    component.set("v.jobRoundCheck", true);
                	component.set("v.isOpen", true);
            }
        } );
        
        $A.enqueueAction(action);
    },
      closeModel: function(component, event, helper) {
      component.set("v.isOpen", false);
   },
 
   saveandclose: function(component, event, helper) {
       helper.show(component,event);
       var templaterecord = component.get("v.template");
       var job_Error_Message = $A.get("$Label.c.Job_Error_Message");
       var Job_Round_Error_Message = $A.get("$Label.c.Job_Round_Error_Message");
        
       var jobField = component.get("v.selectedLookUpRecord");
       var jobRoundField = component.get("v.selectedJobRound");
        
       if(jobField.Id==undefined || jobField.Id==''){
                helper.hide(component,event);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : errorMsg,
                    message:job_Error_Message,
                    messageTemplate: job_Error_Message,
                    duration:'5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            }else if(templaterecord.Nrich__Type_of_Template__c==$A.get("$Label.c.Feedback") && (jobRoundField.Id==undefined || jobRoundField.Id=='')){
                  helper.hide(component,event);  
                  var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : errorMsg,
                        message: Job_Round_Error_Message,
                        messageTemplate: Job_Round_Error_Message,
                        duration:'5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
              }
       
      var action = component.get("c.cloneTemplate");
        action.setParams({
            "templateId": component.get("v.recordId"),
            "templateName":component.get("v.templateName"),
            "jobId":jobField.Id,
            "jobRoundId": jobRoundField.Id,
            "isActive":component.get("v.isActive")
        });
        
        action.setCallback( this, function( response ) { 
        	var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                helper.hide(component,event);
                var templateId = response.getReturnValue();
                if(templateId!=''){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.Success_Message"),
                        message: $A.get("$Label.c.Template_Saved_Message"),
                        messageTemplate: $A.get("$Label.c.Template_Saved_Message"),
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": templateId,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
                }else{
                    helper.hide(component,event);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.Error_Message"),
                        message: $A.get("$Label.c.Error_in_saving_template"),
                        messageTemplate: $A.get("$Label.c.Error_in_saving_template"),
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                    
                }
            }
        } );
        
        $A.enqueueAction(action); 
       
   },
 
    doCancelAction : function(component, event, helper) {
    	$A.get("e.force:closeQuickAction").fire();    
        component.destroy();
    }
})