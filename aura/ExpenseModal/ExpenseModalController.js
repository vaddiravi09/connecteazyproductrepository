({
    doInit : function(component, event, helper) {
        
        component.set("v.IsSpinner", true);
        
        component.set('v.columns', [
            {label: $A.get("$Label.c.Name"), fieldName: 'Name', type: 'text', sortable:true},
            {label: $A.get("$Label.c.Start_Date"), fieldName: 'Nrich__Start_Date__c', type: 'date', sortable:true}, 
            {label: $A.get("$Label.c.end_date"), fieldName: 'Nrich__End_Date__c', type: 'date', sortable:true} ,
           {label:$A.get("$Label.c.Amount"), fieldName: 'Nrich__Expense_Amount__c', type: 'number', sortable:true},
            {label: $A.get("$Label.c.Project"), fieldName: 'Nrich__ProjectName__c', type: 'text', sortable:true},
        ]);
            
           
            
            
            
            var action1 = component.get("c.fetchexpensetypeoptions");  
            action1.setCallback(this, function(response2){
            if(response2.getState() === 'SUCCESS'){
            component.set("v.typeOptions", response2.getReturnValue());
            }
            component.set("v.IsSpinner", false);
            });
            
            var action2 = component.get("c.fetchoptions"); 
            action2.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
            
            component.set("v.options",response.getReturnValue());
            } 
            component.set("v.IsSpinner", false);
            });
            
            var action3 = component.get("c.ViewExpenserecords");  
            action3.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){   
           
            component.set("v.ExpenseDetails",response.getReturnValue());
            }
            component.set("v.IsSpinner", false);
            });
            
            
            $A.enqueueAction(action1);
            $A.enqueueAction(action2);
            $A.enqueueAction(action3);
            
            
            
            },
    selectedtab :function(component, event, helper) {
        var emplist = component.get("v.ExpenseDetails");      
        var empdisplaylist = []
        component.set("v.ExpenseDisplayDetails", empdisplaylist);
        var typeval = event.getParam('id');
       
        component.set("v.TabId", typeval);
        for(var i=0; i<emplist.length; i=i+1){
            if(emplist[i].Nrich__Status__c==typeval)
                empdisplaylist.push(emplist[i]);
        }
        component.set("v.ExpenseDisplayDetails", empdisplaylist);
        
    },        
    
    
    
    fetchstatusval:function(component, event, helper){
        var action = component.get("c.fetchStatus");
        action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
               
                component.set("v.statuslist", outputresponse);  
                var emplist = component.get("v.ExpenseDetails");
                var empdisplaylist = [];
                var typeval = component.get("v.TabId");
               // alert('typeval' +typeval);
                for(var i=0; i<emplist.length; i=i+1){
                    if(emplist[i].Nrich__Expense_Status__c ==typeval)
                        empdisplaylist.push(emplist[i]);
                }
               // alert('infetchstat' +JSON.stringify(empdisplaylist));
                component.set("v.ExpenseDisplayDetails", empdisplaylist);
                component.set("v.IsSpinner", false);
            }
        });
        $A.enqueueAction(action);
        
    },
    
    closemodal : function(component, event, helper){
        component.set("v.openmodal", false);
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" :'false',
                             "typeofmodal" :'ExpenseModal'});   
        compEvent.fire(); 
        
        
        
    },
    insertexpenserecord:function(component, event, helper){
        var action=component.get("c.insertnewexpense");
        var insertiondetails=component.get("v.InsertExpensedetails");
        
        action.setParams({'ExpenseDetails':insertiondetails});
        action.setCallback(this, function(response){
            
            var state = response.getState();
            
            if (state === "SUCCESS"){
                var ExpenseId=response.getReturnValue();
                component.set("v.ExpId" ,ExpenseId);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Success_Message"),
                    message: $A.get("$Label.c.Saved_Record"),
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            }
        });
        
        
        var action1 = component.get("c.SaveFile"); 
        var files = component.find("fileId1").get("v.files");
        var filename='';
        var filecontentval='';
        var filetype='';
        var fileContents='';
        if(files!=undefined){
            var file = files[0];
            filename = file.name;
            filetype = file.type;
            var objFileReader = new FileReader();
            objFileReader.onload = $A.getCallback(function() 
                                                  {
                                                      var expenseid=component.get("v.ExpId");
                                                     
                                                      var fileContents = objFileReader.result;
                                                      var base64 = 'base64,';
                                                      var dataStart = fileContents.indexOf(base64) + base64.length; 
                                                      fileContents = encodeURIComponent(fileContents.substring(dataStart)); 
                                                      filecontentval = fileContents;
                                                      action1.setParams({
                                                          "fileName":filename,
                                                          "base64Data":filecontentval,
                                                          "contentType":filetype,
                                                          "ExpID":expenseid
                                                      });
                                                      
                                                      action1.setCallback(this, function(response) {
                                                          var state = response.getState();
                                                          if (state === "SUCCESS") {
                                                              var toastEvent = $A.get("e.force:showToast");
                                                              toastEvent.setParams({
                                                                  title : $A.get("$Label.c.Success_Message"),
                                                                  message: 'File Uploaded Sucessfully', 
                                                                  duration:' 5000',
                                                                  key: 'info_alt',
                                                                  type: 'success',
                                                                  mode: 'dismissible'
                                                              });
                                                              toastEvent.fire();
                                                              
                                                              
                                                          }
                                                      });
                                                      
                                                      $A.enqueueAction(action1); 
                                                      
                                                  });
                                                         
            
            objFileReader.readAsDataURL (file);
                                                 
        }
                                                  
        $A.enqueueAction(action);
        $A.enqueueAction(action1);
        
        
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" :'false',
                             "typeofmodal" : 'ExpenseModal'});   
        compEvent.fire(); 
        component.set("v.openmodal", false);
    },
    
    onUpload:function(component, event, helper){
        
        
        
        
    },
})