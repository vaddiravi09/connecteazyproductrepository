({
    doInit: function(component, event, helper)
    {
        helper.FetchProjectOptions(component, event,helper);
    },
    AddNewRow : function(component, event, helper){
        component.getEvent("AddRowEvt").fire();
    },
    removeRow : function(component, event, helper){
        component.getEvent("DeleteRowEvt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    },
    projectid:function(component, event, helper){
        var type = event.getSource().get("v.value");
        var action3 = component.get("c.FetchProjectCategoryOptions"); 
        action3.setParams({
            "PrjId":type
        });
        action3.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS' && response!=null){
                component.set("v.options1",response.getReturnValue());
            } 
        });
        $A.enqueueAction(action3);
    }
    
})