({
    FetchProjectOptions : function(component, event) {
        var action4 = component.get("c.FetchProjectAllocation"); 
        action4.setParams({
            "datetimes": component.get("v.datetime")
        });
        
        action4.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS' && response!=null){
                component.set("v.options",response.getReturnValue());
            } 
        });
        $A.enqueueAction(action4);
    },
})