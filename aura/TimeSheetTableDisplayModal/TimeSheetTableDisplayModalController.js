({
    doInit : function(component, event, helper) {
        component.set("v.IsSpinner", true);
        
        component.set('v.columns', [
            {label: $A.get("$Label.c.Name"), fieldName: 'Name', type: 'text', sortable:true},
            {label: $A.get("$Label.c.Date"), fieldName: 'Nrich__Timesheet__r.Date__c', type: 'date', sortable:true}, 
            {label: $A.get("$Label.c.Employee"), fieldName: 'Nrich__Timesheet__r.Nrich__Employeee__r.Name', type: 'text', sortable:true} ,
            
        ]);
            
            
            var alist=[];
                      var Approvedlist=[];
                      var Pendinglist=[];
                      var Rejectedlist=[];
                      
                      var action = component.get("c.ViewTimeSheetDetails");  
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var i=0;
                alist=response.getReturnValue();
                for(alist[i];i<alist.length;i=i+1)
                {
                    if (alist[i].Nrich__Status__c ==$A.get("$Label.c.Approved"))
                    {
                        Approvedlist.push(alist[i]);
                    }
                    if(alist[i].Nrich__Status__c==$A.get("$Label.c.Rejected"))
                    {
                        Rejectedlist.push(alist[i]);
                    }
                    if(alist[i].Nrich__Status__c==$A.get("$Label.c.Submitted_For_Approval"))
                    {
                        Pendinglist.push(alist[i]);
                    }
                }
                
                
                component.set('v.ApprovedTimeSheetDetails',Approvedlist);
                component.set('v.RejectedTimeSheetDetails',Rejectedlist);
                component.set('v.PendingTimeSheetDetails',Pendinglist);
                
            }
            component.set("v.IsSpinner", false);
        });
        $A.enqueueAction(action); 
        
        
    },
    handleColumnSorting: function (component, event, helper) 
    {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        // helper.sortData(component, fieldName, sortDirection);
    },
    closemodal : function(component, event, helper){
        component.set("v.openmodal", false);
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'TimeSheetcomp'});   
        compEvent.fire(); 
        
    },
    
})