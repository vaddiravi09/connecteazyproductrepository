({
    sortData: function (component, fieldName, sortDirection) {
        
        if(component.get("v.ApprovedTimeSheetDetails")){
            var data = component.get("v.ApprovedTimeSheetDetails");
            var res;
            var reverse = sortDirection !== 'asc';
            data.sort(this.sortBy(fieldName, reverse))
            component.set("v.ApprovedTimeSheetDetails", data);
        }
        else if(component.get("v.RejectedTimeSheetDetails")){
            var data = component.get("v.RejectedTimeSheetDetails");
            var res;
            var reverse = sortDirection !== 'asc';
            data.sort(this.sortBy(fieldName, reverse))
            component.set("v.RejectedTimeSheetDetails", data);   
        }
            else if(component.get("v.PendingTimeSheetDetails")){
                var data = component.get("v.PendingTimeSheetDetails");
                var res;
                var reverse = sortDirection !== 'asc';
                data.sort(this.sortBy(fieldName, reverse))
                component.set("v.PendingTimeSheetDetails", data);   
            }
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
})