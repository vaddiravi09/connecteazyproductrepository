({
    doInit : function(component, event, helper) {
        var action = component.get("c.getTemplateType");
        var opts=[];
        action.setCallback(this, function(a){
            var state = a.getState();
            if(state === "SUCCESS"){
                var allValues = a.getReturnValue();
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--Choose One--",
                        value: ""
                    });
                    for(var i=0;i<allValues.length; i=i+1){
                        opts.push({
                            class: "optionClass",
                            label: allValues[i],
                            value: allValues[i]
                        });
                    }
                }
                component.set("v.templatetype", opts);
            }
        });     
        $A.enqueueAction(action);
    },
    getfields : function(component, event, helper) {
        helper.show(component,event);
        helper.fieldfetch(component,event);
        helper.hide(component,event);
    },
    fetchtemplatedetails : function(component, event, helper){  
        if(component.get("v.recordId")!=undefined){
            var action = component.get("c.fetchTemplaterecord");  
            action.setParams({
                "templateId":component.get("v.recordId")
            });
            
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    var templaterec = response.getReturnValue();               
                    component.set("v.template", templaterec.template);
                    if(component.get("v.template").Nrich__Type_of_Template__c===$A.get("$Label.c.Feedback"))
                        component.set("v.jobroundcheck", true);
                    var sectionlistrec = templaterec.sectionList;
                    var selectedfields = templaterec.sectionfieldList; 
                    component.set("v.sectionlist", sectionlistrec);
                    component.set("v.selectedfield", selectedfields);
                    component.set("v.editfield", selectedfields);
                    component.set("v.fieldsList", templaterec.availablefieldList);
                    component.set("v.selectedLookUpRecord", templaterec.job);
                    component.set("v.selectedJobRound",templaterec.jobround);
                    component.set("v.templatefields", true);
                    
                    
                }
            });
            $A.enqueueAction(action);
        }
    },
    drag: function (component, event, helper) {
        event.dataTransfer.setData("text", event.target.id);
    },
    addsection :  function (component, event, helper) {
        var section = component.get("v.sectionnew");
        var sectionlist = component.get("v.sectionlist");
        var lengthval = sectionlist.length;
        sectionlist.push({'sobjectType': 'Nrich__Section__c',
                          'Nrich__Order__c': lengthval+1,
                          'Name': ''});
        component.set("v.sectionlist", sectionlist);
    },
    saveTemplateVal : function (component, event, helper) {
        helper.show(component,event);
        var jobfield = component.get("v.selectedLookUpRecord");
        var jobroundfield = component.get("v.selectedJobRound");
        var tempaterecord = component.get("v.template");
        var errorMsg = $A.get("$Label.c.Error_Message");
        var Template_Error_Message = $A.get("$Label.c.Template_Error_Message");
        var job_Error_Message = $A.get("$Label.c.Job_Error_Message");
        var Job_Round_Error_Message = $A.get("$Label.c.Job_Round_Error_Message");
        var Template_Section_Error_Message = $A.get("$Label.c.Template_Section_Error_Message");
        var Section_Name_Error_Message = $A.get("$Label.c.Section_Name_Error_Message");
        var Section_Order_Error_Message = $A.get("$Label.c.Section_Order_Error_Message");
        var Section_Field_Error_Message = $A.get("$Label.c.Section_Order_Error_Message");
        
        if(tempaterecord!=undefined){
            if(tempaterecord.Name===undefined || tempaterecord.Name===''){
                helper.hide(component,event);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : errorMsg,
                    message:Template_Error_Message,
                    messageTemplate: Template_Error_Message,
                    duration:'5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            }
            else{
                if(jobfield.Id===undefined || jobfield.Id===''){
                    helper.hide(component,event);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : errorMsg,
                        message:job_Error_Message,
                        messageTemplate: job_Error_Message,
                        duration:'5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                }
                else{  
                    tempaterecord.Nrich__Job__c = jobfield.Id;
                    if(tempaterecord.Nrich__Type_of_Template__c===$A.get("$Label.c.Feedback") && (jobroundfield.Id===undefined || jobroundfield.Id==='')){
                        helper.hide(component,event);  
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : errorMsg,
                            message: Job_Round_Error_Message,
                            messageTemplate: Job_Round_Error_Message,
                            duration:'5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();
                    }else{
                        tempaterecord.Nrich__Job_Round__c = jobroundfield.Id;
                        var compsection = component.get("v.sectionlist");
                        var comsectionfield = component.get("v.selectedfield");
                        var sectionnamecheck = $A.get("$Label.c.false");
                        var sectionordercheck = $A.get("$Label.c.false");
                        var requiredfieldcheckName = $A.get("$Label.c.false");
                        var requiredfieldcheckEmail = $A.get("$Label.c.false");
                        var requiredfieldcheckPhone = $A.get("$Label.c.false");
                        var requiredfieldcheckSource = $A.get("$Label.c.false");
                        
                        if(compsection.length<=0){
                            helper.hide(component,event);  
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                title : errorMsg,
                                message: Template_Section_Error_Message,
                                messageTemplate: Template_Section_Error_Message,
                                duration:'5000',
                                key: 'info_alt',
                                type: 'error',
                                mode: 'dismissible'
                            });
                            toastEvent.fire();
                            
                        }
                        else{
                            for(var i=0; i<compsection.length; i=i+1){
                                if(compsection[i].Name===undefined || compsection[i].Name==='')
                                    sectionnamecheck = $A.get("$Label.c.true");
                                if(compsection[i].Nrich__Order__c===undefined)
                                    sectionordercheck = $A.get("$Label.c.true");
                            }
                            if(sectionnamecheck===$A.get("$Label.c.true")){
                                helper.hide(component,event);
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    title : errorMsg,
                                    message: Section_Name_Error_Message,
                                    messageTemplate: Section_Name_Error_Message,
                                    duration:'5000',
                                    key: 'info_alt',
                                    type: 'error',
                                    mode: 'dismissible'
                                });
                                toastEvent.fire();
                            }else if(sectionordercheck===$A.get("$Label.c.true")){
                                helper.hide(component,event);
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    title : errorMsg,
                                    message: Section_Order_Error_Message,
                                    messageTemplate: Section_Order_Error_Message,
                                    duration:'5000',
                                    key: 'info_alt',
                                    type: 'error',
                                    mode: 'dismissible'
                                });
                                toastEvent.fire();
                            }
                                else{
                                    if(comsectionfield.length<=0){
                                        helper.hide(component,event);
                                        var toastEvent = $A.get("e.force:showToast");
                                        toastEvent.setParams({
                                            title : errorMsg,
                                            message: Section_Field_Error_Message,
                                            messageTemplate: Section_Field_Error_Message,
                                            duration:'5000',
                                            key: 'info_alt',
                                            type: 'error',
                                            mode: 'dismissible'
                                        });
                                        toastEvent.fire();
                                    }
                                    else{
                                        for(var i=0; i<comsectionfield.length; i=i+1){
                                            if(tempaterecord.Nrich__Type_of_Template__c===$A.get("$Label.c.Application")){
                                                if(comsectionfield[i].fieldName===$A.get("$Label.c.Name") || comsectionfield[i].fieldName===$A.get("$Label.c.ApplicantFirstName") || comsectionfield[i].fieldName===$A.get("$Label.c.ApplicantLastName") || comsectionfield[i].fieldName===$A.get("$Label.c.ApplicantMiddleName"))
                                                    requiredfieldcheckName = $A.get("$Label.c.true");
                                                if(comsectionfield[i].fieldName===$A.get("$Label.c.TemplateCreationEmail"))
                                                    requiredfieldcheckEmail = $A.get("$Label.c.true");
                                                if(comsectionfield[i].fieldName===$A.get("$Label.c.PrimaryPhone") || comsectionfield[i].fieldName===$A.get("$Label.c.AlternatePhone"))
                                                    requiredfieldcheckPhone = $A.get("$Label.c.true");
                                           		if(comsectionfield[i].fieldName===$A.get("$Label.c.Source"))
                                                    requiredfieldcheckSource = $A.get("$Label.c.true");
                                            }
                                        }
                                        
                                        if((requiredfieldcheckName===$A.get("$Label.c.true") && requiredfieldcheckEmail===$A.get("$Label.c.true") && requiredfieldcheckPhone===$A.get("$Label.c.true") && requiredfieldcheckSource === $A.get("$Label.c.true")) || tempaterecord.Nrich__Type_of_Template__c!=$A.get("$Label.c.Application")){
                                            component.set("v.template", tempaterecord);
                                            
                                            var files = component.find("fileId").get("v.files");
                                            var filename='';
                                            var filecontentval='';
                                            var filetype='';
                                            if(files!=undefined){
                                                var file = files[0];
                                                filename = file.name;
                                                filetype = file.type;
                                                var objFileReader = new FileReader();
                                                objFileReader.onload = $A.getCallback(function() {
                                                    var fileContents = objFileReader.result;
                                                    var base64 = 'base64,';
                                                    var dataStart = fileContents.indexOf(base64) + base64.length; 
                                                    fileContents = encodeURIComponent(fileContents.substring(dataStart)); 
                                                    filecontentval = fileContents;
                                                    var action = component.get("c.saveTemplate");  
                                                    action.setParams({
                                                        "fileName":filename,
                                                        "base64Data":filecontentval,
                                                        "contentType":filetype,
                                                        "template":component.get("v.template"),
                                                        "sections":JSON.stringify(component.get("v.sectionlist")),
                                                        "fieldlist":JSON.stringify(component.get("v.selectedfield"))
                                                    });
                                                    action.setCallback(this, function(response){
                                                        
                                                        var state = response.getState();
                                                        if (state === "SUCCESS") {

                                                            helper.hide(component,event);
                                                            var templateresponse = response.getReturnValue();
                                                            if(templateresponse.isError){
                                                                helper.hide(component,event);
                                                                var toastEvent = $A.get("e.force:showToast");
                                                                toastEvent.setParams({
                                                                    title : $A.get("$Label.c.Error_Message"),
                                                                    message: templateresponse.templateIdMessage,
                                                                    messageTemplate: templateresponse.templateIdMessage,
                                                                    duration:' 5000',
                                                                    key: 'info_alt',
                                                                    type: 'error',
                                                                    mode: 'dismissible'
                                                                });
                                                                toastEvent.fire();
                                                            }else{
                                                                var templateId = templateresponse.templateIdMessage;
                                                                var toastEvent = $A.get("e.force:showToast");
                                                                toastEvent.setParams({
                                                                    title : $A.get("$Label.c.Success_Message"),
                                                                    message: $A.get("$Label.c.Template_Saved_Message"),
                                                                    messageTemplate: $A.get("$Label.c.Template_Saved_Message"),
                                                                    duration:' 5000',
                                                                    key: 'info_alt',
                                                                    type: 'success',
                                                                    mode: 'dismissible'
                                                                });
                                                                toastEvent.fire();
                                                                
                                                                var navEvt = $A.get("e.force:navigateToSObject");
                                                                navEvt.setParams({
                                                                    "recordId": templateId,
                                                                    "slideDevName": "detail"
                                                                });
                                                                navEvt.fire();
                                                            }
                                                        }else
                                                        {
                                                            helper.hide(component,event);
                                                            var toastEvent = $A.get("e.force:showToast");
                                                            toastEvent.setParams({
                                                                title : $A.get("$Label.c.Error_Message"),
                                                                message: $A.get("$Label.c.Error_in_saving_template"),
                                                                messageTemplate: $A.get("$Label.c.Error_in_saving_template"),
                                                                duration:' 5000',
                                                                key: 'info_alt',
                                                                type: 'error',
                                                                mode: 'dismissible'
                                                            });
                                                            toastEvent.fire();
                                                            
                                                        }
                                                    });
                                                    $A.enqueueAction(action);
                                                });
                                                objFileReader.readAsDataURL(file);
                                            }
                                            else{
                                                var action = component.get("c.saveTemplate");  
                                                action.setParams({
                                                    "fileName":filename,
                                                    "base64Data":filecontentval,
                                                    "contentType":filetype,
                                                    "template":component.get("v.template"),
                                                    "sections":JSON.stringify(component.get("v.sectionlist")),
                                                    "fieldlist":JSON.stringify(component.get("v.selectedfield"))
                                                });
                                                action.setCallback(this, function(response){
                                                    var state = response.getState();
                                                    if (state === "SUCCESS") {
                                                        helper.hide(component,event);
                                                        var templateresponse = response.getReturnValue();
                                                        if(templateresponse.isError){
                                                            helper.hide(component,event);
                                                            var toastEvent = $A.get("e.force:showToast");
                                                            toastEvent.setParams({
                                                                title : $A.get("$Label.c.Error_Message"),
                                                                message: templateresponse.templateIdMessage,
                                                                messageTemplate: templateresponse.templateIdMessage,
                                                                duration:' 5000',
                                                                key: 'info_alt',
                                                                type: 'error',
                                                                mode: 'dismissible'
                                                            });
                                                            toastEvent.fire();
                                                        }else{
                                                            var templateId = templateresponse.templateIdMessage;
                                                            var toastEvent = $A.get("e.force:showToast");
                                                            toastEvent.setParams({
                                                                title : $A.get("$Label.c.Success_Message"),
                                                                message: $A.get("$Label.c.Template_Saved_Message"),
                                                                messageTemplate: $A.get("$Label.c.Template_Saved_Message"),
                                                                duration:' 5000',
                                                                key: 'info_alt',
                                                                type: 'success',
                                                                mode: 'dismissible'
                                                            });
                                                            toastEvent.fire();
                                                            
                                                            var navEvt = $A.get("e.force:navigateToSObject");
                                                            navEvt.setParams({
                                                                "recordId": templateId,
                                                                "slideDevName": "detail"
                                                            });
                                                            navEvt.fire();
                                                        }
                                                    }else{
                                                        helper.hide(component,event);
                                                        var toastEvent = $A.get("e.force:showToast");
                                                        toastEvent.setParams({
                                                            title : $A.get("$Label.c.Error_Message"),
                                                            message: $A.get("$Label.c.Error_in_saving_template"),
                                                            messageTemplate: $A.get("$Label.c.Error_in_saving_template"),
                                                            duration:' 5000',
                                                            key: 'info_alt',
                                                            type: 'error',
                                                            mode: 'dismissible'
                                                        });
                                                        toastEvent.fire();
                                                    }
                                                });
                                                $A.enqueueAction(action);
                                            }
                                        }
                                        else{
                                            helper.hide(component,event);
                                            var toastEvent = $A.get("e.force:showToast");
                                            toastEvent.setParams({
                                                title : $A.get("$Label.c.Error_Message"),
                                                message: $A.get("$Label.c.Template_Required_Field_Message"),
                                                messageTemplate: $A.get("$Label.c.Template_Required_Field_Message"),
                                                duration:' 5000',
                                                key: 'info_alt',
                                                type: 'error',
                                                mode: 'dismissible'
                                            });
                                            toastEvent.fire();             
                                        }
                                    }
                                }
                        }
                    }
                }
            }
        }else{
            helper.hide(component,event);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : $A.get("$Label.c.Error_Message"),
                message: $A.get("$Label.c.Error_in_saving_template"),
                messageTemplate: $A.get("$Label.c.Error_in_saving_template"),
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'dismissible'
            });
            toastEvent.fire();
            
        }
    },
    allowDrop: function(component, event, helper) {
        event.preventDefault();
    },
    toogleval: function(component, event, helper) {
        var buttonval = event.getSource().get("v.name");
        var comp = document.getElementById('section'+'-'+buttonval).classList;
        var buttonexpand = document.getElementById('sectionexpand'+'-'+buttonval).classList;
        var buttoncollapse = document.getElementById('sectioncollapse'+'-'+buttonval).classList;
        
        //if(comp=="slds-hide"){
        if(comp.contains('slds-hide')){   
            comp.remove("slds-hide");
            buttonexpand.remove("slds-hide");
            buttoncollapse.remove("slds-show");
            document.getElementById('section'+'-'+buttonval).classList.add('slds-show');
            document.getElementById('sectionexpand'+'-'+buttonval).classList.add('slds-show');
            document.getElementById('sectioncollapse'+'-'+buttonval).classList.add('slds-hide');
        }else{
            
            comp.remove("slds-show");
            buttonexpand.remove("slds-show");
            buttoncollapse.remove("slds-hide");
            document.getElementById('section'+'-'+buttonval).classList.add('slds-hide');
            document.getElementById('sectionexpand'+'-'+buttonval).classList.add('slds-hide');
            document.getElementById('sectioncollapse'+'-'+buttonval).classList.add('slds-show');
        }             
    },  
    deletesection : function (component, event, helper) {
        var buttonval = event.getSource().get("v.name");
        var sectionlistrec = component.get("v.sectionlist");
        sectionlistrec.splice(buttonval, 1);
        component.set("v.sectionlist",sectionlistrec);
    },   
    seconddrop : function (component, event, helper) {
        event.preventDefault();
        var data = event.dataTransfer.getData("text");
        var tar = event.target; 
        
        while(tar.tagName != $A.get("$Label.c.ul") && tar.tagName != $A.get("$Label.c.UL1"))
            tar = tar.parentElement;
        tar.appendChild(document.getElementById(data));
        document.getElementById(data).classList.remove("slds-size_4-of-6");
        document.getElementById(data).classList.add("slds-size_5-of-6");
        var fieldval = component.get("v.selectedfield");
        var indexval=0;
        var existingcheck;
        
        for(var i=0; i<fieldval.length; i=i+1){
            if(fieldval[i].fieldName===data){
                indexval = i;
                existingcheck = $A.get("$Label.c.true");
            }
        }
        
        if(existingcheck===$A.get("$Label.c.true")){
            fieldval.splice(indexval, 1);
            component.set("v.selectedfield", fieldval);
        }
        
    },
    drop: function (component, event, helper) {
        event.preventDefault();
        var data = event.dataTransfer.getData("text");
        var tar = event.target;
        while(tar.tagName != 'ul' && tar.tagName != 'UL')
            tar = tar.parentElement;
        
        tar.appendChild(document.getElementById(data));
        
        var sectionorder = tar.getAttribute('data-Pick-Val');
        var columnorder = tar.getAttribute('data-col-Val');
        var fieldlist = component.get("v.fieldsList");
        var fieldval = component.get("v.selectedfield");
        console.log('before'+JSON.stringify(component.get("v.fieldsList")));
        var sectiondfieldno = 1;
        var indexval;
        var existingcheck;
        
        for(var i=0; i<fieldval.length; i=i+1){
            if(fieldval[i].fieldName===data){
                indexval = i;
                existingcheck = $A.get("$Label.c.true");
                
                fieldval[i].sectionOrder = sectionorder;
                fieldval[i].sectionColumn = columnorder;
            }
            if(fieldval[i].sectionOrder===sectionorder){
                sectiondfieldno = sectiondfieldno+1;
            }
        }
        if(existingcheck===$A.get("$Label.c.true"))
            fieldval[indexval].sectiondfieldOrder = sectiondfieldno;
        
        if(existingcheck!=$A.get("$Label.c.true")){
            for(var i=0; i<fieldlist.length; i++){
                if(fieldlist[i].fieldName===data){  
                    fieldlist[i].sectionOrder = sectionorder;
                    fieldlist[i].sectionColumn = columnorder;
                    fieldlist[i].sectiondfieldOrder = sectiondfieldno;
                    fieldval.push(fieldlist[i]);
                }
            }
        }
        
        component.set("v.selectedfield", fieldval);
        console.log('after'+JSON.stringify(component.get("v.selectedfield")));
        
    },
    openModel: function(component, event, helper) {
        var fieldName = event.getSource().get("v.name");
        var selectedfieldlist = component.get("v.selectedfield");
        if(selectedfieldlist.length>0){
            for(var i=0; i<selectedfieldlist.length; i=i+1){
                if(fieldName===selectedfieldlist[i].fieldName){
                    component.set("v.editfieldName", selectedfieldlist[i].fieldName);
                    component.set("v.fieldlabelval", selectedfieldlist[i].fieldLabel);
                    component.set("v.mandatoryfield", selectedfieldlist[i].mandatory);
                    component.set("v.resumeval", selectedfieldlist[i].resume);
                    if(selectedfieldlist[i].fieldType===$A.get("$Label.c.Upload")){                     
                       component.set("v.resumenotcheck", false);
                        component.set("v.picklisttype", false);
                        component.set("v.radioButtonval", false);
                    }
                    else if(selectedfieldlist[i].fieldType===$A.get("$Label.c.Radio_Button1"))
                        component.set("v.radioButtonval", true);
                    else if(selectedfieldlist[i].fieldType===$A.get("$Label.c.PICKLIST"))
                        component.set("v.radioButtonval", false);
                    if(selectedfieldlist[i].fieldType.toUpperCase()===$A.get("$Label.c.PICKLIST") || selectedfieldlist[i].fieldType.toUpperCase()===$A.get("$Label.c.RADIO_BUTTON")){
                        component.set("v.picklisttype", true); 
                        component.set("v.resumenotcheck", true);
                    }
                    else if(selectedfieldlist[i].fieldType.toUpperCase()!=$A.get("$Label.c.PICKLIST") && selectedfieldlist[i].fieldType!=$A.get("$Label.c.Upload")){
                         component.set("v.picklisttype", false);
                        component.set("v.resumenotcheck", true);
                    }
                }
            }
        }
        
        var fieldsListnew = component.get("v.fieldsList");
        if(fieldsListnew.length>0){
            for(var i=0; i<fieldsListnew.length; i=i+1){
                if(fieldName===fieldsListnew[i].fieldName){
                    component.set("v.editfieldName", fieldsListnew[i].fieldName);
                    component.set("v.fieldlabelval", fieldsListnew[i].fieldLabel);
                    component.set("v.mandatoryfield", fieldsListnew[i].mandatory);
                    component.set("v.resumeval", fieldsListnew[i].resume);
                    if(fieldsListnew[i].fieldType===$A.get("$Label.c.Upload")){
                       component.set("v.resumenotcheck", false);
                        component.set("v.picklisttype", false);
                        component.set("v.radioButtonval", false);
                    }
                    else if(fieldsListnew[i].fieldType===$A.get("$Label.c.Radio_Button1"))
                        component.set("v.radioButtonval", true);
                    else if(fieldsListnew[i].fieldType===$A.get("$Label.c.PICKLIST"))
                        component.set("v.radioButtonval", false);
                    if(fieldsListnew[i].fieldType.toUpperCase()===$A.get("$Label.c.PICKLIST") || fieldsListnew[i].fieldType.toUpperCase()===$A.get("$Label.c.RADIO_BUTTON")){
                        component.set("v.picklisttype", true);
                        component.set("v.resumenotcheck", true);
                    }
                     else if(fieldsListnew[i].fieldType!=$A.get("$Label.c.Upload") && fieldsListnew[i].fieldType.toUpperCase()!=$A.get("$Label.c.PICKLIST")){
                         component.set("v.picklisttype", false);
                        component.set("v.resumenotcheck", true);
                    }
                }
            }
        }
        
        component.set("v.isOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    saveandclose: function(component, event, helper) {
        var fieldnamerec = component.get("v.editfieldName");
        var selectedfieldlist = component.get("v.selectedfield");
        if(selectedfieldlist.length>0){
            for(var i=0; i<selectedfieldlist.length; i=i+1){
                if(fieldnamerec===selectedfieldlist[i].fieldName){
                    selectedfieldlist[i].fieldLabel = component.get("v.fieldlabelval");
                    selectedfieldlist[i].mandatory = component.get("v.mandatoryfield");
                    selectedfieldlist[i].resume = component.get("v.resumeval");
                    if(component.get("v.radioButtonval"))
                        selectedfieldlist[i].fieldType = $A.get("$Label.c.Radio_Button1");
                    else if(selectedfieldlist[i].fieldType==='Picklist' && !component.get("v.radioButtonval"))
                        selectedfieldlist[i].fieldType = 'Picklist';
                }
            }
        }
        component.set("v.selectedfield", selectedfieldlist);
        var fieldsListnew = component.get("v.fieldsList");
        if(fieldsListnew.length>0){
            for(var i=0; i<fieldsListnew.length; i=i+1){
                if(fieldnamerec===fieldsListnew[i].fieldName){
                    fieldsListnew[i].fieldLabel = component.get("v.fieldlabelval");
                    fieldsListnew[i].mandatory = component.get("v.mandatoryfield");
                    fieldsListnew[i].resume = component.get("v.resumeval");
                    if(component.get("v.radioButtonval"))
                        fieldsListnew[i].fieldType = $A.get("$Label.c.Radio_Button1");
                    else if(fieldsListnew[i].fieldType==='Picklist' && !component.get("v.radioButtonval"))
                        fieldsListnew[i].fieldType = 'Picklist';
                }
            }
        }
        component.set("v.fieldsList", fieldsListnew);
        component.set("v.isOpen", false);
    },
})