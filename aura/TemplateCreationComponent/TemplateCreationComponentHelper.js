({
    fieldfetch: function (component, event) {
        
        var action = component.get("c.getFields");
        var type = event.getSource().get("v.value");
        action.setParams({
            "templateType":event.getSource().get("v.value")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {  
                component.set("v.fieldsList", response.getReturnValue());
                component.set("v.dup", response.getReturnValue());
                component.set("v.templatefields", true);   
                if(type===$A.get("$Label.c.Feedback")){
                    component.set("v.jobroundcheck", true);
                }
                else
                    component.set("v.jobroundcheck", false);
            }
        });
        var section = component.get("v.sectionnew");
        var sectionlist = component.get("v.sectionlist");
         	sectionlist = [];
        	sectionlist.length = 0;
            sectionlist.push({'sobjectType': 'Nrich__Section__c',
                'Nrich__Order__c': 1,'Name': ''});
            component.set("v.sectionlist", sectionlist);

        $A.enqueueAction(action);
    },
    show: function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
    },
    hide:function (cmp, event) {
       	var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    }
})