({
    
    doInit : function(component, event, helper){
        
        component.set("v.IsSpinner",true);
        var action = component.get("c.viewrecords");
        
        action.setCallback(this, function(a)
                           {
                               var state = a.getState();
                               if (state === "SUCCESS"){
                                   component.set("v.Viewdetails",a.getReturnValue());
                                   component.set("v.Employeedetails",a.getReturnValue());
                                   var dataval = component.get("v.Viewdetails");
                                   if(!dataval.Nrich__ProfilePic__c.includes($A.get("$Label.c.EmployeeDasboardImage")))
                                      component.set("v.noprofileimage", true);
                                   
                               }
                           });
        var action1 = component.get("c.getProfilePicture"); 
        
        action1.setParams({
            
        });
        
        action1.setCallback(this, function(a) {
            var state = a.getState();
            
            if (state === "SUCCESS")
                var attachment = a.getReturnValue();
            
            if(attachment!=null){
                var link=' /sfc/servlet.shepherd/version/download/'+attachment;
                
                component.set('v.pictureSrc',link); 
                
                console.log('link'+link);
                
            }
            component.set("v.IsSpinner",false);
            
        });
        
        
        $A.enqueueAction(action); 
        $A.enqueueAction(action1);
        
    },
    
    onDragOver: function(component, event) {
        event.preventDefault();
    },
    
    onDrop: function(component, event, helper) {
        var action = component.get("c.saveImage");   
        var files = component.find("fileId1").get("v.files");
        var filename='';
        var filecontentval='';
        var filetype='';
        var fileContents='';
        if(files!=undefined){
            var file = files[0];
            filename = file.name;
            filetype = file.type;
            var objFileReader = new FileReader();
            objFileReader.onload = $A.getCallback(function() 
                                                  {
                                                      var fileContents = objFileReader.result;
                                                      var base64 = 'base64,';
                                                      var dataStart = fileContents.indexOf(base64) + base64.length; 
                                                      fileContents = encodeURIComponent(fileContents.substring(dataStart)); 
                                                      filecontentval = fileContents;
                                                      action.setCallback(this, function(response) {
                                                          var state = response.getState();
                                                          if (state === "SUCCESS") {
                                                          }
                                                      });
                                                  });
            objFileReader.readAsDataURL(file);
            component.set("v.Filename",filename);
            component.set("v.Filetype",filetype);
        }
        helper.readFile(component, helper,files[0]);
    },
    
    openmodal : function(component, event, helper) {
        component.set("v.isOpen", false);
        component.set("v.isEditOpen", true);
    },
    
    openviewmodal : function(component, event, helper){
        component.set("v.isOpen", true);
    },
    
    closemodal : function(component, event, helper){
        component.set("v.isOpen", false);
        component.set("v.isEditOpen", false);
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'ProfileComp'});   
        compEvent.fire();
    },
    
    closemodal1:function(component, event, helper){
        component.set("v.isEditOpen", false);
        component.set("v.isOpen", true); 
    },
    
    insertrecord : function(component, event, helper){
        var action1 = component.get("c.saveImage"); 
        var files = component.find("fileId1").get("v.files");
        var filename='';
        var filecontentval='';
        var filetype='';
        var fileContents='';
        if(files!=undefined){
            var file = files[0];
            filename = file.name;
            filetype = file.type;
            var objFileReader = new FileReader();
            objFileReader.onload = $A.getCallback(function() 
                                                  {
                                                      var fileContents = objFileReader.result;
                                                      var base64 = 'base64,';
                                                      var dataStart = fileContents.indexOf(base64) + base64.length; 
                                                      fileContents = encodeURIComponent(fileContents.substring(dataStart)); 
                                                      filecontentval = fileContents;
                                                      action1.setParams({
                                                          "fileName":filename,
                                                          "base64Data":filecontentval,
                                                          "contentType":filetype
                                                          
                                                      });
                                                      action1.setCallback(this, function(response) {
                                                          var state = response.getState();
                                                          if (state === "SUCCESS") {
                                                              var toastEvent = $A.get("e.force:showToast");
                                                              toastEvent.setParams({
                                                                  title : $A.get("$Label.c.Success_Message"),
                                                                  message: $A.get("$Label.c.Image_Updated_Successfully"), 
                                                                  duration:' 5000',
                                                                  key: 'info_alt',
                                                                  type: 'success',
                                                                  mode: 'dismissible'
                                                              });
                                                              toastEvent.fire();
                                                              
                                                              
                                                          }
                                                      });
                                                      
                                                      $A.enqueueAction(action1); 
                                                      
                                                  });
            
            objFileReader.readAsDataURL (file);
            
        }
        
        var details=component.get("v.Employeedetails");
        
        var action = component.get("c.insertrec");
        
        action.setParams({'empdetails':details,});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                component.set("v.Employeedetails",response.getReturnValue());
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title :$A.get("$Label.c.Success_Message"),
                    message: $A.get("$Label.c.Updated_Record"),
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
                component.set("v.isEditOpen",false);
                $A.get('e.force:refreshView').fire();
            } 
        }); 
        $A.enqueueAction(action);
        helper.readFile(component, helper,file);
    },
    
    // To Get The TabId Frm Profile And Assign it to Edit Modal
    TabController:function(component, event, helper){
        var a=component.get("v.selTabId");
        component.set("v.seleditTabId",a);
    },
    
    
})