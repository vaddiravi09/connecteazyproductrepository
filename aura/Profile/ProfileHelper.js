({
    MAX_FILE_SIZE: 4500000,
    readFile: function(component, helper, file) {
        var self=this; 
        if (!file) return;
        if (!file.type.match(/(image.*)/)) {
            return alert('Image file not supported');
        }
        if (file.size > self.MAX_FILE_SIZE)
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : $A.get("$Label.c.Error_Message"),
                message:$A.get("$Label.c.Image_Size_Error"),
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'dismissible'
            });
            toastEvent.fire();
        }
        else
        {
            var reader = new FileReader();
            reader.onloadend = function() {
                var dataURL = reader.result;
                component.set("v.pictureSrc", dataURL);
                component.set("v.uploadpicture", true);
                var check=component.get("v.Condition");
                if(check==true)
                {
                    helper.upload(component, file, dataURL.match(/,(.*)$/)[1]);
                    
                }
                
            };
            reader.readAsDataURL(file);
        }},
    
    upload: function(component, file, base64Data) {
        var action = component.get("c.saveImage"); 
        action.setParams({
            fileName: file.name,
            base64Data: base64Data, 
            contentType: file.type,
        });
        action.setCallback(this, function(a) {
            component.set("v.message", $A.get("$Label.c.Image_Uploaded"));
        });
        component.set("v.message",$A.get("$Label.c.Uploading"));
        $A.enqueueAction(action); 
    }
    
    
    
    
    
})