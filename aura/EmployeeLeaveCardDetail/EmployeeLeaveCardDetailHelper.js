({
    fetchLeavecard : function(component, event, helper) {
        component.set("v.IsSpinner", true);
        var action = component.get("c.fetchEmployeeLeaveCard");
        action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            
            if (state === "SUCCESS") {                              
                if(outputresponse!=null){
                    component.set("v.employeeCategory", outputresponse);
                    var empCategory = component.get("v.employeeCategory");
                    if(component.get("v.type")===$A.get("$Label.c.Obtained")){
                        component.set('v.columns', [
                            {label: $A.get("$Label.c.Name"), fieldName: 'Name', type: 'text', sortable:true},
                            {label: $A.get("$Label.c.Leaves_Obtained"), fieldName: 'Nrich__Leaves_Obtained__c', type: 'number', sortable:true}                       
                        ]);
                    }
                    else if(component.get("v.type")===$A.get("$Label.c.Remaining")){
                        component.set('v.columns', [
                            {label:  $A.get("$Label.c.Name"), fieldName: 'Name', type: 'text', sortable:true},
                            {label:$A.get("$Label.c.Leaves_Remaining"), fieldName: 'Nrich__Leaves_Remaining__c', type: 'number', sortable:true}                       
                        ]);
                    }
                    var categoryTypelist = [];
                    categoryTypelist.push({
                        class: "optionClass",
                        label: "--- None ---",
                        valusaveEmployeeLeaveRequeste:""
                    });
                    var empleaveid;
                    for(var i=0; i<empCategory.length; i=i+1){
                        categoryTypelist.push({
                            class: "optionClass",
                            label: empCategory[i].Name,
                            value: empCategory[i].Id
                        });
                        empleaveid = empCategory[i].Nrich__Leave_Card__r.Nrich__Employeee;  
                        
                    }
                    
                    component.set("v.employeeId", empleaveid);
                    var inputIndustry = component.find("leavecategory");
                    if(inputIndustry!=null)
                        inputIndustry.set("v.options", categoryTypelist);
                }
                component.set("v.IsSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    fetchLeaveRequest : function(component, event, helper) {
        component.set("v.IsSpinner", true);
        var action = component.get("c.fetchEmployeeLeaveRequests");
        
        action.setParams({
            listType: component.get("v.type")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            
            if (state === "SUCCESS") {   
               
                if(outputresponse!=null){                         
                    component.set('v.columns', [
                        {label: $A.get("$Label.c.Name"), fieldName: 'Name', type: 'text', sortable:true},
                        {label: $A.get("$Label.c.Category"), fieldName: 'Nrich__Leave_Type__c', type: 'text', sortable:true},
                        {label: $A.get("$Label.c.Start_Date"), fieldName: 'Nrich__Start_Date__c', type: 'date', sortable:true},
                        {label: $A.get("$Label.c.end_date"), fieldName: 'Nrich__End_Date__c', type: 'date', sortable:true},
                        {label: $A.get("$Label.c.Approval_Status"), fieldName: 'Nrich__Status__c', type: 'String', sortable:true},
                        {label: $A.get("$Label.c.Approver_Name"), fieldName: 'Nrich__Approver_Name__c', type: 'text', sortable:true}
                    ]);
                    component.set("v.employeeLeaveRequestlist", outputresponse);
                }
                component.set("v.IsSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    saveLeaveRequest : function(component, event, helper) {
        component.set("v.IsSpinner", true);		
        var employeeCategorylist = [];
        var validationcheck = "false";
        employeeCategorylist = component.get("v.employeeCategory");
        var categoryId = component.get("v.employeeLeaveRequest").Nrich__Employee_Leave_Category__c;
        if(employeeCategorylist.length>0){
            for(var i=0; i<employeeCategorylist.length; i=i+1){
                if(employeeCategorylist[i].Id==categoryId){
                    if(employeeCategorylist[i].Nrich__Leaves_Remaining__c<=0 && !employeeCategorylist[i].Nrich__Loss_of_Pay__c)
                        validationcheck = "true";
                    else if(employeeCategorylist[i].Nrich__Leaves_Remaining__c<=0 && employeeCategorylist[i].Nrich__Loss_of_Pay__c){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "info",
                            "title": $A.get("$Label.c.Info"),
                            "message": $A.get("$Label.c.Info_of_LossofPay")
                        });
                        toastEvent.fire();
                    }
                }      
            }
        }
        if(validationcheck=="false"){
            var action = component.get("c.saveEmployeeLeaveRequest");        
            action.setParams({
                leaveRequest: component.get("v.employeeLeaveRequest"),
                employeeId: component.get("v.employeeId")
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                var outputresponse = component.get("v.saveresultStatusval");
                outputresponse = response.getReturnValue();
                if (state === "SUCCESS") {
                    if(outputresponse.isSuccess){
                        component.set("v.submitforapproval", true);
                        component.set("v.saveresultStatusval", outputresponse);
                        component.set("v.IsSpinner", false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({    
                            "type" : "success",
                            "title": $A.get("$Label.c.Success_Message"),
                            "message": $A.get("$Label.c.RecordSavedSubmittedFrApproval")
                        });
                        toastEvent.fire();
                    }
                    else{
                        component.set("v.IsSpinner", false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "error",
                            "title": $A.get("$Label.c.Error"),
                            "message": outputresponse.message
                        });
                        toastEvent.fire();
                    }
                }
            });
            $A.enqueueAction(action);
        }else{
            component.set("v.IsSpinner", false);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type" : "error",
                "title":  $A.get("$Label.c.Error"),
                "message": $A.get("$Label.c.Error_In_LeavesCategory")
            });
            toastEvent.fire();
        }
    },
    submitleaveforapproval : function(component, event, helper) {
        var action = component.get("c.submitLeaveRequest");
        action.setParams({
            leaveRequestId: component.get("v.saveresultStatusval").recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var outputresponse = response.getReturnValue();
            if (state === "SUCCESS") {
                if(outputresponse===true){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type" : "success",
                        "title":  $A.get("$Label.c.Success_Message"),
                        "message": $A.get("$Label.c.RecordSavedSubmittedFrApproval")
                    });
                    toastEvent.fire();
                    component.set("v.openmodal", false);
                    var compEvent = component.getEvent("closemodaleventval");
                    compEvent.setParams({"openclosemodal" : 'false',
                                         "typeofmodal" : 'LeaveRequest'});   
                    compEvent.fire();
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type" : "error",
                        "title": $A.get("$Label.c.Error"),
                        "message": $A.get("$Label.c.Error_In_Approval")
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.employeeLeaveRequestlist");
        var res;
        if(data==null){
            data =component.get("v.employeeCategory");
            res = 'category'
        }
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        if(res!='category')
            component.set("v.employeeLeaveRequestlist", data);
        else
            component.set("v.employeeCategory", data);
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
})