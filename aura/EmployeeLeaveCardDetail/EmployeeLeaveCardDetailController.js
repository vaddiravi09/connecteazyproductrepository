({
	doInit : function(component, event, helper) {
        if(component.get("v.type")==$A.get("$Label.c.Insert") || component.get("v.type")==$A.get("$Label.c.Obtained")|| component.get("v.type")==$A.get("$Label.c.Remaining"))
		helper.fetchLeavecard(component, event, helper);
	},
    getLeaveRequest : function(component, event, helper) {
        if(component.get("v.type")==$A.get("$Label.c.Availed"))
          helper.fetchLeaveRequest(component, event, helper);
    },
    save : function(component, event, helper) {
		helper.saveLeaveRequest(component, event, helper);
	},
    submitforapproval : function(component, event, helper) {
        helper.submitleaveforapproval(component, event, helper);
    },
    closeinsertionleavemodal:function(component, event, helper){
        component.set("v.openmodal", false);
         var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'LeaveRequest'});   
         compEvent.fire();
    },
    handleColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    },
})