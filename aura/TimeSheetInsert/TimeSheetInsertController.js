({ 
    doInit: function(component, event, helper) {
        helper.createObjectData(component, event);
    },    
    // Function to fetch Custom Settings
    getcustomsettingdata:function(component, event, helper){
        helper.getCustomdata(component, event);
    },       
    // Function For Save The Records 
    Save: function(component, event, helper) {
        var timesheetlist=[];
        var buttonName = event.getSource().get("v.name");
        if(buttonName== $A.get("$Label.c.Save")){
            helper.validateFields(component,event)
            var condition=component.get("v.Ifcondition");
            var duplicated=component.get("v.Ifduplicate");
           
            //Error Toast Msg If Required Details Or If it is not more than 24 hrs
            if(condition==true)
            {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Error_Message") ,
                    message:$A.get("$Label.c.Error_In_RequiredFields"),
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            }
            
            
           //Error Toast Msg If TimeSheet Hrs Are More Than  24Hrs 
           if(duplicated==true)
            {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Error_Message") ,
                    message:$A.get("$Label.c.Error_In_TotalHrs") ,
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            }
            
            
            
            
            //If Both Conditions Are False Then Insert Value 
            if(condition==false && duplicated == false  )
            {
                //For Saving 
                var action = component.get("c.SaveTimeSheets");
                action.setParams({
                    "ListTimeSheetDetail":component.get("v.TimeSheetDetailDummy"),"datetimes":component.get("v.datetime")
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        component.set("v.TimeSheetDetailList", []);
                        var details=response.getReturnValue();
                        helper.createObjectData(component, event);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : $A.get("$Label.c.Success_Message"),
                            message: $A.get("$Label.c.Saved_Record") ,
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'success',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();
                        component.set("v.isOpenSubmit", true);
                        component.set("v.IsCancel",false); 
                        var compEvent = component.getEvent("closemodaleventval");
                        compEvent.setParams({"displaytabledetails" : true,
                                             "typeofmodal" : 'TimeSheetDisplay',
                                             "UpdatedTask" :details
                                            });   
                        compEvent.fire(); 
                    }
                    else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : $A.get("$Label.c.Error_Message"),
                            message: $A.get("$Label.c.Error"),
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();                       
                    }
                });
                $A.enqueueAction(action);
            }  
        }
        
        if(buttonName==$A.get("$Label.c.Submit_Monthly")){
            // alert('inApproval')
            var action1=component.get("c.submitforapproval");
            action1.setParams({"datetimes":component.get("v.datetime")
                              });
            action1.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS"){
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.Success_Message"),
                        message: $A.get("$Label.c.Approval_Submitted_Successfully"),
                        
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                }                
            });
            $A.enqueueAction(action1);     
        }
        
        if(buttonName==$A.get("$Label.c.Submit_Weekly")){
            var action1=component.get("c.submitforapprovalweek");
            action1.setParams({"datetimes":component.get("v.datetime")
                              });
            action1.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS"){
                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.Success_Message"),
                        message:  $A.get("$Label.c.Approval_Submitted_Successfully"),
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                }
            });
            $A.enqueueAction(action1);  
        }
    },
    
    // function for create new object Row  
    addNewRow: function(component, event, helper) {
        helper.createObjectData(component, event);
    },
    
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        var index = event.getParam("indexVar");
        var AllRowsList = component.get("v.TimeSheetDetailList");
        AllRowsList.splice(index, 1);
        component.set("v.TimeSheetDetailList", AllRowsList);
    },
    
    Cancel: function(component, event, helper) {
        component.set('v.IsCancel',false);
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'TimeSheetInsertComp'});   
        compEvent.fire(); 
        //$A.get('e.force:refreshView').fire();
    },
    
    SubmitForApproval:function(component, event, helper){
        component.set('v.IsApprovalFinal',true);
    },
    closemodal:function(component, event, helper){
        component.set('v.IsApprovalFinal',false);
    }    
})