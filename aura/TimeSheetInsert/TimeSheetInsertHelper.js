({
    createObjectData: function(component, event) {
        var RowItemList = component.get("v.TimeSheetDetailList");
        RowItemList.push({
            'sobjectType':'Nrich__TimeSheet_Detail__c',
            'Nrich__Project__c': '',
            'Nrich__Timesheet__c':'',
            'Nrich__Project_Category__c':'',
            'Nrich__Description__c': '',
            'Nrich__No_of_hours__c':''
        });
        component.set("v.TimeSheetDetailList", RowItemList);        
    },
    getCustomdata: function(component, event){
        var action = component.get("c.fetchcustomSetting"); 
        action.setCallback(this, function(response){            
            if(response.getState() === 'SUCCESS' && response!=null){  
                component.set("v.CustomSettings", response.getReturnValue());
                component.set("v.SetupPageHrs", response.getReturnValue().Nrich__TotalHoursCap__c);
                component.set("v.TimeSheetType",response.getReturnValue().Nrich__Timesheet_Type__c );
            }    
        });        
        $A.enqueueAction(action);
        
        //For Calculating TotalHrs Fr that Day
        var action1 = component.get("c.AggregateDetails"); 
        action1.setParams({
            "datetimes":component.get("v.datetime")
        });
        
        action1.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS' && response!=null ){  
                var data= response.getReturnValue();
                component.set("v.TimeSheetTotalHours",data);
            }
        });
        
        $A.enqueueAction(action1);
        
        
    },
    
    validateFields: function(component, event) {
        var timesheettype=component.get("v.TimeSheetType");
        var allContactRows = component.get("v.TimeSheetDetailList");
        component.set('v.TimeSheetDetailDummy',allContactRows);
        component.set("v.Ifcondition",false);
        component.set("v.Ifduplicate",false); 
        var timesheethrs=component.get("v.TimeSheetTotalHours");
        var phoneregex=/^[0-9]{1} $/;  
        var totalhrs=parseInt(timesheethrs);
        
        for (var indexVar = 0; indexVar < allContactRows.length; indexVar=indexVar+1) {
            totalhrs=totalhrs +parseInt(allContactRows[indexVar].Nrich__No_of_hours__c);
           
            if (allContactRows[indexVar].Nrich__No_of_hours__c == '' ||  (allContactRows[indexVar].Nrich__Description__c == '') ||(allContactRows[indexVar].Nrich__Project__c == $A.get("$Label.c.Please_Select_Project") ||  ( allContactRows[indexVar].Nrich__No_of_hours__c>24    )      ||(allContactRows[indexVar].Nrich__Project_Category__c==$A.get("$Label.c.Please_Select_ProjectCategory"))))
            {
                component.set("v.Ifcondition",true);
            }
            
            if(totalhrs>24 || ( allContactRows[indexVar].Nrich__No_of_hours__c>24    ) ){
                component.set("v.Ifduplicate",true);
            }
            
        }       
    },
})