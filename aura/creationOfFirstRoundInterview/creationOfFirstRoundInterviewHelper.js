({
	fetchPickListVal: function(component, record_Id) {
        var record_Id = component.get("v.recordId");
        var action = component.get("c.getselectOptions");
        action.setParams({
            "record_Id": record_Id,
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                
                for (var i = 0; i < allValues.length; i=i+1) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i].Name,
                        value: allValues[i].Nrich__Job_Round_Order_Number__c
                    });
                }
                component.find('accIndustry').set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
})