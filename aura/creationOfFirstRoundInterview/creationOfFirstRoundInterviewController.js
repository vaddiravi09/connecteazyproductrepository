({
    doInit: function(component, event, helper) {
        var record_Id = component.get("v.recordId");
        var action = component.get("c.getInterviewRound");
        action.setParams({
            "record_Id": record_Id,
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var values = response.getReturnValue();
                if(values === true){
                    helper.fetchPickListVal(component, record_Id);
                }
                else{
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title :$A.get("$Label.c.Error_Message") ,
                        message: $A.get("$Label.c.Interview_Round_is_already_created"),
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    onPicklistChange: function(component, event, helper) {
        // get the value of select option
        var selected = event.getSource().get("v.value");
        component.set("v.selectedValue",selected);
    },
    
    handleOk: function(component, event, helper) {
        var record_Id = component.get("v.recordId");
        var selected = component.get("v.selectedValue");
        var action = component.get("c.storeSelectedValue");
        action.setParams({
            "selected": selected,
            "record_Id" : record_Id,
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var allValues = response.getReturnValue();
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Success_Message"),
                    message: $A.get("$Label.c.Interview_Round_Created"),
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
               var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                  "recordId": component.get("v.recordId"),
                  "slideDevName": "related"
                });
                navEvt.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    selectChanged: function(component, event, helper){
        var x = component.get("v.selectedValue");
    },
    
    handleOpenModal: function(component, event, helper) {
        //For Display Modal, Set the "openModal" attribute to "true"
        var cmpTarget1 = component.find('changeIt');
        $A.util.removeClass(cmpTarget1, 'change');
    },
    
    handleCloseModal: function(component, event, helper) {
        //For Close Modal, Set the "openModal" attribute to "fasle"  
        var cmpTarget1 = component.find('changeIt');
        $A.util.addClass(cmpTarget1, 'change');
    },
    
    
})