({
    doInit : function(component, event, helper) {
       component.set("v.IsSpinner", true);
        var alist=[];
        var allocatedlist=[];
        var pendinglist=[];
        var action = component.get("c.viewAssetRecords");  
       
        action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                 
                var i=0;
                alist=response.getReturnValue();
                for(i=0;i<alist.length;i=i+1)
                { 
                    if(alist.length>0 )
                    {
                        if (alist[i].Nrich__Asset_Items__c!=null && alist[i].Nrich__Asset_Items__r.Nrich__Status__c ==$A.get("$Label.c.Allocated"))
                        {
                            allocatedlist.push(alist[i]);
                        }
                    }
                    
                }
                component.set("v.AssetAllocatedDetails",allocatedlist);   
            }
        });
        
        var action1 = component.get("c.ViewAssetRequestDetails");   
        action1.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                component.set("v.AssetRequestDetails",response.getReturnValue());
            }
            
        });
        
        
        $A.enqueueAction(action);
        $A.enqueueAction(action1);
        
        var action3 = component.get("c.fetchoptions"); 
        action3.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                 
                component.set("v.options",response.getReturnValue());
            } 
            component.set("v.IsSpinner", false);
        });
        $A.enqueueAction(action3);
        
        
    },
    
    insertassetrecord:function(component, event, helper){
        var buttonName = event.getSource().get("v.name");
        var insertiondetails=component.get("v.InsertAssetdetails");
        
        if(buttonName==$A.get("$Label.c.Save")){
            helper.validateFields(component,event)
            var condition=component.get("v.Ifcondition");
            if(condition==true) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Error_Message"),
                    message:$A.get("$Label.c.Errror_In_Required_Fields") ,
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            }
            if(condition==false){
                var action=component.get("c.insertnewrecords");
                action.setParams({'assetdetails':insertiondetails ,'AsId':component.find("colorId").get("v.value")});
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS"){
                        AssetId=response.getReturnValue();
                        component.set("v.AstId",response.getReturnValue());
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : $A.get("$Label.c.Success_Message"),
                            message: $A.get("$Label.c.RecordSavedSubmittedFrApproval"),
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'success',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                        component.set("v.isOpenSubmit", true);
                    }
                });
                $A.enqueueAction(action);
            }
        }
        else {
            var AssetId=component.get("v.AstId");
            var action1=component.get("c.submitforapproval");
            action1.setParams({'asr':AssetId});
            action1.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS"){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.Success_Message"),
                        message: $A.get("$Label.c.Approval_Submitted_Successfully "),
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
            });
            component.set("v.IsSpinner", false);
            $A.enqueueAction(action1);  
            component.set("v.openmodal", false);
            var compEvent = component.getEvent("closemodaleventval");
            compEvent.setParams({"openclosemodal" : 'false',
                                 "typeofmodal" : 'Assetcomp'});   
            compEvent.fire(); 
            $A.get('e.force:refreshView').fire();
        }
    },
    
    closemodal : function(component, event, helper){
        component.set("v.openmodal", false);
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'Assetcomp'});   
        compEvent.fire(); 
    },
    
    handleComponentEvent : function(component, event, helper) {
        var selectedCertificationGetFromEvent = event.getParam("AssetByEvent");
        component.set("v.SelectedAsset",selectedCertificationGetFromEvent);
    }
})