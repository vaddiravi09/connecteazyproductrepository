( {
    init : function( component, event, helper ) {
        if(component.get("v.Type")==$A.get("$Label.c.Sprint")){
            component.set("v.sprintcheck", true);
            helper.initHelper(component, event, helper);
        }else{
            component.set("v.sprintcheck", false);
            helper.initHelper(component, event, helper);
        }
    },
    initload : function( component, event, helper ) {
        helper.hideSpinner(component);
    },    
    handleComponentEvent : function( component, event, helper ) {
        component.set( "v.sprinthandle", true );
        helper.initHelper(component, event, helper);
    },    
    applySortable : function( component, event, helper ) {        
        var sortableApplied = component.get( "v.sortableApplied" );
        var scriptsLoaded 	= component.get( "v.scriptsLoaded" );
        /**
         * Apply the jQuery Sortable
         * when the DOM is ready and 
         * the Scripts have been loaded.
         */
        if( scriptsLoaded 	&& 
           !sortableApplied && 
           jQuery( ".slds-lane" ).length > 0
          ) {
            component.set( "v.sortableApplied", true );
            
            helper.applySortable( component );
        }
    },
    
    editUserStory : function( component, event, helper ) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": event.getSource().get( "v.name" )
        });
        editRecordEvent.fire();
    },
    
    goToRecord:function( component, event, helper ){
        var recId = event.target.id;
        component.set("v.IdsList",recId);
        if(component.get("v.Type")==$A.get("$Label.c.Sprint")){
            component.set("v.Type",$A.get("$Label.c.User_Story"));   
            component.set("v.sprintcheck", false);
            component.set("v.resetbutton", false);
            helper.initHelper(component, event, helper);
        }
        else if(component.get("v.Type")==$A.get("$Label.c.User_Story")){
            component.set("v.Type",$A.get("$Label.c.Task"));  
            component.set("v.sprintcheck", false);
            component.set("v.resetbutton", false);
            helper.initHelper(component, event, helper);
        }
        else {
              window.open('/' + recId, '_blank');  
        }
  
    },
    OnReset:function( component, event, helper ){
        var prj=component.get("v.selectedProject");
        var rec = [];
        component.set("v.IdsList",rec);
        component.set("v.Type",$A.get("$Label.c.Task")); 
        component.set("v.resetbutton", true);
        helper.initHelper(component, event, helper);
        
    },
    
} )