({
    hideSpinner : function( component ) {
        var eleSpinner = component.find("spinner");
        
        $A.util.addClass( eleSpinner, "slds-hide" );
    },
    showSpinner : function( component ) {
        var eleSpinner = component.find("spinner");
        
        $A.util.removeClass( eleSpinner, "slds-hide" );
    },
     initHelper : function(component, event, helper){
       
        helper.showSpinner(component);
        var projectIdval = '';
        var resourceIdval = '';
        if(component.get("v.selectedProject")!=null && component.get("v.selectedProject").Id!=null)
            projectIdval = component.get("v.selectedProject").Id;
        if(component.get("v.selectedResource")!=null && component.get("v.selectedResource").Id!=null)
            resourceIdval = component.get("v.selectedResource").Id;
        var action = component.get( "c.getStatusdetails" );
        action.setParams({
            "ProjectID": projectIdval,
            "ResourceId": resourceIdval,
            "statusType": component.get("v.Type"),
            "Priority": component.get("v.Priority"),
            "IdsList":component.get("v.IdsList")
        });
        component.set( "v.scriptsLoaded", true );
        
        action.setCallback( this, function( response ) {
            var state = response.getState();
            
            if ( state === "SUCCESS" ) {
                var result = response.getReturnValue();
                component.set("v.projectstatusList", result);
                if(component.get( "v.sprinthandle"))
                    component.set( "v.sortableApplied", false);
                helper.hideSpinner(component);
            }
            else {
                helper.hideSpinner(component);
               helper.showToast( 
                    {
                        "title"		: $A.get("$Label.c.Error"), 
                        "message"	:  $A.get("$Label.c.Error")+ JSON.stringify( response.getError() ) + ", State: " + state, 
                        "isSuccess"	: "error",
                        "type" : "Error"
                    } 
                );
                
            }
        } );
        
        $A.enqueueAction( action );
    },
    showToast : function( data ) {
        var toastEvent = $A.get( "e.force:showToast" );
        
        /*toastEvent.setParams(
            {
                duration	: 2000,
                title		: data.title,
                message		: data.message,
                type		: data.type ? data.type : (data.isSuccess ? "success" : "error")
            }
        );
        
        toastEvent.fire();*/
    },
    applySortable : function( component ) {     
        var helper = this;
        
        jQuery( ".slds-lane" ).sortable(
            {
                revert				: true,
                connectWith			: ".slds-lane",
                handle 				: ".slds-title",
                placeholder 		: "slds-item slds-m-around--small slds-item-placeholder"
            }
        );
        
        jQuery( ".slds-lane" ).on(
            "sortstart",
            $A.getCallback(
                function( event, ui ) {                    
                    jQuery( ui.item ).addClass( "moving-card" );                   
                }
            )
        );
        
        jQuery( ".slds-lane" ).on(
            "sortstop",
            $A.getCallback(
                function( event, ui ) {                    
                    jQuery( ui.item ).removeClass( "moving-card" );                    
                    
                    var leadId       		= $( ui.item ).data( "id" );
                    var oldLeadStatus 		= $( ui.item ).data( "status" );
                    var newLeadStatus   	= $( ui.item ).parent().data( "name" );
                    helper.showSpinner( component );
                    
                    var action = component.get( "c.UpdateStatus" );
                    var params = {
                        "objectId" 		: leadId,
                        "Status" : newLeadStatus,
                        "statusType" : component.get("v.Type")
                    };
                    
                    /**
                         * Maintain the ordering within
                         * the lane.
                         */
                    /*  $( ui.item ).parent().children().each(
                            function() {
                                params.ordering.push( $( this ).data( "id" ) );
                            }
                        );*/
                    
                    action.setParams( params );
                    
                    action.setCallback( 
                        this, 
                        function( response ) {
                            var state = response.getState();
                            
                            helper.hideSpinner( component );
                            
                            if( state === "SUCCESS" ) {                                    
                                var updateStatus = response.getReturnValue();
                                
                                /**
                                     * Show a separate message
                                     * if the cards were just
                                     * re-arranged within the
                                     * same column.
                                     */                                    
                                if( oldLeadStatus === newLeadStatus ) {
                                    updateStatus.isSuccess  = true;
                                   updateStatus.message	=   $A.get("$Label.c.ColumnOrderingwasUpdated");
		                     
                                 //  updateStatus.message	= "Column Ordering was Updated.";      
                                }
                                
                                $( ui.item ).attr( "data-status", newLeadStatus );
                                
                                helper.showToast( updateStatus );
                                if(!updateStatus.isSuccess){
                                    component.set( "v.sprinthandle", true );
                                    helper.initHelper(component, event, helper);
                                }
                            }                                
                        }
                    );
                    
                    $A.enqueueAction( action );
                    
                }
            )
        );
    }
})