({
    doInit : function(component, event, helper) {
        
        component.set("v.IsSpinner", true);
        
        component.set('v.columns', [
            {label: $A.get("$Label.c.Name"), fieldName: 'Name', type: 'text', sortable:true},
            {label: $A.get("$Label.c.Employee_Start_Date"), fieldName: 'Nrich__Employee_Start_Date__c', type: 'date', sortable:true}, 
            {label: $A.get("$Label.c.Employee_End_Date"), fieldName: 'Nrich__Employee_End_Date__c', type: 'date', sortable:true} ,
            {label:$A.get("$Label.c.Project"), fieldName: 'Nrich__Project_Name__c', type: 'text', sortable:true},
           // {label: $A.get("$Label.c.Project_Manager"), fieldName: 'Nrich__ProjectManagerName__c', type: 'text', sortable:true},
        ]);
            
            var action = component.get("c.ViewProjectDetails");  
            action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
            var alist=response.getReturnValue();
            component.set('v.CurrentProjectDetails',alist.CurrentProjectList);
            component.set('v.PastProjectDetails',alist.PastProjectList);
            component.set('v.OwnedByMeProjectDetails',alist.OwnProjectList);
            
            }
            component.set("v.IsSpinner", false);
            });
            $A.enqueueAction(action); 
            
            
            
            },
            
   handleColumnSorting: function (component, event, helper) 
            {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection');
            component.set("v.sortedBy", fieldName);
            component.set("v.sortedDirection", sortDirection);
            helper.sortData(component, fieldName, sortDirection);
            },
            
  closemodal : function(component, event, helper)
            {
            component.set("v.openmodal", false);
            var compEvent = component.getEvent("closemodaleventval");
            compEvent.setParams({"openclosemodal" : 'false',
            "typeofmodal" : 'Projectcomp'});   
            compEvent.fire(); 
            
            },
            
            })