({
    doInit : function(component, event, helper) {
        component.set('v.columns', [
            {label: $A.get("$Label.c.Title"), fieldName: 'Nrich__Policy_Title__c', type: 'text', sortable:true},
            {label: $A.get("$Label.c.Status"), fieldName: 'Nrich__status__c', type: 'text', sortable:true},
            {label:$A.get("$Label.c.Date_Issued"), fieldName: 'Nrich__Date_Issued__c', type: 'date', sortable:true},
            {type: 'button', typeAttributes: {label: $A.get("$Label.c.View"), name: 'View', 'iconName': 'utility:preview', iconPosition: 'right' }},
            {type: 'button', typeAttributes: {label:$A.get("$Label.c.Accept"), name: 'Accept', 'iconName': 'utility:approval', iconPosition: 'right' }}            
        ]);
        
        component.set('v.columns2', [
            {label: $A.get("$Label.c.Title"), fieldName: 'Nrich__Policy_Title__c', type: 'text', sortable:true},
            {label: $A.get("$Label.c.Status"), fieldName: 'Nrich__status__c', type: 'text', sortable:true},
            {label: $A.get("$Label.c.Date_Issued"), fieldName: 'Nrich__Date_Issued__c', type: 'date', sortable:true},
            {type: 'button', typeAttributes: {label: $A.get("$Label.c.View"), name: 'View', 'iconName': 'utility:preview', iconPosition: 'right' }},
        ]);
            },
            closeinsertionleavemodal:function(component, event, helper){
            component.set("v.openmodal", false);
            var compEvent = component.getEvent("closemodaleventval");
            compEvent.setParams({"openclosemodal" : 'false',
            "typeofmodal" : 'policy'});   
            compEvent.fire();
            },
            handleColumnSorting: function (component, event, helper) {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection');
            component.set("v.sortedBy", fieldName);
            component.set("v.sortedDirection", sortDirection);
            helper.sortData(component, fieldName, sortDirection);
            },
            goback: function (component, event, helper) {
            component.set("v.opendetailmodal", false);
            component.set("v.opensignatureblock", "false");
            component.set("v.approvalbutton", "false");
            },
            handleRowAction: function (component, event, helper) {
            var action = event.getParam('action');
            var row = event.getParam('row');
            var listval = [];
                      listval.push(row);
        component.set("v.selectedlist", listval);
        switch (action.name) {
            case  $A.get("$Label.c.Accept"):
                helper.submitforapproval(component, event)
                if(component.get("v.signatureBlockRequired")){
                    helper.goToRecord(component, row)
                    component.set("v.opensignatureblock", "true");          
                    component.set("v.approvalbutton", "true");
                }
                break;
            case $A.get("$Label.c.View"):
                helper.goToRecord(component, row)
                break;
        }  
    },
    
    Approvepolicy: function (component, event, helper) {
        if(component.get("v.signatureBlockRequired"))
            component.set("v.opensignatureblock", "true");          
        component.set("v.approvalbutton", "true");
    },
    
    handleAssetEvent: function (component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({    
            "type" : "success",
            "title": $A.get("$Label.c.Success_Message"),
            "message":$A.get("$Label.c.Saved_Record")
        });
        toastEvent.fire();
        component.set("v.opensignatureblock", "false");          
        component.set("v.approvalbutton", "true");
    }
})