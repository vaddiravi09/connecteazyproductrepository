({
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.employeepolicyList");
        var res;
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.employeepolicyList", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
    submitforapproval : function (component, event) {
        component.set("v.IsSpinner",true);
        var action = component.get("c.AcceptorRejectPolicy"); 
        var todorec = component.get("v.selectedlist");
        action.setParams({
            employeepolicy: JSON.stringify(todorec)
        });
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){               
                component.set("v.employeepolicyList", response.getReturnValue());
                component.set("v.IsSpinner",false);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({    
                    "type" : "success",
                    "title": $A.get("$Label.c.Success_Message"),
                    "message":$A.get("$Label.c.Updated_Record")
                });
                toastEvent.fire();
            }
        });
        
        $A.enqueueAction(action); 
    },
    goToRecord : function(component, row) {
        if(row != undefined){
            component.set("v.IsSpinner",true);
            component.set("v.selectedEmployeepolicy", row);   
            if(component.get("v.selectedEmployeepolicy").Nrich__status__c===$A.get("$Label.c.Accepted"))
                component.set("v.approvalbutton", "true");      
            
            var action = component.get("c.getPolicyDocument"); 
            action.setParams({
                policyId: component.get("v.selectedEmployeepolicy").Nrich__Policy__c
            });
            action.setCallback(this, function(response){
                if(response.getState() ==='SUCCESS'){  
                    var link=response.getReturnValue();
                    component.set("v.policydocumentId",link);
                    component.set("v.IsSpinner",false); 
                    component.set("v.opendetailmodal", true);
                }
            });
            
            $A.enqueueAction(action);          
        }      
    },
})