({
    doInit : function(component, event, helper) {
        document.body.setAttribute('style', 'overflow: hidden;');
        var action = component.get("c.fetchEmployeeDetails");
        action.setParams({
            "searchword":component.get("v.searchKeyword"),
            "interviewRoundId":component.get("v.recordId"),
            "starttime":component.get("v.startTime"),
            "endtime":component.get("v.endTime"),
            "offsetval":0
        });
        action.setCallback(this, function(a){
            var state = a.getState();
            if(state === "SUCCESS"){
                var employeedetails = a.getReturnValue();
                component.set("v.employee", employeedetails);
            }
        });
        $A.enqueueAction(action);
        
    },
    applicantDetails : function(component, event, helper) {
        var action = component.get("c.fetchApplicantDetails");
        action.setParams({
            "interviewRoundId":component.get("v.recordId")
        });
        action.setCallback(this, function(a){
            var state = a.getState();
            if(state === "SUCCESS"){
                var applicantdetails = a.getReturnValue();
                component.set("v.interviewround", applicantdetails);
                component.set("v.applicantId",applicantdetails.applicantId);
                component.set("v.startTime", applicantdetails.interviewStartDate);
                component.set("v.endTime", applicantdetails.interviewEndDate);
                component.set("v.scheduled",true);
            }
        });
        $A.enqueueAction(action);
    },
    navigateToApplicant : function(component, event, helper) {
      var applicantId = component.get("v.applicantId");

        var sObectEvent = $A.get("e.force:navigateToSObject");
        sObectEvent .setParams({
            "recordId": applicantId,
            "slideDevName": "detail"
        });
        sObectEvent.fire();  
    },
    fetchPanels : function(component, event, helper) {
        var action = component.get("c.fetchPanelMembers");
        action.setParams({
            "interviewRoundId":component.get("v.recordId")
        });
        action.setCallback(this, function(a){
            var state = a.getState();
            if(state === "SUCCESS"){
                var panelmembers = a.getReturnValue();
                if(panelmembers.length>0){
                    component.set("v.selectedemployee", panelmembers);
                    component.set("v.reschedule", true);
                }
            }
        });
        $A.enqueueAction(action);
    },
    checkValidDate : function(component, event, helper) {
        var startTime = component.get("v.startTime");
    	var endTime = component.get("v.endTime");
        var today = new Date();
        today = today.toISOString();   
        if(startTime != '' && startTime!=undefined ){
            if(startTime.valueOf() < today.valueOf()){
                var startTimeError = $A.get("$Label.c.Invalid_Schedule_Start_Time"); 
                component.set("v.errorFlag",true);
                component.set("v.errorMessage", startTimeError);
            }else{
                component.set("v.errorFlag",false);
            }
            
        }
        
        if(startTime != '' && endTime!= '' && startTime!=undefined && endTime!=undefined ){
        	var errorval = component.get("v.errorFlag");
            if(errorval==false){
                var splitval = [];
                splitval = startTime.split('T');
                var splitvalend = [];
                splitvalend = endTime.split('T');
                if(splitval.length>1){
                    if(splitval[0]!=splitvalend[0]){
                        var startTimeError = $A.get("$Label.c.HRInterviewScduleError"); 
                        component.set("v.errorFlag",true);
                        component.set("v.errorMessage", startTimeError);
                    }               
                }
             }
            var errorval = component.get("v.errorFlag");
            if(errorval==false){
                if(endTime.valueOf() < startTime.valueOf()){
                    var endTimeError = $A.get("$Label.c.Invalid_Schedule_End_Time"); 
                    component.set("v.errorFlag",true);
                    component.set("v.errorMessage", endTimeError);
                }else{
                    component.set("v.errorFlag",false);
                }
            }
        }
        
	}, 
    searchonTime : function(component, event, helper) {
        
        var action = component.get("c.fetchEmployeeDetails");
        action.setParams({
            "searchword":component.get("v.searchKeyword"),
            "interviewRoundId":component.get("v.recordId"),
            "starttime":component.get("v.startTime"),
            "endtime":component.get("v.endTime"),
            "offsetval":0
        });
        action.setCallback(this, function(a){
            var state = a.getState();
            if(state === "SUCCESS"){
                var employeedetails = a.getReturnValue();
                component.set("v.employee", employeedetails);
            }
        });
        $A.enqueueAction(action);
    },
    feedbackcheck : function(component, event, helper) {
        component.set("v.feedbackCount", true);
    },
    scheduleInterview : function(component, event, helper) {
        var startTime = component.get("v.startTime");
        var endTime = component.get("v.endTime");
        var feedbackval = component.get("v.feedbackCount");
        var selectedEmployeeList = component.get("v.selectedemployee");
        var selected = component.get("v.selected");
        if(selectedEmployeeList.length>0){
			selected = true;            
            for(var i=0; i<selectedEmployeeList.length;i=i+1){
                if(selectedEmployeeList[i].employeefeedback == true){
                    feedbackval = true;
                    break;
                }else{
                    feedbackval = false;
                }
                	
            }            
        }
        
        if (!selected){
            
            component.set("v.errorFlag",true);
            component.set("v.errorMessage",$A.get("$Label.c.Error_select_atleast_one_panel_member"));
            
        }else if (startTime == undefined && endTime == undefined && selected) 
            {
                var startEndDateError = $A.get("$Label.c.Empty_Start_and_End_Time"); 
                component.set("v.errorFlag",true);
                component.set("v.errorMessage",startEndDateError);
            
            }
            else if (startTime == undefined && selected) 
            {
                 var startTimeError = $A.get("$Label.c.Empty_Start_Time"); 
                component.set("v.errorFlag",true);
                component.set("v.errorMessage",startTimeError);
               
            }else if(endTime == undefined && selected){
                var endTimeError = $A.get("$Label.c.Empty_End_Time"); 
              	component.set("v.errorFlag",true);
                component.set("v.errorMessage",endTimeError);
               
            }
            else if(!feedbackval && selected){
               component.set("v.errorFlag",true);
               component.set("v.errorMessage", $A.get("$Label.c.Error_Feedback_Checkbox"));
               
            }
            else{
               helper.show(component,event);
            var action = component.get("c.schedule");
            action.setParams({
                "employeeval":JSON.stringify(component.get("v.selectedemployee")),
                "interviewRoundId":component.get("v.recordId"),
                "starttime":component.get("v.startTime"),
                "endtime":component.get("v.endTime"),
                "reschedule": component.get("v.reschedule")
            });
            action.setCallback(this, function(a){
                var state = a.getState();
                if(state ==="SUCCESS"){   
                    console.log('error'+a.getReturnValue());
                    if(a.getReturnValue()!='success'){
                        var msgs = a.getReturnValue();
                       helper.hide(component,event);
                       component.set("v.errorMessage", msgs);
                        component.set("v.errorFlag", true);
                    }else{
                        helper.hide(component,event);
                    component.set("v.reschedule", true);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.Success_Message"),
                        message: $A.get("$Label.c.Scheduled_Successfully"),
                        messageTemplate: $A.get("$Label.c.Scheduled_Successfully"),
                        duration:' 3000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                   
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                      "recordId": component.get("v.recordId"),
                      "slideDevName": "detail"
                    });
                    navEvt.fire();
                  }
                }
            });
            $A.enqueueAction(action);
                }

    },
    
    selectEmployee : function(component, event, helper) {
        var rowCount = event.getSource().get("v.name");
        var empList = component.get("v.employee");
        var selectedEmployee = empList[rowCount];
        var selectedEmployeeList = component.get("v.selectedemployee");
        selectedEmployeeList.push(selectedEmployee);
        component.set("v.selectedemployee",selectedEmployeeList);
        empList.splice(rowCount, 1);    
        component.set("v.employee", empList);
        component.set("v.selected", true);
    },
    
    unselectEmployee : function(component, event, helper) {
        var rowCount = event.getSource().get("v.name");
        var empList = component.get("v.employee");      
        var selectedEmployeeList = component.get("v.selectedemployee");
        var selectedEmployee = selectedEmployeeList[rowCount];
        empList.push(selectedEmployee);
        component.set("v.employee", empList);
        if(selectedEmployeeList.length>0){        
            selectedEmployeeList.splice(rowCount, 1);
        }else{
            component.set("v.selected", false);
        }
        component.set("v.selectedemployee",selectedEmployeeList);
       
    },
    
    employeefilter : function(component, event, helper) {
        var action = component.get("c.fetchEmployeeDetails");
        action.setParams({
            "searchword":component.get("v.searchKeyword"),
            "interviewRoundId":component.get("v.recordId"),
            "starttime":component.get("v.startTime"),
            "endtime":component.get("v.endTime"),
            "offsetval":0
        });
        action.setCallback(this, function(a){
            var state = a.getState();
            if(state === "SUCCESS"){
                var employeedetails = a.getReturnValue();
                component.set("v.employee", employeedetails);
            }
        });
        $A.enqueueAction(action);
    },
    

})