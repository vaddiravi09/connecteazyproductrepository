({
    
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.todolist");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.todolist", data);
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
    
    getRowActions: function (component, row, doneCallback) {       
        actions.push({
            'label': $A.get("$Label.c.Approve"),
            'iconName': 'utility:approval',
            'name':  $A.get("$Label.c.Approve")
        });
        actions.push({
            'label': $A.get("$Label.c.Reject"),
            'iconName': 'utility:ban',
            'name': $A.get("$Label.c.Reject")
        });
        
        // simulate a trip to the server
        setTimeout($A.getCallback(function () {
            doneCallback(actions);
        }), 200);
    },
    
    submitforapproval : function (component, event) {
        component.set("v.IsSpinner",true);
        var action = component.get("c.DynamicApprovalProcess"); 
        var todorec = component.get("v.selectedlist");
        action.setParams({
            todoString: JSON.stringify(todorec),
            approve: component.get("v.approved")
        });
        action.setCallback(this, function(response){
            if(response.getState() ==='SUCCESS'){               
                component.set("v.todolist", response.getReturnValue());
                component.set("v.IsSpinner",false);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({    
                    "type" : "success",
                    "title": $A.get("$Label.c.Success_Message"),
                    "message":$A.get("$Label.c.Updated_Record") 
                });
                toastEvent.fire();
            }
        });
        
        $A.enqueueAction(action); 
    },
    goToRecord : function(component, row) {
        if(row != undefined){        
            window.open('/' + row.Id,'_blank');
        }        
    },
})