({
	doInit : function(component, event, helper) {
        component.set('v.columns', [
            {label: $A.get("$Label.c.Title"), fieldName: 'Nrich__Title__c', type: 'text', sortable:true},
            {label: $A.get("$Label.c.Body"), fieldName: 'Nrich__Body__c', type: 'text', sortable:true},
            {label: $A.get("$Label.c.Due_Date"), fieldName: 'Nrich__Due_Date__c', type: 'date', sortable:true},
            {type: 'button', typeAttributes: {label: $A.get("$Label.c.View"), name: 'View', 'iconName': 'utility:preview', iconPosition: 'right' }},
            {type: 'button', typeAttributes: {label: $A.get("$Label.c.Approve"), name: 'Approve', 'iconName': 'utility:approval', iconPosition: 'right' }},
            {type: 'button', typeAttributes: {label: $A.get("$Label.c.Reject"), name: 'Reject', 'iconName': 'utility:ban', iconPosition: 'right'}}
        ]);
	},
    closemodal : function(component, event, helper){
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'todolistcomp'});   
        compEvent.fire();
    },
    handleColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    },
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        var listval = component.get("v.selectedlist");
        listval.push(row);
        component.set("v.selectedlist", listval);
       
        switch (action.name) {
            case $A.get("$Label.c.Approve"):
                component.set("v.approved", true)
                helper.submitforapproval(component, event)
                break;
            case $A.get("$Label.c.Reject"):
                component.set("v.approved", false)
                helper.submitforapproval(component, event)
                break;
            case $A.get("$Label.c.View"):
                helper.goToRecord(component, row)
                break;
           }
    },
    handleRowSelection : function (component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        component.set("v.selectedlist", selectedRows);
    },
    bulkapprove : function (component, event, helper) {
        if(component.get("v.selectedlist").length>0){
            component.set("v.approved", true)
            helper.submitforapproval(component, event);
        }else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({    
                "type" : "error",
                "title": $A.get("$Label.c.Error"),
                "message":$A.get("$Label.c.Please_Select_Records") 
            });
            toastEvent.fire();
        }
    },
    bulkreject : function (component, event, helper) {
        if(component.get("v.selectedlist").length>0){
            component.set("v.approved", false)
            helper.submitforapproval(component, event);
        }else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({    
                "type" : "error",
                "title": $A.get("$Label.c.Error"),
                "message": $A.get("$Label.c.Please_Select_Records")
            });
            toastEvent.fire();
        }
    },
    
})