({
	doInit : function(component, event, helper) {
		component.set("v.IsSpinner", true);
		var action = component.get("c.fetchAchievementCategory");              
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){  
               if(response.getReturnValue()!=null) {            
                 component.set("v.categorylist", response.getReturnValue());
                }
                 component.set("v.IsSpinner", false);
            }
        });
        
        $A.enqueueAction(action); 
	},
	closemodal : function(component, event, helper){
        var compEvent = component.getEvent("closemodaleventval");
        compEvent.setParams({"openclosemodal" : 'false',
                             "typeofmodal" : 'achivementcomp'});   
        compEvent.fire();
    },
})