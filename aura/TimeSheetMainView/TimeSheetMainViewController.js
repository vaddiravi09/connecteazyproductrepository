({
    afterScriptsLoaded: function(cmp,evt,helper){
        cmp.set('v.columns', [		
            {label:$A.get("$Label.c.Project_Name"), fieldName: 'Nrich__Project_Name__c', type: 'text', sortable:true},		
            {label:$A.get("$Label.c.Task"), fieldName: 'Nrich__Project_Category_Name__c', type: 'text', sortable:true},		
            {label:$A.get("$Label.c.Details"), fieldName: 'Nrich__Description__c', type: 'text', sortable:true},		
            {label:$A.get("$Label.c.Hours") , fieldName: 'Nrich__No_of_hours__c', type: 'number', sortable:true}		
        ]);
        
        cmp.set('v.IsSpinner',true);
        var action = cmp.get("c.FetchLeaveRequests");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data= response.getReturnValue();
                cmp.set('v.TimeApprovedList',data.TimeSheetApprovedList);
                cmp.set('v.Approvedlist',data.ApprovedList);
                cmp.set('v.HolidayList',data.HolidayList);
                cmp.set('v.SubmittedList',data.SubmittedForApprovalList);
                cmp.set("v.flag",true);
            }
            cmp.set('v.IsSpinner',false);
        });
        $A.enqueueAction(action);
    },
    customsetval:function(component, event, helper){
        helper.getCustomdata(component, event);
    },
    
    //Calls helper when afterscripts are loaded
    Callhelper :function(component, event, helper)
    {
        helper.fetchCalenderEvents(component);
    },
    
    
    closemodaleventmethod :  function(component, event, helper) {        
        var typeofevent  = event.getParam("typeofmodal");
        var openclose  = event.getParam("openclosemodal");
        var updatedtimesheetdetails= event.getParam("UpdatedTask");
               
        var completedetailsvar=[];
        component.set("v.adddisable", false);
        var datevalue;
        if(updatedtimesheetdetails!=undefined){
            for(var i=0;i<updatedtimesheetdetails.length;i=i+1){
                completedetailsvar.push(updatedtimesheetdetails[i]);
                datevalue = updatedtimesheetdetails[i].Nrich__Date__c;
            }
            component.set("v.ViewCompleteddetails" ,completedetailsvar);
        }
        
        //from child component send boolean true to  displaytabledetails in parent
        var displaytable=event.getParam("displaytabledetails");
        component.set("v.adddisable", false);
        if(typeofevent=='TimeSheetInsertComp')
        {	
            component.set("v.Addtask",openclose);
            component.set("v.adddisable", false);
            component.set("v.eventoutput",true);
        }
        else if(typeofevent=="ProjectAllocations")		
            component.set("v.allocatedProjectView", openclose);
            else if(typeofevent=='TimeSheetDisplay'){
                component.set("v.eventoutput",displaytable);
                component.set("v.Addtask",false);
                helper.refreshcalendarview(component, datevalue);
            }
    },
    //Loads project category options
    projectid:function(component, event, helper){
        helper.FetchProjectOptions(component, event);
    },
    
    CopyTask:function(component,evt,helper){
        helper.CopyDetails(component);
    },
    Cancel:function(component,evt,helper){
        
        component.set("v.IsRadioCheck", false);		
        component.set("v.IsRadioChecklist", false);        		
        component.set("v.selectedRowBoolean", false);
        
        //Assigning Old Data to that same table attribute 
        var data= component.get("v.ViewCompletedDummyDetails");
        component.set("v.ViewCompleteddetails",data);
    },
    
    FrmValidation:function(component,event,helper){
        // helper.FrmValidationHelper(component,event);
    },
    
    TaskController:function(component,evt,helper){
        component.set('v.Addtask',true);
        component.set("v.adddisable", true);
        component.set('v.eventoutput',false);
        component.set('v.IsRadioCheck',false);
    },
    
    onCheck:function(component,evt,helper){
        component.set('v.IsRadioCheck',false);
        component.set("v.selectedRowBoolean", false);
        var condition= evt.getSource().get("v.checked");
        var dataval = component.get("v.selectedRowsList");
        
        if(condition==false)
        {
            dataval.push(1);
            component.set("v.selectedRowsList", dataval);  
             window.scrollTo({
              top: 300,
              behavior: 'smooth',
            });   
        }else{
            var data = component.get("v.selectedRowsList");
            data.pop();
            component.set("v.selectedRowsList", data);
            window.scrollTo({
              top: 0,
              behavior: 'smooth',
            });
        }
        if(component.get("v.selectedRowsList").length>0)		        
            component.set('v.IsRadioCheck',true);

           
    },
    
    EditInput:function(component,evt,helper){
        var value= evt.currentTarget.dataset.row;
        var divid='input'+value;
        var divid1='output'+value;
        document.getElementById(divid1).style.display ="none";
        document.getElementById(divid).style.display ="table-row";
    },
    
    onCancelled:function(component,evt,helper)
    {
        var value= evt.currentTarget.dataset.row;
        var divid='input'+value;
        var divid1='output'+value;
        document.getElementById(divid1).style.display ="table-row";
        document.getElementById(divid).style.display ="none";
    }, 
    Save:function(component,evt,helper)
    {
        helper.SaveUpdated(component,evt);
    },
    
    SubmitForApproval:function(component,evt,helper){
        component.set('v.showhelptext',false);
        component.set('v.IsApprovalFinal',true);
        
        var type=component.get("v.CustomSettings").Nrich__Timesheet_Type__c;
        
        
        var i=0;
        var count=0;
        var action1=component.get("c.fetchdetails");
        action1.setParams({"datetimes":component.get("v.datetime"),"Approvaltype":type
                          });
        action1.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var data=response.getReturnValue();
                //alert('data' +JSON.stringify(data));
                for(data[i];i<data.length;i=i+1){
                    count++;
                }
                //alert('count' +count);
                if(type=='Weekly' && count<5)
                {
                    component.set('v.showhelptext',true);
                }
                
                if(type=='Monthly' && count<20){
                    component.set('v.showhelptext',true);
                }                
            }
            
        });
        $A.enqueueAction(action1);  
        
        
    },
    closemodal:function(component, event, helper){
        component.set('v.IsApprovalFinal',false);
        
    },
    // Call Approval Mehtod From Copy Button
    SaveApproval:function(component,event,helper){
        helper.SaveApproval1(component,event);
    },
    openallocations : function(component,event,helper){		    
        component.set("v.allocatedProjectView", true);		
    },		
    opentimesheets : function(component,event,helper){		
        component.set("v.timesheetListView", true);		
    },		
    handleColumnSorting: function (component, event, helper) {		
        var fieldName = event.getParam('fieldName');		
        var sortDirection = event.getParam('sortDirection');		
        component.set("v.sortedBy", fieldName);		
        component.set("v.sortedDirection", sortDirection);		
        helper.sortData(component, fieldName, sortDirection);		
    },		
    closeinsertionleavemodal : function(component,event,helper){		
        component.set("v.timesheetListView", false);		
    },		
    copytaskfromtable : function(component,event,helper){		
        var selectedRowsval = event.getParam('selectedRows');		
        
        if(selectedRowsval.length>0){       		
            for(var i=0; i<selectedRowsval.length; i=i+1){		
                selectedRowsval[i].Nrich__CopyTask__c = true;		
            }		
            component.set("v.selectedRowsChecked", selectedRowsval);		
            component.set("v.IsRadioChecklist", true);		
            component.set("v.selectedRowBoolean", true);		
        }		
        else{		
            component.set("v.IsRadioChecklist", false);		
        }		
    },
    
    DeletingTask:function(component,event,helper){
        var value= event.currentTarget.dataset.row;
        helper.DeleteSelectedTask(component,event, value, helper);
    }
    
})