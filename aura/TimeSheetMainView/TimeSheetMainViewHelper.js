({
    loadDataToCalendar :function(component,data){
        var approved=[];
        var holiday=[];
        var submitted=[];
        var tsapproved=[];
        var TMApprovedcolor=component.get("v.ApprovedColor");
        var TMSubmittedcolor=component.get("v.SubmittedColor");
        var Holidaycolor=component.get("v.HolidayColor");
        var LeaveReqcolor=component.get("v.LeaveReqColor");
        
        
        approved=component.get("v.Approvedlist");
        holiday=component.get("v.HolidayList");
        submitted=component.get("v.SubmittedList");
        tsapproved=component.get("v.TimeApprovedList");
        
        var contactid=component.get("v.recordId");
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var currentDate = d.getFullYear() + '/' +
            (month<10 ? '0' : '') + month + '/' +
            (day<10 ? '0' : '') + day;
        var self = this;
        $('#calendar').fullCalendar({
            header: {
                //  left: 'month,basicWeek,prev',
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            
            dayRender: function(date, cell,component) {
                var today = $.fullCalendar.moment();
                //Find Current date for default date
                var formatteddate=date.format();
                //LeaveReqDates
                for(var i=0;i<=approved.length;i=i+1)
                {
                    if(formatteddate==approved[i]){
                        cell.css("background",LeaveReqcolor);                        
                    }
                }
                
                //Holiday dates
                for(var i=0;i<=holiday.length;i=i+1)
                {
                    if(formatteddate==holiday[i])
                    {
                        cell.css("background",Holidaycolor);
                    }
                }
                //Submitted Fr Approval
                for(var i=0;i<=submitted.length;i=i+1)
                {
                    if(formatteddate==submitted[i])
                    {
                        cell.css("background",TMSubmittedcolor);
                    }
                }
                
                //TimesheetApproved Dates
                for(var i=0;i<=tsapproved.length;i=i+1)
                {
                    if(formatteddate==tsapproved[i])
                    {
                        cell.css("background",TMApprovedcolor);                        
                    }
                }
            },
            
            selectable : true,
            defaultDate: currentDate,
            editable: false,
            eventLimit: true,
            events:data,
            dragScroll : true,
            droppable: true,
            weekNumbers : false,
            eventDrop: false,
            eventClick: function(event, jsEvent, view) {
                component.set('v.Addtask',false);
                var element = document.getElementById("myDIV");
                element.classList.remove("icon");
                var device = $A.get("$Browser.formFactor");
                if(device==='PHONE')
                 element.classList.add("icon2");
                var date = event.start;
                var now = date? new Date(date) : new Date();
                // Get the previous Monday		
                var monday = new Date(now);		
                monday.setDate(monday.getDate() - monday.getDay() + 0);		
                
                // Get next Sunday		
                var sunday = new Date(now);		
                sunday.setDate(sunday.getDate() - sunday.getDay() + 6);		
                
                //Get FirstDay Of Month 		
                var monthStartDay = new Date(now.getFullYear(), now.getMonth(), 1);		
                
                //Get LastDay Of Month		
                var monthEndDay = new Date(now.getFullYear(), now.getMonth() + 1, 0);		
                // alert('start' +monthStartDay.toString().split(' ').slice(0, 4).join(' ') +'--End' +monthEndDay);		
                
                component.set("v.StartDate" ,monday.toString().split(' ').slice(0, 4).join(' '));		
                component.set("v.EndDate" ,sunday.toString().split(' ').slice(0, 4).join(' '));		
                component.set("v.MonstartDate",monthStartDay.toString().split(' ').slice(0, 4).join(' '));		
                component.set("v.MonendDate",monthEndDay.toString().split(' ').slice(0, 4).join(' '));		
                component.set('v.Editable',true);		
                component.set("v.IsShowSupport",true);		
                component.set('v.submitted',true);		
                component.set("v.create",true);		
                component.set("v.DisplayProjectAllocations",false);	
                component.set("v.IsRadioCheck",false);
                component.set("v.adddisable", false);
                //component.set("v.displaytablefrmchild",true);
                component.set("v.TodayDate",now.toString().split(' ').slice(0, 4).join(' '));		
                var datelist = date.format().toString().split('-');		
                // component.set("v.wkdate",datelist);
                // If Already Submitted For Approval		
                var dd=date.format();		
                for(var i=0;i<=submitted.length;i=i+1){		
                    if(dd==submitted[i])		
                    {		    
                        component.set('v.submitted',false);		
                        component.set('v.Editable',false); 		                       
                    }		
                }		
                
                //If TimeSheet Approved		
                for(var i=0;i<=tsapproved.length;i=i+1)		
                {		
                    if(dd==tsapproved[i])		
                    {		
                        component.set('v.submitted',false);		
                        component.set('v.Editable',false); 		                        
                    }		                    
                }		
                
                //If OnLeave		
                for(var i=0;i<=approved.length;i=i+1)		
                {		
                    if(dd==approved[i]){		
                        component.set('v.submitted',false);		
                        component.set('v.Editable',false);                   		
                    }		
                }		
                
                //If On Holiday 		
                for(var i=0;i<=holiday.length;i=i+1)		
                {		
                    if(dd==holiday[i])		
                    {		
                        component.set('v.submitted',false);		
                        component.set('v.Editable',false); 		                        
                    }		
                }		
                component.set("v.datetime",datelist);
                //For Getting TimeSheetDetails Based On The Particular Clicked Date		
                component.set("v.eventoutput",true);		
                
                var action=component.get("c.GetClickedOutput");		
                action.setParams({		
                    "datetimes":datelist		
                });	
                action.setCallback(this, function (response){		
                    var state = response.getState(); 		
                    if (state === "SUCCESS") {		
                        var data= response.getReturnValue();		
                        component.set("v.ViewCompleteddetails",data);
                        component.set("v.ViewCompletedDummyDetails",data );
                        if(data.length>3){                           		
                            component.set("v.DisplayTimesheetView",true);		
                        }else{		
                            component.set("v.DisplayTimesheetView",false);		
                        }		
                    }                  		
                    
                });		
                $A.enqueueAction(action);
                
                component.set('v.IsSpinner',true);
                //For Displaying Project Allocation On DayClick
                var action4=component.get("c.FetchProjectAllocation") ;		
                var opts=[];		
                action4.setParams({		
                    "datetimes":datelist		
                });		
                action4.setCallback(this, function (response){	
                    var state = response.getState(); 
                    if (state === "SUCCESS") {
                        var data= response.getReturnValue();	
                        if(data !=null){
                            component.set("v.ViewProjectAllocations",data);
                            var len=component.get("v.ViewProjectAllocations").length;
                            
                            if(len>0){		
                                component.set("v.DisplayProjectAllocations",true);		
                            }		
                            if(len>3){		
                                var minilist = [];		
                                for(var i=0; i<3; i=i+1){		
                                    minilist.push(data[i]);		
                                }		
                                component.set("v.ViewProjectAllocationsMini", minilist);		
                                component.set("v.DisplayProjectView", true);		
                            }		
                            else{		
                                component.set("v.ViewProjectAllocationsMini", data);		
                                component.set("v.DisplayProjectView", false);		
                            }		
                        }  		
                    }		
                    component.set('v.IsSpinner',false);
                });
                $A.enqueueAction(action4);
                
                //For Table Inline Editing		
                var action3 = component.get("c.FetchProjectAllocation"); 		
                var opts=[];		
                // alert(JSON.stringify(component.get("v.datetime")));		
                action3.setParams({		
                    "datetimes":component.get("v.datetime")		
                });		
                
                action3.setCallback(this, function(response){		
                    if(response.getState() === 'SUCCESS'){		
                        var allValues = response.getReturnValue();		
                        
                        if (allValues.length > 0) {		
                            opts.push({		
                                class: "optionClass",		
                                label: "--Choose One--",		
                                value: ""		
                            });		
                            for(var i=0;i<allValues.length; i=i+1){		
                                opts.push({		
                                    class: "optionClass",		
                                    label: allValues[i].Nrich__Project__r.Name,		
                                    value: allValues[i].Nrich__Project__c		
                                });		
                            }		
                        }		
                        component.set("v.options", opts);		
                    } 		
                    else		
                    {		
                        var toastEvent = $A.get("e.force:showToast");		
                        toastEvent.setParams({		
                            title : $A.get("$Label.c.Error_In_Project_Options"),
                            message:'Error',		
                            duration:' 5000',		
                            key: 'info_alt',		
                            type: 'error',		
                            mode: 'dismissible'		
                        });		
                        toastEvent.fire();		
                    }		
                });
                $A.enqueueAction(action3);          
            },
            
            //Day Click Js
            dayClick :function(date, jsEvent, view) {
                // component.set('v.IsSpinner',true);
                //On Click Removing The css class icon
                
                component.set('v.Addtask',false);
                var element = document.getElementById("myDIV");
                element.classList.remove("icon");
                var device = $A.get("$Browser.formFactor");
                if(device==='PHONE'){
                 element.classList.add("icon2");
                }
                
                var now = date? new Date(date) : new Date();
                // Get the previous Monday
                var monday = new Date(now);
                monday.setDate(monday.getDate() - monday.getDay() + 0);
                
                // Get next Sunday
                var sunday = new Date(now);
                sunday.setDate(sunday.getDate() - sunday.getDay() + 6);
                
                //Get FirstDay Of Month 
                var monthStartDay = new Date(now.getFullYear(), now.getMonth(), 1);
                
                //Get LastDay Of Month
                var monthEndDay = new Date(now.getFullYear(), now.getMonth() + 1, 0);
                // alert('start' +monthStartDay.toString().split(' ').slice(0, 4).join(' ') +'--End' +monthEndDay);
                
                component.set("v.StartDate" ,monday.toString().split(' ').slice(0, 4).join(' '));
                component.set("v.EndDate" ,sunday.toString().split(' ').slice(0, 4).join(' '));
                component.set("v.MonstartDate",monthStartDay.toString().split(' ').slice(0, 4).join(' '));
                component.set("v.MonendDate",monthEndDay.toString().split(' ').slice(0, 4).join(' '));
                component.set('v.Editable',true);
                component.set("v.IsShowSupport",true);
                component.set('v.submitted',true);
                component.set("v.create",true);
                component.set("v.DisplayProjectAllocations",false);
                component.set("v.IsRadioCheck",false);
                component.set("v.adddisable", false);
                //component.set("v.displaytablefrmchild",true);
                component.set("v.TodayDate",now.toString().split(' ').slice(0, 4).join(' '));
                var datelist = date.format().toString().split('-');
                
                // If Already Submitted For Approval
                var dd=date.format();
                for(var i=0;i<=submitted.length;i=i+1){
                    if(dd==submitted[i])
                    {                       
                        component.set('v.submitted',false);
                        component.set('v.Editable',false); 
                    }
                }
                
                //If TimeSheet Approved
                for(var i=0;i<=tsapproved.length;i=i+1)
                {
                    if(dd==tsapproved[i])
                    {
                        component.set('v.submitted',false);
                        component.set('v.Editable',false);                       
                    }        
                }
                
                //If OnLeave
                for(var i=0;i<=approved.length;i=i+1)
                {
                    if(dd==approved[i]){
                        component.set('v.submitted',false);
                        component.set('v.Editable',false); 
                    }
                }
                
                //If On Holiday 
                for(var i=0;i<=holiday.length;i=i+1)
                {
                    if(dd==holiday[i])
                    {
                        component.set('v.Editable',false); 
                    }
                }
                component.set("v.datetime",datelist);
                
                //For Getting TimeSheetDetails Based On The Particular Clicked Date
                component.set("v.eventoutput",true);
                
                var action=component.get("c.GetClickedOutput");
                action.setParams({
                    "datetimes":datelist
                });
                action.setCallback(this, function (response){
                    var state = response.getState(); 
                    if (state === "SUCCESS") {
                        var data= response.getReturnValue();
                        component.set("v.ViewCompleteddetails",data);
                        if(data.length>3){                           
                            component.set("v.DisplayTimesheetView",true);
                        }else{
                            component.set("v.DisplayTimesheetView",false);
                        }
                    }                  
                    
                });
                $A.enqueueAction(action);
                
                component.set('v.IsSpinner',true);
                //For Displaying Project Allocation On DayClick
                var action4=component.get("c.FetchProjectAllocation") ;
                action4.setParams({
                    "datetimes":datelist
                });
                action4.setCallback(this, function (response){
                    var state = response.getState(); 
                    if (state === "SUCCESS") {
                        var data= response.getReturnValue();
                        if(data!=null){
                            
                            component.set("v.ViewProjectAllocations",data);
                            var len=component.get("v.ViewProjectAllocations").length;
                            if(len>0){
                                component.set("v.DisplayProjectAllocations",true);
                            }
                            if(len>3){
                                var minilist = [];
                                for(var i=0; i<3; i=i+1){
                                    minilist.push(data[i]);
                                }
                                component.set("v.ViewProjectAllocationsMini", minilist);
                                component.set("v.DisplayProjectView", true);
                            }
                            else{
                                component.set("v.ViewProjectAllocationsMini", data);
                                component.set("v.DisplayProjectView", false);
                            }
                        } 
                    }
                    component.set('v.IsSpinner',false);
                });
                $A.enqueueAction(action4);
                
                
                //For Table Inline Editing
                var action3 = component.get("c.FetchProjectAllocation"); 
                var opts=[];
                // alert(JSON.stringify(component.get("v.datetime")));
                action3.setParams({
                    "datetimes":component.get("v.datetime")
                });
                
                
                action3.setCallback(this, function(response){
                    if(response.getState() === 'SUCCESS'){
                        var allValues = response.getReturnValue();
                        
                        if (allValues.length > 0) {
                            opts.push({
                                class: "optionClass",
                                label: "--Choose One--",
                                value: ""
                            });
                            for(var i=0;i<allValues.length; i=i+1){
                                opts.push({
                                    class: "optionClass",
                                    label: allValues[i].Nrich__Project__r.Name,
                                    value: allValues[i].Nrich__Project__c 
                                });
                            }
                        }
                        component.set("v.options", opts);
                    } 
                    else
                    {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : $A.get("$Label.c.Error_In_Project_Options"),
                            message:'Error',
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();
                    }
                    
                });
                $A.enqueueAction(action3);
            },
            
            eventMouseover : function(event, jsEvent, view) {
                
            },
        });
        
        var device = $A.get("$Browser.formFactor");
        if(device==='PHONE'){
          $(".fc-prev-button").removeClass("fc-corner-right");
        }        
    },
    
    formatFullCalendarData : function(component,events) {
        //format data according to the script expectation      
        var josnDataArray = [];        
        if(events==null){
            josnDataArray.push({
                'title':0,
                'start':0,
                'id':0
            });  
        }
        else{
            
            for(var i = 0;i < events.length;i=i+1){
                
                var date=events[i].Nrich__Date__c 
                var No_Of_hours=events[i].Nrich__TotalHours__c;
                josnDataArray.push({
                    'title':events[i].Nrich__TotalHours__c,
                    'start':date,
                    'id':events[i].Id
                });
            }
        }
        return josnDataArray;
        
    },
    refreshcalendarview : function(component, dataval) {
        var action=component.get("c.getOneDayEvent");
        action.setParams({
            "datevalue":dataval
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data= response.getReturnValue();
                var myCalendar = $('#calendar'); 
                myCalendar.fullCalendar();
                var myEvent = {
                    title:data[0].Nrich__TotalHours__c,
                    start: data[0].Nrich__Date__c,
                    id: data[0].Id
                };
                var allEvents = $("#calendar").fullCalendar("clientEvents");
                var exists = false;
                $.each(allEvents, function(index, value) {
                    if(new Date(value.start).toDateString() === new Date(data[0].Nrich__Date__c).toDateString()) {
                        exists = true;
                    }
                });
                
                if (!exists) 
                    myCalendar.fullCalendar( 'renderEvent', myEvent, true);
                else{
                    myCalendar.fullCalendar('removeEvents',data[0].Id);
                    myCalendar.fullCalendar( 'renderEvent', myEvent, true);
                }
            }
        });
        $A.enqueueAction(action);
    },
    fetchCalenderEvents : function(component) {
        var action=component.get("c.getAllEvents");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data= response.getReturnValue();
                var jsona=[];
                var josnArr = this.formatFullCalendarData(component,response.getReturnValue());              
                this.loadDataToCalendar(component,josnArr);
                component.set("v.Objectlist",josnArr);
            } else if (state === "ERROR") {
                
            }
        });
        $A.enqueueAction(action);
    }, 
    
    //To Get Total TimeSheet Hours Within Month Range 
    FrmValidationHelper:function(component,event){
        var action1 = component.get("c.AggregateTimeSheetDetails1"); 
        action1.setParams({
            "datetimes":component.get("v.FromDate")
        });
        
        action1.setCallback(this, function(response){
            
            if(response.getState() === 'SUCCESS' ){  
                
                var data=response.getReturnValue();
                
                component.set("v.TimeSheetTotalHours",data.MonTotalHrsCp );
                component.set("v.WeekTMTotalHours",data.WeekTotalHrsCp);
                component.set("v.DayTMTotalHours",data.DayTotalHrsCp);
                
            }
        });
        
        $A.enqueueAction(action1);
        
        var action=component.get("c.FetchSelectedTimeSheetHr");
        action.setParams({
            "timesheetdetaillist": component.get("v.ViewCompleteddetails"),
            
            
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var data= response.getReturnValue();
                component.set("v.HrsFrCopyValidation",data);
                
                
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Error_Message"),
                    message:'Error',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    
    
    // this.SaveUpdated(component,event,frmdate);
    
    
    
    
    
    
    
    CopyDetails:function(component){
        var submitted=[];
        var timesheetapproved=[];
        var leaveapproved=[];
        submitted=component.get("v.SubmittedList");
        timesheetapproved=component.get("v.TimeApprovedList");
        leaveapproved=component.get("V.Approvedlist");
        
        component.set("v.CopyTaskDisable",false);
        component.set("v.MaxMonthCap",false);
        component.set("v.MaxWeekCap",false);
        
        //For Submmited Days Task Cannot Be Copied
        for(var i=0;i<=submitted.length;i=i+1){
            if(component.get("v.FromDate")==submitted[i] ){
                component.set("v.CopyTaskDisable",true);
            }
        }
        
        //For TimeSheetApproved
        for(var i=0;i<=timesheetapproved.length;i=i+1)
        {
            if(component.get("v.FromDate")==timesheetapproved[i] ){
                component.set("v.CopyTaskDisable",true);
            } 
            
        }
        
        //For Approved Leaves
        for(var i=0;i<=leaveapproved.length;i=i+1){
            if(component.get("v.FromDate")==leaveapproved[i]){
                component.set("v.CopyTaskDisable",true);
            }
        }
        
        
        
        if(component.get("v.FromDate")>component.get("v.ToDate")){
            component.set("v.CopyTaskDisable",true);
        }
        
        
        var checking=component.get("v.CopyTaskDisable");
        
        if(checking==true)
        {
            
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : $A.get("$Label.c.Error_In_Task"),
                message:$A.get("$Label.c.Error"),
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'dismissible'
            });
            toastEvent.fire();
        }
        //If Both Conditions Are False Then Only Copy The Task
        if(checking==false )
        {
            var action=component.get("c.CopySave");
            var dataval;
            if(component.get("v.selectedRowBoolean"))
                dataval = component.get("v.selectedRowsChecked");
            else
                dataval = component.get("v.ViewCompleteddetails");            
            action.setParams({
                "timesheetdetaillist": dataval,
                "FromDate":component.get("v.FromDate"),
                "ToDate":component.get("v.ToDate")
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS"){
                    var data= response.getReturnValue();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.Success_Message"),
                        message:  $A.get("$Label.c.Saved_Record"),
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                    // $A.get('e.force:refreshView').fire();
                    component.set("v.IsApproval",true);
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title :$A.get("$Label.c.Error_Message"),
                        message:$A.get("$Label.c.Error"),
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
        
    },
    
    FetchProjectOptions : function(component, event) {
        var type = event.getSource().get("v.value");
        var action3 = component.get("c.FetchProjectCategoryOptions"); 
        action3.setParams({
            "PrjId":type
        });
        action3.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'  && response !== null){
                component.set("v.options1",response.getReturnValue());
            } 
        });
        $A.enqueueAction(action3);
    },
    
    SaveUpdated:function(component, event){
        var action = component.get("c.SaveUpdated");
        //alert('updatedvalues' +JSON.stringify(component.get("v.ViewCompleteddetails")));
        action.setParams({
            "ListTimeSheetDetail":component.get("v.ViewCompleteddetails"),"datetimes":component.get("v.datetime")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.TimeSheetDetailList", []);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title :$A.get("$Label.c.Success_Message"),
                    message:$A.get("$Label.c.Saved_Record") ,
                    duration:'5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Error_In_Update"),
                    message:$A.get("$Label.c.Error"),
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'dismissible'
                });
                toastEvent.fire();                 
            }
        });
        $A.enqueueAction(action);
    },
    
    
    getCustomdata: function(component, event){
        var action = component.get("c.fetchcustomSetting"); 
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS' &&  response!=null){  
                
                component.set("v.CustomSettings", response.getReturnValue());
                component.set("v.ApprovedColor",response.getReturnValue().Nrich__Approved__c );
                component.set("v.SubmittedColor",response.getReturnValue().Nrich__Submitted__c );
                component.set("v.HolidayColor",response.getReturnValue().Nrich__Holiday__c );
                component.set("v.LeaveReqColor",response.getReturnValue().Nrich__Leave__c );
                component.set("v.SetupPageHrs", response.getReturnValue().Nrich__TotalHoursCap__c);
                component.set("v.TimeSheetType", response.getReturnValue().Nrich__Timesheet_Type__c);
                
                
                
            }
        });
        $A.enqueueAction(action);
    },
    
    SaveApproval1:function(component, event){
        
        var buttonName = event.getSource().get("v.name");
        
        //alert('buttonName' +buttonName);
        //This is from Copy Button
        if((buttonName==$A.get("$Label.c.Submit_Monthly")) || (buttonName==$A.get("$Label.c.Submit_Weekly")))
        {
            var datelist = component.get("v.FromDate").toString().split('-');
            // alert('datelist'+datelist);
        }
        
        //This is from Display View 
        if((buttonName==$A.get("$Label.c.SubmitForApprovalWeeklView")||(buttonName==$A.get("$Label.c.SubmitForApprovalMonthlyView"))))
        {
            var datelist = component.get("v.datetime");
            //alert('datelist' +datelist);
        }
        
        
        if((buttonName==$A.get("$Label.c.Submit_Monthly") || (buttonName==$A.get("$Label.c.SubmitForApprovalMonthlyView")))) {
            var action1=component.get("c.submitforapproval");
            action1.setParams({"datetimes":datelist
                              });
            action1.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS"){
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.Success_Message"),
                        message: $A.get("$Label.c.Approval_Submitted_Successfully"),
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                }
            });
            $A.enqueueAction(action1);  
        }
        if((buttonName==$A.get("$Label.c.Submit_Weekly")||(buttonName==$A.get("$Label.c.SubmitForApprovalWeeklView")))) {
            // var buttonName = event.getSource().get("v.name");
            //var datelist = component.get("v.FromDate").toString().split('-');
            var action1=component.get("c.submitforapprovalweek");
            
            
            action1.setParams({"datetimes":datelist
                              });
            action1.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS"){
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.Success_Message"),
                        message:$A.get("$Label.c.Approval_Submitted_Successfully"),
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                }
                else{
                    alert('error');
                }
            });
            $A.enqueueAction(action1);  
        }
        
    },
    
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.ViewCompleteddetails");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.ViewCompleteddetails", data);
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
    
    DeleteSelectedTask:function(component,event, dataval, helper){
        var datarec =component.get("v.ViewCompleteddetails");
        var action = component.get("c.DeleteTask");
        var dateval = datarec[dataval].Nrich__Date__c;
        action.setParams({
            "TimeSheetDetailsList": datarec[dataval]
        });
        action.setCallback(this, function(response) {
            var state = response.getState();            
            if (state == "SUCCESS") {
                var resultval = response.getReturnValue();
                if(resultval!=undefined && resultval!=null){
                    component.set("v.ViewCompleteddetails", resultval);
                    component.set("v.eventoutput",true);
                    helper.refreshcalendarview(component, dateval);
                }
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title :$A.get("$Label.c.Success_Message") ,
                    message:$A.get("$Label.c.RecordDeleted"),
                    duration:'5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
                // $A.get('e.force:refreshView').fire();
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.Error_Message"),
                    message:$A.get("$Label.c.Error"),
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'dismissible'
                });
                toastEvent.fire();             
            }
        });
        $A.enqueueAction(action);
        
    }
    
})